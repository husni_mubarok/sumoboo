<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
 Route::get('/', 'Auth\LoginController@showLoginForm');

Auth::routes();

//User Management
Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
	//Home
	Route::get('/', ['as' => 'editor.index', 'uses' => 'EditorController@index']);
	Route::post('/',['as' => 'editor.index', 'uses' => 'EditorController@index']);
	//Profile
		//detail
	Route::get('/profile', ['as' => 'editor.profile.show', 'uses' => 'ProfileController@show']);
		//edit
	Route::get('/profile/edit', ['as' => 'editor.profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('/profile/edit', ['as' => 'editor.profile.update', 'uses' => 'ProfileController@update']);
		//edit password
	Route::get('/profile/password', ['as' => 'editor.profile.edit_password', 'uses' => 'ProfileController@edit_password']);
	Route::put('/profile/password', ['as' => 'editor.profile.update_password', 'uses' => 'ProfileController@update_password']);

	//User
		//index
	Route::get('/user', ['middleware' => ['role:user|read'], 'as' => 'editor.user.index', 'uses' => 'UserController@index']);
		//create
	Route::get('/user/create', ['middleware' => ['role:user|create'], 'as' => 'editor.user.create', 'uses' => 'UserController@create']);
	Route::post('/user/create', ['middleware' => ['role:user|create'], 'as' => 'editor.user.store', 'uses' => 'UserController@store']);
		//edit
	Route::get('/user/{id}/edit', ['middleware' => ['role:user|update'], 'as' => 'editor.user.edit', 'uses' => 'UserController@edit']);
	Route::put('/user/{id}/edit', ['middleware' => ['role:user|update'], 'as' => 'editor.user.update', 'uses' => 'UserController@update']);
		//delete
	Route::delete('/user/{id}/delete', ['middleware' => ['role:user|delete'], 'as' => 'editor.user.delete', 'uses' => 'UserController@delete']);

	//Module
		//index
	Route::get('/module', ['middleware' => ['role:module|read'], 'as' => 'editor.module.index', 'uses' => 'ModuleController@index']);
		//create
	Route::get('/module/create', ['middleware' => ['role:module|create'], 'as' => 'editor.module.create', 'uses' => 'ModuleController@create']);
	Route::post('/module/create', ['middleware' => ['role:module|create'], 'as' => 'editor.module.store', 'uses' => 'ModuleController@store']);
		//edit
	Route::get('/module/{id}/edit', ['middleware' => ['role:module|update'], 'as' => 'editor.module.edit', 'uses' => 'ModuleController@edit']);
	Route::put('/module/{id}/edit', ['middleware' => ['role:module|update'], 'as' => 'editor.module.update', 'uses' => 'ModuleController@update']);
		//delete
	Route::delete('/module/{id}/delete', ['middleware' => ['role:module|delete'], 'as' => 'editor.module.delete', 'uses' => 'ModuleController@delete']);

	//Action
		//index
	Route::get('/action', ['middleware' => ['role:action|read'], 'as' => 'editor.action.index', 'uses' => 'ActionController@index']);
		//create
	Route::get('/action/create', ['middleware' => ['role:action|create'], 'as' => 'editor.action.create', 'uses' => 'ActionController@create']);
	Route::post('/action/create', ['middleware' => ['role:action|create'], 'as' => 'editor.action.store', 'uses' => 'ActionController@store']);
		//edit
	Route::get('/action/{id}/edit', ['middleware' => ['role:action|update'], 'as' => 'editor.action.edit', 'uses' => 'ActionController@edit']);
	Route::put('/action/{id}/edit', ['middleware' => ['role:action|update'], 'as' => 'editor.action.update', 'uses' => 'ActionController@update']);
		//delete
	Route::delete('/action/{id}/delete', ['middleware' => ['role:action|delete'], 'as' => 'editor.action.delete', 'uses' => 'ActionController@delete']);

	//Privilege
		//index
	Route::get('/privilege', ['middleware' => ['role:privilege|read'], 'as' => 'editor.privilege.index', 'uses' => 'PrivilegeController@index']);
		//create
	Route::get('/privilege/create', ['middleware' => ['role:privilege|create'], 'as' => 'editor.privilege.create', 'uses' => 'PrivilegeController@create']);
	Route::post('/privilege/create', ['middleware' => ['role:privilege|create'], 'as' => 'editor.privilege.store', 'uses' => 'PrivilegeController@store']);
		//edit
	Route::get('/privilege/{id}/edit', ['middleware' => ['role:privilege|update'], 'as' => 'editor.privilege.edit', 'uses' => 'PrivilegeController@edit']);
	Route::put('/privilege/{id}/edit', ['middleware' => ['role:privilege|update'], 'as' => 'editor.privilege.update', 'uses' => 'PrivilegeController@update']);
		//delete
	Route::delete('/privilege/{id}/delete', ['middleware' => ['role:privilege|delete'], 'as' => 'editor.privilege.delete', 'uses' => 'PrivilegeController@delete']);



});


Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
	//Cashbond List
		//index
	Route::get('/cashbond', ['as' => 'editor.cashbond.index', 'uses' => 'CashbondController@index']);
		//create
	Route::get('/cashbond/create', ['as' => 'editor.cashbond.create', 'uses' => 'CashbondController@create']);
	Route::post('/cashbond/create', ['as' => 'editor.cashbond.store', 'uses' => 'CashbondController@store']);
		//edit
	Route::get('/cashbond/{id}/edit', ['as' => 'editor.cashbond.edit', 'uses' => 'CashbondController@edit']);
	Route::put('/cashbond/{id}/edit', ['as' => 'editor.cashbond.update', 'uses' => 'CashbondController@update']);

	//waitingowner
	Route::put('/cashbond/{id}/waitingowner', ['as' => 'editor.cashbond.waitingowner', 'uses' => 'CashbondController@updatewaitingowner']);
	//waiting finalize
	Route::get('/cashbond/{id}/finalize', ['as' => 'editor.cashbond.finalize', 'uses' => 'CashbondController@finalize']);
	Route::put('/cashbond/{id}/finalize', ['as' => 'editor.cashbond.updatefinalize', 'uses' => 'CashbondController@updatefinalize']);

	//waiting owner action
	Route::get('/cashbond/{id}/issued', ['as' => 'editor.cashbond.issued', 'uses' => 'CashbondController@issued']);
	Route::put('/cashbond/{id}/issued', ['as' => 'editor.cashbond.updateissued', 'uses' => 'CashbondController@updateissued']);

	//closing
	Route::get('/cashbond/{id}/closing', ['as' => 'editor.cashbond.closing', 'uses' => 'CashbondController@closing']);
	Route::post('/cashbond/{id}/closing', ['as' => 'editor.cashbond.storeclosing', 'uses' => 'CashbondController@storeclosing']);
	//detail item
	Route::get('/cashbond/{id}/detailitem', ['as' => 'editor.cashbond.detailitem', 'uses' => 'CashbondController@detailitem']);
	Route::post('/cashbond/detailitem', ['as' => 'editor.cashbond.storedetailitem', 'uses' => 'CashbondController@storedetailitem']);

	//verified and reject
	Route::get('/cashbond/{id}/verified', ['as' => 'editor.cashbond.verified', 'uses' => 'CashbondController@verified']);
	Route::put('/cashbond/{id}/updateverified', ['as' => 'editor.cashbond.updateverified', 'uses' => 'CashbondController@updateverified']);
	Route::put('/cashbond/{id}/updatereject', ['as' => 'editor.cashbond.updatereject', 'uses' => 'CashbondController@updatereject']);
	Route::put('/cashbond/{id}/updatewaitingverified', ['as' => 'editor.cashbond.updatewaitingverified', 'uses' => 'CashbondController@updatewaitingverified']);

	//view
	Route::get('/cashbond/{id}/paid', ['as' => 'editor.cashbond.paid', 'uses' => 'CashbondController@paid']);
	Route::get('/cashbond/{id}/approved', ['as' => 'editor.cashbond.approved', 'uses' => 'CashbondController@approved']);
	Route::get('/cashbond/{id}/view', ['as' => 'editor.cashbond.view', 'uses' => 'CashbondController@view']);
	//delete
	Route::delete('/cashbond/{id}/delete', ['as' => 'editor.cashbond.delete', 'uses' => 'CashbondController@delete']);
	//delete detail
	Route::delete('/cashbond/{id}/deletedetail', ['as' => 'editor.cashbond.deletedetail', 'uses' => 'CashbondController@deletedetail']);
	//delete
	Route::delete('/cashbond/{id}/delete', ['middleware' => ['role:cashbond|delete'],'as' => 'editor.cashbond.delete', 'uses' => 'CashbondController@delete']);

	//cashbond remainding
	Route::put('/cashbond/update_remainder/{id}', ['as' => 'editor.cashbond.update_remainder', 'uses' => 'CashbondController@updateRemainder']);
	Route::get('/cashbond/{id}/remainder', ['as' => 'editor.cashbond.remainder', 'uses' => 'CashbondController@remainder']);



	//cashbond bank
	Route::get('/cashbondbank', ['as' => 'editor.cashbondbank.index', 'uses' => 'CashbondController@indexbank']);

	Route::get('/cashbond_report', ['as' => 'editor.cashbond.report', 'uses' => 'CashbondController@report']);
	Route::post('/cashbond_report', ['as' => 'editor.cashbond.report', 'uses' => 'CashbondController@report']);
	//Item Central
		//index
	Route::get('/item_central', ['middleware' => ['role:item_central|read'],'as' => 'editor.item_central.index', 'uses' => 'ItemCentralController@index']);
		//index bank
	Route::get('/item_central_bank', ['middleware' => ['role:item_central|read'],'as' => 'editor.item_central_bank.index', 'uses' => 'ItemCentralController@indexbank']);
		//create
	Route::get('/item_central/create', ['middleware' => ['role:item_central|create'],'as' => 'editor.item_central.create', 'uses' => 'ItemCentralController@create']);
	Route::post('/item_central/create', ['middleware' => ['role:item_central|create'],'as' => 'editor.item_central.store', 'uses' => 'ItemCentralController@store']);
	//import
	Route::post('/item_central/storeimport', ['as' => 'editor.item_central.storeimport', 'uses' => 'ItemCentralController@storeimport']);
	//export
	Route::get('/item_central/storeexport/{type}', ['as' => 'editor.item_central.storeexport', 'uses' => 'ItemCentralController@storeexport']);
	//truncate import data
	Route::post('/item_central/truncateimport', ['as' => 'editor.item_central.truncateimport', 'uses' => 'ItemCentralController@truncateimport']);
		//edit
	Route::get('/item_central/{id}/edit', ['middleware' => ['role:item_central|update'],'as' => 'editor.item_central.edit', 'uses' => 'ItemCentralController@edit']);
	Route::put('/item_central/{id}/edit', ['middleware' => ['role:item_central|update'],'as' => 'editor.item_central.update', 'uses' => 'ItemCentralController@update']);
	//delivery
	Route::get('/item_central/{id}/delivery', ['middleware' => ['role:item_central|update'],'as' => 'editor.item_central.delivery', 'uses' => 'ItemCentralController@delivery']);
	Route::put('/item_central/{id}/delivery', ['as' => 'editor.item_central.updatedelivery', 'uses' => 'ItemCentralController@updatedelivery']);
	//invoice
	Route::get('/item_central/{id}/invoice', ['middleware' => ['role:item_central|update'],'as' => 'editor.item_central.invoice', 'uses' => 'ItemCentralController@invoice']);
	Route::put('/item_central/{id}/invoice', ['as' => 'editor.item_central.updateinvoice', 'uses' => 'ItemCentralController@updateinvoice']);
	//issued
	Route::get('/item_central/{id}/issued', ['middleware' => ['role:item_central|issued'],'as' => 'editor.item_central.issued', 'uses' => 'ItemCentralController@issued']);
	Route::put('/item_central/{id}/issued', ['as' => 'editor.item_central.updateissued', 'uses' => 'ItemCentralController@updateissued']);

	//paid
	Route::get('/item_central/{id}/paid', ['middleware' => ['role:item_central|paid'],'as' => 'editor.item_central.paid', 'uses' => 'ItemCentralController@paid']);
	Route::put('/item_central/{id}/paid', ['as' => 'editor.item_central.updatepaid', 'uses' => 'ItemCentralController@updatepaid']);

	Route::get('/item_central/{id}/view', ['as' => 'editor.item_central.view', 'uses' => 'ItemCentralController@view']);
	//delete
	Route::delete('/item_central/{id}/delete', ['middleware' => ['role:item_central|delete'],'as' => 'editor.item_central.delete', 'uses' => 'ItemCentralController@delete']);

	Route::get('/item_report', ['middleware' => ['role:item_central|read'],'as' => 'editor.item_central.report', 'uses' => 'ItemCentralController@report']);
	Route::post('/item_report', ['middleware' => ['role:item_central|read'],'as' => 'editor.item_central.report', 'uses' => 'ItemCentralController@report']);
	//Category Item
		//index
	Route::get('/item_category', ['as' => 'editor.item_category.index', 'uses' => 'ItemCategoryController@index']);
		//create
	Route::get('/item_category/create', ['as' => 'editor.item_category.create', 'uses' => 'ItemCategoryController@create']);
	Route::post('/item_category/create', ['as' => 'editor.item_category.store', 'uses' => 'ItemCategoryController@store']);
		//edit
	Route::get('/item_category/{id}/edit', ['as' => 'editor.item_category.edit', 'uses' => 'ItemCategoryController@edit']);
	Route::put('/item_category/{id}/edit', ['as' => 'editor.item_category.update', 'uses' => 'ItemCategoryController@update']);
		//delete
	Route::delete('/item_category/{id}/delete', ['as' => 'editor.item_category.delete', 'uses' => 'ItemCategoryController@delete']);

	//Type Item
		//index
	Route::get('/item_type', ['as' => 'editor.item_type.index', 'uses' => 'ItemTypeController@index']);
		//create
	Route::get('/item_type/create', ['as' => 'editor.item_type.create', 'uses' => 'ItemTypeController@create']);
	Route::post('/item_type/create', ['as' => 'editor.item_type.store', 'uses' => 'ItemTypeController@store']);
		//edit
	Route::get('/item_type/{id}/edit', ['as' => 'editor.item_type.edit', 'uses' => 'ItemTypeController@edit']);
	Route::put('/item_type/{id}/edit', ['as' => 'editor.item_type.update', 'uses' => 'ItemTypeController@update']);
		//delete
	Route::delete('/item_type/{id}/delete', ['as' => 'editor.item_type.delete', 'uses' => 'ItemTypeController@delete']);

	//Item
		//index
	Route::get('/item/{id}', ['as' => 'editor.item.index', 'uses' => 'ItemController@index']);
		//create
	Route::get('/item/{id}/create', ['as' => 'editor.item.create', 'uses' => 'ItemController@create']);
	Route::post('/item/{id}/create', ['as' => 'editor.item.store', 'uses' => 'ItemController@store']);
		//edit
	Route::get('/item/{id}/{id2}/edit', ['as' => 'editor.item.edit', 'uses' => 'ItemController@edit']);
	Route::put('/item/{id}/{id2}/edit', ['as' => 'editor.item.update', 'uses' => 'ItemController@update']);
		//delete
	Route::delete('/item/{id}/{id2}/delete', ['as' => 'editor.item.delete', 'uses' => 'ItemController@delete']);

	//Vendor Item
		//index
	Route::get('/vendoritem', ['as' => 'editor.vendoritem.index', 'uses' => 'VendorItemController@index']);
		//create and edit
	Route::get('/vendoritem/{id}/edit', ['as' => 'editor.vendoritem.edit', 'uses' => 'VendorItemController@edit']);
	Route::put('/vendoritem/{id}/edit', ['as' => 'editor.vendoritem.store', 'uses' => 'VendorItemController@store']);
		//delete
	Route::delete('/vendoritem/{id}/delete', ['as' => 'editor.vendoritem.delete', 'uses' => 'VendorItemController@delete']);

	//Branch
		//index
	Route::get('/branch', ['as' => 'editor.branch.index', 'uses' => 'BranchController@index']);
		//create
	Route::get('/branch/create', ['as' => 'editor.branch.create', 'uses' => 'BranchController@create']);
	Route::post('/branch/create', ['as' => 'editor.branch.store', 'uses' => 'BranchController@store']);
		//edit
	Route::get('/branch/{id}/edit', ['as' => 'editor.branch.edit', 'uses' => 'BranchController@edit']);
	Route::put('/branch/{id}/edit', ['as' => 'editor.branch.update', 'uses' => 'BranchController@update']);
		//delete
	Route::delete('/branch/{id}/delete', ['as' => 'editor.branch.delete', 'uses' => 'BranchController@delete']);

});

//Invoice
Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
	//Invoice List
		//index
	Route::get('/invoice', ['as' => 'editor.invoice.index', 'uses' => 'InvoiceController@index']);
	Route::get('/invoice/bank', ['as' => 'editor.invoice.bank', 'uses' => 'InvoiceController@indexbank']);
		//create
	Route::get('/invoice/create', ['as' => 'editor.invoice.create', 'uses' => 'InvoiceController@create']);
	Route::post('/invoice/create', ['as' => 'editor.invoice.store', 'uses' => 'InvoiceController@store']);
		//edit
	Route::get('/invoice/{id}/edit', ['as' => 'editor.invoice.edit', 'uses' => 'InvoiceController@edit']);
	Route::put('/invoice/{id}/edit', ['as' => 'editor.invoice.update', 'uses' => 'InvoiceController@update']);
	//paid
	Route::get('/invoice/{id}/paid', ['as' => 'editor.invoice.paid', 'uses' => 'InvoiceController@paid']);
	Route::put('/invoice/{id}/paid', ['as' => 'editor.invoice.updatepaid', 'uses' => 'InvoiceController@updatepaid']);
		//delete
	Route::delete('/invoice/{id}/delete', ['as' => 'editor.invoice.delete', 'uses' => 'InvoiceController@delete']);

	//Invoice Type
		//index
	Route::get('/invoice_type', ['as' => 'editor.invoice_type.index', 'uses' => 'InvoiceTypeController@index']);
		//create
	Route::get('/invoice_type/create', ['as' => 'editor.invoice_type.create', 'uses' => 'InvoiceTypeController@create']);
	Route::post('/invoice_type/create', ['as' => 'editor.invoice_type.store', 'uses' => 'InvoiceTypeController@store']);
		//edit
	Route::get('/invoice_type/{id}/edit', ['as' => 'editor.invoice_type.edit', 'uses' => 'InvoiceTypeController@edit']);
	Route::put('/invoice_type/{id}/edit', ['as' => 'editor.invoice_type.update', 'uses' => 'InvoiceTypeController@update']);
		//delete
	Route::delete('/invoice_type/{id}/delete', ['as' => 'editor.invoice_type.delete', 'uses' => 'InvoiceTypeController@delete']);

	//Vendor
		//index
	Route::get('/vendor', ['as' => 'editor.vendor.index', 'uses' => 'VendorController@index']);
		//create
	Route::get('/vendor/create', ['as' => 'editor.vendor.create', 'uses' => 'VendorController@create']);
	Route::post('/vendor/create', ['as' => 'editor.vendor.store', 'uses' => 'VendorController@store']);
		//edit
	Route::get('/vendor/{id}/edit', ['as' => 'editor.vendor.edit', 'uses' => 'VendorController@edit']);
	Route::put('/vendor/{id}/edit', ['as' => 'editor.vendor.update', 'uses' => 'VendorController@update']);
		//delete
	Route::delete('/vendor/{id}/delete', ['as' => 'editor.vendor.delete', 'uses' => 'VendorController@delete']);

	//Invoice
		//index
	Route::get('/invoice', ['as' => 'editor.invoice.index', 'uses' => 'InvoiceController@index']);
		//create
	Route::get('/invoice/create', ['as' => 'editor.invoice.create', 'uses' => 'InvoiceController@create']);
	Route::post('/invoice/create', ['as' => 'editor.invoice.store', 'uses' => 'InvoiceController@store']);
		//edit
	Route::get('/invoice/{id}/edit', ['as' => 'editor.invoice.edit', 'uses' => 'InvoiceController@edit']);
	Route::put('/invoice/{id}/edit', ['as' => 'editor.invoice.update', 'uses' => 'InvoiceController@update']);

		//view
	Route::get('/invoice/{id}/view', ['as' => 'editor.invoice.view', 'uses' => 'InvoiceController@view']);

		//approval
	Route::put('/invoice/{id}/updaterequset', ['as' => 'editor.invoice.updaterequset', 'uses' => 'InvoiceController@updaterequset']);

	Route::get('/invoice/{id}/paid', ['as' => 'editor.invoice.paid', 'uses' => 'InvoiceController@paid']);
	Route::put('/invoice/{id}/updatepaid', ['as' => 'editor.invoice.updatepaid', 'uses' => 'InvoiceController@updatepaid']);

		//delete
	Route::delete('/invoice/{id}/delete', ['as' => 'editor.invoice.delete', 'uses' => 'InvoiceController@delete']);

});

//Invoice direct to vendor
Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
	//Invoice direct to vendor list
		//index
	Route::get('/invoicedirect', ['as' => 'editor.invoicedirect.index', 'uses' => 'InvoiceDirectController@index']);
	Route::get('/invoicedirect/bank', ['as' => 'editor.invoicedirect.bank', 'uses' => 'InvoiceDirectController@indexbank']);
		//create
	Route::get('/invoicedirect/create', ['as' => 'editor.invoicedirect.create', 'uses' => 'InvoiceDirectController@create']);
	Route::post('/invoicedirect/create', ['as' => 'editor.invoicedirect.store', 'uses' => 'InvoiceDirectController@store']);
		//edit
	Route::get('/invoicedirect/{id}/edit', ['as' => 'editor.invoicedirect.edit', 'uses' => 'InvoiceDirectController@edit']);
	Route::put('/invoicedirect/{id}/edit', ['as' => 'editor.invoicedirect.update', 'uses' => 'InvoiceDirectController@update']);
		//delete
	Route::delete('/invoicedirect/{id}/delete', ['as' => 'editor.invoicedirect.delete', 'uses' => 'InvoiceDirectController@delete']);

		//approval
	Route::get('/invoicedirect/{id}/approval', ['as' => 'editor.invoicedirect.approval', 'uses' => 'InvoiceDirectController@approval']);
	Route::put('/invoicedirect/{id}/updaterequset', ['as' => 'editor.invoicedirect.updaterequset', 'uses' => 'InvoiceDirectController@updaterequset']);
	Route::put('/invoicedirect/{id}/updatepaid', ['as' => 'editor.invoicedirect.updatepaid', 'uses' => 'InvoiceDirectController@updatepaid']);

	//view
	Route::get('/invoicedirect/{id}/view', ['as' => 'editor.invoicedirect.view', 'uses' => 'InvoiceDirectController@view']);



	Route::get('/invoice_report_uti', ['as' => 'editor.invoice.report_uti', 'uses' => 'InvoiceController@report_uti']);
	Route::post('/invoice_report_uti', ['as' => 'editor.invoice.report_uti', 'uses' => 'InvoiceController@report_uti']);

	Route::get('/invoice_report_cons', ['as' => 'editor.invoice.report_cons', 'uses' => 'InvoiceController@report_cons']);
	Route::post('/invoice_report_cons', ['as' => 'editor.invoice.report_cons', 'uses' => 'InvoiceController@report_cons']);

});

//Payroll
Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
	//Payroll List
		//index
	Route::get('/payroll', ['middleware' => ['role:payroll|read'], 'as' => 'editor.payroll.index', 'uses' => 'PayrollController@index']);
	//create header
	Route::get('/payroll/createheader', ['middleware' => ['role:payroll|create'], 'as' => 'editor.payroll.createheader', 'uses' => 'PayrollController@createheader']);
	Route::post('/payroll/createheader', ['middleware' => ['role:payroll|create'], 'as' => 'editor.payroll.storeheader', 'uses' => 'PayrollController@storeheader']);
		//create
	Route::get('/payroll/{id}/create', ['middleware' => ['role:payroll|create'], 'as' => 'editor.payroll.create', 'uses' => 'PayrollController@create']);
	Route::post('/payroll/{id}/create', ['middleware' => ['role:payroll|create'], 'as' => 'editor.payroll.store', 'uses' => 'PayrollController@store']);
		//edit
	Route::get('/payroll/{id}/edit', ['middleware' => ['role:payroll|update'], 'as' => 'editor.payroll.edit', 'uses' => 'PayrollController@edit']);
	Route::put('/payroll/{id}/edit', ['middleware' => ['role:payroll|update'], 'as' => 'editor.payroll.update', 'uses' => 'PayrollController@update']);
		//detail
	Route::get('/payroll/{id}/detail', ['middleware' => ['role:payroll|read'], 'as' => 'editor.payroll.detail', 'uses' => 'PayrollController@detail']);
		//status - submit
	Route::put('/payroll/{id}/submit', ['middleware' => ['role:payroll|submit'], 'as' => 'editor.payroll.submit', 'uses' => 'PayrollController@submit']);
		//status - finance_approve
	Route::put('/payroll/{id}/finance_approve', ['middleware' => ['role:payroll|paid'], 'as' => 'editor.payroll.finance_approve', 'uses' => 'PayrollController@finance_approve']);
		//status - owner_approve
	Route::put('/payroll/{id}/owner_approve', ['middleware' => ['role:payroll|issued'], 'as' => 'editor.payroll.owner_approve', 'uses' => 'PayrollController@owner_approve']);
		//status - finance_payment
	Route::put('/payroll/{id}/finance_payment', ['middleware' => ['role:payroll|paid'], 'as' => 'editor.payroll.finance_payment', 'uses' => 'PayrollController@finance_payment']);
	// 	//detail create
	// Route::get('/payroll/{id}/detail/create', ['middleware' => ['role:payroll|update'], 'as' => 'editor.payroll.detail_create', 'uses' => 'PayrollController@detail_create']);
	// Route::post('/payroll/{id}/detail/create', ['middleware' => ['role:payroll|update'], 'as' => 'editor.payroll.detail_store', 'uses' => 'PayrollController@detail_store']);
	// 	//detail edit
	// Route::get('/payroll/{id}/detail/edit', ['middleware' => ['role:payroll|update'], 'as' => 'editor.payroll.detail_edit', 'uses' => 'PayrollController@detail_edit']);
	// Route::put('/payroll/{id}/detail/edit', ['middleware' => ['role:payroll|update'], 'as' => 'editor.payroll.detail_update', 'uses' => 'PayrollController@detail_update']);
	// 	//detail delete
	// Route::delete('/payroll/{id}/detail/delete', ['middleware' => ['role:payroll|update'], 'as' => 'editor.payroll.detail_delete', 'uses' => 'PayrollController@detail_delete']);
		//delete
	Route::delete('/payroll/{id}/delete', ['middleware' => ['role:payroll|delete'], 'as' => 'editor.payroll.delete', 'uses' => 'PayrollController@delete']);

	//Cashbond Payroll
		//index
	Route::get('/cashbond_payroll', ['middleware' => ['role:cashbond_payroll|read'], 'as' => 'editor.cashbond_payroll.index', 'uses' => 'CashbondPayrollController@index']);
		//create
	Route::get('/cashbond_payroll/create', ['middleware' => ['role:cashbond_payroll|create'], 'as' => 'editor.cashbond_payroll.create', 'uses' => 'CashbondPayrollController@create']);
	Route::post('/cashbond_payroll/create', ['middleware' => ['role:cashbond_payroll|create'], 'as' => 'editor.cashbond_payroll.store', 'uses' => 'CashbondPayrollController@store']);
		//edit
	Route::get('/cashbond_payroll/{id}/edit', ['middleware' => ['role:cashbond_payroll|update'], 'as' => 'editor.cashbond_payroll.edit', 'uses' => 'CashbondPayrollController@edit']);
	Route::put('/cashbond_payroll/{id}/edit', ['middleware' => ['role:cashbond_payroll|update'], 'as' => 'editor.cashbond_payroll.update', 'uses' => 'CashbondPayrollController@update']);
		//detail
	Route::get('/cashbond_payroll/{id}/detail', ['middleware' => ['role:cashbond_payroll|read'], 'as' => 'editor.cashbond_payroll.detail', 'uses' => 'CashbondPayrollController@detail']);
		//status - submit
	Route::put('/cashbond_payroll/{id}/submit', ['middleware' => ['role:cashbond_payroll|submit'], 'as' => 'editor.cashbond_payroll.submit', 'uses' => 'CashbondPayrollController@submit']);
		//status - finance_approve
	Route::put('/cashbond_payroll/{id}/finance_approve', ['middleware' => ['role:cashbond_payroll|paid'], 'as' => 'editor.cashbond_payroll.finance_approve', 'uses' => 'CashbondPayrollController@finance_approve']);
		//status - owner_approve
	Route::put('/cashbond_payroll/{id}/owner_approve', ['middleware' => ['role:cashbond_payroll|issued'], 'as' => 'editor.cashbond_payroll.owner_approve', 'uses' => 'CashbondPayrollController@owner_approve']);
	//status - finance_payment
	Route::put('/cashbond_payroll/{id}/finance_payment', ['middleware' => ['role:cashbond_payroll|paid'], 'as' => 'editor.cashbond_payroll.finance_payment', 'uses' => 'CashbondPayrollController@finance_payment']);
		//delete
	Route::delete('/cashbond_payroll/{id}/delete', ['middleware' => ['role:cashbond_payroll|delete'], 'as' => 'editor.cashbond_payroll.delete', 'uses' => 'CashbondPayrollController@delete']);

	//Employee
		//index
	Route::get('/employee', ['as' => 'editor.employee.index', 'uses' => 'EmployeeController@index']);
		//create
	Route::get('/employee/create', ['as' => 'editor.employee.create', 'uses' => 'EmployeeController@create']);
	Route::post('/employee/create', ['as' => 'editor.employee.store', 'uses' => 'EmployeeController@store']);
		//edit
	Route::get('/employee/{id}/edit', ['as' => 'editor.employee.edit', 'uses' => 'EmployeeController@edit']);
	Route::put('/employee/{id}/edit', ['as' => 'editor.employee.update', 'uses' => 'EmployeeController@update']);
		//delete
	Route::delete('/employee/{id}/delete', ['as' => 'editor.employee.delete', 'uses' => 'EmployeeController@delete']);
		//branch
	Route::get('/employee/{id}/branch', ['as' => 'editor.employee.branch', 'uses' => 'EmployeeController@branch']);
	Route::post('/employee/{id}/branch', ['as' => 'editor.employee.storebranch', 'uses' => 'EmployeeController@storebranch']);
	Route::delete('/employee/{id}/branch', ['as' => 'editor.employee.deleteemployeebranch', 'uses' => 'EmployeeController@deleteemployeebranch']);

	//Religion
		//index
	Route::get('/religion', ['as' => 'editor.religion.index', 'uses' => 'ReligionController@index']);
		//create
	Route::get('/religion/create', ['as' => 'editor.religion.create', 'uses' => 'ReligionController@create']);
	Route::post('/religion/create', ['as' => 'editor.religion.store', 'uses' => 'ReligionController@store']);
		//edit
	Route::get('/religion/{id}/edit', ['as' => 'editor.religion.edit', 'uses' => 'ReligionController@edit']);
	Route::put('/religion/{id}/edit', ['as' => 'editor.religion.update', 'uses' => 'ReligionController@update']);
		//delete
	Route::delete('/religion/{id}/delete', ['as' => 'editor.religion.delete', 'uses' => 'ReligionController@delete']);

	//Revenue
		//index | outstanding
	Route::get('/revenue', ['middleware' => ['role:revenue|read'], 'as' => 'editor.revenue.index', 'uses' => 'RevenueController@index']);
		//bank
	Route::get('/revenue/bank', ['middleware' => ['role:revenue|read'], 'as' => 'editor.revenue.bank', 'uses' => 'RevenueController@bank']);
		//create
	Route::get('/revenue/create', ['middleware' => ['role:revenue|create'], 'as' => 'editor.revenue.create', 'uses' => 'RevenueController@create']);
	Route::post('/revenue/create', ['middleware' => ['role:revenue|create'], 'as' => 'editor.revenue.store', 'uses' => 'RevenueController@store']);
		//detail
	Route::get('/revenue/{id}', ['middleware' => ['role:revenue|read'], 'as' => 'editor.revenue.detail', 'uses' => 'RevenueController@detail']);
		//download
	Route::post('/revenue/{id}/download', ['middleware' => ['role:revenue|read'], 'as' => 'editor.revenue.download', 'uses' => 'RevenueController@download']);
		//close
	Route::put('/revenue/close', ['middleware' => ['role:revenue|update'], 'as' => 'editor.revenue.close', 'uses' => 'RevenueController@close']);
		//request edit
	Route::put('/revenue/request_edit', ['middleware' => ['role:revenue|update'], 'as' => 'editor.revenue.request_edit', 'uses' => 'RevenueController@request_edit']);
		//finance approve
	Route::put('/revenue/finance_approve', ['middleware' => ['role:revenue|paid'], 'as' => 'editor.revenue.finance_approve', 'uses' => 'RevenueController@finance_approve']);
		//owner approve
	Route::put('/revenue/owner_approve', ['middleware' => ['role:revenue|issued'], 'as' => 'editor.revenue.owner_approve', 'uses' => 'RevenueController@owner_approve']);
		//edit
	Route::get('/revenue/{id}/edit', ['middleware' => ['role:revenue|update'], 'as' => 'editor.revenue.edit', 'uses' => 'RevenueController@edit']);
	Route::put('/revenue/{id}/edit', ['middleware' => ['role:revenue|update'], 'as' => 'editor.revenue.update', 'uses' => 'RevenueController@update']);
		//delete
	Route::delete('/revenue/{id}/delete', ['middleware' => ['role:revenue|delete'], 'as' => 'editor.revenue.delete', 'uses' => 'RevenueController@delete']);

	Route::get('/report_tax_ppn', ['middleware' => ['role:revenue|read'], 'as' => 'editor.revenue.report_tax_ppn', 'uses' => 'RevenueController@report_tax_ppn']);
	Route::post('/report_tax_ppn', ['middleware' => ['role:revenue|read'], 'as' => 'editor.revenue.report_tax_ppn', 'uses' => 'RevenueController@report_tax_ppn']);

	Route::get('/report_tax_pph', ['middleware' => ['role:revenue|read'], 'as' => 'editor.revenue.report_tax_pph', 'uses' => 'RevenueController@report_tax_pph']);
	Route::post('/report_tax_pph', ['middleware' => ['role:revenue|read'], 'as' => 'editor.revenue.report_tax_pph', 'uses' => 'RevenueController@report_tax_pph']);


	//Franchise Fee
	//
		//index
	Route::get('/franchise_fee', ['as' => 'editor.franchise_fee.index', 'uses' => 'FranchiseFeeController@index']);
		//create
	Route::get('/franchise_fee/create', ['as' => 'editor.franchise_fee.create', 'uses' => 'FranchiseFeeController@create']);
	Route::post('/franchise_fee/create', ['as' => 'editor.franchise_fee.store', 'uses' => 'FranchiseFeeController@store']);
		//detail
	Route::get('/franchise_fee/{id}', ['as' => 'editor.franchise_fee.detail', 'uses' => 'FranchiseFeeController@detail']);
		//edit
	Route::get('/franchise_fee/{id}/edit', ['as' => 'editor.franchise_fee.edit', 'uses' => 'FranchiseFeeController@edit']);
	Route::put('/franchise_fee/{id}/edit', ['as' => 'editor.franchise_fee.update', 'uses' => 'FranchiseFeeController@update']);
		//status - submit
	Route::put('/franchise_fee/{id}/submit', ['middleware' => ['role:franchise_fee|submit'], 'as' => 'editor.franchise_fee.submit', 'uses' => 'FranchiseFeeController@submit']);
		//status - finance_approve
	Route::put('/franchise_fee/{id}/finance_approve', ['middleware' => ['role:franchise_fee|paid'], 'as' => 'editor.franchise_fee.finance_approve', 'uses' => 'FranchiseFeeController@finance_approve']);
		//status - owner_approve
	Route::put('/franchise_fee/{id}/owner_approve', ['middleware' => ['role:franchise_fee|issued'], 'as' => 'editor.franchise_fee.owner_approve', 'uses' => 'FranchiseFeeController@owner_approve']);
		//status - finance_payment
	//edit
	Route::get('/franchise_fee/{id}/paid', ['as' => 'editor.franchise_fee.paid', 'uses' => 'FranchiseFeeController@paid']);
	Route::put('/franchise_fee/{id}/finance_payment', ['middleware' => ['role:franchise_fee|paid'], 'as' => 'editor.franchise_fee.finance_payment', 'uses' => 'FranchiseFeeController@finance_payment']);
		//delete
	Route::delete('/franchise_fee/{id}/delete', ['as' => 'editor.franchise_fee.delete', 'uses' => 'FranchiseFeeController@delete']);

//tax report
//ppn
	Route::get('tax_ppn', ['as' => 'editor.tax_ppn.index', 'uses' => 'TaxPPNController@index']);
		//create
	Route::get('/tax_ppn/create', ['as' => 'editor.tax_ppn.create', 'uses' => 'TaxPPNController@create']);
	Route::post('/tax_ppn/create', ['as' => 'editor.tax_ppn.store', 'uses' => 'TaxPPNController@store']);
		//detail
	Route::get('/tax_ppn/{id}', ['as' => 'editor.tax_ppn.detail', 'uses' => 'TaxPPNController@detail']);
		//edit
	Route::get('/tax_ppn/{id}/edit', ['as' => 'editor.tax_ppn.edit', 'uses' => 'TaxPPNController@edit']);
	Route::put('/tax_ppn/{id}/edit', ['as' => 'editor.tax_ppn.update', 'uses' => 'TaxPPNController@update']);
		//status - submit
	Route::put('/tax_ppn/{id}/submit', ['middleware' => ['role:franchise_fee|submit'], 'as' => 'editor.tax_ppn.submit', 'uses' => 'TaxPPNController@submit']);
		//status - finance_approve
	Route::put('/tax_ppn/{id}/finance_approve', ['middleware' => ['role:franchise_fee|paid'], 'as' => 'editor.tax_ppn.finance_approve', 'uses' => 'TaxPPNController@finance_approve']);
		//status - owner_approve
	Route::put('/tax_ppn/{id}/owner_approve', ['middleware' => ['role:franchise_fee|issued'], 'as' => 'editor.tax_ppn.owner_approve', 'uses' => 'TaxPPNController@owner_approve']);
		//status - finance_payment
	//edit
	Route::get('/tax_ppn/{id}/paid', ['as' => 'editor.tax_ppn.paid', 'uses' => 'TaxPPNController@paid']);
	Route::put('/tax_ppn/{id}/finance_payment', ['middleware' => ['role:franchise_fee|paid'], 'as' => 'editor.tax_ppn.finance_payment', 'uses' => 'TaxPPNController@finance_payment']);
		//delete
	Route::delete('/tax_ppn/{id}/delete', ['as' => 'editor.tax_ppn.delete', 'uses' => 'TaxPPNController@delete']);

//pph
	Route::get('tax_pph', ['as' => 'editor.tax_pph.index', 'uses' => 'TaxPPHController@index']);
		//create
	Route::get('/tax_pph/create', ['as' => 'editor.tax_pph.create', 'uses' => 'TaxPPHController@create']);
	Route::post('/tax_pph/create', ['as' => 'editor.tax_pph.store', 'uses' => 'TaxPPHController@store']);
		//detail
	Route::get('/tax_pph/{id}', ['as' => 'editor.tax_pph.detail', 'uses' => 'TaxPPHController@detail']);
		//edit
	Route::get('/tax_pph/{id}/edit', ['as' => 'editor.tax_pph.edit', 'uses' => 'TaxPPHController@edit']);
	Route::put('/tax_pph/{id}/edit', ['as' => 'editor.tax_pph.update', 'uses' => 'TaxPPHController@update']);
		//status - submit
	Route::put('/tax_pph/{id}/submit', ['middleware' => ['role:franchise_fee|submit'], 'as' => 'editor.tax_pph.submit', 'uses' => 'TaxPPHController@submit']);
		//status - finance_approve
	Route::put('/tax_pph/{id}/finance_approve', ['middleware' => ['role:franchise_fee|paid'], 'as' => 'editor.tax_pph.finance_approve', 'uses' => 'TaxPPHController@finance_approve']);
		//status - owner_approve
	Route::put('/tax_pph/{id}/owner_approve', ['middleware' => ['role:franchise_fee|issued'], 'as' => 'editor.tax_pph.owner_approve', 'uses' => 'TaxPPHController@owner_approve']);
		//status - finance_payment
	//edit
	Route::get('/tax_pph/{id}/paid', ['as' => 'editor.tax_pph.paid', 'uses' => 'TaxPPHController@paid']);
	Route::put('/tax_pph/{id}/finance_payment', ['middleware' => ['role:franchise_fee|paid'], 'as' => 'editor.tax_pph.finance_payment', 'uses' => 'TaxPPHController@finance_payment']);
		//delete
	Route::delete('/tax_pph/{id}/delete', ['as' => 'editor.tax_pph.delete', 'uses' => 'TaxPPHController@delete']);




	//Maintenance List

	//Employee
		//index
	Route::get('/maintenance', ['as' => 'editor.maintenance.index', 'uses' => 'MaintenanceController@index']);
		//create
	Route::get('/maintenance/create', ['as' => 'editor.maintenance.create', 'uses' => 'MaintenanceController@create']);
	Route::post('/maintenance/create', ['as' => 'editor.maintenance.store', 'uses' => 'MaintenanceController@store']);
	//edit
	Route::get('/maintenance/{id}/edit', ['as' => 'editor.maintenance.edit', 'uses' => 'MaintenanceController@edit']);
	Route::put('/maintenance/{id}/edit', ['as' => 'editor.maintenance.update', 'uses' => 'MaintenanceController@update']);
	//delete
	Route::delete('/maintenance/{id}/delete', ['as' => 'editor.maintenance.delete', 'uses' => 'MaintenanceController@delete']);

	Route::get('/document', ['as' => 'editor.document.index', 'uses' => 'DocumentController@index']);
		//create
	Route::get('/document/create', ['as' => 'editor.document.create', 'uses' => 'DocumentController@create']);
	Route::post('/document/create', ['as' => 'editor.document.store', 'uses' => 'DocumentController@store']);
	//edit
	Route::get('/document/{id}/edit', ['as' => 'editor.document.edit', 'uses' => 'DocumentController@edit']);
	Route::put('/document/{id}/edit', ['as' => 'editor.document.update', 'uses' => 'DocumentController@update']);
	//delete
	Route::delete('/document/{id}/delete', ['as' => 'editor.document.delete', 'uses' => 'DocumentController@delete']);


	Route::get('/document_file/{id}', ['as' => 'editor.document_file.index', 'uses' => 'DocumentFileController@index']);
		//create
	Route::get('/document_file/{id}/create', ['as' => 'editor.document_file.create', 'uses' => 'DocumentFileController@create']);
	Route::post('/document_file/{id}/create', ['as' => 'editor.document_file.store', 'uses' => 'DocumentFileController@store']);
	//edit
	Route::get('/document_file/{id}/edit', ['as' => 'editor.document_file.edit', 'uses' => 'DocumentFileController@edit']);
	Route::put('/document_file/{id}/edit', ['as' => 'editor.document_file.update', 'uses' => 'DocumentFileController@update']);
	//delete
	Route::delete('/document_file/{id}/delete', ['as' => 'editor.document_file.delete', 'uses' => 'DocumentFileController@delete']);


	//branch
	//

	Route::get('/op', ['as' => 'editor.op.index', 'uses' => 'OpController@index']);
		//create
	Route::get('/op/create', ['as' => 'editor.op.create', 'uses' => 'OpController@create']);
	Route::post('/op/create', ['as' => 'editor.op.store', 'uses' => 'OpController@store']);
	//edit
	Route::get('/op/{id}/edit', ['as' => 'editor.op.edit', 'uses' => 'OpController@edit']);
	Route::put('/op/{id}/edit', ['as' => 'editor.op.update', 'uses' => 'OpController@update']);
	//delete
	Route::delete('/op/{id}/delete', ['as' => 'editor.op.delete', 'uses' => 'OpController@delete']);




});

Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
//Cashbond List
		//index
	Route::get('cash_operational', ['as' => 'editor.cash_operational.index', 'uses' => 'CashOPController@index']);
		//create
	Route::get('/cash_operational/create', ['as' => 'editor.cash_operational.create', 'uses' => 'CashOPController@create']);
	Route::post('/cash_operational/create', ['as' => 'editor.cash_operational.store', 'uses' => 'CashOPController@store']);
		//edit
	Route::get('/cash_operational/{id}/edit', ['as' => 'editor.cash_operational.edit', 'uses' => 'CashOPController@edit']);
	Route::put('/cash_operational/{id}/edit', ['as' => 'editor.cash_operational.update', 'uses' => 'CashOPController@update']);

	//waitingowner
	Route::put('/cash_operational/{id}/waitingowner', ['as' => 'editor.cash_operational.waitingowner', 'uses' => 'CashOPController@updatewaitingowner']);
	//waiting finalize
	Route::get('/cash_operational/{id}/finalize', ['as' => 'editor.cash_operational.finalize', 'uses' => 'CashOPController@finalize']);
	Route::put('/cash_operational/{id}/finalize', ['as' => 'editor.cash_operational.updatefinalize', 'uses' => 'CashOPController@updatefinalize']);

	//waiting owner action
	Route::get('/cash_operational/{id}/issued', ['as' => 'editor.cash_operational.issued', 'uses' => 'CashOPController@issued']);
	Route::put('/cash_operational/{id}/issued', ['as' => 'editor.cash_operational.updateissued', 'uses' => 'CashOPController@updateissued']);

	//closing
	Route::get('/cash_operational/{id}/closing', ['as' => 'editor.cash_operational.closing', 'uses' => 'CashOPController@closing']);
	Route::post('/cash_operational/{id}/closing', ['as' => 'editor.cash_operational.storeclosing', 'uses' => 'CashOPController@storeclosing']);
	//detail item
	Route::get('/cash_operational/{id}/detailitem', ['as' => 'editor.cash_operational.detailitem', 'uses' => 'CashOPController@detailitem']);
	Route::post('/cash_operational/detailitem', ['as' => 'editor.cash_operational.storedetailitem', 'uses' => 'CashOPController@storedetailitem']);

	//verified and reject
	Route::get('/cash_operational/{id}/verified', ['as' => 'editor.cash_operational.verified', 'uses' => 'CashOPController@verified']);
	Route::put('/cash_operational/{id}/updateverified', ['as' => 'editor.cash_operational.updateverified', 'uses' => 'CashOPController@updateverified']);
	Route::put('/cash_operational/{id}/updatereject', ['as' => 'editor.cash_operational.updatereject', 'uses' => 'CashOPController@updatereject']);
	Route::put('/cash_operational/{id}/updatewaitingverified', ['as' => 'editor.cash_operational.updatewaitingverified', 'uses' => 'CashOPController@updatewaitingverified']);

	//view
	Route::get('/cash_operational/{id}/paid', ['as' => 'editor.cash_operational.paid', 'uses' => 'CashOPController@paid']);
	Route::get('/cash_operational/{id}/approved', ['as' => 'editor.cash_operational.approved', 'uses' => 'CashOPController@approved']);
	Route::get('/cash_operational/{id}/view', ['as' => 'editor.cash_operational.view', 'uses' => 'CashOPController@view']);
	//delete
	Route::delete('/cash_operational/{id}/delete', ['as' => 'editor.cash_operational.delete', 'uses' => 'CashOPController@delete']);
	//delete detail
	Route::delete('/cash_operational/{id}/deletedetail', ['as' => 'editor.cash_operational.deletedetail', 'uses' => 'CashOPController@deletedetail']);
	//delete
	Route::delete('/cash_operational/{id}/delete', ['middleware' => ['role:cash_operational|delete'],'as' => 'editor.cash_operational.delete', 'uses' => 'CashOPController@delete']);

	//cashbond remainding
	Route::get('/cash_operational/{id}/remainder', ['as' => 'editor.cash_operational.remainder', 'uses' => 'CashOPController@remainder']);
	Route::put('/cashbond/{id}/update_remainder', ['as' => 'editor.cash_operational.update_remainder', 'uses' => 'CashOPController@updateRemainder']);


	//cashbond bank
	Route::get('/cash_operationalbank', ['as' => 'editor.cash_operationalbank.index', 'uses' => 'CashOPController@indexbank']);

	Route::get('/cash_operational_report', ['as' => 'editor.cash_operational.report', 'uses' => 'CashOPController@report']);
	Route::post('/cash_operational_report', ['as' => 'editor.cash_operational.report', 'uses' => 'CashOPController@report']);

});

Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
//Cashbond List
		//index
	Route::get('maintenance_list', ['as' => 'editor.maintenance_list.index', 'uses' => 'MaintenanceListController@index']);
		//create
	Route::get('/maintenance_list/create', ['as' => 'editor.maintenance_list.create', 'uses' => 'MaintenanceListController@create']);
	Route::post('/maintenance_list/create', ['as' => 'editor.maintenance_list.store', 'uses' => 'MaintenanceListController@store']);
		//edit
	Route::get('/maintenance_list/{id}/edit', ['as' => 'editor.maintenance_list.edit', 'uses' => 'MaintenanceListController@edit']);
	Route::put('/maintenance_list/{id}/edit', ['as' => 'editor.maintenance_list.update', 'uses' => 'MaintenanceListController@update']);

	//waitingowner
	Route::put('/maintenance_list/{id}/waitingowner', ['as' => 'editor.maintenance_list.waitingowner', 'uses' => 'MaintenanceListController@updatewaitingowner']);
	//waiting finalize
	Route::get('/maintenance_list/{id}/finalize', ['as' => 'editor.maintenance_list.finalize', 'uses' => 'MaintenanceListController@finalize']);
	Route::put('/maintenance_list/{id}/finalize', ['as' => 'editor.maintenance_list.updatefinalize', 'uses' => 'MaintenanceListController@updatefinalize']);

	//waiting owner action
	Route::get('/maintenance_list/{id}/issued', ['as' => 'editor.maintenance_list.issued', 'uses' => 'MaintenanceListController@issued']);
	Route::put('/maintenance_list/{id}/issued', ['as' => 'editor.maintenance_list.updateissued', 'uses' => 'MaintenanceListController@updateissued']);

	//closing
	Route::get('/maintenance_list/{id}/closing', ['as' => 'editor.maintenance_list.closing', 'uses' => 'MaintenanceListController@closing']);
	Route::post('/maintenance_list/{id}/closing', ['as' => 'editor.maintenance_list.storeclosing', 'uses' => 'MaintenanceListController@storeclosing']);
	//detail item
	Route::get('/maintenance_list/{id}/detailitem', ['as' => 'editor.maintenance_list.detailitem', 'uses' => 'MaintenanceListController@detailitem']);
	Route::post('/maintenance_list/detailitem', ['as' => 'editor.maintenance_list.storedetailitem', 'uses' => 'MaintenanceListController@storedetailitem']);

	//verified and reject
	Route::get('/maintenance_list/{id}/verified', ['as' => 'editor.maintenance_list.verified', 'uses' => 'MaintenanceListController@verified']);
	Route::put('/maintenance_list/{id}/updateverified', ['as' => 'editor.maintenance_list.updateverified', 'uses' => 'MaintenanceListController@updateverified']);
	Route::put('/maintenance_list/{id}/updatereject', ['as' => 'editor.maintenance_list.updatereject', 'uses' => 'MaintenanceListController@updatereject']);
	Route::put('/maintenance_list/{id}/updatewaitingverified', ['as' => 'editor.maintenance_list.updatewaitingverified', 'uses' => 'MaintenanceListController@updatewaitingverified']);

	//view
	Route::get('/maintenance_list/{id}/paid', ['as' => 'editor.maintenance_list.paid', 'uses' => 'MaintenanceListController@paid']);
	Route::get('/maintenance_list/{id}/approved', ['as' => 'editor.maintenance_list.approved', 'uses' => 'MaintenanceListController@approved']);
	Route::get('/maintenance_list/{id}/view', ['as' => 'editor.maintenance_list.view', 'uses' => 'MaintenanceListController@view']);
	//delete
	Route::delete('/maintenance_list/{id}/delete', ['as' => 'editor.maintenance_list.delete', 'uses' => 'MaintenanceListController@delete']);
	//delete detail
	Route::delete('/maintenance_list/{id}/deletedetail', ['as' => 'editor.maintenance_list.deletedetail', 'uses' => 'MaintenanceListController@deletedetail']);
	//delete
	Route::delete('/maintenance_list/{id}/delete', ['middleware' => ['role:maintenance_list|delete'],'as' => 'editor.maintenance_list.delete', 'uses' => 'MaintenanceListController@delete']);

	//cashbond remainding
	Route::get('/maintenance_list/{id}/remainder', ['as' => 'editor.maintenance_list.remainder', 'uses' => 'MaintenanceListController@remainder']);
	Route::put('/cashbond/{id}/update_remainder', ['as' => 'editor.maintenance_list.update_remainder', 'uses' => 'MaintenanceListController@updateRemainder']);


	//cashbond bank
	Route::get('/maintenance_listbank', ['as' => 'editor.maintenance_listbank.index', 'uses' => 'MaintenanceListController@indexbank']);

	Route::get('/maintenance_list_report', ['as' => 'editor.maintenance_list.report', 'uses' => 'MaintenanceListController@report']);
	Route::post('/maintenance_list_report', ['as' => 'editor.maintenance_list.report', 'uses' => 'MaintenanceListController@report']);

});


//XHR
Route::get('/getvendor', ['as' => 'get.vendor', 'uses' => 'XHRController@get_vendor']);
Route::get('/getvendorinv', ['as' => 'get.vendorinv', 'uses' => 'XHRController@get_vendor_inv']);
Route::get('/getiteminv', ['as' => 'get.iteminv', 'uses' => 'XHRController@get_item_inv']);
Route::get('/getitem', ['as' => 'get.item', 'uses' => 'XHRController@get_item']);
Route::post('/get_turnover', ['as' => 'get.turnover', 'uses' => 'XHRController@get_turnover']);
Route::get('/cashbondpayroll', ['as' => 'get.cashbondpayroll', 'uses' => 'XHRController@get_cashbond_payroll']);

Route::group(['prefix' => 'api', 'middleware' => 'auth', 'namespace' => 'Editor'], function(){
	Route::get('/month', ['as' => 'editor.index', 'uses' => 'EditorController@month_output']);
	Route::get('/revenue/{month}', ['as' => 'editor.index', 'uses' => 'EditorController@revenue_output']);
	Route::get('/expense_output', ['as' => 'editor.index', 'uses' => 'EditorController@expense_output_chart']);
	Route::get('/revenue_output', ['as'=> 'editor.index', 'uses'=> 'EditorController@revenue_output_chart']);
});


Route::get('/profit_lost', ['as' => 'editor.pl', 'uses' => 'PLController@index']);
Route::post('/profit_lost', ['as' => 'editor.pl', 'uses' => 'PLController@index']);
