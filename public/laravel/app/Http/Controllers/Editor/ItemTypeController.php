<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\ItemTypeRequest;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Model\ItemType;

class ItemTypeController extends Controller
{
    public function index()
    {
    	if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14; 
        $item_types = ItemType::paginate(15);
    	return view ('editor.item_type.index', compact('item_types'))->with('number',$no);
    }

    public function create()
    {
    	$item_type_list = ItemType::all()->pluck('name', 'id');
        //$item_type_list->all();
        //dd($item_type_list);
        return view ('editor.item_type.form', ['item_type_list' => $item_type_list]);

    }

    public function store(ItemTypeRequest $request)
    {
    	$item_type = new ItemType;
    	$item_type->type_name = $request->input('type_name');
    	$item_type->type_desc = $request->input('type_desc');
        $item_type->created_by = Auth::id();
    	$item_type->save();

    	return redirect()->action('Editor\ItemTypeController@index');
    }

    public function edit($id)
    {
    	$item_type = ItemType::Find($id); 

        //dd($item_type);
        // echo($item_type);
    	return view ('editor.item_type.form', compact('item_type'));
    }

    public function update($id, ItemTypeRequest $request)
    {
    	$item_type = ItemType::Find($id);
        $item_type->type_name = $request->input('type_name');
    	$item_type->type_desc = $request->input('type_desc');
        $item_type->updated_by = Auth::id();
    	$item_type->save();

    	return redirect()->action('Editor\ItemTypeController@index');
    }

    public function delete($id)
    {
    	ItemType::Find($id)->delete();
    	return redirect()->action('Editor\ItemTypeController@index');
    }
}
