<?php

namespace App\Http\Controllers\Editor;

use Auth;
use File;
use Session;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\CashbondRequest;
use App\Http\Requests\CashbondClosingRequest;
use App\Http\Controllers\Controller;

//use App\Model\CashOP;
use App\Model\MaintenanceList;

use App\Model\Item;
use App\Model\MaintenanceDetail;
use App\Model\CashbondDetailItem;
use App\Model\Branch;

class MaintenanceListController extends Controller
{
	public function index()
	{
		if (Input::has('page'))
           {
             $page = Input::get('page');
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14;

        //dd(Auth::id());

        //$value = Session::get('variableName');
        //dd(Session::get('branch_id'));

		$cashbonds = MaintenanceList::where('status_code', '<>', 'verified')
		->where('branch_id', Session::get('branch_id'))
		->orderBy('created_at', 'DESC')
		->paginate(15);
		//dd($cashbonds);
		return view ('editor.maintenance_list.index', compact('cashbonds'))->with('number',$no);
	}

	public function indexbank()
	{
		if (Input::has('page'))
           {
             $page = Input::get('page');
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14;
		$cashbonds = MaintenanceList::where('status_code', 'Verified')
		->where('branch_id', Session::get('branch_id'))
		->orderBy('created_at', 'DESC')
		->paginate(15);

		return view ('editor.maintenance_list_bank.index', compact('cashbonds'))->with('number',$no);
	}

	public function create()
	{
		$now = Carbon::now()->format('Y-m-d');
		return view ('editor.maintenance_list.form', compact('cashbond', 'now'));
	}

	public function store(CashbondRequest $request)
	{
		$cashbond = new MaintenanceList;
		$cashbond->start_date = $request->input('start_date');
		$cashbond->end_date = $request->input('end_date');
		$cashbond->budget_request = $request->input('budget_request');
		$cashbond->comment = $request->input('comment');
		$cashbond->status_code = 'Waiting for Owner Action';
		$cashbond->branch_id = Session::get('branch_id');
		if($request->file('attch_foto'))
    	{
    		//return date('h-i-s').$request->file('attch_foto')->getClientOriginalName();
			$file_dir = "uploads/maintenance_list/";
			if(!File::exists($file_dir))
		    {
		        File::makeDirectory($file_dir, $mode = 0777, true, true);
            }


            $file_ext_attach_foto = $request->file('attch_foto')->getClientOriginalExtension();



            $cashbond->photo = date('h-i-s').$request->file('attch_foto')->getClientOriginalName();



            $request->file('attch_foto')->move($file_dir, $cashbond->photo);


		    $thumbnail_dir = $file_dir."thumbnail/";
		    if(!File::exists($thumbnail_dir))
		    {
		    	File::makeDirectory($thumbnail_dir, $mode = 0777, true, true);
		    }
		    $thumbnail1 = Image::make($file_dir.$cashbond->photo);
            $thumbnail1->fit(200, 200)->save($thumbnail_dir.$cashbond->photo);


        }


		$cashbond->save();

		return redirect('editor/maintenance_list');
	}

	public function edit($id)
	{
		$cashbond = MaintenanceList::Find($id);
		$budget_request_show = number_format($cashbond->budget_request);
		//dd($budget_request_show);
		return view ('editor.maintenance_list.form', compact('cashbond', 'budget_request_show'));
	}

	public function update($id, CashbondRequest $request)
	{
		$cashbond = MaintenanceList::Find($id);
		$cashbond->start_date = $request->input('start_date');
		$cashbond->end_date = $request->input('end_date');
		$cashbond->budget_request = $request->input('budget_request');
		$cashbond->comment = $request->input('comment');
		$cashbond->status_code = 'Waiting for Owner Action';
		$cashbond->save();

		return redirect('editor/maintenance_list');

	}

	public function updatewaitingowner($id, Request $request)
	{
		$now = Carbon::now();
		$cashbond = MaintenanceList::Find($id);
		$cashbond->status_code = 'Waiting for Financial Action';
		$cashbond->save();

		return redirect()->action('Editor\MaintenanceListController@index');
	}

	public function finalize($id)
	{
		$cashbond = MaintenanceList::Find($id);

		$cashbond_detail = MaintenanceDetail::Where('maintenance_id', $id)->get();
		// dd($cashbond_detail);
		return view ('editor.maintenance_list.finalize', compact('cashbond_detail', 'cashbond'));
	}

	public function updatefinalize($id, Request $request)
	{
		$cashbond = MaintenanceList::Find($id);
		$cashbond->comment = $request->input('comment');
		// $cashbond->budget_issued = $request->input('budget_issued');
		$cashbond->status_code = 'Transfered';
		$cashbond->save();

		if($request->transfer_receipt)
		{
			$cashbond = MaintenanceList::FindOrFail($cashbond->id);

			$original_directory = "uploads/maintenance_list_dir/";

			if(!File::exists($original_directory))
			{
				File::makeDirectory($original_directory, $mode = 0777, true, true);
			}

			// $file_extension = $request->image->getClientOriginalExtension();
			$cashbond->transfer_receipt = Carbon::now()->format("d-m-Y h-i-s").$request->transfer_receipt->getClientOriginalName();
			$request->transfer_receipt->move($original_directory, $cashbond->transfer_receipt);

			// $thumbnail_directory = $original_directory."thumbnail/";
			// if(!File::exists($thumbnail_directory))
			// {
			// 	File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
			// }
			// $thumbnail = Image::make($original_directory.$invoice->invoice_attachment);
			// $thumbnail->fit(300,300)->save($thumbnail_directory.$invoice->invoice_attachment);

			$cashbond->save();
		}

		return redirect('editor/maintenance_list');
	}

	public function issued($id)
	{
		$cashbond = MaintenanceList::Find($id);

		$budget_request_show = number_format($cashbond->budget_request);
		//dd($budget_request_show);

		// dd(number_format($cashbond->budget_request));

		return view ('editor.maintenance_list.issued', compact('cashbond', 'budget_request_show'));
	}

	public function updateissued($id, Request $request)
	{
		$now = Carbon::now();
		$cashbond = MaintenanceList::Find($id);
		$cashbond->status_code = 'Waiting for Financial Action';
		$cashbond->budget_issued = $request->input('budget_issued');
		$cashbond->save();

		return redirect('editor/maintenance_list');
	}

	public function closing($id)
	{
		$cashbond = MaintenanceList::Find($id);
		// $cashbond_detail = CashbondDetail::where('cashbond_id', $id)->get();
		$sql = 'SELECT
					COUNT(cashbond_detail_item.id) AS count_item,
					maintenance_detail.id,
					maintenance_detail.maintenance_id,
					maintenance_detail.date,
					maintenance_detail.description,
					maintenance_detail.nota_number,
					maintenance_detail.amount,
					maintenance_detail.cashbond_attachment
				FROM
					maintenance_detail
				LEFT JOIN cashbond_detail_item ON maintenance_detail.id = cashbond_detail_item.cashbond_detail_id
				WHERE
					maintenance_detail.maintenance_id = '.$id.'
				GROUP BY
					maintenance_detail.id,
					maintenance_detail.maintenance_id,
					maintenance_detail.date,
					maintenance_detail.description,
					maintenance_detail.nota_number,
					maintenance_detail.amount,
					maintenance_detail.cashbond_attachment';
		$cashbond_detail = DB::table(DB::raw("($sql) as rscashbonddetail"))->get();

		$sql1 = 'SELECT
						IFNULL(DERIVEDTBL.count_item_par,NULL) AS count
					FROM
						(
							SELECT
								COUNT(cashbond_detail_item.id) AS count_item_par,
								maintenance_detail.maintenance_id
							FROM
								maintenance_detail
							LEFT JOIN cashbond_detail_item ON maintenance_detail.id = cashbond_detail_item.cashbond_detail_id
							GROUP BY
								maintenance_detail.maintenance_id,
								maintenance_detail.id
						) AS DERIVEDTBL
					WHERE
						DERIVEDTBL.count_item_par = 0 AND DERIVEDTBL.maintenance_id = '.$id.'
					GROUP BY
					DERIVEDTBL.count_item_par';

		$cashbond_item_count = DB::table(DB::raw("($sql1) as rsitemcount1"))->first();

		//dd($cashbond_item_count);

		$month_list = [
			'1' => 'January',
			'2' => 'February',
			'3' => 'March',
			'4' => 'April',
			'5' => 'May',
			'6' => 'June',
			'7' => 'July',
			'8' => 'August',
			'9' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December',
		];

		return view ('editor.maintenance_list.closing', compact('cashbond', 'cashbond_detail', 'cashbond_item_count', 'month_list'));
	}

	public function storeclosing($id, CashbondClosingRequest $request)
	{



 		$cashbond_detail = new MaintenanceDetail;
		$cashbond_detail->maintenance_id = $request->input('id');
		$cashbond_detail->date = $request->input('date');
		$cashbond_detail->nota_number = $request->input('nota_number');
		$cashbond_detail->description = $request->input('description');
		$cashbond_detail->amount = $request->input('amount');
		$cashbond_detail->save();

		 $cashbond = MaintenanceList::Find($id);
		 $cashbond->cashout += $request->input('amount');
		// //dd($cashbond);
		// $cashbond->add_amount = $request->input('add_amount');
		// $cashbond->add_comment = $request->input('add_comment');
		$cashbond->save();

		if($request->image)
		{
			$cashbond_detail = MaintenanceDetail::FindOrFail($cashbond_detail->id);

			$original_directory = "uploads/cashop_detail/";

			if(!File::exists($original_directory))
			{
				File::makeDirectory($original_directory, $mode = 0777, true, true);
			}

			// $file_extension = $request->image->getClientOriginalExtension();
			$cashbond_detail->cashbond_attachment = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
			$request->image->move($original_directory, $cashbond_detail->cashbond_attachment);

			// $thumbnail_directory = $original_directory."thumbnail/";
			// if(!File::exists($thumbnail_directory))
			// {
			// 	File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
			// }
			// $thumbnail = Image::make($original_directory.$invoice->invoice_attachment);
			// $thumbnail->fit(300,300)->save($thumbnail_directory.$invoice->invoice_attachment);

			$cashbond_detail->save();
		}


		return redirect()->back();
	}

	public function updatewaitingverified($id, Request $request)
	{
		$cashbond = MaintenanceList::Find($id);
		$cashbond->status_code = 'Waiting Verified';
		// $cashbond->add_amount = $request->input('add_amount');
		// $cashbond->add_comment = $request->input('add_comment');
		$cashbond->save();

		return redirect('editor/maintenance_list');
	}

	public function detailitem($id)
	{
		$cashbond_detail = MaintenanceDetail::Find($id);

		// dd($cashbond_detail);
		$cashbond_detail_item = CashbondDetailItem::where('cashbond_detail_id', $id)->get();



		$item_list =  DB::table('item')
		->join('item_category', 'item.item_category_id', '=', 'item_category.id')
		->join('item_type', 'item_category.item_type_id', '=', 'item_type.id')
		->where('item_type.id', 2)->pluck('item.item_name', 'item.id');

		//dd($item_list);

		return view ('editor.maintenance_list.detailitem', compact('cashbond_detail', 'cashbond_detail_item', 'item_list'));
	}

	public function storedetailitem(Request $request)
	{

		CashbondDetailItem::where('cashbond_detail_id', $request->input('id'))->delete();
		$cart = $request->input('item_id');
		if($cart != null)
		{
			foreach($request->input('item_id') as $key => $quantitydetail)
			{

				$cashbond_detail_item = new CashbondDetailItem;
				$cashbond_detail_item->item_id = $request->input('item_id.'.$key);
				$cashbond_detail_item->quantity = $request->input('quantity.'.$key);
				$cashbond_detail_item->uom = $request->input('uom.'.$key);
				$cashbond_detail_item->price = $request->input('price.'.$key);
				$cashbond_detail_item->total = $request->input('total.'.$key);
				$cashbond_detail_item->paid = $request->input('paid.'.$key);
				$cashbond_detail_item->cashbond_detail_id = $request->input('id');
				$cashbond_detail_item->save();
			}
		}

		return redirect('editor/maintenance_list/'.$request->input('cashbond_id').'/closing');
	}

	public function verified($id)
	{
		$cashbond = MaintenanceList::Find($id);
		//set cashbond remainder
		$cashbond->remainder =  $cashbond->budget_issued-$cashbond->cashout;
		$cashbond->save();

		$cashbond_detail = MaintenanceDetail::Where('maintenance_id', $id)->get();


		// //return $cashbond;
		// foreach ($cashbond_detail as $cd) {
		// 	$cashbond_detail_item = DB::table('cashbond_detail_item')
		// 	->leftjoin('item', 'cashbond_detail_item.item_id', '=', 'item.id')
		// 	->leftjoin('cashbond_detail', 'cashbond_detail_item.cashbond_detail_id', '=', 'cashbond_detail.id')
		// 	->select('cashbond_detail_item.id',
		// 		'cashbond_detail_item.cashbond_detail_id',
		// 		'cashbond_detail.cashbond_id',
		// 		'cashbond_detail.cashbond_attachment',
		// 		'item.item_name',
		// 		'cashbond_detail_item.item_id',
		// 		'cashbond_detail_item.quantity',
		// 		'cashbond_detail_item.price',
		// 		'cashbond_detail_item.uom',
		// 		'cashbond_detail_item.total',
		// 		'cashbond_detail_item.paid')
		// 	->where('cashbond_detail_item.cashbond_detail_id', '=', $cd->id)
		// 	->whereNull('cashbond_detail.deleted_at')
		// 	->get();
		// 	$array_val["id"] = $cd->id;
		// 	$array_val["date"] = $cd->date;
		// 	$array_val["nota_number"] = $cd->nota_number;
		// 	$array_val["description"] = $cd->description;
		// 	$array_val["amount"] = $cd->amount;
		// 	$array_val["cashbond_attachment"] = $cd->cashbond_attachment;
		// 	$cdi_arrays = null;
		// 	foreach ($cashbond_detail_item as $cdi) {
		// 		$cdi_array = null;
		// 		$cdi_array["id"] = $cdi->id;
		// 		$cdi_array["item_name"] = $cdi->item_name;
		// 		$cdi_array["quantity"] = $cdi->quantity;
		// 		$cdi_array["price"] = $cdi->price;
		// 		$cdi_array["uom"] = $cdi->uom;
		// 		$cdi_array["total"] = $cdi->total;
		// 		$cdi_array["paid"] = $cdi->paid;
		// 		$cdi_arrays[] = $cdi_array;
		// 	}
		// 	$array_val["detail"] = $cdi_arrays;
		// 	$array_all[]= $array_val;
		// }
		//dd($array_all);
		return view ('editor.maintenance_list.verified', compact('cashbond_detail', 'cashbond'));
	}

	public function updateverified($id, Request $request)
	{
		$cashbond = MaintenanceList::Find($id);

		if ($cashbond->remainder == 0) {
			 $cashbond->status_code = 'Verified';
		     $cashbond->save();
		} else {
			 $cashbond->status_code = "remainder";
			 $cashbond->save();
		}



		return redirect('editor/maintenance_list');

	}

	public function updatereject($id)
	{
		$cashbond = MaintenanceList::Find($id);
		$cashbond->status_code = 'Waiting for Revision';
		$cashbond->save();

		return redirect('editor/maintenance_list');
	}

	public function view($id)
	{
		$cashbond = MaintenanceList::Find($id);

		$cashbond_detail = MaintenanceDetail::Where('maintenance_id', $id)->get();

		return view ('editor.maintenance_list.view', compact('cashbond_detail', 'cashbond'));
	}

	public function deletedetail($id)
	{
		$cash = MaintenanceDetail::Find($id)->delete();
		//cash kurang
		$id_cash_op = $cash->cash_operational_id;
		$cash_op = MaintenanceList::find($id_cash_op);
		$cash_op->cashout -= $cash->amount;
		$cash_op->save();
		//
		$cash->delete();
		return redirect()->back();
	}

	public function delete($id)
	{
		MaintenanceList::Find($id)->delete();
		return redirect()->action('Editor\MaintenanceList@index');
	}

	public function remainder($id)
	{
		$cashbond = MaintenanceList::find($id);

		return view('editor.maintenance_list.remainder',compact('cashbond'));
	}

	public function updateRemainder($id,Request $request)
	{
		$cashbond = MaintenanceList::find($id);
		//return $request->all();
		//upload section
		//Store Image and create Thumbnail
    	if($request->file('remainder_receipt'))
    	{
			$file_dir = "uploads/maintenance_list_dir/remainder/";
			if(!File::exists($file_dir))
		    {
		        File::makeDirectory($file_dir, $mode = 0777, true, true);
            }

            $file_ext_attach_foto = $request->file('remainder_receipt')->getClientOriginalExtension();
            $cashbond->remainder_receipt = date('h-i-s').$request->file('remainder_receipt')->getClientOriginalName();

            $request->file('remainder_receipt')->move($file_dir, $cashbond->remainder_receipt);


		    $thumbnail_dir = $file_dir."thumbnail/";
		    if(!File::exists($thumbnail_dir))
		    {
		    	File::makeDirectory($thumbnail_dir, $mode = 0777, true, true);
		    }
		    $thumbnail1 = Image::make($file_dir.$cashbond->remainder_receipt);
            $thumbnail1->fit(200, 200)->save($thumbnail_dir.$cashbond->remainder_receipt);

        }

		//end
		$cashbond->status_code = 'Verified';
		$cashbond->remainder = 0;
		$cashbond->save();

		return redirect()->route('editor.maintenance_list.index');

	}

	public function Report(Request $request)
	{

			$start_date = $request->start_date;
			$end_date = $request->end_date;

			if ($request->start_date && $request->end_date) {
				$reports = DB::select("SELECT month(start_date) month,sum(IFNULL(cashout,0) + IFNULL(add_amount,0)) AS grand_total
				FROM `cashbond`
				WHERE `status_code` = 'Verified' AND `cashbond`.`deleted_at` IS NULL
				and date(start_date) between date('".$start_date."') and date('".$end_date."')
				group by month(start_date)
				");


				$graph_sql = DB::select("SELECT week(start_date) week,sum(IFNULL(cashout,0) + IFNULL(add_amount,0)) AS grand_total
				FROM `cashbond`
				WHERE `status_code` = 'Verified' AND `cashbond`.`deleted_at` IS NULL
				and date(start_date) between date('".$start_date."') and date('".$end_date."')
				group by week(start_date)
				");

				$graph = "";
				$i = 1;
				foreach ($graph_sql as $item) {

					$graph .= "['".$i."',".$item->grand_total."],";
					$i++;
				}

				$graph = substr($graph, 0, -1);
			}






		return view('editor.maintenance_list.report',compact('bulan','tahun','reports','graph'));
	}
}
