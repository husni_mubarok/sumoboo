<?php

namespace App\Http\Controllers\Editor;

use Auth;
use File;
use Session;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests\RevenueRequest;
use App\Http\Controllers\Controller;
use App\Model\Revenue;
use App\Model\RevenueAttachment;

class RevenueController extends Controller
{
    public function index()
    {
    	$revenues = Revenue::where('status', '!=', '1')
        ->where('branch_id', Session::get('branch_id'))
        ->latest()
        ->get();

    	return view ('editor.revenue.index', compact('revenues'));
    }

    public function bank()
    {
        $revenues = Revenue::where('status', '1')
        ->where('branch_id', Session::get('branch_id'))
        ->latest()
        ->get();

        //return $revenues;
        return view ('editor.revenue.bank', compact('revenues'));
    }

    public function create()
    {
    	return view ('editor.revenue.form');
    }

    public function store(RevenueRequest $request)
    {
    	$revenue = new Revenue;
    	$revenue->date = $request->input('date');
    	$revenue->cash = $request->input('cash');
    	$revenue->debit_bca = $request->input('debit_bca');
    	$revenue->mastercard = $request->input('mastercard');
    	$revenue->visacard = $request->input('visacard');
    	$revenue->flazz = $request->input('flazz');
    	$revenue->other = $request->input('other');
    	$revenue->total = $request->input('total');
        $revenue->tax_rate = $request->input('tax_rate');
        $revenue->tax = $request->input('tax');
        $revenue->service_charge_rate = $request->input('service_charge_rate');
    	$revenue->service_charge = $request->input('service_charge');
    	$revenue->vat = $request->input('vat');
        $revenue->branch_id = Session::get('branch_id');
    	$revenue->created_by = Auth::id();
    	$revenue->save();

        //Store Image and create Thumbnail
        if($request->file('filename'))
        {
            $revenue_attachment = new RevenueAttachment;
            $revenue_attachment->revenue_id = $revenue->id;

            $file_dir = "uploads/revenue/".$revenue->date."/";
            if(!File::exists($file_dir))
            {
                File::makeDirectory($file_dir, $mode = 0777, true, true);
            }
            $file_extension = $request->file('filename')->getClientOriginalExtension();
            $revenue_attachment->filename = date('h-i-s').$request->file('filename')->getClientOriginalName();
            $request->file('filename')->move($file_dir, $revenue_attachment->filename);
            $revenue_attachment->created_by = Auth::id();
            $revenue_attachment->save();
        }

    	return redirect()->action('Editor\RevenueController@detail', $revenue->id);
    }

    public function detail($id)
    {
    	$revenue = Revenue::find($id);
    	return view ('editor.revenue.detail', compact('revenue'));
    }

    public function download($id)
    {
        $revenue = Revenue::find($id);
        $file_directory = "uploads/revenue/".$revenue->date."/".$revenue->revenue_attachment->filename;

         $dir = "uploads/revenue/".$revenue->date;


       $files = scandir($dir, SCANDIR_SORT_DESCENDING);
       $newest_file = $files[1];

       $file_directory = "uploads/revenue/".$revenue->date."/".$newest_file;

        return response()->download($file_directory);
    }

    public function edit($id)
    {
    	$revenue = Revenue::find($id);
    	return view ('editor.revenue.form', compact('revenue'));
    }

    public function update($id, RevenueRequest $request)
    {
    	$revenue = Revenue::find($id);
    	$revenue->date = $request->input('date');
    	$revenue->cash = $request->input('cash');
    	$revenue->debit_bca = $request->input('debit_bca');
    	$revenue->mastercard = $request->input('mastercard');
    	$revenue->visacard = $request->input('visacard');
    	$revenue->flazz = $request->input('flazz');
    	$revenue->other = $request->input('other');
    	$revenue->total = $request->input('total');
        $revenue->tax_rate = $request->input('tax_rate');
        $revenue->tax = $request->input('tax');
        $revenue->service_charge_rate = $request->input('service_charge_rate');
    	$revenue->service_charge = $request->input('service_charge');
    	$revenue->vat = $request->input('vat');
    	$revenue->updated_by = Auth::id();
    	$revenue->save();

        //Store Image and create Thumbnail
        if($request->file('filename'))
        {
            if($revenue->revenue_attachment)
            {
                $revenue_attachment = $revenue->revenue_attachment;
            }
            else
            {
                $revenue_attachment = new RevenueAttachment;
                $revenue_attachment->revenue_id = $revenue->id;
            }

            $file_dir = "uploads/revenue/".$revenue->date."/";
            if(!File::exists($file_dir))
            {
                File::makeDirectory($file_dir, $mode = 0777, true, true);
            }
            $file_extension = $request->file('filename')->getClientOriginalExtension();
            $revenue_attachment->filename = date('h-i-s').$request->file('filename')->getClientOriginalName();
            $request->file('filename')->move($file_dir, $revenue_attachment->filename);
            $revenue_attachment->updated_by = Auth::id();
            $revenue_attachment->save();
        }

    	return redirect()->action('Editor\RevenueController@detail', $revenue->id);
    }

    public function close(Request $request)
    {
    	$revenue = Revenue::find($request->input('revenue_id'));
    	$revenue->status = 1;
    	$revenue->save();

    	return redirect()->action('Editor\RevenueController@index');
    }

    public function request_edit(Request $request)
    {
    	$revenue = Revenue::find($request->input('revenue_id'));
    	$revenue->status = 2;
    	$revenue->save();

    	return redirect()->action('Editor\RevenueController@index');
    }

    public function finance_approve(Request $request)
    {
    	$revenue = Revenue::find($request->input('revenue_id'));
    	if($request->input('review') == 0)
    	{
    		$revenue->status = 1;
    	} elseif($request->input('review') == 1) {
    		$revenue->status = 3;
    	}
    	$revenue->save();

    	return redirect()->action('Editor\RevenueController@index');
    }

    public function owner_approve(Request $request)
    {
    	$revenue = Revenue::find($request->input('revenue_id'));
    	if($request->input('review') == 0)
    	{
    		$revenue->status = 1;
    	} elseif($request->input('review') == 1) {
    		$revenue->status = 0;
    	}
    	$revenue->save();

    	return redirect()->action('Editor\RevenueController@index');
    }

    public function delete($id)
    {
    	$revenue = Revenue::find($id);
    	$revenue->deleted_by = Auth::id();
    	$revenue->save();
    	$revenue->delete();

    	return redirect()->action('Editor\RevenueController@index');
    }

    public function report_tax_ppn(Request $request)
    {
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($start_date && $end_date) {
             $taxs = DB::select("SELECT sum(vat) as net ,sum(tax) as total_tax
                FROM revenue WHERE date(`date`) between date('".$start_date."') and date('".$end_date."') and deleted_at is null");
        }



        return view('editor.revenue.report_tax_ppn',compact('bulan','tahun','taxs'));


    }

     public function report_tax_pph(Request $request)
    {
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($start_date && $end_date) {
             $taxs = DB::select("SELECT sum(vat) as net ,sum(tax) as total_tax
                FROM revenue WHERE date(`date`) between date('".$start_date."') and date('".$end_date."') and deleted_at is null");
        }




        return view('editor.revenue.report_tax_ppn',compact('bulan','tahun','taxs'));


    }
}
