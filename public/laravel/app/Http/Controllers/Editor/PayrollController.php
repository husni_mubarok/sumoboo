<?php

namespace App\Http\Controllers\Editor;

use Auth;
use File;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\PayrollRequest;
use App\Http\Requests\PayrollDetailRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll;
use App\Model\PayrollDetail;
use App\Model\Employee;
use App\Model\User;

class PayrollController extends Controller
{
	public function index()
	{
		if (Input::has('page'))
           {
             $page = Input::get('page');
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14;
		$payrolls = Payroll::orderBy('created_at', 'DESC')
		->where('branch_id', Session::get('branch_id'))
		->paginate(15);
    	return view ('editor.payroll.index', compact('payrolls'))->with('number',$no);
	}

	public function createheader()
	{
		$month_list = [
			'January' => 'January',
			'February' => 'February',
			'March' => 'March',
			'April' => 'April',
			'May' => 'May',
			'June' => 'June',
			'July' => 'July',
			'August' => 'August',
			'September' => 'September',
			'October' => 'October',
			'November' => 'November',
			'December' => 'December',
		];


		return view ('editor.payroll.header', compact('month_list'));
	}

	public function storeheader(PayrollRequest $request)
	{
		$payroll = new Payroll;
		$payroll->year = $request->input('year');
		$payroll->month = $request->input('month');
		$payroll->comment = $request->input('comment');
		$payroll->branch_id = Session::get('branch_id');
		$payroll->default_work_days = $request->input('default_work_days');
		$payroll->created_by = Auth::id();
		$payroll->updated_by = Auth::id();
		$payroll->save();

		return redirect()->action('Editor\PayrollController@create', $payroll->id);
	}

	public function create($id)
	{
		$month_list = [
			'January' => 'January',
			'February' => 'February',
			'March' => 'March',
			'April' => 'April',
			'May' => 'May',
			'June' => 'June',
			'July' => 'July',
			'August' => 'August',
			'September' => 'September',
			'October' => 'October',
			'November' => 'November',
			'December' => 'December',
		];
		//$employees = Employee::all();

		// $employees = DB::table('employee')
  //       ->join('employee_branch', 'employee.id', '=', 'employee_branch.employee_id')
  //       ->select('employee.id',
  //           'employee.emp_name',
  //           'employee.emp_full_name',
  //           'employee.emp_bod',
  //           'employee.emp_address',
  //           'employee.emp_ktp_number',
  //           'employee.emp_bank',
  //           'employee.emp_bank_rek',
  //           'employee.emp_email',
  //           'employee.emp_phone',
  //           'employee.emp_gender',
  //           'employee.emp_religion',
  //           'employee.emp_hiring_date',
  //           'employee.emp_position',
  //           DB::raw('IFNULL(employee.slr_basic,0) as slr_basic'),
  //           DB::raw('IFNULL(FORMAT(employee.slr_basic,0),0) as slr_basic_show'),
  //           DB::raw('IFNULL(employee.slr_transport,0) as slr_transport'),
  //           DB::raw('IFNULL(FORMAT(employee.slr_transport,0),0) as slr_transport_show'),
  //           DB::raw('IFNULL(employee.slr_tunjangan,0) as slr_tunjangan'),
  //           DB::raw('IFNULL(FORMAT(employee.slr_tunjangan,0),0) as slr_tunjangan_show'),
  //           DB::raw('IFNULL(employee.slr_tunjangan_makan,0) as slr_tunjangan_makan'),
  //           DB::raw('IFNULL(FORMAT(employee.slr_tunjangan_makan,0),0) as slr_tunjangan_makan_show'),
  //           DB::raw('IFNULL(employee.slr_cashbond,0) as slr_cashbond'),
  //           DB::raw('IFNULL(FORMAT(employee.slr_cashbond,0),0) as slr_cashbond_show'),
  //           DB::raw('IFNULL(employee.slr_thr,0) as slr_thr'),
  //           DB::raw('IFNULL(FORMAT(employee.slr_thr,0),0) as slr_thr_show'),
  //           'employee.status',
  //           DB::raw('IFNULL(employee.slr_pot_deposit,0) as slr_pot_deposit'),
  //           DB::raw('0 as slr_pot_deposit_show'),
  //           DB::raw('0 as slr_peng_deposit'),
  //           DB::raw('0 as slr_peng_deposit_show'),
  //           DB::raw('0 as additional_cost'),
  //           DB::raw('0 as additional_cost_show'),
  //           DB::raw('0 as boarding_house'),
  //           DB::raw('0 as boarding_house_show'),
  //           DB::raw('"" as add_cost_desc'),
  //           DB::raw('0 as workdays'))
  //       ->where('employee_branch.branch_id', '=',  Session::get('branch_id'))
  //       ->whereNull('employee.deleted_at')
  //       ->get();

        //DB::enableQueryLog();
        //$employees = collect($employees)->map(function($x){ return (array) $x; })->toArray();

		//dd($employees);

       //dd($cashbond_payroll);



            $sql_employee = 'SELECT
                            employee.id,
                            employee.emp_name,
                            employee.emp_full_name,
                            employee.emp_bod,
                            employee.emp_address,
                            employee.emp_ktp_number,
                            employee.emp_bank,
                            employee.emp_bank_rek,
                            employee.emp_email,
                            employee.emp_phone,
                            employee.emp_gender,
                            employee.emp_religion,
                            employee.emp_hiring_date,
                            employee.emp_position,
                            IFNULL(employee.slr_basic, 0) AS slr_basic,
                            IFNULL(
                                FORMAT(employee.slr_basic, 0),
                                0
                            ) AS slr_basic_show,
                            IFNULL(employee.slr_voucher, 0) AS slr_voucher,
                            IFNULL(
                                FORMAT(employee.slr_voucher, 0),
                                0
                            ) AS slr_voucher_show,
                            IFNULL(employee.slr_transport, 0) AS slr_transport,
                            IFNULL(
                                FORMAT(employee.slr_transport, 0),
                                0
                            ) AS slr_transport_show,
                            IFNULL(employee.slr_tunjangan, 0) AS slr_tunjangan,
                            IFNULL(
                                FORMAT(employee.slr_tunjangan, 0),
                                0
                            ) AS slr_tunjangan_show,
                            IFNULL(
                                employee.slr_tunjangan_makan,
                                0
                            ) AS slr_tunjangan_makan,
                            IFNULL(
                                FORMAT(
                                    employee.slr_tunjangan_makan,
                                    0
                                ),
                                0
                            ) AS slr_tunjangan_makan_show,
                            IFNULL(
                                SUM(
                                    cashbond_payroll_detail.cashbond
                                ),
                                0
                            ) AS slr_cashbond,
                            IFNULL(
                                FORMAT(
                                    SUM(
                                        cashbond_payroll_detail.cashbond
                                    ),
                                    0
                                ),
                                0
                            ) AS slr_cashbond_show,
                            IFNULL(employee.slr_thr, 0) AS slr_thr,
                            IFNULL(
                                FORMAT(employee.slr_thr, 0),
                                0
                            ) AS slr_thr_show,
                            employee.`status`,
                            IFNULL(employee.slr_pot_deposit, 0) AS slr_pot_deposit,
                            0 AS slr_pot_deposit_show,
                            0 AS slr_peng_deposit,
                            0 AS slr_peng_deposit_show,
                            0 AS additional_cost,
                            0 AS additional_cost_show,
                            0 AS boarding_house,
                            0 AS boarding_house_show,
                            "" AS add_cost_desc,
                            0 AS workdays
                        FROM
                            employee
                        LEFT JOIN employee_branch ON employee_branch.employee_id = employee.id
                        LEFT JOIN (
                            SELECT
                                cashbond_payroll_detail.cashbond,
                                cashbond_payroll_detail.employee_id,
                                cashbond_payroll.deleted_at,
                                payroll.id AS id_payroll
                            FROM
                                cashbond_payroll_detail
                            LEFT JOIN cashbond_payroll ON cashbond_payroll_detail.cashbond_payroll_id = cashbond_payroll.id
                            LEFT JOIN payroll ON MONTHNAME(cashbond_payroll.date) = payroll.`month`
                            AND YEAR (cashbond_payroll.date) = payroll.`year`
                            WHERE
                                payroll.id = '.$id.' AND cashbond_payroll_detail.deleted_at IS NULL AND cashbond_payroll.deleted_at IS NULL
                        ) AS cashbond_payroll_detail ON employee.id = cashbond_payroll_detail.employee_id
                        WHERE
                            employee_branch.branch_id =  '.Session::get('branch_id').'
                        AND employee.deleted_at IS NULL and employee.status = 0

                        GROUP BY
                            employee.id,
                            employee.emp_name,
                            employee.emp_full_name,
                            employee.emp_bod,
                            employee.emp_address,
                            employee.emp_ktp_number,
                            employee.emp_bank,
                            employee.emp_bank_rek,
                            employee.emp_email,
                            employee.emp_phone,
                            employee.emp_gender,
                            employee.emp_religion,
                            employee.emp_hiring_date,
                            employee.emp_position,
                            employee.slr_basic,
                            employee.slr_transport,
                            employee.slr_tunjangan,
                            employee.slr_tunjangan_makan,
                            employee.slr_thr,
                            employee.`status`';
      	$employees = DB::table(DB::raw("($sql_employee) as employee"))->get();

      	//dd($employees);

        $payroll_usr = DB::table('payroll')
        ->leftjoin('user', 'payroll.updated_by', '=', 'user.id')
        ->select('payroll.id',
					'payroll.updated_at',
					'user.username')
        ->where('payroll.id', '=',  $id)
        ->first();

        //dd($payroll);

		$payroll = Payroll::find($id);

		return view ('editor.payroll.form', compact('month_list', 'employees', 'payroll', 'payroll_usr'))->with('employees', $employees);
	}

	public function store($id, Request $request)
	{
		// $payroll = new Payroll;
		// $payroll->year = $request->input('year');
		// $payroll->month = $request->input('month');
		// $payroll->comment = $request->input('comment');
		// $payroll->branch_id = Session::get('branch_id');
		// $payroll->default_work_days = $request->input('default_work_days');
		// $payroll->created_by = Auth::id();
		// $payroll->updated_by = Auth::id();
		// $payroll->save();

		$payroll = Payroll::find($id);
		$payroll->comment = $request->input('comment');
		$payroll->default_work_days = $request->input('default_work_days');
		$payroll->updated_by = Auth::id();
		$payroll->save();

		foreach($request->input('payroll_detail') as $payroll_detail_key => $payroll_detail_data)
		{
			$payroll_detail = new PayrollDetail;
			$payroll_detail->payroll_id = $payroll->id;
			$payroll_detail->employee_id = $payroll_detail_key;
			$payroll_detail->workdays = $payroll_detail_data['workdays'];
			$payroll_detail->slr_basic = $payroll_detail_data['slr_basic'];
			 $payroll_detail->slr_voucher = $payroll_detail_data['slr_voucher'];
			$payroll_detail->slr_cashbond = $payroll_detail_data['slr_cashbond'];
			$payroll_detail->slr_pot_deposit = $payroll_detail_data['slr_pot_deposit'];
			$payroll_detail->additional_cost = $payroll_detail_data['additional_cost'];
			$payroll_detail->boarding_house = $payroll_detail_data['boarding_house'];
			$payroll_detail->add_cost_desc = $payroll_detail_data['add_cost_desc'];
			$payroll_detail->slr_thr = $payroll_detail_data['slr_thr'];
			$payroll_detail->slr_transport = $payroll_detail_data['slr_transport'];
			$payroll_detail->slr_tunjangan_makan = $payroll_detail_data['slr_tunjangan_makan'];
			$payroll_detail->slr_peng_deposit = $payroll_detail_data['slr_peng_deposit'];
			$payroll_detail->slr_total = ($payroll_detail_data['workdays']/$request->input('default_work_days')*($payroll_detail_data['slr_basic'] ))+ $payroll_detail_data['slr_transport']+ $payroll_detail_data['slr_tunjangan_makan'] + $payroll_detail_data['slr_thr'] + $payroll_detail_data['slr_peng_deposit'] +  $payroll_detail_data['slr_voucher'];

			$payroll_detail->slr_thp =  (($payroll_detail_data['workdays']/$request->input('default_work_days')*($payroll_detail_data['slr_basic'] ))+ $payroll_detail_data['slr_transport']+ $payroll_detail_data['slr_tunjangan_makan'] + $payroll_detail_data['slr_thr'] + $payroll_detail_data['slr_peng_deposit'] +  $payroll_detail_data['slr_voucher'])
			- ($payroll_detail_data['slr_cashbond']+$payroll_detail_data['slr_pot_deposit']+$payroll_detail_data['boarding_house']+$payroll_detail_data['additional_cost']);
			$payroll_detail->created_by = Auth::id();
			$payroll_detail->save();
		}

		return redirect()->action('Editor\PayrollController@detail', $id);
	}

	public function edit($id)
	{
		$month_list = [
			'January' => 'January',
			'February' => 'February',
			'March' => 'March',
			'April' => 'April',
			'May' => 'May',
			'June' => 'June',
			'July' => 'July',
			'August' => 'August',
			'September' => 'September',
			'October' => 'October',
			'November' => 'November',
			'December' => 'December',
		];

		$employees = DB::table('employee')
        ->join('payroll_detail', 'employee.id', '=', 'payroll_detail.employee_id')
        ->select('employee.id',
            'employee.emp_name',
            'employee.emp_full_name',
            'employee.emp_bod',
            'employee.emp_address',
            'employee.emp_ktp_number',
            'employee.emp_bank',
            'employee.emp_bank_rek',
            'employee.emp_email',
            'employee.emp_phone',
            'employee.emp_gender',
            'employee.emp_religion',
            'employee.emp_hiring_date',
            'employee.emp_position',
            'payroll_detail.workdays',
            'payroll_detail.slr_basic',
			DB::raw('FORMAT(payroll_detail.slr_basic,0) as slr_basic_show'),
			'payroll_detail.slr_voucher',
            DB::raw('FORMAT(payroll_detail.slr_basic,0) as slr_voucher_show'),
            'payroll_detail.slr_transport',
            DB::raw('FORMAT(payroll_detail.slr_transport,0) as slr_transport_show'),
            'payroll_detail.slr_tunjangan_makan',
            DB::raw('FORMAT(payroll_detail.slr_tunjangan_makan,0) as slr_tunjangan_makan_show'),
            'payroll_detail.additional_cost',
            DB::raw('FORMAT(payroll_detail.additional_cost,0) as additional_cost_show'),
            'payroll_detail.add_cost_desc',
            'payroll_detail.slr_cashbond',
            DB::raw('FORMAT(payroll_detail.slr_cashbond,0) as slr_cashbond_show'),
            'payroll_detail.boarding_house',
            DB::raw('FORMAT(payroll_detail.boarding_house,0) as boarding_house_show'),
            'payroll_detail.slr_thr',
            DB::raw('FORMAT(payroll_detail.slr_thr,0) as slr_thr_show'),
            'payroll_detail.slr_pot_deposit',
            DB::raw('FORMAT(payroll_detail.slr_pot_deposit,0) as slr_pot_deposit_show'),
            'payroll_detail.slr_peng_deposit',
            DB::raw('FORMAT(payroll_detail.slr_peng_deposit,0) as slr_peng_deposit_show'),
            'payroll_detail.slr_total',
            DB::raw('FORMAT(payroll_detail.slr_total,0) as slr_total_show'),
            'payroll_detail.slr_thp')
        ->where('payroll_detail.payroll_id', '=',  $id)
        ->whereNull('employee.deleted_at')
        ->whereNull('payroll_detail.deleted_by')
        ->whereNull('payroll_detail.deleted_at')
        ->get();

        //dd($employees);

        $payroll_usr = DB::table('payroll')
        ->leftjoin('user', 'payroll.updated_by', '=', 'user.id')
        ->select('payroll.id',
					'payroll.updated_at',
					'user.username')
        ->where('payroll.id', '=',  $id)
        ->first();

        //dd($payroll);

		$payroll = Payroll::find($id);

		return view ('editor.payroll.form', compact('month_list', 'employees', 'payroll', 'payroll_usr'));
	}

	public function update($id, Request $request)
	{

		//Payroll::where('id', $id)->delete();

		$payroll = Payroll::find($id);
		$payroll->comment = $request->input('comment');
		$payroll->default_work_days = $request->input('default_work_days');
		$payroll->updated_by = Auth::id();
		$payroll->save();

		//dd($payroll);

		// DB::beginTransaction();
  //       try {
		// foreach($payroll->payroll_detail as $detail)
		// {
		// 	$detail->deleted_by = Auth::id();
		// 	$detail->save();
		// 	$detail->delete();
		// }

		PayrollDetail::where('payroll_id', $id)->delete();

		foreach($request->input('payroll_detail') as $payroll_detail_key => $payroll_detail_data)
		{
			//$payroll_detail = PayrollDetail::FindOrFail($payroll_detail_key);

			$payroll_detail = new PayrollDetail;
			// $payroll_detail->payroll_id = $payroll->id;
			// $payroll_detail->employee_id = $payroll_detail_key;
			$payroll_detail->payroll_id = $payroll->id;
			$payroll_detail->employee_id = $payroll_detail_key;
			$payroll_detail->workdays = $payroll_detail_data['workdays'];
			$payroll_detail->slr_basic = $payroll_detail_data['slr_basic'];
			 $payroll_detail->slr_voucher = $payroll_detail_data['slr_voucher'];
			$payroll_detail->slr_cashbond = $payroll_detail_data['slr_cashbond'];
			$payroll_detail->slr_pot_deposit = $payroll_detail_data['slr_pot_deposit'];
			$payroll_detail->additional_cost = $payroll_detail_data['additional_cost'];
			$payroll_detail->boarding_house = $payroll_detail_data['boarding_house'];
			$payroll_detail->add_cost_desc = $payroll_detail_data['add_cost_desc'];
			$payroll_detail->slr_thr = $payroll_detail_data['slr_thr'];
			$payroll_detail->slr_transport = $payroll_detail_data['slr_transport'];
			$payroll_detail->slr_tunjangan_makan = $payroll_detail_data['slr_tunjangan_makan'];
			$payroll_detail->slr_peng_deposit = $payroll_detail_data['slr_peng_deposit'];
			$payroll_detail->slr_total = ($payroll_detail_data['workdays']/$request->input('default_work_days')*($payroll_detail_data['slr_basic'] ))+ $payroll_detail_data['slr_transport']+ $payroll_detail_data['slr_tunjangan_makan'] + $payroll_detail_data['slr_thr'] + $payroll_detail_data['slr_peng_deposit'] +  $payroll_detail_data['slr_voucher'];

            $payroll_detail->slr_thp =  (($payroll_detail_data['workdays']/$request->input('default_work_days')*($payroll_detail_data['slr_basic'] ))+ $payroll_detail_data['slr_transport']+ $payroll_detail_data['slr_tunjangan_makan'] + $payroll_detail_data['slr_thr'] + $payroll_detail_data['slr_peng_deposit'] +  $payroll_detail_data['slr_voucher'])
            - ($payroll_detail_data['slr_cashbond']+$payroll_detail_data['slr_pot_deposit']+$payroll_detail_data['boarding_house']+$payroll_detail_data['additional_cost']);
			$payroll_detail->updated_by = Auth::id();
			$payroll_detail->save();
		}

		return redirect()->action('Editor\PayrollController@detail', $payroll->id);

		 // } catch (\Exception $e) {
   //              DB::rollback();
   //      }
	}

	public function detail($id)
	{
		$payroll = Payroll::find($id);

		$payroll_usr = DB::table('payroll')
        ->leftjoin('user', 'payroll.updated_by', '=', 'user.id')
        ->select('payroll.id',
					'payroll.updated_at',
					'user.username')
        ->where('payroll.id', '=',  $id)
        ->first();
    $detail = PayrollDetail::where('payroll_id',$id)->orderBy('updated_at', 'desc')->get();
		//dd($payroll->payroll_detail);
		return view ('editor.payroll.detail', compact('payroll', 'payroll_usr','detail'));
	}

	// public function detail_create($id)
	// {
	// 	$payroll = Payroll::find($id);
	// 	$employee_list = Employee::pluck('emp_name', 'id');
	// 	return view ('editor.payroll.detail_form', compact('payroll', 'employee_list'));
	// }

	// public function detail_store($id, PayrollDetailRequest $request)
	// {
	// 	$payroll_detail = new PayrollDetail;
	// 	$payroll_detail->payroll_id = $id;
	// 	$payroll_detail->employee_id = $request->input('employee_id');
	// 	$payroll_detail->workdays = $request->input('workdays');
	// 	$payroll_detail->slr_basic = $request->input('slr_basic');
	// 	$payroll_detail->slr_transport = $request->input('slr_transport');
	// 	$payroll_detail->slr_tunjangan_makan = $request->input('slr_tunjangan_makan');
	// 	$payroll_detail->slr_cashbond = $request->input('slr_cashbond');
	// 	$payroll_detail->slr_thr = $request->input('slr_thr');
	// 	$payroll_detail->slr_pot_deposit = $request->input('slr_pot_deposit');
	// 	$payroll_detail->slr_total = $request->input('slr_total');
	// 	$payroll_detail->slr_thp = $request->input('slr_thp');
	// 	$payroll_detail->created_by = Auth::id();
	// 	$payroll_detail->save();

	// 	return redirect()->action('Editor\PayrollController@detail', $payroll_detail->payroll_id);
	// }

	// public function detail_edit($id)
	// {
	// 	$payroll_detail = PayrollDetail::find($id);
	// 	$payroll = Payroll::find($payroll_detail->payroll_id);
	// 	$employee_list = Employee::pluck('emp_name', 'id');
	// 	return view ('editor.payroll.detail_form', compact('payroll_detail', 'payroll', 'employee_list'));
	// }

	// public function detail_update($id, PayrollDetailRequest $request)
	// {
	// 	$payroll_detail = PayrollDetail::find($id);
	// 	$payroll_detail->employee_id = $request->input('employee_id');
	// 	$payroll_detail->workdays = $request->input('workdays');
	// 	$payroll_detail->slr_basic = $request->input('slr_basic');
	// 	$payroll_detail->slr_transport = $request->input('slr_transport');
	// 	$payroll_detail->slr_tunjangan_makan = $request->input('slr_tunjangan_makan');
	// 	$payroll_detail->slr_cashbond = $request->input('slr_cashbond');
	// 	$payroll_detail->slr_thr = $request->input('slr_thr');
	// 	$payroll_detail->slr_pot_deposit = $request->input('slr_pot_deposit');
	// 	$payroll_detail->slr_total = $request->input('slr_total');
	// 	$payroll_detail->slr_thp = $request->input('slr_thp');
	// 	$payroll_detail->updated_by = Auth::id();
	// 	$payroll_detail->save();

	// 	return redirect()->action('Editor\PayrollController@detail', $payroll_detail->payroll_id);
	// }

	// public function detail_delete($id)
	// {
	// 	$payroll_detail = PayrollDetail::find($id);
	// 	$payroll_detail->deleted_by = Auth::id();
	// 	$payroll_detail->save();
	// 	$payroll_detail->delete();

	// 	return redirect()->action('Editor\PayrollController@detail', $id);
	// }

	public function submit($id, Request $request)
	{
		$payroll = Payroll::find($id);
		$payroll->status = 1;
		$payroll->save();

		return redirect()->action('Editor\PayrollController@detail', $payroll->id);
	}

	public function finance_approve($id, Request $request)
	{
		$payroll = Payroll::find($id);
		if($request->input('review') == 0)
		{
			$payroll->status = 0;
		} elseif($request->input('review') == 1) {
			$payroll->status = 2;
			$payroll->approved_date = date('Y-m-d');
		}
		$payroll->save();

		return redirect()->action('Editor\PayrollController@detail', $payroll->id);
	}

	public function owner_approve($id, Request $request)
	{
		$payroll = Payroll::find($id);
		$payroll->status = 3;
		$payroll->save();

		return redirect()->action('Editor\PayrollController@detail', $payroll->id);
	}

	public function finance_payment($id, Request $request)
	{
		$payroll = Payroll::find($id);
		$payroll->status = 4;
		$payroll->paid_date = date('Y-m-d');
		$payroll->save();

		if($request->attachment_receipt)
        {
            $payroll = Payroll::FindOrFail($payroll->id);

            $original_directory = "uploads/payroll/attachment_receipt/";

            if(!File::exists($original_directory))
            {
                File::makeDirectory($original_directory, $mode = 0777, true, true);
            }

            // $file_extension = $request->image->getClientOriginalExtension();
            $payroll->attachment_receipt = Carbon::now()->format("d-m-Y h-i-s").$request->attachment_receipt->getClientOriginalName();
            $request->attachment_receipt->move($original_directory, $payroll->attachment_receipt);


            $payroll->save();
        }

		return redirect()->action('Editor\PayrollController@detail', $payroll->id);
	}

	public function delete($id)
	{
		$payroll = Payroll::find($id);
		$payroll->deleted_by = Auth::id();
		$payroll->save();

		foreach($payroll->payroll_detail as $detail)
		{
			$detail->deleted_by = Auth::id();
			$detail->save();
			$detail->delete();
		}

		$payroll->delete();

		return redirect()->action('Editor\PayrollController@index');
	}
}
