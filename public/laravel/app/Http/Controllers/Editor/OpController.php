<?php

namespace App\Http\Controllers\Editor;

use Auth;
use File;
use Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Op;
use Illuminate\Support\Facades\Storage;

class OpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $lists = Op::paginate(15);


       return view('editor.op.index', compact('lists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bulan = null;
        $tahun = null;
        return view('editor.op.form',compact('tahun','bulan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'start_date' => 'required',
            'end_date' => 'required',
            'franchise_amortization' => 'required',
            'asset_deprecation' => 'required',
            'rent_ruko' => 'required',


        ]);

        $op = new Op();
        $op->start_date = $request->start_date;
        $op->end_date = $request->end_date;
        $op->franchise_amortization = $request->franchise_amortization;
        $op->asset_deprecation = $request->asset_deprecation;
        $op->rent_ruko = $request->rent_ruko;


        $op->created_by = Auth::id();




        $op->save();

        return redirect()->route('editor.op.index');

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $op = Op::find($id);
        //return $op;
        return view('editor.Op.form', compact('op'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'start_date' => 'required',
            'end_date' => 'required',
            'franchise_amortization' => 'required',
            'asset_deprecation' => 'required',
            'rent_ruko' => 'required',


        ]);

        $op = Op::find($id);

        $op->start_date = $request->start_date;
        $op->end_date = $request->end_date;
        $op->franchise_amortization = $request->franchise_amortization;
        $op->asset_deprecation = $request->asset_deprecation;
        $op->rent_ruko = $request->rent_ruko;


        $op->created_by = Auth::id();


        $op->save();

        return redirect()->route('editor.op.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $main = Op::find($id);
        $main->deleted_by = Auth::id();
        $main->save();
        $main->delete();
        return back();
    }



}
