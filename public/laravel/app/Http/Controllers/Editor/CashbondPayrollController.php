<?php

namespace App\Http\Controllers\Editor;

use Auth;
use File;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\CashbondPayrollRequest;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Model\CashbondPayroll;
use App\Model\CashbondPayrollDetail;
use App\Model\Employee;

class CashbondPayrollController extends Controller
{
    public function index()
    {
    	if (Input::has('page'))
           {
             $page = Input::get('page');
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14;
        $cashbond_payrolls = CashbondPayroll::orderBy('created_at', 'DESC')
        ->where('branch_id', Session::get('branch_id'))
        ->paginate(15);

    	return view ('editor.cashbond_payroll.index', compact('cashbond_payrolls'))->with('number',$no);
    }

    public function create()
    {
    	//$employees = Employee::all();
        $employees = DB::table('employee')
        ->join('employee_branch', 'employee.id', '=', 'employee_branch.employee_id')
        ->select('employee.id',
            'employee.emp_name',
            'employee.emp_full_name',
            'employee.emp_bod',
            'employee.emp_address',
            'employee.emp_ktp_number',
            'employee.emp_bank',
            'employee.emp_bank_rek',
            'employee.emp_email',
            'employee.emp_phone',
            'employee.emp_gender',
            'employee.emp_religion',
            'employee.emp_hiring_date',
            'employee.emp_position',
            DB::raw('IFNULL(employee.slr_basic,0) as slr_basic'),
            DB::raw('IFNULL(employee.slr_transport,0) as slr_transport'),
            DB::raw('IFNULL(employee.slr_tunjangan,0) as slr_tunjangan'),
            DB::raw('IFNULL(employee.slr_tunjangan_makan,0) as slr_tunjangan_makan'),
            DB::raw('IFNULL(employee.slr_cashbond,0) as slr_cashbond'),
            DB::raw('FORMAT(IFNULL(employee.slr_cashbond,0),0) as slr_cashbond_show'),
            DB::raw('IFNULL(employee.slr_thr,0) as slr_thr'),
            'employee.status')
        ->where('employee_branch.branch_id', '=',  Session::get('branch_id'))
        ->where('status','=','0')
        ->whereNull('employee.deleted_at')
        ->get();
    	return view ('editor.cashbond_payroll.form', compact('employees'));
        // @if($detail_emp["emp_status"]==0 || $detail_emp["emp_status"]=='')
    }

    public function store(CashbondPayrollRequest $request)
    {
    	$cashbond_payroll = new CashbondPayroll;
    	$cashbond_payroll->date = date('Y-m-d', strtotime($request->input('date')));
    	$cashbond_payroll->status = 0;
        $cashbond_payroll->branch_id = Session::get('branch_id');
    	$cashbond_payroll->created_by = Auth::id();
    	$cashbond_payroll->save();

    	foreach($request->input('cashbond_payroll_detail') as $detail_key => $detail_value)
    	{
    		$cashbond_payroll_detail = new CashbondPayrollDetail;
    		$cashbond_payroll_detail->cashbond_payroll_id = $cashbond_payroll->id;
    		$cashbond_payroll_detail->employee_id = $detail_key;
            $cashbond_payroll_detail->cashbond = $detail_value['cashbond'];
            $cashbond_payroll_detail->miscellaneous = $detail_value['misc'];
            $cashbond_payroll_detail->remark = $detail_value['remark'];
    		$cashbond_payroll_detail->created_by = Auth::id();
    		$cashbond_payroll_detail->save();
    	}

    	return redirect()->action('Editor\CashbondPayrollController@detail', $cashbond_payroll->id);
    }

    public function edit($id)
    {
    	$cashbond_payroll = CashbondPayroll::find($id);
    	//$employees = Employee::all();
        $employees = DB::table('employee')
        ->join('employee_branch', 'employee.id', '=', 'employee_branch.employee_id')
        ->leftjoin('cashbond_payroll_detail', 'employee.id', '=', 'cashbond_payroll_detail.employee_id')
        ->select('employee.id',
            'employee.emp_name',
            'employee.emp_full_name',
            'employee.emp_bod',
            'employee.emp_address',
            'employee.emp_ktp_number',
            'employee.emp_bank',
            'employee.emp_bank_rek',
            'employee.emp_email',
            'employee.emp_phone',
            'employee.emp_gender',
            'employee.emp_religion',
            'employee.emp_hiring_date',
            'employee.emp_position',
            DB::raw('IFNULL(employee.slr_basic,0) as slr_basic'),
            DB::raw('IFNULL(employee.slr_transport,0) as slr_transport'),
            DB::raw('IFNULL(employee.slr_tunjangan,0) as slr_tunjangan'),
            DB::raw('IFNULL(employee.slr_tunjangan_makan,0) as slr_tunjangan_makan'),
            DB::raw('IFNULL(cashbond_payroll_detail.cashbond,0) as slr_cashbond'),
            DB::raw('FORMAT(IFNULL(cashbond_payroll_detail.cashbond,0),0) as slr_cashbond_show'),
            DB::raw('IFNULL(employee.slr_thr,0) as slr_thr'),
            'employee.status')
        ->where('employee_branch.branch_id', '=',  Session::get('branch_id'))
        ->where('cashbond_payroll_detail.cashbond_payroll_id', '=', $id)
        ->whereNull('employee.deleted_at')
        ->whereNull('cashbond_payroll_detail.deleted_at')
        ->get();

    	return view ('editor.cashbond_payroll.form', compact('cashbond_payroll', 'employees'));
    }

    public function update($id, CashbondPayrollRequest $request)
    {
    	$cashbond_payroll = CashbondPayroll::find($id);
    	$cashbond_payroll->date = date('Y-m-d', strtotime($request->input('date')));
    	$cashbond_payroll->updated_by = Auth::id();
        $cashbond_payroll->save();


    	CashbondPayrollDetail::where('cashbond_payroll_id', $cashbond_payroll->id)->delete();
    	foreach($request->input('cashbond_payroll_detail') as $detail_key => $detail_value)
    	{
    		$cashbond_payroll_detail = new CashbondPayrollDetail;
    		$cashbond_payroll_detail->cashbond_payroll_id = $cashbond_payroll->id;
    		$cashbond_payroll_detail->employee_id = $detail_key;
            $cashbond_payroll_detail->cashbond = $detail_value['cashbond'];
            $cashbond_payroll_detail->miscellaneous = $detail_value['misc'];
            $cashbond_payroll_detail->remark = $detail_value['remark'];
    		$cashbond_payroll_detail->save();
    	}

    	return redirect()->action('Editor\CashbondPayrollController@detail', $cashbond_payroll->id);
    }

    public function detail($id)
    {
    	$cashbond_payroll = CashbondPayroll::find($id);

    	return view ('editor.cashbond_payroll.detail', compact('cashbond_payroll'));
    }

    public function submit($id, Request $request)
	{
		$cashbond_payroll = CashbondPayroll::find($id);
		$cashbond_payroll->status = 1;
		$cashbond_payroll->save();

		return redirect()->action('Editor\CashbondPayrollController@detail', $cashbond_payroll->id);
	}

	public function finance_approve($id, Request $request)
	{
		$cashbond_payroll = CashbondPayroll::find($id);
		if($request->input('review') == 0)
		{
			$cashbond_payroll->status = 0;
		} elseif($request->input('review') == 1) {
			$cashbond_payroll->status = 2;
		}
		$cashbond_payroll->save();

		return redirect()->action('Editor\CashbondPayrollController@detail', $cashbond_payroll->id);
	}

	public function owner_approve($id, Request $request)
	{
		$cashbond_payroll = CashbondPayroll::find($id);
		$cashbond_payroll->status = 3;
		$cashbond_payroll->save();

		return redirect()->action('Editor\CashbondPayrollController@detail', $cashbond_payroll->id);
	}

    public function finance_payment($id, Request $request)
    {
        $cashbond_payroll = CashbondPayroll::find($id);
        $cashbond_payroll->status = 4;
        $cashbond_payroll->save();

        if($request->attachment_receipt)
        {
            $cashbond_payroll = CashbondPayroll::FindOrFail($cashbond_payroll->id);

            $original_directory = "uploads/cashbondpayroll/attachment_receipt/";

            if(!File::exists($original_directory))
            {
                File::makeDirectory($original_directory, $mode = 0777, true, true);
            }

            // $file_extension = $request->image->getClientOriginalExtension();
            $cashbond_payroll->attachment_receipt = Carbon::now()->format("d-m-Y h-i-s").$request->attachment_receipt->getClientOriginalName();
            $request->attachment_receipt->move($original_directory, $cashbond_payroll->attachment_receipt);


            $cashbond_payroll->save();
        }

        return redirect()->action('Editor\CashbondPayrollController@detail', $cashbond_payroll->id);
    }

    public function delete($id)
    {
    	$cashbond_payroll = CashbondPayroll::find($id);
    	$cashbond_payroll->deleted_by = Auth::id();
    	$cashbond_payroll->save();
        $cashbond_payroll->delete();

        return redirect()->action('Editor\CashbondPayrollController@index');


    }


}
