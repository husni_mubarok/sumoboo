<?php

namespace App\Http\Controllers\Editor;

use Auth;
use File;
use Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use App\Model\TaxPPN;
use App\Http\Requests\TaxPPNRequest;

class TaxPPNController extends Controller
{
    public function index()
    {
    	if (Input::has('page'))
           {
             $page = Input::get('page');
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14;
        $franchise_fees = TaxPPN::orderBy('created_at', 'DESC')
        ->where('branch_id', Session::get('branch_id'))
        ->paginate(15);
    	return view ('editor.tax_ppn.index', compact('franchise_fees'))->with('number',$no);
    }

    public function create()
    {
    	$month_list = [
			'1' => 'January',
			'2' => 'February',
			'3' => 'March',
			'4' => 'April',
			'5' => 'May',
			'6' => 'June',
			'7' => 'July',
			'8' => 'August',
			'9' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December',
		];
    	return view ('editor.tax_ppn.form', compact('month_list'));
    }

    public function store(TaxPPNRequest $request)
    {
    	$franchise_fee = new TaxPPN;
    	$franchise_fee->start_date = $request->input('start_date');
    	$franchise_fee->end_date = $request->input('end_date');
    	$franchise_fee->status = 0;
    	$franchise_fee->royalty_percentage = $request->input('royalty_percentage');
    	$franchise_fee->royalty_value = $request->input('royalty_value');

    	//Store Image and create Thumbnail
    	if($request->file('invoice_file'))
    	{
			$file_dir = "uploads/tax_ppn/";
			if(!File::exists($file_dir))
		    {
		        File::makeDirectory($file_dir, $mode = 0777, true, true);
		    }
		    $file_extension = $request->file('invoice_file')->getClientOriginalExtension();
		    $franchise_fee->invoice_file = date('h-i-s').$request->file('invoice_file')->getClientOriginalName();
		    $request->file('invoice_file')->move($file_dir, $franchise_fee->invoice_file);

		    $thumbnail_dir = $file_dir."thumbnail/";
		    if(!File::exists($thumbnail_dir))
		    {
		    	File::makeDirectory($thumbnail_dir, $mode = 0777, true, true);
		    }
		    $thumbnail = Image::make($file_dir.$franchise_fee->invoice_file);
		    $thumbnail->fit(200, 200)->save($thumbnail_dir.$franchise_fee->invoice_file);
    	}

        $franchise_fee->branch_id = Session::get('branch_id');
    	$franchise_fee->created_by = Auth::id();
    	$franchise_fee->save();

    	return redirect()->action('Editor\TaxPPNController@detail', $franchise_fee->id);
    }

    public function detail($id)
    {
        $franchise_fee = TaxPPN::find($id);

        return view ('editor.tax_ppn.detail', compact('franchise_fee'));
    }

    public function submit($id, Request $request)
    {
    	$franchise_fee = TaxPPN::find($id);
    	$franchise_fee->status = 1;
    	$franchise_fee->save();

    	return redirect()->action('Editor\TaxPPNController@index');
    }

    public function finance_approve($id, Request $request)
    {
    	$franchise_fee = TaxPPN::find($id);
    	if($request->input('review') == 0)
    	{
    		$franchise_fee->status = 0;
    	} elseif($request->input('review') == 1) {
    		$franchise_fee->status = 2;
    	}
    	$franchise_fee->save();

    	return redirect()->action('Editor\TaxPPNController@index');
    }

    public function owner_approve($id, Request $request)
    {
    	$franchise_fee = TaxPPN::find($id);
		$franchise_fee->status = 3;
    	$franchise_fee->save();

    	return redirect()->action('Editor\TaxPPNController@index');
    }

     public function paid($id)
    {
        $franchise_fee = TaxPPN::find($id);
        return view ('editor.tax_ppn.paid', compact('franchise_fee'));
    }
    public function finance_payment($id, Request $request)
    {
    	$franchise_fee = TaxPPN::find($id);
		$franchise_fee->status = 4;
    	$franchise_fee->save();

         if($request->attachment_receipt)
        {
            $franchise_fee = TaxPPN::FindOrFail($franchise_fee->id);

            $original_directory = "uploads/tax_ppn/attachment_receipt/";

            if(!File::exists($original_directory))
            {
                File::makeDirectory($original_directory, $mode = 0777, true, true);
            }

            // $file_extension = $request->image->getClientOriginalExtension();
            $franchise_fee->attachment_receipt = Carbon::now()->format("d-m-Y h-i-s").$request->attachment_receipt->getClientOriginalName();
            $request->attachment_receipt->move($original_directory, $franchise_fee->attachment_receipt);

            // $thumbnail_directory = $original_directory."thumbnail/";
            // if(!File::exists($thumbnail_directory))
            // {
            //  File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
            // }
            // $thumbnail = Image::make($original_directory.$invoice->invoice_attachment);
            // $thumbnail->fit(300,300)->save($thumbnail_directory.$invoice->invoice_attachment);

            $franchise_fee->save();
        }


    	return redirect()->action('Editor\TaxPPNController@index');
    }

    public function edit($id)
    {
    	$franchise_fee = TaxPPN::find($id);
    	$month_list = [
			'1' => 'January',
			'2' => 'February',
			'3' => 'March',
			'4' => 'April',
			'5' => 'May',
			'6' => 'June',
			'7' => 'July',
			'8' => 'August',
			'9' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December',
		];
    	return view ('editor.tax_ppn.form', compact('franchise_fee', 'month_list'));
    }

    public function update($id, TaxPPNRequest $request)
    {
    	$franchise_fee = TaxPPN::find($id);
    	$franchise_fee->start_date = $request->input('start_date');
    	$franchise_fee->end_date = $request->input('end_date');
    	$franchise_fee->royalty_percentage = $request->input('royalty_percentage');
    	$franchise_fee->royalty_value = $request->input('royalty_value');

    	//Store Image and create Thumbnail
    	if($request->file('invoice_file'))
    	{
			$file_dir = "uploads/tax_ppn/";
			if(!File::exists($file_dir))
		    {
		        File::makeDirectory($file_dir, $mode = 0777, true, true);
		    }
		    $file_extension = $request->file('invoice_file')->getClientOriginalExtension();
		    $franchise_fee->invoice_file = date('h-i-s').$request->file('invoice_file')->getClientOriginalName();
		    $request->file('invoice_file')->move($file_dir, $franchise_fee->invoice_file);

		    $thumbnail_dir = $file_dir."thumbnail/";
		    if(!File::exists($thumbnail_dir))
		    {
		    	File::makeDirectory($thumbnail_dir, $mode = 0777, true, true);
		    }
		    $thumbnail = Image::make($file_dir.$franchise_fee->invoice_file);
		    $thumbnail->fit(200, 200)->save($thumbnail_dir.$franchise_fee->invoice_file);
    	}

    	$franchise_fee->updated_by = Auth::id();
    	$franchise_fee->save();

    	return redirect()->action('Editor\TaxPPNController@detail', $franchise_fee->id);
    }

    public function delete($id)
    {
    	$franchise_fee = TaxPPN::find($id);
    	$franchise_fee->deleted_by = Auth::id();
    	$franchise_fee->save();
    	$franchise_fee->delete();

    	return redirect()->action('Editor\TaxPPNController@index');
    }
}
