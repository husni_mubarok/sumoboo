<?php

namespace App\Http\Controllers\Editor;

use Auth;
use File;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\InvoiceRequest;
use App\Http\Controllers\Controller;
use App\Model\Invoice;
use App\Model\Vendor;
use App\Model\InvoiceType;
use App\Model\Branch;
use Intervention\Image\Facades\Image;
//use Request;

class InvoiceController extends Controller
{

	public function index(Request $request)
	{
		$invoice_type = $request->type;
		$page = 1;
		if (Input::has('page'))
        {
             $page = Input::get('page');
        }

        $no = 15*$page-14;
		$invoices = DB::table('invoice')
		->leftjoin('invoice_type', 'invoice.invoice_type_id', '=', 'invoice_type.id')
		->leftjoin('vendor', 'invoice.vendor_id', '=', 'vendor.id')
		->leftjoin('branch', 'invoice.branch_id', '=', 'branch.id')
		->select('invoice.id',
			'invoice_type.inv_type_name',
			'invoice.invoice_date',
			'invoice.invoice_period',
			'vendor.vendor_name',
			'invoice.invoice_bank',
			'invoice.invoice_rekening',
			'invoice.approved',
			'invoice.paid',
			'invoice.month',
			'invoice.year',
			'invoice.reference_no',
			'invoice.invoice_attachment',
			'branch.branch_name',
			'invoice.invoice_total',
			'invoice.additional_cost',
			'invoice.add_cost_desc')
		->whereNull('invoice.deleted_at')
		->where('invoice.branch_id', Session::get('branch_id'))
		->where('invoice.paid', null)
		->orderBy('invoice.created_at', 'DESC');

		if ($invoice_type) {
			$invoices = $invoices->where('invoice.invoice_type_id',$invoice_type);
		}



		$invoices = $invoices->paginate(15);

		$invoice_type_list = InvoiceType::where('item_category_id',4)->get(['id','inv_type_name']);

		return view ('editor.invoice.index', compact('invoices','invoice_type_list','invoice_type'))->with('number',$no);
	}

	public function indexbank(Request $request)
	{
		// $invoices = Invoice::all();
		$invoice_type = $request->type;
		$page = 1;
		if (Input::has('page'))
        {
             $page = Input::get('page');
		}

		$no = 15 * $page - 14;

		$invoices = DB::table('invoice')
		->leftjoin('invoice_type', 'invoice.invoice_type_id', '=', 'invoice_type.id')
		->leftjoin('vendor', 'invoice.vendor_id', '=', 'vendor.id')
		->leftjoin('branch', 'invoice.branch_id', '=', 'branch.id')
		->select('invoice.id',
			'invoice_type.inv_type_name',
			'invoice.invoice_date',
			'invoice.invoice_period',
			'vendor.vendor_name',
			'invoice.invoice_bank',
			'invoice.invoice_rekening',
			'invoice.approved',
			'invoice.paid',
			'invoice.month',
			'invoice.year',
			'invoice.reference_no',
			'invoice.paid_date',
			'invoice.invoice_attachment',
			'invoice.attachment_receipt',
			'branch.branch_name',
			'invoice.invoice_total',
			'invoice.additional_cost',
			'invoice.add_cost_desc')
		->whereNull('invoice.deleted_at')
		->where('invoice.branch_id', Session::get('branch_id'))
		->where('invoice.paid', 1)
		->orderBy('invoice.created_at', 'DESC');

		if ($invoice_type) {
			$invoices = $invoices->where('invoice.invoice_type_id',$invoice_type);
		}



		$invoices = $invoices->paginate(15);

		$invoice_type_list = InvoiceType::where('item_category_id',4)->get(['id','inv_type_name']);


		return view ('editor.invoice.bank', compact('invoices','invoice_type_list','invoice_type'))->with('number',$no);
	}

	public function create()
	{
		$vendor_list = Vendor::all()->pluck('vendor_name', 'id');
		$invoice_type_list = InvoiceType::where('item_category_id', 4)->pluck('inv_type_name', 'id');

		$month_list = [
			'1' => 'January',
			'2' => 'February',
			'3' => 'March',
			'4' => 'April',
			'5' => 'May',
			'6' => 'June',
			'7' => 'July',
			'8' => 'August',
			'9' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December',
		];

		$year = '2017';

		return view ('editor.invoice.form', compact('invoice', 'vendor_list', 'invoice_type_list', 'month_list', 'year'));
	}

	public function store(InvoiceRequest $request)
	{
		// DB::beginTransaction();
		// try {
            //get last transaction id

		$invoice = new Invoice;
		$invoice->invoice_type_id = $request->input('invoice_type_id');
		$invoice->invoice_date = $request->input('invoice_date');
		$invoice->month = $request->input('month');
		$invoice->year = $request->input('year');
		$invoice->vendor_id = $request->input('vendor_id');
		$invoice->invoice_bank = $request->input('invoice_bank');
		$invoice->invoice_rekening = $request->input('invoice_rekening');
		$invoice->invoice_total = $request->input('invoice_total');
		$invoice->additional_cost = $request->input('additional_cost');
		$invoice->add_cost_desc = $request->input('add_cost_desc');
		$invoice->comment = $request->input('comment');
		$invoice->branch_id = Session::get('branch_id');
		$invoice->created_by = Auth::id();
		$invoice->save();

		if($request->image)
		{
			$invoice = Invoice::FindOrFail($invoice->id);

			$original_directory = "uploads/invoice/";

			if(!File::exists($original_directory))
			{
				File::makeDirectory($original_directory, $mode = 0777, true, true);
			}

			//$file_extension = $request->image->getClientOriginalExtension();
			$invoice->invoice_attachment = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
			$request->image->move($original_directory, $invoice->invoice_attachment);

			// $thumbnail_directory = $original_directory."thumbnail/";
			// if(!File::exists($thumbnail_directory))
			// {
			// 	File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
			// }
			// $thumbnail = Image::make($original_directory.$invoice->invoice_attachment);
			// $thumbnail->fit(300,300)->save($thumbnail_directory.$invoice->invoice_attachment);

			$invoice->save();
		}

		return redirect('editor/invoice');
		// } catch (\Exception $e) {
		// 	DB::rollback();
		// }

	}

	public function edit($id)
	{
		$vendor_list = Vendor::all()->pluck('vendor_name', 'id');
		$invoice_type_list = InvoiceType::where('item_category_id', 4)->pluck('inv_type_name', 'id');
		$invoice = Invoice::Find($id);

		$month_list = [
			'1' => 'January',
			'2' => 'February',
			'3' => 'March',
			'4' => 'April',
			'5' => 'May',
			'6' => 'June',
			'7' => 'July',
			'8' => 'August',
			'9' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December',
		];


		return view ('editor.invoice.form', compact('invoice', 'vendor_list', 'invoice_type_list', 'month_list'));
	}

	public function view($id)
	{

		$month_list = [
			'1' => 'January',
			'2' => 'February',
			'3' => 'March',
			'4' => 'April',
			'5' => 'May',
			'6' => 'June',
			'7' => 'July',
			'8' => 'August',
			'9' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December',
		];

		$vendor_list = Vendor::all()->pluck('vendor_name', 'id');
		$invoice_type_list = InvoiceType::where('item_category_id', 4)->pluck('inv_type_name', 'id');
		$invoice = Invoice::Find($id);

		return view ('editor.invoice.view', compact('invoice', 'vendor_list', 'invoice_type_list', 'month_list'));
	}

	public function update($id, Request $request)
	{
		// DB::beginTransaction();
		// try {

		//Invoice::where('id', $id)->delete();

		$invoice = Invoice::Find($id);
		$invoice->invoice_type_id = $request->input('invoice_type_id');
		$invoice->invoice_date = $request->input('invoice_date');
		$invoice->invoice_period = $request->input('invoice_period');
		$invoice->vendor_id = $request->input('vendor_id');
		$invoice->invoice_bank = $request->input('invoice_bank');
		$invoice->invoice_rekening = $request->input('invoice_rekening');
		$invoice->additional_cost = $request->input('additional_cost');
		$invoice->add_cost_desc = $request->input('add_cost_desc');
		$invoice->invoice_total = $request->input('invoice_total');
		$invoice->comment = $request->input('comment');
		$invoice->save();

		if($request->image)
		{
			$invoice = Invoice::FindOrFail($invoice->id);

			$original_directory = "uploads/invoice/";

			if(!File::exists($original_directory))
			{
				File::makeDirectory($original_directory, $mode = 0777, true, true);
			}

			// $file_extension = $request->image->getClientOriginalExtension();
			$invoice->invoice_attachment = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
			$request->image->move($original_directory, $invoice->invoice_attachment);

			// $thumbnail_directory = $original_directory."thumbnail/";
			// if(!File::exists($thumbnail_directory))
			// {
			// 	File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
			// }
			// $thumbnail = Image::make($original_directory.$invoice->invoice_attachment);
			// $thumbnail->fit(300,300)->save($thumbnail_directory.$invoice->invoice_attachment);

			$invoice->save();
		}

		// 	DB::commit();

		return redirect('editor/invoice');
		// } catch (\Exception $e) {
		// 	DB::rollback();
		// }
	}

	public function updaterequset($id)
	{
		$now = Carbon::now();
		$invoice = Invoice::Find($id);
		$invoice->approved = 1;
		$invoice->approved_date = $now;
		$invoice->save();

		return redirect()->action('Editor\InvoiceController@index');
	}

	public function paid($id)
	{

		$month_list = [
			'1' => 'January',
			'2' => 'February',
			'3' => 'March',
			'4' => 'April',
			'5' => 'May',
			'6' => 'June',
			'7' => 'July',
			'8' => 'August',
			'9' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December',
		];

		$vendor_list = Vendor::all()->pluck('vendor_name', 'id');
		$invoice_type_list = InvoiceType::where('item_category_id', 4)->pluck('inv_type_name', 'id');
		$invoice = Invoice::Find($id);

		return view ('editor.invoice.paid', compact('invoice', 'vendor_list', 'invoice_type_list', 'month_list'));
	}

	public function updatepaid(Request $request, $id)
	{
		$now = Carbon::now();
		$invoice = Invoice::Find($id);
		$invoice->paid = 1;
		$invoice->paid_date = $now;
		$invoice->paid_date = $request->input('paid_date');
		$invoice->reference_no =  $request->input('reference_no');
		$invoice->save();

		if($request->attachment_receipt)
		{
			$invoice = Invoice::FindOrFail($invoice->id);

			$original_directory = "uploads/invoice/attachment_receipt/";

			if(!File::exists($original_directory))
			{
				File::makeDirectory($original_directory, $mode = 0777, true, true);
			}

			// $file_extension = $request->image->getClientOriginalExtension();
			$invoice->attachment_receipt = Carbon::now()->format("d-m-Y h-i-s").$request->attachment_receipt->getClientOriginalName();
			$request->attachment_receipt->move($original_directory, $invoice->attachment_receipt);

			// $thumbnail_directory = $original_directory."thumbnail/";
			// if(!File::exists($thumbnail_directory))
			// {
			// 	File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
			// }
			// $thumbnail = Image::make($original_directory.$invoice->invoice_attachment);
			// $thumbnail->fit(300,300)->save($thumbnail_directory.$invoice->invoice_attachment);

			$invoice->save();
		}

		return redirect()->action('Editor\InvoiceController@index');
	}

	public function delete($id)
	{
		Invoice::Find($id)->delete();
		return redirect()->action('Editor\InvoiceController@index');
	}

	public function report_uti(Request $request)
	{

		$start_date = null;
		$end_date = null;
		$vendor_id = null;
		$vendor = DB::select('select * from vendor where invoice_type_id !=3');

		if ($request->isMethod('post') && $request->start_date && $request->end_date) {

			$start_date = $request->start_date;
			$end_date = $request->end_date;
			$vendor_id = $request->vendor_id;

				$reports = DB::select("SELECT MONTH(invoice.invoice_date) as month,MONTHNAME(invoice.invoice_date) as monthname, sum(ifnull(additional_cost,0) + ifnull(invoice_total,0)) as grand_total
				FROM `invoice`
				LEFT JOIN `invoice_type` ON `invoice`.`invoice_type_id` = `invoice_type`.`id`
				LEFT JOIN `vendor` ON `invoice`.`vendor_id` = `vendor`.`id`
				LEFT JOIN `branch` ON `invoice`.`branch_id` = `branch`.`id`
				WHERE `invoice`.`deleted_at` IS NULL AND `invoice`.`branch_id` = '".Session::get('branch_id')."' AND `invoice`.`paid` =1

				and date(invoice.invoice_date) between date('".$start_date."') and date('".$end_date."')
				and vendor.id = '".$vendor_id."'
				group by MONTH(invoice.invoice_date)");






			$graph = "";
			foreach ($reports as $item) {
				$month = $item->monthname;


				$graph .= "['".$month."',".$item->grand_total."],";
			}

			$graph = substr($graph, 0, -1);
		}



		return view('editor.invoice.report_uti',compact('start_date','end_date','type','reports','graph','vendor','vendor_id'));

	}

	public function report_cons(Request $request)
	{
		$vendor_name = DB::select('select * from vendor where deleted_at is null');
		$vendor_item =  DB::table('vendor_item')
        ->join('item', 'vendor_item.item_id', '=', 'item.id' )
        ->join('vendor', 'vendor_item.vendor_id', '=', 'vendor.id')
        ->select('item.item_name',
            'item.item_desc','vendor.vendor_name',
            'item.id')
        ->whereNull('vendor_item.deleted_at')
        ->orderBy('vendor.vendor_name')
        ->get();

        $vendor_item =  $vendor_item->groupBy('vendor_name');
        $item_id = null;
		$start_date = null;
		$end_date = null;


		if ($request->isMethod('post') && $request->start_date && $request->end_date) {

			$start_date = $request->start_date;
			$end_date = $request->end_date;
			$item_id = $request->item;

				  $reports = DB::select("SELECT month(created_at) bulan,sum(quantity) as jml
							FROM invoice_direct_detail
							WHERE deleted_at IS NULL AND item_id='1'
							AND DATE(created_at) BETWEEN DATE('".$start_date."') AND DATE('".$end_date."')
							group by month(created_at)

							");






			$graph = "";
			foreach ($reports as $item) {


				$graph .= "['".$item->bulan."',".$item->jml."],";
			}

			$graph = substr($graph, 0, -1);

					//echo $graph;
		}



		return view('editor.invoice.report_cons',compact('start_date','end_date','vendor_name','vendor_item','item_id','reports','graph'));

	}
}
