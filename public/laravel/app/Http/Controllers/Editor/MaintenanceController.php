<?php

namespace App\Http\Controllers\Editor;

use Auth;
use File;
use Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Maintenance;
use Illuminate\Support\Facades\Storage;

class MaintenanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Maintenance::paginate(15);
       return view('editor.maintenance.index', compact('lists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('editor.maintenance.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'service_name' => 'required',
            'harga' => 'required',
            'date' => 'required',
            'attch_foto' => 'required|image',
            'attch_harga' => 'required|image',
            'alasan' => 'required',
            
        ]);

        $main_list = new Maintenance();
        $main_list->service_name = $request->service_name;
        $main_list->harga = $request->harga;
        $main_list->date = $request->date;
       
        $main_list->alasan = $request->alasan;
        $main_list->created_by = Auth::id();

        //Store Image and create Thumbnail
    	if($request->file('attch_foto') && $request->file('attch_harga') )
    	{
			$file_dir = "uploads/maintenance/";
			if(!File::exists($file_dir))
		    {
		        File::makeDirectory($file_dir, $mode = 0777, true, true);
            }
            

            $file_ext_attach_foto = $request->file('attch_foto')->getClientOriginalExtension();
            $file_ext_attach_harga = $request->file('attch_harga')->getClientOriginalExtension();


            $main_list->attch_foto = date('h-i-s').$request->file('attch_foto')->getClientOriginalName();
            $main_list->attch_harga = date('h-i-s').$request->file('attch_harga')->getClientOriginalName();


            $request->file('attch_foto')->move($file_dir, $main_list->attch_foto);
            $request->file('attch_harga')->move($file_dir, $main_list->attch_harga);

		    $thumbnail_dir = $file_dir."thumbnail/";
		    if(!File::exists($thumbnail_dir))
		    {
		    	File::makeDirectory($thumbnail_dir, $mode = 0777, true, true);
		    }
		    $thumbnail1 = Image::make($file_dir.$main_list->attch_foto);
            $thumbnail1->fit(200, 200)->save($thumbnail_dir.$main_list->attch_foto);
            
            $thumbnail2 = Image::make($file_dir.$main_list->attch_harga);
		    $thumbnail2->fit(200, 200)->save($thumbnail_dir.$main_list->attch_harga);
        }
        
        
        $main_list->save();

        return redirect()->route('editor.maintenance.index');
    
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $main_list = Maintenance::find($id);
        return view('editor.maintenance.form', compact('main_list'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'service_name' => 'required',
            'harga' => 'required',
            'date' => 'required',

            'alasan' => 'required',
            
        ]);
        
        $main_list = Maintenance::find($id);

        $main_list->service_name = $request->service_name;
        $main_list->harga = $request->harga;
        $main_list->date = $request->date;
       
        $main_list->alasan = $request->alasan;
        $main_list->updated_by = Auth::id();

       




        //Store Image and create Thumbnail
    	if($request->file('attch_foto') && $request->file('attch_harga') )
    	{
             //delete file
            $file_foto1 = "uploads/maintenance/".$main_list->attch_foto;
            $file_foto2 = "uploads/maintenance/".$main_list->attch_harga;

            $thumbnail_foto1 = "uploads/maintenance/thumbnail/".$main_list->attch_foto;
            $thumbnail_foto2 = "uploads/maintenance/thumbnail/".$main_list->attch_harga;

            File::delete($file_foto1);
            File::delete($file_foto2);
            File::delete($thumbnail_foto1);
            File::delete($thumbnail_foto2);



			$file_dir = "uploads/maintenance/";
			if(!File::exists($file_dir))
		    {
		        File::makeDirectory($file_dir, $mode = 0777, true, true);
            }
            

            $file_ext_attach_foto = $request->file('attch_foto')->getClientOriginalExtension();
            $file_ext_attach_harga = $request->file('attch_harga')->getClientOriginalExtension();


            $main_list->attch_foto = date('h-i-s').$request->file('attch_foto')->getClientOriginalName();
            $main_list->attch_harga = date('h-i-s').$request->file('attch_harga')->getClientOriginalName();


            $request->file('attch_foto')->move($file_dir, $main_list->attch_foto);
            $request->file('attch_harga')->move($file_dir, $main_list->attch_harga);

		    $thumbnail_dir = $file_dir."thumbnail/";
		    if(!File::exists($thumbnail_dir))
		    {
		    	File::makeDirectory($thumbnail_dir, $mode = 0777, true, true);
		    }
		    $thumbnail1 = Image::make($file_dir.$main_list->attch_foto);
            $thumbnail1->fit(200, 200)->save($thumbnail_dir.$main_list->attch_foto);
            
            $thumbnail2 = Image::make($file_dir.$main_list->attch_harga);
		    $thumbnail2->fit(200, 200)->save($thumbnail_dir.$main_list->attch_harga);
        }
        
        
        $main_list->save();

        return redirect()->route('editor.maintenance.index');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $main = Maintenance::find($id);

        $file_foto1 = "uploads/maintenance/".$main->attch_foto;
        $file_foto2 = "uploads/maintenance/".$main->attch_harga;

        $thumbnail_foto1 = "uploads/maintenance/thumbnail/".$main->attch_foto;
        $thumbnail_foto2 = "uploads/maintenance/thumbnail/".$main->attch_harga;

        $main->deleted_by = Auth::id();
        $main->save();
  
        File::delete($file_foto1);
        File::delete($file_foto2);
        File::delete($thumbnail_foto1);
        File::delete($thumbnail_foto2);

        

        $main->delete();
        return back();
    }


    
}
