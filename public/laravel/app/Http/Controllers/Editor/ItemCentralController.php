<?php

namespace App\Http\Controllers\Editor;

use Auth;
use File;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\ItemCentralRequest;
use App\Http\Requests\ItemCentralImageRequest;
use App\Http\Requests\ItemCentralImportRequest;
use App\Http\Controllers\Controller;
use App\Model\ItemCentral;
use App\Model\Item;
use App\Model\ItemCentralDetail;
use App\Model\ItemType;
use App\Model\ItemCentralImport;
use Intervention\Image\Facades\Image;
use Excel;


class ItemCentralController extends Controller
{
	public function index()
	{
		if (Input::has('page'))
		{
			$page = Input::get('page');
		}
		else
		{
			$page = 1;
		}
		$no = 15*$page-14;
		$item_centrals = ItemCentral::where('status_code', '<>', 'pay')
		->where('branch_id', Session::get('branch_id'))
		->orderBy('created_at', 'DESC')
		->paginate(15);

		//dd($item_centrals);

		return view ('editor.item_central.index', compact('item_centrals'))->with('number',$no);
	}

	public function indexbank()
	{
		if (Input::has('page'))
		{
			$page = Input::get('page');
		}
		else
		{
			$page = 1;
		}
		$no = 15*$page-14;
		$item_centrals = ItemCentral::where('status_code', 'pay')
		->where('branch_id', Session::get('branch_id'))
		->orderBy('created_at', 'DESC')
		->paginate(15);

		return view ('editor.item_central_bank.index', compact('item_centrals'))->with('number',$no);
	}

	public function truncateimport()
	{
		ItemCentralImport::query()->truncate();
		return redirect('editor/item_central/create');
	}

	public function create()
	{
		$now = Carbon::now()->format('Y-m-d');

		$sql = 'SELECT
		item.id,
		item.item_name,
		item.item_desc,
		item.item_category_id,
		item.price,
		FORMAT(item.price,0) AS price_show,
		item.uom,
		item_central_import.quantity,
		item_central_import.stock AS quantity_stock ,
		FORMAT(item.price * item_central_import.quantity,0) AS total
		FROM
		item_central_import
		RIGHT JOIN item ON item_central_import.item_name = item.item_name
		WHERE
		item.item_category_id = 1';
		$item_central_detail = DB::table(DB::raw("($sql) as rssumdetails"))->get();

		$sql1 = 'SELECT
		item.id,
		item.item_name,
		item.item_desc,
		item.item_category_id,
		item.price,
		FORMAT(item.price,0) AS price_show,
		item.uom,
		item_central_import.quantity,
		item_central_import.stock AS quantity_stock,
		FORMAT(item.price * item_central_import.quantity,0) AS total
		FROM
		item_central_import
		RIGHT JOIN item ON item_central_import.item_name = item.item_name
		WHERE
		item.item_category_id = 2';
		$item_central_detail_supply = DB::table(DB::raw("($sql1) as rssumdetails"))->get();

		return view ('editor.item_central.form', compact('item_central', 'item_central_detail', 'item_central_detail_supply', 'now'));
	}

	public function store(ItemCentralRequest $request)
	{
		// DB::beginTransaction();
		// try {
            //get last transaction id
		ItemCentralImport::query()->truncate();

		$item_central = new ItemCentral;
		$item_central->date = $request->input('date');
		$item_central->comment = $request->input('comment');
		$item_central->status_code = 'Waiting for Delivery';
		$item_central->branch_id = Session::get('branch_id');
		$item_central->created_by = Auth::id();
		$item_central->save();

		foreach($request->input('detail') as $key => $detail_data)
		{
			$item_central_detail = new ItemCentralDetail;
			$item_central_detail->item_central_id = $item_central->id;
			$item_central_detail->item_id = $key;
			$item_central_detail->quantity = $detail_data['quantity'];
			$item_central_detail->quantity_stock = $detail_data['quantity_stock'];
			$item_central_detail->uom = $detail_data['uom'];
			$item_central_detail->total = $detail_data['price']*$detail_data['quantity'];
			$item_central_detail->price = $detail_data['price'];
			$item_central_detail->save();
		}

		return redirect('editor/item_central');
		// } catch (\Exception $e) {
		// 	DB::rollback();
		// }

	}

	public function edit($id)
	{
		$item_central_detail = DB::table('item')
		->leftjoin('item_central_detail', 'item_central_detail.item_id', '=', 'item.id')
		->select('item_central_detail.id',
			'item_central_detail.item_central_id',
			'item_central_detail.item_id',
			'item_central_detail.item_name',
			'item_central_detail.quantity',
			'item_central_detail.quantity_stock',
			'item_central_detail.uom',
			'item_central_detail.nota_number',
			'item_central_detail.date',
			'item_central_detail.price',
			DB::raw('IFNULL(FORMAT(item_central_detail.price,0),0) as price_show'),
			'item_central_detail.spend_by',
			'item_central_detail.total',
			'item.item_name')
		->where('item_central_detail.item_central_id', '=', $id)
		->where('item.item_category_id', '=', 1)
		->whereNull('item_central_detail.deleted_at')
		->get();

		$item_central_detail_supply = DB::table('item')
		->leftjoin('item_central_detail', 'item_central_detail.item_id', '=', 'item.id')
		->select('item_central_detail.id',
			'item_central_detail.item_central_id',
			'item_central_detail.item_id',
			'item_central_detail.item_name',
			'item_central_detail.quantity',
			'item_central_detail.quantity_stock',
			'item_central_detail.uom',
			'item_central_detail.nota_number',
			'item_central_detail.date',
			'item_central_detail.price',
			DB::raw('IFNULL(FORMAT(item_central_detail.price,0),0) as price_show'),
			'item_central_detail.spend_by',
			'item_central_detail.total',
			'item.item_name')
		->where('item_central_detail.item_central_id', '=', $id)
		->where('item.item_category_id', '=', 2)
		->whereNull('item_central_detail.deleted_at')
		->get();


		//dd($item_central_detail);
		// $item_central_detail = Item::all();
		$item_list = Item::all()->pluck('item_name', 'id');
		$item_central = ItemCentral::Find($id);
  		//dd($item_central);
		return view ('editor.item_central.form', compact('item_central', 'item_list', 'item_central_detail', 'item_central_detail_supply'));
	}

	public function update($id, ItemCentralRequest $request)
	{
		// DB::beginTransaction();
		// try {

		//ItemCentral::where('id', $id)->delete();

		$maxlastid = DB::table('item_central')->max('id')+1;

		$item_central = ItemCentral::Find($id);
		$item_central->date = $request->input('date');
		$item_central->comment = $request->input('comment');
		$item_central->status_code = 'Waiting for Delivery';
		$item_central->save();

		$cart = $request->input('quantity');
             //get last header id
		$lastheaderid = $item_central->id;
			//dd($cart);
		// if($cart != null)
		// {
		foreach($request->input('detail') as $key => $detail_data)
		{
			$item_central_detail = ItemCentralDetail::Find($key);
			$item_central_detail->quantity = $detail_data['quantity'];
			$item_central_detail->quantity_stock = $detail_data['quantity_stock'];
			$item_central_detail->uom = $detail_data['uom'];
			$item_central_detail->total = $detail_data['price']*$detail_data['quantity'];
			$item_central_detail->price = $detail_data['price'];
			$item_central_detail->item_central_id = $item_central->id;
			$item_central_detail->save();
		}
		// }

		// 	DB::commit();

		return redirect('editor/item_central');
		// } catch (\Exception $e) {
		// 	DB::rollback();
		// }
	}

	public function delivery($id)
	{

		$item_central_detail = DB::table('item')
		->leftjoin('item_central_detail', 'item_central_detail.item_id', '=', 'item.id')
		->select('item_central_detail.id',
			'item_central_detail.item_central_id',
			'item_central_detail.item_id',
			'item_central_detail.item_name',
			'item_central_detail.quantity',
			'item_central_detail.quantity_actual',
			'item_central_detail.quantity_stock',
			'item_central_detail.uom',
			'item_central_detail.nota_number',
			'item_central_detail.date',
			'item_central_detail.price',
			'item_central_detail.spend_by',
			'item_central_detail.total',
			'item.item_name')
		->where('item_central_detail.item_central_id', '=', $id)
		->where('item.item_category_id', '=', 1)
		->where('item_central_detail.quantity', '>', 0)
		->whereNull('item_central_detail.deleted_at')
		->get();

		$item_central_detail_supply = DB::table('item')
		->leftjoin('item_central_detail', 'item_central_detail.item_id', '=', 'item.id')
		->select('item_central_detail.id',
			'item_central_detail.item_central_id',
			'item_central_detail.item_id',
			'item_central_detail.item_name',
			'item_central_detail.quantity',
			'item_central_detail.quantity_actual',
			'item_central_detail.quantity_stock',
			'item_central_detail.uom',
			'item_central_detail.nota_number',
			'item_central_detail.date',
			'item_central_detail.price',
			'item_central_detail.spend_by',
			'item_central_detail.total',
			'item.item_name')
		->where('item_central_detail.item_central_id', '=', $id)
		->where('item.item_category_id', '=', 2)
		->where('item_central_detail.quantity', '>', 0)
		->whereNull('item_central_detail.deleted_at')
		->get();

		//dd($item_central_detail);
		// $item_central_detail = Item::all();
		$item_list = Item::all()->pluck('item_name', 'id');
		$item_central = ItemCentral::Find($id);
  		//dd($item_central);
		return view ('editor.item_central.delivery', compact('item_central', 'item_list', 'item_central_detail', 'item_central_detail_supply'));
	}

	public function updatedelivery($id, Request $request)
	{
		$now = Carbon::now();
		$item_central = ItemCentral::Find($id);
		$item_central->status_code = 'Waiting for Invoice';
		$item_central->comment = $request->input('comment');
		$item_central->save();

		foreach($request->input('detail') as $key => $detail_data)
		{
			$item_central_detail = ItemCentralDetail::Find($key);
			$item_central_detail->quantity_actual = $detail_data['quantity_actual'];
			$item_central_detail->total = $detail_data['price']*$detail_data['quantity_actual'];
			$item_central_detail->price = $detail_data['price'];
			$item_central_detail->save();
		}

		return redirect()->action('Editor\ItemCentralController@index');
	}

	public function invoice($id)
	{
		$item_central_detail = DB::table('item')
		->leftjoin('item_central_detail', 'item_central_detail.item_id', '=', 'item.id')
		->select('item_central_detail.id',
			'item_central_detail.item_central_id',
			'item_central_detail.item_id',
			'item_central_detail.item_name',
			'item_central_detail.quantity',
			'item_central_detail.quantity_actual',
			'item_central_detail.quantity_stock',
			'item_central_detail.uom',
			'item_central_detail.nota_number',
			'item_central_detail.date',
			'item_central_detail.price',
			'item_central_detail.spend_by',
			'item_central_detail.total',
			'item.item_name')
		->where('item_central_detail.item_central_id', '=', $id)
		->where('item.item_category_id', '=', 1)
		->where('item_central_detail.quantity_actual', '>', 0)
		->whereNull('item_central_detail.deleted_at')
		->get();

		$item_central_detail_supply = DB::table('item')
		->leftjoin('item_central_detail', 'item_central_detail.item_id', '=', 'item.id')
		->select('item_central_detail.id',
			'item_central_detail.item_central_id',
			'item_central_detail.item_id',
			'item_central_detail.item_name',
			'item_central_detail.quantity',
			'item_central_detail.quantity_actual',
			'item_central_detail.quantity_stock',
			'item_central_detail.uom',
			'item_central_detail.nota_number',
			'item_central_detail.date',
			'item_central_detail.price',
			'item_central_detail.spend_by',
			'item_central_detail.total',
			'item.item_name')
		->where('item_central_detail.item_central_id', '=', $id)
		->where('item.item_category_id', '=', 2)
		->where('item_central_detail.quantity_actual', '>', 0)
		->whereNull('item_central_detail.deleted_at')
		->get();

		//dd($item_central_detail);
		// $item_central_detail = Item::all();
		$item_list = Item::all()->pluck('item_name', 'id');
		$item_central = ItemCentral::Find($id);
  		//dd($item_central);
		return view ('editor.item_central.invoice', compact('item_central', 'item_list', 'item_central_detail', 'item_central_detail_supply'));
	}

	public function updateinvoice($id, ItemCentralImageRequest $request)
	{
		$now = Carbon::now();
		$item_central = ItemCentral::Find($id);
		$item_central->date = $request->input('date');
		$item_central->status_code = 'Waiting for Owner Acknowledgement';
		$item_central->comment = $request->input('comment');
		$item_central->total_transport = $request->input('total_transport');
		$item_central->save();

		if($request->image)
		{
			$itemcentral = ItemCentral::FindOrFail($item_central->id);

			$original_directory = "uploads/itemcentral/";
			if(!File::exists($original_directory))
			{
				File::makeDirectory($original_directory, $mode = 0777, true, true);
			}

			// $file_extension = $request->image->getClientOriginalExtension();
			$itemcentral->itemcentral_attachment = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
			$request->image->move($original_directory, $itemcentral->itemcentral_attachment);

			// $thumbnail_directory = $original_directory."thumbnail/";
			// if(!File::exists($thumbnail_directory))
			// {
			// 	File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
			// }
			// $thumbnail = Image::make($original_directory.$itemcentral->itemcentral_attachment);
			// $thumbnail->fit(300,300)->save($thumbnail_directory.$itemcentral->itemcentral_attachment);

			$itemcentral->save();
		}

		// foreach($request->input('detail') as $key => $detail_data)
		// {
		// 	$item_central_detail = ItemCentralDetail::Find($key);
		// 	$item_central_detail->uom = $detail_data['uom'];
		// 	$item_central_detail->quantity_actual = $detail_data['quantity_actual'];
		// 	$item_central_detail->total = $detail_data['price']*$detail_data['quantity_actual'];
		// 	$item_central_detail->price = $detail_data['price'];
		// 	$item_central_detail->save();
		// }

		return redirect()->action('Editor\ItemCentralController@index');
	}

	public function issued($id)
	{
		$item_central_detail = DB::table('item')
		->leftjoin('item_central_detail', 'item_central_detail.item_id', '=', 'item.id')
		->select('item_central_detail.id',
			'item_central_detail.item_central_id',
			'item_central_detail.item_id',
			'item_central_detail.item_name',
			'item_central_detail.quantity',
			'item_central_detail.quantity_actual',
			'item_central_detail.quantity_stock',
			'item_central_detail.uom',
			'item_central_detail.nota_number',
			'item_central_detail.date',
			'item_central_detail.price',
			'item_central_detail.spend_by',
			'item_central_detail.total',
			'item.item_name')
		->where('item_central_detail.item_central_id', '=', $id)
		->where('item.item_category_id', '=', 1)
		->where('item_central_detail.quantity', '>', 0)
		->whereNull('item_central_detail.deleted_at')
		->get();

		$item_central_detail_supply = DB::table('item')
		->leftjoin('item_central_detail', 'item_central_detail.item_id', '=', 'item.id')
		->select('item_central_detail.id',
			'item_central_detail.item_central_id',
			'item_central_detail.item_id',
			'item_central_detail.item_name',
			'item_central_detail.quantity',
			'item_central_detail.quantity_actual',
			'item_central_detail.quantity_stock',
			'item_central_detail.uom',
			'item_central_detail.nota_number',
			'item_central_detail.date',
			'item_central_detail.price',
			'item_central_detail.spend_by',
			'item_central_detail.total',
			'item.item_name')
		->where('item_central_detail.item_central_id', '=', $id)
		->where('item.item_category_id', '=', 2)
		->where('item_central_detail.quantity', '>', 0)
		->whereNull('item_central_detail.deleted_at')
		->get();

		//dd($item_central_detail);
		// $item_central_detail = Item::all();
		$item_list = Item::all()->pluck('item_name', 'id');
		$item_central = ItemCentral::Find($id);
  		//dd($item_central);
		return view ('editor.item_central.issued', compact('item_central', 'item_list', 'item_central_detail', 'item_central_detail_supply'));
	}

	public function updateissued($id, Request $request)
	{
		$now = Carbon::now();
		$item_central = ItemCentral::Find($id);
		$item_central->status_code = 'Issued';
		$item_central->comment = $request->input('comment');
		$item_central->save();

		return redirect()->action('Editor\ItemCentralController@index');
	}

	public function paid($id)
	{
		$item_central_detail = DB::table('item')
		->leftjoin('item_central_detail', 'item_central_detail.item_id', '=', 'item.id')
		->select('item_central_detail.id',
			'item_central_detail.item_central_id',
			'item_central_detail.item_id',
			'item_central_detail.item_name',
			'item_central_detail.quantity',
			'item_central_detail.quantity_actual',
			'item_central_detail.quantity_stock',
			'item_central_detail.uom',
			'item_central_detail.nota_number',
			'item_central_detail.date',
			'item_central_detail.price',
			'item_central_detail.spend_by',
			'item_central_detail.total',
			'item.item_name')
		->where('item_central_detail.item_central_id', '=', $id)
		->where('item.item_category_id', '=', 1)
		->where('item_central_detail.quantity', '>', 0)
		->whereNull('item_central_detail.deleted_at')
		->get();

		$item_central_detail_supply = DB::table('item')
		->leftjoin('item_central_detail', 'item_central_detail.item_id', '=', 'item.id')
		->select('item_central_detail.id',
			'item_central_detail.item_central_id',
			'item_central_detail.item_id',
			'item_central_detail.item_name',
			'item_central_detail.quantity',
			'item_central_detail.quantity_actual',
			'item_central_detail.quantity_stock',
			'item_central_detail.uom',
			'item_central_detail.nota_number',
			'item_central_detail.date',
			'item_central_detail.price',
			'item_central_detail.spend_by',
			'item_central_detail.total',
			'item.item_name')
		->where('item_central_detail.item_central_id', '=', $id)
		->where('item.item_category_id', '=', 2)
		->where('item_central_detail.quantity', '>', 0)
		->whereNull('item_central_detail.deleted_at')
		->get();

		//dd($item_central_detail);
		// $item_central_detail = Item::all();
		$item_list = Item::all()->pluck('item_name', 'id');
		$item_central = ItemCentral::Find($id);
  		//dd($item_central);
		return view ('editor.item_central.paid', compact('item_central', 'item_list', 'item_central_detail', 'item_central_detail_supply'));
	}

	public function updatepaid($id, Request $request)
	{
		$now = Carbon::now();
		$item_central = ItemCentral::Find($id);
		$item_central->status_code = 'Pay';
		$item_central->comment = $request->input('comment');
		$item_central->save();

		if($request->transfer_receipt)
		{
			$itemcentral = ItemCentral::FindOrFail($item_central->id);

			$original_directory = "uploads/itemcentral/";
			if(!File::exists($original_directory))
			{
				File::makeDirectory($original_directory, $mode = 0777, true, true);
			}

			// $file_extension = $request->image->getClientOriginalExtension();
			$itemcentral->transfer_receipt = Carbon::now()->format("d-m-Y h-i-s").$request->transfer_receipt->getClientOriginalName();
			$request->transfer_receipt->move($original_directory, $itemcentral->transfer_receipt);

			// $thumbnail_directory = $original_directory."thumbnail/";
			// if(!File::exists($thumbnail_directory))
			// {
			// 	File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
			// }
			// $thumbnail = Image::make($original_directory.$itemcentral->itemcentral_attachment);
			// $thumbnail->fit(300,300)->save($thumbnail_directory.$itemcentral->itemcentral_attachment);

			$itemcentral->save();
		}

		return redirect()->action('Editor\ItemCentralController@index');
	}


	public function view($id)
	{
		$item_central_detail = DB::table('item')
		->leftjoin('item_central_detail', 'item_central_detail.item_id', '=', 'item.id')
		->select('item_central_detail.id',
			'item_central_detail.item_central_id',
			'item_central_detail.item_id',
			'item_central_detail.item_name',
			'item_central_detail.quantity',
			'item_central_detail.quantity_actual',
			'item_central_detail.quantity_stock',
			'item_central_detail.uom',
			'item_central_detail.nota_number',
			'item_central_detail.date',
			'item_central_detail.price',
			'item_central_detail.spend_by',
			'item_central_detail.total',
			'item.item_name')
		->where('item_central_detail.item_central_id', '=', $id)
		->where('item.item_category_id', '=', 1)
		->where('item_central_detail.quantity', '>', 0)
		->whereNull('item_central_detail.deleted_at')
		->get();

		$item_central_detail_supply = DB::table('item')
		->leftjoin('item_central_detail', 'item_central_detail.item_id', '=', 'item.id')
		->select('item_central_detail.id',
			'item_central_detail.item_central_id',
			'item_central_detail.item_id',
			'item_central_detail.item_name',
			'item_central_detail.quantity',
			'item_central_detail.quantity_actual',
			'item_central_detail.quantity_stock',
			'item_central_detail.uom',
			'item_central_detail.nota_number',
			'item_central_detail.date',
			'item_central_detail.price',
			'item_central_detail.spend_by',
			'item_central_detail.total',
			'item.item_name')
		->where('item_central_detail.item_central_id', '=', $id)
		->where('item.item_category_id', '=', 2)
		->where('item_central_detail.quantity', '>', 0)
		->whereNull('item_central_detail.deleted_at')
		->get();

		//dd($item_central_detail);
		//dd($item_central_detail);
		// $item_central_detail = Item::all();
		$item_list = Item::all()->pluck('item_name', 'id');
		$item_central = ItemCentral::Find($id);
  		//dd($item_central);
		return view ('editor.item_central.view', compact('item_central', 'item_list', 'item_central_detail', 'item_central_detail_supply'));
	}

	public function storeimport(ItemCentralImportRequest $request)
	{
		// $item_central = new ItemCentral;
		// $item_central->date = $request->input('date');
		// $item_central->comment = $request->input('comment');
		// $item_central->status_code = 'Waiting for Delivery';
		// $item_central->save();
		ItemCentralImport::query()->truncate();


		if($request->hasFile('import_file')){
			$path = $request->file('import_file')->getRealPath();
			$data = Excel::load($path, function($reader) {})->get();

			if(!empty($data) && $data->count()){
				foreach ($data->toArray() as $key => $value) {
					if(!empty($value)){
						foreach ($value as $v) {
							$insert[] = ['item_name' => $v['item_name'], 'quantity' => $v['quantity']];
						}
					}
				}

				if(!empty($insert)){
					ItemCentralImport::insert($insert);
					return back()->with('success','Insert Record successfully.');
				}
			}
		}
		return back()->with('error','Please Check your file, Something is wrong there.');
	}

	public function storeexport(Request $request, $type)
	{
		// $data = Item::get()->toArray();

		// $data = DB::table('item')
		// ->select('item.item_name',
		// 	'updated_by AS quantity',
		// 	'updated_by AS stock')
		// ->whereNull('item.deleted_at')
		// ->get()->toArray();

		// $sql = 'SELECT item_name, 0 AS quantity, 0 AS stock FROM item';
 	// 	$data = DB::table(DB::raw("($sql) as item"))->get();

 	// 	$data = $data->toArray();

		$data =  \DB::table('item')
		->join('item_category', 'item.item_category_id', '=', 'item_category.id')
		->select(array('item.seq_no AS no', 'item.item_name', 'item_category.category_name', 'item.uom', \DB::raw('0 as quantity')))
		->where('item.deleted_at', null)
		->where('item.item_category_id', 1)
		->orWhere('item.item_category_id', 2)
		->orderBy('item.seq_no', 'ASC')
		->get();
		$data = collect($data)->map(function($x){ return (array) $x; })->toArray();

		//dd($data);

		return Excel::create('sumoboo_item_central_template', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
			{
				$sheet->fromArray($data);
			});
		})->download($type);
	}
	public function delete($id)
	{
		ItemCentral::Find($id)->delete();
		return redirect()->action('Editor\ItemCentralController@index');
	}


	public function report(Request $request)
	{

		$item_id = null;
		$start_date = null;
		$end_date = null;
		if ($request->isMethod('post')) {
			$item_id = $request->item;
			$start_date = $request->start_date;
			$end_date = $request->end_date;

			$reports = DB::select("select * from
			(select month(c.date) as month,monthname(c.date) as monthname,a.item_name,sum(b.quantity) as jml
			from
			item a
			join item_central_detail b on (a.id = b.item_id)
			join item_central c on (b.item_central_id = c.id)
			where  date(c.date) between date('".$start_date."') and date('".$end_date."')
			and a.id=".$item_id." group by month(c.date) )
			a order by a.month
			");



			$graph = "";
			foreach ($reports as $item) {
				$graph .= "['".$item->month."',".$item->jml."],";
			}

			$graph = substr($graph, 0, -1);
		}






		$items_cons = Item::where('item_category_id',1)->get();
		$items_sup = Item::where('item_category_id',2)->get();
		return view('editor.item_central.report',compact('items_cons','items_sup','reports','graph','item_id','start_date','end_date'));

	}


}
