<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\VendorRequest;
use App\Http\Controllers\Controller;
use App\Model\Vendor;
use App\Model\InvoiceType;

class VendorController extends Controller
{
    public function index()
    {
    	if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14;
        $vendors =  DB::table('vendor')
        ->leftjoin('invoice_type', 'vendor.invoice_type_id', '=', 'invoice_type.id')
        ->select('vendor.id',
            'invoice_type.inv_type_name',
            'vendor.vendor_name',
            'vendor.vendor_phone',
            'vendor.vendor_address',
            'vendor.vendor_email',
            'vendor.vendor_bank',
            'vendor.vendor_rekening',
            'vendor.vendor_pic',
            'vendor.vendor_pic_num') 
        ->whereNull('vendor.deleted_at')
        ->orderBy('vendor.vendor_name', 'ASC')
        ->paginate(15); 

        //dd($vendors);

    	return view ('editor.vendor.index', compact('vendors'))->with('number',$no);
    }

    public function create()
    { 
        $invoice_type_list = InvoiceType::all()->pluck('inv_type_name', 'id');
        return view ('editor.vendor.form', compact('invoice_type_list'));
    }

    public function store(VendorRequest $request)
    {
    	$vendor = new Vendor;
        $vendor->invoice_type_id = $request->input('invoice_type_id');
    	$vendor->vendor_name = $request->input('vendor_name');
    	$vendor->vendor_phone = $request->input('vendor_phone');
        $vendor->vendor_address = $request->input('vendor_address');
        $vendor->vendor_email = $request->input('vendor_email');
        $vendor->vendor_bank = $request->input('vendor_bank');
        $vendor->vendor_rekening = $request->input('vendor_rekening');
        $vendor->vendor_pic = $request->input('vendor_pic');
        $vendor->vendor_pic_num = $request->input('vendor_pic_num');
        $vendor->created_by = Auth::id();
    	$vendor->save();

    	return redirect()->action('Editor\VendorController@index');
    }

    public function edit($id)
    {
    	$vendor = Vendor::Find($id); 
        $invoice_type_list = InvoiceType::all()->pluck('inv_type_name', 'id');
    	return view ('editor.vendor.form', compact('vendor', 'invoice_type_list'));
    }

    public function update($id, VendorRequest $request)
    {
    	$vendor = Vendor::Find($id);
        $vendor->invoice_type_id = $request->input('invoice_type_id');
        $vendor->vendor_name = $request->input('vendor_name');
        $vendor->vendor_phone = $request->input('vendor_phone');
        $vendor->vendor_address = $request->input('vendor_address');
        $vendor->vendor_email = $request->input('vendor_email');
        $vendor->vendor_bank = $request->input('vendor_bank');
        $vendor->vendor_rekening = $request->input('vendor_rekening');
        $vendor->vendor_pic = $request->input('vendor_pic');
        $vendor->vendor_pic_num = $request->input('vendor_pic_num');
        $vendor->updated_by = Auth::id();
    	$vendor->save();

    	return redirect()->action('Editor\VendorController@index');
    }

    public function delete($id)
    {
    	Vendor::Find($id)->delete();
    	return redirect()->action('Editor\VendorController@index');
    }
}
