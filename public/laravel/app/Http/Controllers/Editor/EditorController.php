<?php

namespace App\Http\Controllers\Editor;
use Session;
use Illuminate\Http\Request;
use DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Payroll;
use App\Model\Revenue;

class EditorController extends Controller
{
    public function index()
    {
        $months = DB::table('month')
          ->select('id', 'month_name')
          ->get();


    	return view('editor.index',compact('months'));
    }

    public function revenue_output($month)
    {
      $years = date('Y');
      $revenue = Revenue::where('branch_id', Session::get('branch_id'))
              //  ->where('date', '>=', $date)
                ->whereMonth('date', '=', $month)

                ->whereYear('date', '=', $years)
                ->orderBy('date', 'desc')
                ->get();


      return $revenue;
    }

    public function month_output(){
      $month = DB::table('month')
      ->select('id', 'month_name')
      ->get();
      return $month;
    }


    public function expense_output_chart()
    {
      $sessionId = Session::get('branch_id');
      $expand = DB::select("
      SELECT
        *
        FROM
        (
            SELECT
                '".$sessionId."' AS branch_id,
                month.id AS id_month,
                month.month_name AS month_name,
                IFNULL(SUM(DERIVEDTBL.amount), 0) AS amount
            FROM
                (
                    SELECT
                        *
                    FROM
                        view_expense
                    WHERE
                        view_expense.branch_id = ".$sessionId."
                ) AS DERIVEDTBL
            RIGHT JOIN month ON MONTHNAME(DERIVEDTBL.date_tr) = month .month_name
            GROUP BY
                DERIVEDTBL.branch_id,
                month.id,
                MONTHNAME(DERIVEDTBL.date_tr),
                month.month_name
            UNION ALL
                SELECT
                    2 AS branch_id,
                    month.id AS id_month,
                    month.month_name AS month_name,
                    IFNULL(SUM(DERIVEDTBL.amount), 0) AS amount
                FROM
                    (
                        SELECT
                            *
                        FROM
                            view_expense
                        WHERE
                            view_expense.branch_id = ".$sessionId."
                    ) AS DERIVEDTBL
                RIGHT JOIN month ON MONTHNAME(DERIVEDTBL.date_tr) = month .month_name
                GROUP BY
                    DERIVEDTBL.branch_id,
                    month.id,
                    MONTHNAME(DERIVEDTBL.date_tr),
                    month.month_name
        ) AS DERIVEDTBL1
        WHERE DERIVEDTBL1.branch_id = ".$sessionId."
        ORDER BY
        DERIVEDTBL1.id_month
      ");
      return $expand;
    }

    public function revenue_output_chart()
    {
      $sessionId = Session::get('branch_id');
      $revenue = DB::select("SELECT ifnull(b.branch_id,".$sessionId.") branch_id, a.id as id_month,a.month_name,ifnull(b.amount,0) as amount FROM month a left join (SELECT '".$sessionId."' AS branch_id,
                           derivedtbl.`month` AS id_month,
                           derivedtbl.`month_name`,
                           IFNULL( derivedtbl.`year`, YEAR (NOW()) ) AS `year`,
                           sum(derivedtbl.amount) AS amount
                    FROM
                      ( SELECT item_central.branch_id,
                               `month`.month_name,
                               'Suplay Sumoboo' AS `description`,
                               `month`.`id` AS `month`,
                               YEAR (`item_central`.`date`) AS `year`,
                                    sum( ( ( ifnull( `item_central`.`total_item_central`, 0 ) + ifnull( `item_central`.`total_invoice`, 0 ) ) + ifnull( `item_central`.`total_transport`, 0 ) ) ) AS `amount`
                       FROM `month`
                       LEFT JOIN `item_central` ON `month`.id = MONTH (`item_central`.`date`)
                       GROUP BY `item_central`.`branch_id`,
                                MONTH (`item_central`.`date`), YEAR (`item_central`.`date`), `month`.month_name
                       UNION ALL SELECT `invoice_direct`.`branch_id`,
                                        `month`.month_name,
                                        'Consumable direct to vendor' AS `type`,
                                        `month`.`id` AS `month`,
                                        YEAR ( `invoice_direct`.`invoice_date` ) AS `year`,
                                             sum( `invoice_direct`.`invoice_total` ) AS `amount`
                       FROM `month`
                       LEFT JOIN `invoice_direct` ON `month`.id = MONTH ( `invoice_direct`.`invoice_date` )
                       GROUP BY `invoice_direct`.`branch_id`,
                                MONTH ( `invoice_direct`.`invoice_date` ), YEAR ( `invoice_direct`.`invoice_date` ), `month`.month_name
                       UNION ALL SELECT `invoice`.`branch_id`,
                                        `month`.month_name,
                                        'Consumable (supporting restorant)' AS `type`,
                                        `month`.`id` AS `month`,
                                        YEAR (`invoice`.`invoice_date`) AS `year`,
                                             sum(`invoice`.`invoice_total`) AS `amount`
                       FROM `month`
                       LEFT JOIN `invoice` ON `month`.id = MONTH (`invoice`.`invoice_date`)
                       GROUP BY `invoice`.`branch_id`,
                                MONTH (`invoice`.`invoice_date`), YEAR (`invoice`.`invoice_date`), `month`.month_name ) AS derivedtbl
                    WHERE derivedtbl.branch_id = ".$sessionId."
                    GROUP BY derivedtbl.`branch_id`,
                             derivedtbl.`month`,
                             derivedtbl.`month_name`
                    ORDER BY derivedtbl.`month` ASC) b on (a.id = b.id_month)");
      return $revenue;
    }
}
