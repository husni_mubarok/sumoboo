<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\EmployeeRequest;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\Religion;
use App\Model\Branch;
use App\Model\EmployeeBranch;

class EmployeeController extends Controller
{
    public function index()
    {	
        
       $branch = Branch::all();
    //dd($branch);

       foreach ($branch as $key => $branch_list) {
        //$employees = Employee::where('')->paginate(15);

        if (Input::has('page'))
        {
           $page = Input::get('page');    
       }
       else
       {
           $page = 1;
       }
       $no = 15*$page-14; 


        $employees = DB::table('employee')
        ->join('employee_branch', 'employee.id', '=', 'employee_branch.employee_id')  
        ->select('employee.id',
            'employee.emp_name',
            'employee.emp_full_name',
            'employee.emp_bod',
            'employee.emp_address',
            'employee.emp_ktp_number',
            'employee.emp_bank',
            'employee.emp_bank_rek',
            'employee.emp_email',
            'employee.emp_phone',
            'employee.emp_gender',
            'employee.emp_religion',
            'employee.emp_hiring_date',
            'employee.emp_position',
            'employee.slr_basic',
            'employee.slr_voucher',
            'employee.slr_transport',
            'employee.slr_tunjangan',
            'employee.slr_tunjangan_makan',
            'employee.slr_cashbond',
            'employee.slr_thr',
            'employee.status',
            'employee.slr_pot_deposit')
        ->where('employee_branch.branch_id', '=', $branch_list->id)
        ->whereNull('employee.deleted_at')
        ->get();

        //dd($employees);

        $array_val["id"] = $branch_list->id;
        $array_val["branch_name"] = $branch_list->branch_name;
        $employee_arrays = null;

        foreach ($employees as $employee_list) {
         $employee_array = null;
         $employee_array["id"] = $employee_list->id;
         $employee_array["emp_name"] = $employee_list->emp_name;
         $employee_array["emp_full_name"] = $employee_list->emp_full_name;
         $employee_array["emp_address"] = $employee_list->emp_address;
         $employee_array["emp_ktp_number"] = $employee_list->emp_ktp_number;
         $employee_array["emp_email"] = $employee_list->emp_email;
         $employee_array["emp_phone"] = $employee_list->emp_phone;
         $employee_array["emp_gender"] = $employee_list->emp_gender;
         $employee_array["emp_religion"] = $employee_list->emp_religion;
         $employee_array["emp_hiring_date"] = $employee_list->emp_hiring_date;
         $employee_array["emp_position"] = $employee_list->emp_position;
         $employee_array["emp_bank"] = $employee_list->slr_basic;
         $employee_array["emp_bank_rek"] = $employee_list->slr_basic;
         $employee_array["slr_basic"] = $employee_list->slr_basic;
         $employee_array["slr_voucher"] = $employee_list->slr_voucher;
         $employee_array["slr_transport"] = $employee_list->slr_transport;
         $employee_array["slr_tunjangan"] = $employee_list->slr_tunjangan;
         $employee_array["slr_tunjangan_makan"] = $employee_list->slr_tunjangan_makan;
         $employee_array["slr_cashbond"] = $employee_list->slr_cashbond;
         $employee_array["slr_thr"] = $employee_list->slr_thr;
         $employee_array["slr_pot_deposit"] = $employee_list->slr_pot_deposit;
         $employee_array["emp_status"] = $employee_list->status;
         $employee_arrays[] = $employee_array;
     }

     $array_val["detail"] = $employee_arrays;
     $array_all[] = $array_val;  
 }

 //dd($array_all);

 return view ('editor.employee.index', compact('employees'))->with('number',$no)->with('array_all', $array_all);
}

public function create()
{ 
 $religion_list = Religion::all()->pluck('religion_name', 'religion_name');
 $status_list = [
      '0' => 'Active', 
      '1' => 'Not Active',  
    ];
 return view ('editor.employee.form', compact('religion_list', 'status_list'));
}

public function store(EmployeeRequest $request)
{

    $employee = new Employee;
    $employee->emp_name = $request->input('emp_name');
    $employee->emp_full_name = $request->input('emp_full_name');
    $employee->emp_bod = $request->input('emp_bod');
    $employee->emp_address = $request->input('emp_address');
    $employee->emp_ktp_number = $request->input('emp_ktp_number');
    $employee->emp_bank = $request->input('emp_bank');
    $employee->emp_bank_rek = $request->input('emp_bank_rek');
    $employee->emp_email = $request->input('emp_email');
    $employee->emp_phone = $request->input('emp_phone');
    $employee->emp_gender = $request->input('emp_gender');
    $employee->emp_religion = $request->input('emp_religion');
    $employee->emp_hiring_date = $request->input('emp_hiring_date');
    $employee->emp_position = $request->input('emp_position');
    $employee->slr_basic = $request->input('slr_basic');
    $employee->slr_voucher = $request->input('slr_voucher');
    $employee->slr_transport = $request->input('slr_transport');
    $employee->slr_tunjangan = $request->input('slr_tunjangan');
    $employee->slr_tunjangan_makan = $request->input('slr_tunjangan_makan');
    $employee->slr_cashbond = $request->input('slr_cashbond');
    $employee->slr_thr = $request->input('slr_thr');
    $employee->slr_pot_deposit = $request->input('slr_pot_deposit');
    $employee->status = $request->input('status');
    $employee->created_by = Auth::id();
    $employee->save();

    
    return redirect('editor/employee/'.$employee->id.'/branch');
}

public function branch($id)
{ 
 $employee = Employee::Find($id); 
 $religion_list = Religion::all()->pluck('religion_name', 'religion_name');
 $branch_list = Branch::pluck('branch_name', 'id');
 $employee_branch = EmployeeBranch::where('employee_id', $id)->get();

        //dd($employee_branch);

 return view ('editor.employee.branch', compact('religion_list', 'branch_list', 'employee', 'employee_branch'));
}

public function storebranch($id, Request $request)
{

    $employeebranch = new EmployeeBranch;
    $employeebranch->employee_id = $id;
    $employeebranch->branch_id = $request->input('branch_id'); 
    $employeebranch->save();

    return redirect('editor/employee/'.$id.'/branch');
        //return redirect()->action('Editor\EmployeeController@index');
}

public function deleteemployeebranch($id)
{

    EmployeeBranch::Find($id)->delete();

    return redirect()->back();
}

public function edit($id)
{
   $employee = Employee::Find($id); 
    $status_list = [
      '0' => 'Active', 
      '1' => 'Not Active',  
    ];

   $religion_list = Religion::all()->pluck('religion_name', 'religion_name');
   return view ('editor.employee.form', compact('employee', 'religion_list', 'status_list'));
}

public function update($id, EmployeeRequest $request)
{
   $employee = Employee::Find($id);
   $employee->emp_name = $request->input('emp_name');
   $employee->emp_full_name = $request->input('emp_full_name');
   $employee->emp_bod = $request->input('emp_bod');
   $employee->emp_address = $request->input('emp_address');
   $employee->emp_ktp_number = $request->input('emp_ktp_number');
   $employee->emp_bank = $request->input('emp_bank');
   $employee->emp_bank_rek = $request->input('emp_bank_rek');
   $employee->emp_email = $request->input('emp_email');
   $employee->emp_phone = $request->input('emp_phone');
   $employee->emp_gender = $request->input('emp_gender');
   $employee->emp_religion = $request->input('emp_religion');
   $employee->emp_hiring_date = $request->input('emp_hiring_date');
   $employee->emp_position = $request->input('emp_position');
   $employee->slr_basic = $request->input('slr_basic');
   $employee->slr_voucher = $request->input('slr_voucher');
   $employee->slr_transport = $request->input('slr_transport');
   $employee->slr_tunjangan = $request->input('slr_tunjangan');
   $employee->slr_tunjangan_makan = $request->input('slr_tunjangan_makan');
   $employee->slr_cashbond = $request->input('slr_cashbond');
   $employee->slr_thr = $request->input('slr_thr');
   $employee->slr_pot_deposit = $request->input('slr_pot_deposit');
   $employee->status = $request->input('status');
   $employee->updated_by = Auth::id();
   $employee->save();

   return redirect()->action('Editor\EmployeeController@index');
}

public function delete($id)
{
   Employee::Find($id)->delete();
   return redirect()->action('Editor\EmployeeController@index');
}
}
