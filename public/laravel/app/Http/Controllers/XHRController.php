<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Model\Vendor;
use App\Model\Item;
use App\Model\Revenue;
use App\Model\CashbondPayroll;

class XHRController extends Controller
{

    public function get_vendor(Request $request)
    {
        $vendor = Vendor::FindOrFail($request->input('vendor_id'));
        $details[] = $vendor;

        return $details;
    }

    public function get_item(Request $request)
    {
        $item = Item::FindOrFail($request->input('item_id'));
        $details[] = $item;

        return $details;
    }

    public function get_vendor_inv(Request $request)
    {
        $vendor = Vendor::Where('invoice_type_id', $request->input('invoice_type_id'))->get();
        $invoice_types = $vendor;
        // foreach($vendor->vendor_invoice as $key => $vendor_invoice)
        // {
        //     $invoice_types[$key] = $vendor_invoice->id;
        //     $invoice_types[$key] = $vendor_invoice->vendor_name;
        // }
        return $invoice_types;
    }

    public function get_item_inv(Request $request)
    {
        $vendor_item =  DB::table('vendor_item')
        ->join('item', 'item.id', '=', 'vendor_item.item_id')
        ->select('item.item_name',
            'item.item_desc',
            'item.id')
        ->whereNull('vendor_item.deleted_at')
        ->where('vendor_item.vendor_id', $request->input('vendor_id'))
        ->get();

        $items = $vendor_item;
        return $items;
    }

    public function get_turnover(Request $request)
    {
        $start = $request->start_date;
        $end = $request->end_date;
        $sum = Revenue::whereBetween('date',[$start, $end])
        ->where('branch_id', Session::get('branch_id'))
        ->sum('vat');;
        return $sum;
    }

    public function get_cashbond_payroll(Request $request)
    {
       $cashbond_payroll = DB::table('cashbond_payroll')
       ->join('cashbond_payroll_detail', 'cashbond_payroll.id', '=', 'cashbond_payroll_detail.cashbond_payroll_id')
       ->select('cashbond_payroll_detail.employee_id',
        DB::raw('SUM(cashbond_payroll_detail.cashbond) as cashbond'))
       ->where(DB::raw('MONTHNAME(cashbond_payroll.date)'), $request->input('month'))
       ->where('cashbond_payroll.branch_id', Session::get('branch_id'))
       ->groupBy('cashbond_payroll_detail.employee_id')
       ->get();

       //echo $cashbond_payroll;

       $cashbondpayroll = $cashbond_payroll;
       return $cashbondpayroll;
   }
}