<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CashbondRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => 'required',  
            'end_date' => 'required',  
            'budget_request' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'start_date.required' => 'Start date is required', 
            'end_date.required' => 'End date is required',
            'budget_request.required' => 'Budget request is required'
        ];
    }
}
