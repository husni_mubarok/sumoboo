<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PayrollRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'year' => 'required',
            'month' => 'required',
            'default_work_days' => 'required|numeric',
            'comment' => '',
            'payroll.*.employee_id' => 'required|integer',
            'payroll.*.workdays' => 'required|integer',
            'payroll.*.slr_basic' => 'required|numeric',
            'payroll.*.slr_transport' => 'required|numeric',
            'payroll.*.slr_tunjangan_makan' => 'required|numeric',
            'payroll.*.slr_cashbond' => 'required|numeric',
            'payroll.*.slr_thr' => 'required|numeric',
            'payroll.*.slr_pot_deposit' => 'required|numeric',
            'payroll.*.slr_total' => 'required|numeric',
            'payroll.*.slr_thp' => 'required|numeric',
            'payroll.*.default_work_days' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'year.required' => 'Year is required.',

            'month.required' => 'Month is required.',

            'employee_id.required' => 'Employee is required.',
            'employee_id.integer' => 'Choose a valid employee.',

            'workdays.required' => 'Work days is required.',
            'workdays.integer' => 'Work days must be a valid number.',

            'slr_basic.required' => 'Basic salary is required.',
            'slr_basic.numeric' => 'Basic salary must be a valid number.',

            'slr_transport.required' => 'Transport cost is required.',
            'slr_transport.numeric' => 'Transport cost must be a valid number.',

            'slr_tunjangan_makan.required' => 'Consumption cost is required.',
            'slr_tunjangan_makan.numeric' => 'Consumption cost must be a valid number.',

            'slr_cashbond.required' => 'Cash bond is required.',
            'slr_cashbond.numeric' => 'Cash bond must be a valid number.',

            'slr_thr.required' => 'THR is required.',
            'slr_thr.numeric' => 'THR must be a valid number.',

            'slr_pot_deposit.required' => 'Pot Deposit is required.',
            'slr_pot_deposit.numeric' => 'Pot Deposit must be a valid number.',

            'slr_total.required' => 'Total salary is required.',
            'slr_total.numeric' => 'Total salary must be a valid number.',

            'slr_thp.required' => 'THP is required.',
            'slr_thp.numeric' => 'THP must be a valid number.',

            'default_work_days.required' => 'Default work days is required.',
            'default_work_days.numeric' => 'THP must be a valid number.',
        ];
    }
}
