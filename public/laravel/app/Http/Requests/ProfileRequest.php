<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => '',
            'image' => 'image|between:0,5000',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'E-mail is required',
            'email.email' => 'E-mail must be a valid address',

            'first_name.required' => 'First name is required', 

            'last_name.required' => '',
            
            'image.image' => 'Image must be a valid file', 
            'image.between' => 'Image size must be less than 5MB'
        ];
    }
}
