<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FranchiseFeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'royalty_percentage' => 'required|numeric',
            'royalty_value' => 'required|numeric',
            'invoice_file' => 'image|between:0,2048',
        ];
    }

    public function messages()
    {
        return [
            'start_date.required' => 'Start date is required.',
            'start_date.date' => 'Input a valid date',

            'end_date.required' => 'End date is required.',
            'end_date.date' => 'Input a valid date',

            'royalty_percentage.required' => 'Royalty Percentage is required.',
            'royalty_percentage.numeric' => 'Royalty Percentage must be a number.',

            'royalty_value.required' => 'Royalty Value is required.',
            'royalty_value.numeric' => 'Royalty Value must be a number.',

            'invoice_file.image' => 'Invoice file must be of image format.',
            'invoice_file.between' => 'Invoice file size too large.',
        ];
    }
}
