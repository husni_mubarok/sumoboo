<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_name' => 'required',
            'uom' => 'required',
            'item_category_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'item_name.required' => 'Name is required',
            'uom.required' => 'UOM is required',
            'item_category_id.required' => 'Item category is required',
        ];
    }
}
