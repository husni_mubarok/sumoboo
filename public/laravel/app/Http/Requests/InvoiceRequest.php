<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'month' => 'required', 
            'year' => 'required', 
            'invoice_date' => 'required', 
            'image' => 'required|max:5000', 
        ];
    }

    public function messages()
    {
        return [
            'month.required' => 'Month is required',  
            'year.required' => 'Year is required',  
            'invoice_date.required' => 'Invoice date is required',  
            'image.required' => 'Attachment is required', 
            'image.max' => 'Maximum file size is 5 MB',  
        ];
    }
}
