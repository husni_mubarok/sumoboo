<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CashbondPayrollRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'cashbond_payroll_detail.*.cashbond' => 'required|numeric',  
        ];
    }

    public function messages()
    {
        return [
            'date.required' => 'Date is required.',
            'date.date' => 'Date must be of valid format.',

            'cashbond_payroll_detail.*.cashbond.required' => 'Cashbond is required.', 
            'cashbond_payroll_detail.*.cashbond.numeric' => 'Cashbond must be a valid number.', 
        ];
    }
}
