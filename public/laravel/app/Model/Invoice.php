<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $table = 'invoice';
 
	public function vendor()
	{
		return $this->belongsTo('App\Model\Vendor', 'invoice_type_id', 'id');
	}

	public function invoice_type()
	{
		return $this->belongsTo('App\Model\InvoiceType', 'invoice_type_id', 'id');
	}

	public function branch()
	{
		return $this->belongsTo('App\Model\Branch', 'branch_id', 'id');
	}
}



