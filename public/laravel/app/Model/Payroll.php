<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payroll extends Model
{
	use SoftDeletes;

	protected $table = 'payroll';
	protected $dates = ['deleted_at'];

	public function payroll_detail()
	{
		return $this->hasMany('App\Model\PayrollDetail', 'payroll_id', 'id');
	}

	public function user()
	{
		return $this->hasMany('App\Model\User', 'id', 'updated_by');
	}
}
