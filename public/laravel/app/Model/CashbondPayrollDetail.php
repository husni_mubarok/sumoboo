<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CashbondPayrollDetail extends Model
{
    use SoftDeletes;

    protected $table = 'cashbond_payroll_detail';
    protected $dates = ['deleted_at'];

    public function cashbond_payroll()
    {
    	return $this->belongsTo('App\Model\CashbondPayroll', 'cashbond_payroll_id', 'id');
    }

    public function employee()
    {
    	return $this->belongsTo('App\Model\Employee', 'employee_id', 'id');
    }
}
