<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CashOP extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $table = 'cash_operational';

    public function cashbond_detail()
	{
		return $this->hasMany('App\Model\CashbondDetail', 'cash_operational_id', 'id');
	}

	public function branch()
	{
		return $this->belongsTo('App\Model\Branch', 'branch_id', 'id');
	}
}



