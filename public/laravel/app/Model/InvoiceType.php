<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceType extends Model
{
	use SoftDeletes;
	protected $table = 'invoice_type';
	protected $dates = ['deleted_at'];

	public function invoice()
	{
		return $this->belongsTo('App\Model\Invoice', 'invoice_type_id', 'id');
	}

	public function vendor()
	{
		return $this->belongsTo('App\Model\Vendor', 'invoice_type_id', 'id');
	}

	public function item_category()
	{
		return $this->belongsTo('App\Model\ItemCategory', 'item_category_id', 'id');
	}
}
