<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Model
{
	use SoftDeletes;
	protected $table = 'vendor';
	protected $dates = ['deleted_at'];

	public function invoice()
	{
		return $this->hasMany('App\Model\Invoice', 'vendor_id', 'id');
	}

	public function invoice_type()
	{
		return $this->belongsTo('App\Model\InvoiceType', 'invoice_type_id', 'id');
	}

	public function vendor_item()
	{
		return $this->belongsTo('App\Model\VendorItem', 'vendor_id', 'id');
	}
}
