<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MaintenanceDetail extends Model
{
    protected $table = 'maintenance_detail';

    public function cashbond()
	{
		return $this->belongsTo('App\Model\MaintenanceList', 'maintenance_id', 'id');
	}
}



