<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
	use SoftDeletes;
	protected $table = 'employee';
	protected $dates = ['deleted_at'];

	public function cashbond_payroll()
	{
		return $this->hasMany('App\Model\CashbondPayroll', 'employee_id', 'id');
	}

	public function user()
	{
		return $this->hasMany('App\User', 'id', 'employee_id');
	}
}
