<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'document';

     public function files()
    {
        return $this->hasMany('App\Model\DocumentFile', 'document_id');

    }
}
