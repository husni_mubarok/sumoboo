@extends('layouts.editor.template')
@section('content')
<style type="text/css">
	.modal {
		.modal-dialog {
			width: 90%;
			height: 90%;
			margin: 5;
			padding: 5;
		}

		.modal-content {
			height: auto;
			min-height: 90%;
			border-radius: 5;
		}
	</style>
   <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.css" />

	<!-- Content Header (Page header) -->
	<section class="content-header hidden-xs">
		<h1>
			CMS
			<small>Content Management System</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="{{ URL::route('editor.payroll.index') }}"><i class="fa fa-list-ul"></i> Payroll List</a></li>
			<li class="active">
				@if(isset($payroll))
				<i class="fa fa-pencil"></i> Edit
				@else
				<i class="fa fa-plus"></i> Create
				@endif
			</li>
		</ol>
	</section>
	@actionStart('payroll', 'create|update')
	<section class="content">
		<section class="content box box-solid">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-12">
						<div class="x_panel">
							<h2>
								@if(isset($payroll))
								<i class="fa fa-pencil"></i>
								@else
								<i class="fa fa-plus"></i>
								@endif
								&nbsp;Payroll List
							</h2>
						</div>
						<hr>
						<div class="col-md-12">
							@include('errors.error')
							@if(isset($payroll))
							{!! Form::model($payroll, array('route' => ['editor.payroll.update', $payroll->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_payroll'))!!}
							@else
							{!! Form::open(array('route' => ['editor.payroll.update', $payroll->id], 'class'=>'create', 'id' => 'form_payroll'))!!}
							@endif
							{{ csrf_field() }}

							<div class="x_content">
								<div class="col-md-4">
									{{ Form::label('year', 'Year') }}
									{{ Form::number('year', old('year', date('Y')), ['class' => 'form-control', 'id' => 'year', 'placeholder' => 'Year of Payroll', 'min' => (date('Y') - 20), 'max' => (date('Y') + 20), 'disabled' => 'disabled']) }}
									<br>
								</div>


								<div class="col-md-4">
									{{ Form::label('month', 'Month') }}
									{{ Form::select('month', $month_list, old('month'), ['class' => 'form-control', 'id' => 'month', 'placeholder' => 'Month of Payroll', 'disabled' => 'disabled']) }}
									<br>
								</div>
								<div class="col-md-4">
									{{ Form::label('default_work_days', 'Default Work Days') }}
									{{ Form::number('default_work_days', old('default_work_days'), ['class' => 'form-control', 'id' => 'default_work_days', 'placeholder' => 'Default Work Days']) }}
									<br>
								</div>
								<div class="col-md-12">
									{{ Form::label('comment', 'Comment') }}
									{{ Form::text('comment', old('comment'), ['class' => 'form-control', 'id' => 'comment', 'placeholder' => 'Comment']) }}<br>

								</div>
								<div class="col-md-5">
									{{ Form::label('generate_work_days', 'Generate Work Days') }}
									{{ Form::number('generate_work_days', old('generate_work_days'), ['class' => 'form-control', 'id' => 'generate_work_days', 'placeholder' => 'Generate Work Days']) }}
									<br/>
									<a href="#" class="btn btn-primary btn-sm" id="btn_generate_wd"><i class="fa fa-magic"></i>&nbsp;Generate</a><br><br>
								</div>
								<div class="col-md-12">
									<!-- <div class="div_overflow_big payroll"> -->
										<table id="table_payroll" class="table table-hover table-striped table-bordered">
											<thead>
												<tr>
													<th rowspan="2">#</th>
													<th rowspan="2">Name</th>
													<th rowspan="2">Position</th>
													<th rowspan="2">Work Days</th>
													<th rowspan="2">Basic Salary</th>
													<th rowspan="2">Voucher</th>
													<th colspan="4"><center>Additional</center></th>
													<th colspan="5"><center>Reduction</center></th>

												</tr>
												<tr>
													<th>Holiday Allowance</th>
													<th>Transport</th>
													<th>Meal</th>
													<th>Peng Deposit</th>
													<th>Cash Bond</th>
													<th>Deposit</th>
													<th>Dormitory</th>
													<th>Additional Cost</th>
													<th>Notes</th>
													{{-- <th width="10%">Total</th>
													<th>THP</th> --}}
												</tr>
											</thead>
											<tbody>
												@foreach($employees as $key => $employee)
												<tr>
													<td>{{$key+1}}</td>
													<td>{{$employee->emp_full_name}}</td>
													<td>{{$employee->emp_position}}</td>

													<td>
														{{ Form::number('payroll_detail['.$employee->id.'][workdays]', old($employee->id.'[workdays]', $employee->workdays), ['class' => 'form-control', 'id' => 'workdays_'.$employee->id, 'min' => '0']) }}
													</td>
													<td>

														{{ Form::text('payroll_detail['.$employee->id.'][slr_basic_show]', old($employee->id.'[slr_basic_show]', $employee->slr_basic_show), ['class' => 'form-control', 'id' => 'slr_basic_show_'.$employee->id, 'min' => '0', 'oninput' => 'cal_sparator'.$employee->id.'();']) }}

														{{ Form::hidden('payroll_detail['.$employee->id.'][slr_basic]', old($employee->id.'[slr_basic]', $employee->slr_basic), ['class' => 'form-control', 'id' => 'slr_basic_'.$employee->id, 'min' => '0']) }}

													</td>
													<td>

														{{ Form::text('payroll_detail['.$employee->id.'][slr_voucher_show]', old($employee->id.'[slr_voucher_show]', $employee->slr_voucher_show), ['class' => 'form-control', 'id' => 'slr_voucher_show_'.$employee->id, 'min' => '0', 'oninput' => 'cal_sparator'.$employee->id.'();']) }}

														{{ Form::hidden('payroll_detail['.$employee->id.'][slr_voucher]', old($employee->id.'[slr_voucher]', $employee->slr_voucher), ['class' => 'form-control', 'id' => 'slr_voucher_'.$employee->id, 'min' => '0']) }}

													</td>
													<td>
														{{ Form::text('payroll_detail['.$employee->id.'][slr_thr_show]', old($employee->id.'[slr_thr_show]', $employee->slr_thr_show), ['class' => 'form-control', 'id' => 'slr_thr_show_'.$employee->id, 'min' => '0', 'oninput' => 'cal_sparator'.$employee->id.'();']) }}

														{{ Form::hidden('payroll_detail['.$employee->id.'][slr_thr]', old($employee->id.'[slr_thr]', $employee->slr_thr), ['class' => 'form-control', 'id' => 'slr_thr_'.$employee->id, 'min' => '0']) }}
													</td>
													<td>
														{{ Form::text('payroll_detail['.$employee->id.'][slr_transport_show]', old($employee->id.'[slr_transport_show]', $employee->slr_transport_show), ['class' => 'form-control', 'id' => 'slr_transport_show_'.$employee->id, 'min' => '0', 'oninput' => 'cal_sparator'.$employee->id.'();']) }}

														{{ Form::hidden('payroll_detail['.$employee->id.'][slr_transport]', old($employee->id.'[slr_transport]', $employee->slr_transport), ['class' => 'form-control', 'id' => 'slr_transport_'.$employee->id, 'min' => '0']) }}
													</td>
													<td>
														{{ Form::text('payroll_detail['.$employee->id.'][slr_tunjangan_makan_show]', old($employee->id.'[slr_tunjangan_makan_show]', $employee->slr_tunjangan_makan_show), ['class' => 'form-control', 'id' => 'slr_tunjangan_makan_show_'.$employee->id, 'min' => '0', 'oninput' => 'cal_sparator'.$employee->id.'();']) }}

														{{ Form::hidden('payroll_detail['.$employee->id.'][slr_tunjangan_makan]', old($employee->id.'[slr_tunjangan_makan]', $employee->slr_tunjangan_makan), ['class' => 'form-control', 'id' => 'slr_tunjangan_makan_'.$employee->id, 'min' => '0']) }}
													</td>
													<td>
														{{ Form::text('payroll_detail['.$employee->id.'][slr_peng_deposit_show]', old($employee->id.'[slr_peng_deposit_show]', $employee->slr_peng_deposit_show), ['class' => 'form-control', 'id' => 'slr_peng_deposit_show_'.$employee->id, 'min' => '0', 'oninput' => 'cal_sparator'.$employee->id.'();']) }}

														{{ Form::hidden('payroll_detail['.$employee->id.'][slr_peng_deposit]', old($employee->id.'[slr_peng_deposit]', $employee->slr_peng_deposit), ['class' => 'form-control', 'id' => 'slr_peng_deposit_'.$employee->id, 'min' => '0']) }}
													</td>
													<td>

														{{ Form::text('payroll_detail['.$employee->id.'][slr_cashbond_show]', old($employee->id.'[slr_cashbond_show]', $employee->slr_cashbond_show), ['class' => 'form-control', 'id' => 'slr_cashbond_show_'.$employee->id, 'min' => '0', 'oninput' => 'cal_sparator'.$employee->id.'();']) }}

														{{ Form::hidden('payroll_detail['.$employee->id.'][slr_cashbond]', old($employee->id.'[slr_cashbond]', $employee->slr_cashbond), ['class' => 'form-control', 'id' => 'slr_cashbond_'.$employee->id, 'min' => '0']) }}
													</td>
													<td>
														{{ Form::text('payroll_detail['.$employee->id.'][slr_pot_deposit_show]', old($employee->id.'[slr_pot_deposit_show]', $employee->slr_pot_deposit_show), ['class' => 'form-control', 'id' => 'slr_pot_deposit_show_'.$employee->id, 'min' => '0', 'oninput' => 'cal_sparator'.$employee->id.'();']) }}

														{{ Form::hidden('payroll_detail['.$employee->id.'][slr_pot_deposit]', old($employee->id.'[slr_pot_deposit]', $employee->slr_pot_deposit), ['class' => 'form-control', 'id' => 'slr_pot_deposit_'.$employee->id, 'min' => '0']) }}
													</td>
													<td>
														{{ Form::text('payroll_detail['.$employee->id.'][boarding_house_show]', old($employee->id.'[boarding_house_show]', $employee->boarding_house_show), ['class' => 'form-control', 'id' => 'boarding_house_show_'.$employee->id, 'min' => '0', 'oninput' => 'cal_sparator'.$employee->id.'();']) }}


														{{ Form::hidden('payroll_detail['.$employee->id.'][boarding_house]', old($employee->id.'[boarding_house]', $employee->boarding_house), ['class' => 'form-control', 'id' => 'boarding_house_'.$employee->id, 'min' => '0']) }}
													</td>
													<td>
														{{ Form::text('payroll_detail['.$employee->id.'][additional_cost_show]', old($employee->id.'[additional_cost_show]', $employee->additional_cost_show), ['class' => 'form-control', 'id' => 'additional_cost_show_'.$employee->id, 'min' => '0', 'oninput' => 'cal_sparator'.$employee->id.'();']) }}

														{{ Form::hidden('payroll_detail['.$employee->id.'][additional_cost]', old($employee->id.'[additional_cost]', $employee->additional_cost), ['class' => 'form-control', 'id' => 'additional_cost_'.$employee->id, 'min' => '0']) }}
													</td>
													<td>
														{{ Form::text('payroll_detail['.$employee->id.'][add_cost_desc]', old($employee->id.'[add_cost_desc]', $employee->add_cost_desc), ['class' => 'form-control', 'id' => 'add_cost_desc_'.$employee->id, 'min' => '0']) }}
													</td>
													{{-- <td id='slr_total_text_'{{$employee->id}}> 0 </td>
													{{ Form::hidden('payroll_detail['.$employee->id.'][slr_total]', 0, ['id' => 'slr_total_'.$employee->id]) }}
													<td id='slr_thp_'{{$employee->id}}> 0 </td>
													{{ Form::hidden('payroll_detail['.$employee->id.'][slr_thp]', 0, ['id' => 'slr_thp_'.$employee->id]) }} --}}
												</tr>

												<script type="text/javascript">
													function cal_sparator{{$employee->id}}() {
		//slr
		var slr_basic_show{{$employee->id}} = document.getElementById('slr_basic_show_{{$employee->id}}').value;
		var result{{$employee->id}} = document.getElementById('slr_basic_{{$employee->id}}');
		var rsslr_basic{{$employee->id}} = (slr_basic_show{{$employee->id}});
		result{{$employee->id}}.value = rsslr_basic{{$employee->id}}.replace(/,/g, "");

		n{{$employee->id}}= document.getElementById('slr_basic_show_{{$employee->id}}');

		n{{$employee->id}}.onkeyup=n{{$employee->id}}.onchange= function(e){
			e=e|| window.event;
			var who=e.target || e.srcElement,temp{{$employee->id}};
			if(who.id==='slr_basic_{{$employee->id}}')  temp{{$employee->id}}= validDigits(who.value,0);
			else temp{{$employee->id}}= validDigits(who.value);
			who.value= addCommas(temp{{$employee->id}});
		}
		n{{$employee->id}}.onblur= function(){
			var
			temp{{$employee->id}}=parseFloat(validDigits(n{{$employee->id}}.value));
			if(temp{{$employee->id}})n{{$employee->id}}.value=addCommas(temp{{$employee->id}}.toFixed(0));
		}
		//slr voucher
		var slr_voucher_show{{$employee->id}} = document.getElementById('slr_voucher_show_{{$employee->id}}').value;
        var result{{$employee->id}} = document.getElementById('slr_voucher_{{$employee->id}}');
        var rsslr_voucher{{$employee->id}} = (slr_voucher_show{{$employee->id}});
        result{{$employee->id}}.value = rsslr_voucher{{$employee->id}}.replace(/,/g, "");

        n{{$employee->id}}= document.getElementById('slr_voucher_show_{{$employee->id}}');

        n{{$employee->id}}.onkeyup=n{{$employee->id}}.onchange= function(e){
            e=e|| window.event;
            var who=e.target || e.srcElement,temp{{$employee->id}};
            if(who.id==='slr_voucher_{{$employee->id}}')  temp{{$employee->id}}= validDigits(who.value,0);
            else temp{{$employee->id}}= validDigits(who.value);
            who.value= addCommas(temp{{$employee->id}});
        }
        n{{$employee->id}}.onblur= function(){
            var
            temp{{$employee->id}}=parseFloat(validDigits(n{{$employee->id}}.value));
            if(temp{{$employee->id}})n{{$employee->id}}.value=addCommas(temp{{$employee->id}}.toFixed(0));
        }

		//Slr cashbond
		var slr_cashbond_show{{$employee->id}} = document.getElementById('slr_cashbond_show_{{$employee->id}}').value;
		var result{{$employee->id}} = document.getElementById('slr_cashbond_{{$employee->id}}');
		var rsslr_cashbond{{$employee->id}} = (slr_cashbond_show{{$employee->id}});
		result{{$employee->id}}.value = rsslr_cashbond{{$employee->id}}.replace(/,/g, "");

		n{{$employee->id}}= document.getElementById('slr_cashbond_show_{{$employee->id}}');

		n{{$employee->id}}.onkeyup=n{{$employee->id}}.onchange= function(e){
			e=e|| window.event;
			var who=e.target || e.srcElement,temp{{$employee->id}};
			if(who.id==='slr_cashbond_{{$employee->id}}')  temp{{$employee->id}}= validDigits(who.value,0);
			else temp{{$employee->id}}= validDigits(who.value);
			who.value= addCommas(temp{{$employee->id}});
		}
		n{{$employee->id}}.onblur= function(){
			var
			temp{{$employee->id}}=parseFloat(validDigits(n{{$employee->id}}.value));
			if(temp{{$employee->id}})n{{$employee->id}}.value=addCommas(temp{{$employee->id}}.toFixed(0));
		}

		//Slr pot deposit
		var slr_pot_deposit_show{{$employee->id}} = document.getElementById('slr_pot_deposit_show_{{$employee->id}}').value;
		var result{{$employee->id}} = document.getElementById('slr_pot_deposit_{{$employee->id}}');
		var rsslr_pot_deposit{{$employee->id}} = (slr_pot_deposit_show{{$employee->id}});
		result{{$employee->id}}.value = rsslr_pot_deposit{{$employee->id}}.replace(/,/g, "");

		n{{$employee->id}}= document.getElementById('slr_pot_deposit_show_{{$employee->id}}');

		n{{$employee->id}}.onkeyup=n{{$employee->id}}.onchange= function(e){
			e=e|| window.event;
			var who=e.target || e.srcElement,temp{{$employee->id}};
			if(who.id==='slr_pot_deposit_{{$employee->id}}')  temp{{$employee->id}}= validDigits(who.value,0);
			else temp{{$employee->id}}= validDigits(who.value);
			who.value= addCommas(temp{{$employee->id}});
		}
		n{{$employee->id}}.onblur= function(){
			var
			temp{{$employee->id}}=parseFloat(validDigits(n{{$employee->id}}.value));
			if(temp{{$employee->id}})n{{$employee->id}}.value=addCommas(temp{{$employee->id}}.toFixed(0));
		}

		//Boarding house
		var boarding_house_show{{$employee->id}} = document.getElementById('boarding_house_show_{{$employee->id}}').value;
		var result{{$employee->id}} = document.getElementById('boarding_house_{{$employee->id}}');
		var rsboarding_house{{$employee->id}} = (boarding_house_show{{$employee->id}});
		result{{$employee->id}}.value = rsboarding_house{{$employee->id}}.replace(/,/g, "");

		n{{$employee->id}}= document.getElementById('boarding_house_show_{{$employee->id}}');

		n{{$employee->id}}.onkeyup=n{{$employee->id}}.onchange= function(e){
			e=e|| window.event;
			var who=e.target || e.srcElement,temp{{$employee->id}};
			if(who.id==='boarding_house_{{$employee->id}}')  temp{{$employee->id}}= validDigits(who.value,0);
			else temp{{$employee->id}}= validDigits(who.value);
			who.value= addCommas(temp{{$employee->id}});
		}
		n{{$employee->id}}.onblur= function(){
			var
			temp{{$employee->id}}=parseFloat(validDigits(n{{$employee->id}}.value));
			if(temp{{$employee->id}})n{{$employee->id}}.value=addCommas(temp{{$employee->id}}.toFixed(0));
		}

		//Additional Cost
		var additional_cost_show{{$employee->id}} = document.getElementById('additional_cost_show_{{$employee->id}}').value;
		var result{{$employee->id}} = document.getElementById('additional_cost_{{$employee->id}}');
		var rsadditional_cost{{$employee->id}} = (additional_cost_show{{$employee->id}});
		result{{$employee->id}}.value = rsadditional_cost{{$employee->id}}.replace(/,/g, "");

		n{{$employee->id}}= document.getElementById('additional_cost_show_{{$employee->id}}');

		n{{$employee->id}}.onkeyup=n{{$employee->id}}.onchange= function(e){
			e=e|| window.event;
			var who=e.target || e.srcElement,temp{{$employee->id}};
			if(who.id==='additional_cost_{{$employee->id}}')  temp{{$employee->id}}= validDigits(who.value,0);
			else temp{{$employee->id}}= validDigits(who.value);
			who.value= addCommas(temp{{$employee->id}});
		}
		n{{$employee->id}}.onblur= function(){
			var
			temp{{$employee->id}}=parseFloat(validDigits(n{{$employee->id}}.value));
			if(temp{{$employee->id}})n{{$employee->id}}.value=addCommas(temp{{$employee->id}}.toFixed(0));
		}

		//slr thr
		var slr_thr_show{{$employee->id}} = document.getElementById('slr_thr_show_{{$employee->id}}').value;
		var result{{$employee->id}} = document.getElementById('slr_thr_{{$employee->id}}');
		var rsslr_thr{{$employee->id}} = (slr_thr_show{{$employee->id}});
		result{{$employee->id}}.value = rsslr_thr{{$employee->id}}.replace(/,/g, "");

		n{{$employee->id}}= document.getElementById('slr_thr_show_{{$employee->id}}');

		n{{$employee->id}}.onkeyup=n{{$employee->id}}.onchange= function(e){
			e=e|| window.event;
			var who=e.target || e.srcElement,temp{{$employee->id}};
			if(who.id==='slr_thr_{{$employee->id}}')  temp{{$employee->id}}= validDigits(who.value,0);
			else temp{{$employee->id}}= validDigits(who.value);
			who.value= addCommas(temp{{$employee->id}});
		}
		n{{$employee->id}}.onblur= function(){
			var
			temp{{$employee->id}}=parseFloat(validDigits(n{{$employee->id}}.value));
			if(temp{{$employee->id}})n{{$employee->id}}.value=addCommas(temp{{$employee->id}}.toFixed(0));
		}

		//transport
		var slr_transport_show{{$employee->id}} = document.getElementById('slr_transport_show_{{$employee->id}}').value;
		var result{{$employee->id}} = document.getElementById('slr_transport_{{$employee->id}}');
		var rsslr_transport{{$employee->id}} = (slr_transport_show{{$employee->id}});
		result{{$employee->id}}.value = rsslr_transport{{$employee->id}}.replace(/,/g, "");

		n{{$employee->id}}= document.getElementById('slr_transport_show_{{$employee->id}}');

		n{{$employee->id}}.onkeyup=n{{$employee->id}}.onchange= function(e){
			e=e|| window.event;
			var who=e.target || e.srcElement,temp{{$employee->id}};
			if(who.id==='slr_transport_{{$employee->id}}')  temp{{$employee->id}}= validDigits(who.value,0);
			else temp{{$employee->id}}= validDigits(who.value);
			who.value= addCommas(temp{{$employee->id}});
		}
		n{{$employee->id}}.onblur= function(){
			var
			temp{{$employee->id}}=parseFloat(validDigits(n{{$employee->id}}.value));
			if(temp{{$employee->id}})n{{$employee->id}}.value=addCommas(temp{{$employee->id}}.toFixed(0));
		}

		//tunjangan makan
		var slr_tunjangan_makan_show{{$employee->id}} = document.getElementById('slr_tunjangan_makan_show_{{$employee->id}}').value;
		var result{{$employee->id}} = document.getElementById('slr_tunjangan_makan_{{$employee->id}}');
		var rsslr_tunjangan_makan{{$employee->id}} = (slr_tunjangan_makan_show{{$employee->id}});
		result{{$employee->id}}.value = rsslr_tunjangan_makan{{$employee->id}}.replace(/,/g, "");

		n{{$employee->id}}= document.getElementById('slr_tunjangan_makan_show_{{$employee->id}}');

		n{{$employee->id}}.onkeyup=n{{$employee->id}}.onchange= function(e){
			e=e|| window.event;
			var who=e.target || e.srcElement,temp{{$employee->id}};
			if(who.id==='slr_tunjangan_makan_{{$employee->id}}')  temp{{$employee->id}}= validDigits(who.value,0);
			else temp{{$employee->id}}= validDigits(who.value);
			who.value= addCommas(temp{{$employee->id}});
		}
		n{{$employee->id}}.onblur= function(){
			var
			temp{{$employee->id}}=parseFloat(validDigits(n{{$employee->id}}.value));
			if(temp{{$employee->id}})n{{$employee->id}}.value=addCommas(temp{{$employee->id}}.toFixed(0));
		}

		//peng deposit
		var slr_peng_deposit_show{{$employee->id}} = document.getElementById('slr_peng_deposit_show_{{$employee->id}}').value;
		var result{{$employee->id}} = document.getElementById('slr_peng_deposit_{{$employee->id}}');
		var rsslr_peng_deposit{{$employee->id}} = (slr_peng_deposit_show{{$employee->id}});
		result{{$employee->id}}.value = rsslr_peng_deposit{{$employee->id}}.replace(/,/g, "");

		n{{$employee->id}}= document.getElementById('slr_peng_deposit_show_{{$employee->id}}');

		n{{$employee->id}}.onkeyup=n{{$employee->id}}.onchange= function(e){
			e=e|| window.event;
			var who=e.target || e.srcElement,temp{{$employee->id}};
			if(who.id==='slr_peng_deposit_{{$employee->id}}')  temp{{$employee->id}}= validDigits(who.value,0);
			else temp{{$employee->id}}= validDigits(who.value);
			who.value= addCommas(temp{{$employee->id}});
		}
		n{{$employee->id}}.onblur= function(){
			var
			temp{{$employee->id}}=parseFloat(validDigits(n{{$employee->id}}.value));
			if(temp{{$employee->id}})n{{$employee->id}}.value=addCommas(temp{{$employee->id}}.toFixed(0));
		}

	}
</script>



@endforeach
</tfooter>
<!-- <thead>
	<tr>
		<th>#</th>
		<th>Name</th>
		<th>Position</th>
		<th>Work Days</th>
		<th>Basic Salary</th>
		<th>Holiday Allowance</th>
		<th>Transport</th>
		<th>Meal</th>
		<th>Peng Deposit</th>
		<th>Cash Bond</th>
		<th>Deposit</th>
		<th>Dormitory</th>
		<th>Additional Cost</th>
		<th>Notes</th>
		{{-- <th width="10%">Total</th>
		<th>THP</th> --}}
	</tr> -->
</tfooter>
</table>
</div>
<br/>
<button type="button" id="btn_confirm" data-toggle="modal" data-target="#modal_confirm" class="btn btn-success btn-lg pull-right"><i class="fa fa-check"></i> Save</button><br><br>
@if(isset($payroll))
<span class="label label-default"><i class="fa fa-time"></i>
	Updated at: {{date("d M Y", strtotime($payroll_usr->updated_at))}}
</span><br/>
<span class="label label-default"><i class="fa fa-time"></i>
	Updated by: {{$payroll_usr->username}}
</span>
@endif
</div>
</div>
{!! Form::close() !!}
</div>
</div>
</div>
</div>
</div>
</section>
@stop
@actionEnd

@section('modal')
<div class="modal fade" id="modal_confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title">Payroll Summary</h4>
		</div>
		<div class="modal-body">
			<table class="table">
				<tr>
					<th width="20%">Year</th>
					<td width="20%" id="summary_payroll_year"></td>
					<th width="20%">Month</th>
					<td width="20%" id="summary_payroll_month"></td>
					<th width="20%">Default Work Days</th>
					<td width="20%" id="summary_payroll_default_work_days"></td>
				</tr>
				<tr>
					<th>Comment</th>
					<td colspan="3" id="summary_payroll_comment"></td>
				</tr>
			</table>
			<table class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th rowspan="2">#</th>
						<th rowspan="2">Name</th>
						<th rowspan="2">Position</th>
						<th rowspan="2">Work Days</th>
						<th rowspan="2">Basic Salary</th>
						<th rowspan="2">Voucher</th>
						<th colspan="4"><center>Additional</center></th>
						<th rowspan="2">Sallary</th>
						<th colspan="5"><center>Reduction</center></th>
						<th rowspan="2">THP</th>
					</tr>
					<tr>
						<th>Holiday Allowance</th>
						<th>Transport</th>
						<th>Meal</th>
						<th>Peng Deposit</th>
						<th>Cash Bond</th>
						<th>Deposit</th>
						<th>Dormitory</th>
						<th>Additional Cost</th>
						<th>Notes</th>
					</tr>
				</thead>
				<tbody id="summary_payroll_detail">
					{{-- PAYROLL DETAIL --}}
				</tbody>
				<tfoot>
					<tr>
						<td colspan="16" align="right"><b>TOTAL</b></td>
						<td id="grand_total"></td>
					</tr>
				</tfoot>
			</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
			<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
		</div>
	</div>
</div>
</div>
@stop

@section('scripts')
<script>
	$('#btn_submit').on('click', function()
	{
		$('#form_payroll').submit();
	});

	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	$('#btn_confirm').on('click', function()
	{
		$('#summary_payroll_year').text($('#year').val());
		$('#summary_payroll_month').text($('#month').val());
		$('#summary_payroll_comment').text($('#comment').val());
		$('#summary_payroll_default_work_days').text($('#default_work_days').val());

		var new_row = '';
		var i = 0;
		var grand_total = 0;
		@if(isset($employees))
		jQuery.each({!! $employees !!}, function (key, value)
		{
			i++;
			// var total_sallary = Math.round((parseFloat($('#workdays_'+value['id']).val())/parseFloat($('#default_work_days').val()) *parseFloat($('#slr_basic_'+value['id']).val()))+ parseFloat($('#slr_transport_'+value['id']).val()) + parseFloat($('#slr_tunjangan_makan_'+value['id']).val()) + parseFloat($('#slr_thr_'+value['id']).val())+ parseFloat($('#slr_peng_deposit_'+value['id']).val()));

			//var total = Math.round((parseFloat($('#workdays_'+value['id']).val())/parseFloat($('#default_work_days').val()) *parseFloat($('#slr_basic_'+value['id']).val()))+ parseFloat($('#slr_transport_'+value['id']).val()) + parseFloat($('#slr_tunjangan_makan_'+value['id']).val()) + parseFloat($('#slr_thr_'+value['id']).val())+ parseFloat($('#slr_peng_deposit_'+value['id']).val()) - (parseFloat($('#slr_cashbond_'+value['id']).val()) + parseFloat($('#slr_pot_deposit_'+value['id']).val()) + parseFloat($('#additional_cost_'+value['id']).val()) + parseFloat($('#boarding_house_'+value['id']).val())));

			var total_sallary = Math.round((parseFloat($('#workdays_'+value['id']).val())/parseFloat($('#default_work_days').val()) *parseFloat($('#slr_basic_'+value['id']).val()))+ parseFloat($('#slr_transport_'+value['id']).val()) + parseFloat($('#slr_tunjangan_makan_'+value['id']).val()) + parseFloat($('#slr_thr_'+value['id']).val())+ parseFloat($('#slr_peng_deposit_'+value['id']).val()) + parseFloat($('#slr_voucher_'+value['id']).val()));

			var total = Math.round((parseFloat($('#workdays_'+value['id']).val())/parseFloat($('#default_work_days').val()) *parseFloat($('#slr_basic_'+value['id']).val()))+ parseFloat($('#slr_transport_'+value['id']).val()) + parseFloat($('#slr_tunjangan_makan_'+value['id']).val()) + parseFloat($('#slr_thr_'+value['id']).val())+ parseFloat($('#slr_peng_deposit_'+value['id']).val()) + parseFloat($('#slr_voucher_'+value['id']).val()) - (parseFloat($('#slr_cashbond_'+value['id']).val()) + parseFloat($('#slr_pot_deposit_'+value['id']).val()) + parseFloat($('#additional_cost_'+value['id']).val()) + parseFloat($('#boarding_house_'+value['id']).val())));


			new_row += '<tr>';
			new_row += '<td>';
			new_row += i;
			new_row += '</td>';
			new_row += '<td>';
			new_row += value['emp_full_name'];
			new_row += '</td>';
			new_row += '<td>';
			new_row += value['emp_position'];
			new_row += '</td>';
			new_row += '<td>';
			new_row += $('#workdays_'+value['id']).val();
			new_row += '</td>';
			new_row += '<td>';
			new_row += numberWithCommas($('#slr_basic_'+value['id']).val());
			new_row += '</td>';
			new_row += '<td>';
			new_row += numberWithCommas($('#slr_voucher_'+value['id']).val());
			new_row += '</td>';
			new_row += '<td>';
			new_row += numberWithCommas($('#slr_thr_'+value['id']).val());
			new_row += '</td>';
			new_row += '<td>';
			new_row += numberWithCommas($('#slr_transport_'+value['id']).val());
			new_row += '</td>';
			new_row += '<td>';
			new_row += numberWithCommas($('#slr_tunjangan_makan_'+value['id']).val());
			new_row += '</td>';
			new_row += '<td>';
			new_row += numberWithCommas($('#slr_peng_deposit_'+value['id']).val());
			new_row += '</td>';
			new_row += '<td>';
			new_row += numberWithCommas(total_sallary);
			new_row += '</td>';
			new_row += '<td>';
			new_row += numberWithCommas($('#slr_cashbond_'+value['id']).val());
			new_row += '</td>';
			new_row += '<td>';
			new_row += numberWithCommas($('#slr_pot_deposit_'+value['id']).val());
			new_row += '</td>';
			new_row += '<td>';
			new_row += numberWithCommas($('#boarding_house_'+value['id']).val());
			new_row += '</td>';
			new_row += '<td>';
			new_row += numberWithCommas($('#additional_cost_'+value['id']).val());
			new_row += '</td>';
			new_row += '<td>';
			new_row += $('#add_cost_desc_'+value['id']).val();
			new_row += '</td>';
			new_row += '<td>';
			new_row += numberWithCommas(total);
			new_row += '</td>';
			new_row += '</tr>';

			grand_total += total;
		});

		$('#grand_total').html(numberWithCommas(grand_total));

		$('#summary_payroll_detail').empty().append(new_row);

		@endif

	});


	$('#btn_generate_wd').on('click', function()
	{
		@foreach($employees as $key => $employee)
		document.getElementById('workdays_{{$employee->id}}').value = document.getElementById('generate_work_days').value;
		@endforeach
	});

	$("#month").on('click', function()
	{
		$.ajax({
			url : '{{ URL::route('get.cashbondpayroll') }}',
			data : {'month':$("#month").val()},
			type : 'GET',
			headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
			success : function(data, textStatus, jqXHR){


				//$('#invoice_bank').empty();
				//$('#invoice_rekening').empty();
				jQuery.each(data, function(i, val)
				{

					console.log("sadasd");
					console.log(data[0].cashbond);

					@foreach($employees as $key => $employee)
					$('#slr_cashbond_{{$employee->id}}').empty();
					document.getElementById('slr_cashbond_{{$employee->id}}').value = data[0].cashbond;
					@endforeach

					// document.getElementById ("invoice_bank").value = data[0].vendor_bank;
					// document.getElementById ("invoice_rekening").value = data[0].vendor_rekening;
				});
			},
			error : function()
			{
				// $('#invoice_bank').empty();
				// $('#invoice_rekening').empty();
			},
		})
	});

</script>

@if(isset($payroll))
<script>
	jQuery.each({!! $payroll->payroll_detail !!}, function(key, value)
	{
		$('#workdays_'+value['id']).val(value['workdays']);
		$('#slr_basic_'+value['id']).val(value['slr_basic']);
		$('#slr_voucher_'+value['id']).val(value['slr_voucher']);
		$('#slr_transport_'+value['id']).val(value['slr_transport']);
		$('#slr_tunjangan_makan_'+value['id']).val(value['slr_tunjangan_makan']);
		$('#slr_cashbond_'+value['id']).val(value['slr_cashbond']);
		$('#slr_thr_'+value['id']).val(value['slr_thr']);
		$('#slr_pot_deposit_'+value['id']).val(value['slr_pot_deposit']);
	});


	$(document).ready(function() {

	$("#table_payroll").dataTable( {
		"sScrollX": true,
         "scrollY": "500px",
         "scrollX": true,
         "sScrollXInner": "170%",
		 "bPaginate": false,
		 "sScrollXInner": "170%",
         "autoWidth": true,
         "rowReorder": true,
         fixedColumns:   {
          leftColumns: 3
         },
	});
	});


</script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
@endif

@stop

