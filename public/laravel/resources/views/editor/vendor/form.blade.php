@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.vendor.index') }}"><i class="fa fa-handshake-o"></i> Vendor</a></li>
	</ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-5">
					<div class="x_panel">
						<h2>
							
							@if(isset($vendor))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							&nbsp;Vendor
						</h2>
						<hr>
						<div class="x_content">
							@include('errors.error')
							@if(isset($vendor))
							{!! Form::model($vendor, array('route' => ['editor.vendor.update', $vendor->id], 'method' => 'PUT', 'class'=>'update'))!!}
							@else
							{!! Form::open(array('route' => 'editor.vendor.store', 'class'=>'create'))!!}
							@endif
							{{ csrf_field() }}
							<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
								{{ Form::label('Invoice Type') }}
								{{ Form::select('invoice_type_id', $invoice_type_list, old('invoice_type_id'), array('class' => 'form-control', 'required' => 'true')) }}<br/>
								
								{{ Form::label('vendor_name', 'Name') }}
								{{ Form::text('vendor_name', old('vendor_name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true')) }}<br/>

								{{ Form::label('vendor_phone', 'Phone') }}
								{{ Form::text('vendor_phone', old('vendor_phone'), array('class' => 'form-control', 'placeholder' => 'Phone*', 'required' => 'true')) }}<br/>

								{{ Form::label('vendor_address', 'Address') }}
								{{ Form::text('vendor_address', old('vendor_address'), array('class' => 'form-control', 'placeholder' => 'Address*', 'required' => 'true')) }}<br/>

								{{ Form::label('vendor_email', 'Email') }}
								{{ Form::text('vendor_email', old('vendor_email'), array('class' => 'form-control', 'placeholder' => 'Email*', 'required' => 'true')) }}<br/>

								{{ Form::label('vendor_bank', 'Bank') }}
								{{ Form::text('vendor_bank', old('vendor_bank'), array('class' => 'form-control', 'placeholder' => 'Bank*', 'required' => 'true')) }}<br/>

								{{ Form::label('vendor_rekening', 'Rek No') }}
								{{ Form::text('vendor_rekening', old('vendor_rekening'), array('class' => 'form-control', 'placeholder' => 'Rek No*', 'required' => 'true')) }}<br/>

								{{ Form::label('vendor_pic', 'PIC') }}
								{{ Form::text('vendor_pic', old('vendor_pic'), array('class' => 'form-control', 'placeholder' => 'PIC*', 'required' => 'true')) }}<br/>

								{{ Form::label('vendor_pic_num', 'PIC Number') }}
								{{ Form::text('vendor_pic_num', old('vendor_pic_num'), array('class' => 'form-control', 'placeholder' => 'PIC Number*', 'required' => 'true')) }}<br/> 
								<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
								<a href="{{ URL::route('editor.vendor.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
@stop

