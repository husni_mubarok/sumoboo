@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->

<style type="text/css">
	th { font-size: 11px; }
	td { font-size: 11px; }
</style>
<section class="content-header hidden-xs">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{ URL::route('editor.tax_pph.index') }}"><i class="fa fa-industry"></i> Tax PPH</a></li>
    <li class="active"><i class="fa fa-search"></i> Detail</li>
  </ol>
</section>
@actionStart('franchise_fee', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-12">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-search"></i>&nbsp;&nbsp;Tax PPH
	                	</h2>
		                <hr>
			           <div class="x_content">
			                <div class="col-md-6 col-xs-12 col-sm-12">
			                	<table class="table">
			                		<tr>
			                			<th>Period</th>
			                			<td>{{ $franchise_fee->start_date }} - {{ $franchise_fee->end_date }}</td>
			                		</tr>
			                		<tr>
			                			<th>Branch</th>
			                			<td>{{$franchise_fee->branch->branch_name}}</td>
			                		</tr>
			                		<tr>
			                			<th>Royalty ({{$franchise_fee->royalty_percentage}} %)</th>
			                			<td>{{number_format($franchise_fee->royalty_value)}}</td>
			                		</tr>
			                		@if($franchise_fee->invoice_date)
			                		<tr>
			                			<th>Invoice Date</th>
			                			<td>{{date('D, d F Y', strtotime($franchise_fee->invoice_date))}}</td>
			                		</tr>
			                		@endif
			                	</table>
			                </div>
			                <div class="col-md-6 col-xs-12 col-sm-12">
			                	{{-- PART 2 --}}
			                </div>

			                <div class="col-md-12 col-xs-12 col-sm-12">
			                	@if($franchise_fee->status == 0)
			                		@actionStart('franchise_fee', 'submit')
				                	{!! Form::open(['route' => ['editor.tax_pph.submit', $franchise_fee->id], 'method' => 'PUT', 'id' => 'form_submit']) !!}
				                	<button type="button" class="btn btn-lg btn-success" id="btn_submit">
				                		<i class="fa fa-check"></i> Submit
			                		</button>
				                	{!! Form::close() !!}
				                	@actionEnd

			                	@elseif($franchise_fee->status == 1)
			                		@actionStart('franchise_fee', 'paid')
				                	{!! Form::open(['route' => ['editor.tax_pph.finance_approve', $franchise_fee->id], 'method' => 'PUT', 'id' => 'form_finance']) !!}
				                	<input type="hidden" name="review" value="0" id="finance_review">
			                		<table>
		                				<tr>
		                					<td>
							                	<button type="button" class="btn btn-lg btn-danger" id="btn_finance_reject">
							                		<i class="fa fa-remove"></i> Reject
						                		</button>
						                	</td>
						                	<td>
							                	<button type="button" class="btn btn-lg btn-success" id="btn_finance_approve">
							                		<i class="fa fa-check"></i> Approve
						                		</button>
						                	</td>
					                	</tr>
				                	</table>
				                	{!! Form::close() !!}
				                	@actionEnd

			                	@elseif($franchise_fee->status == 2)
			                		@actionStart('franchise_fee', 'issued')
				                	{!! Form::open(['route' => ['editor.tax_pph.owner_approve', $franchise_fee->id], 'method' => 'PUT', 'id' => 'form_owner']) !!}
				                	<button type="button" class="btn btn-lg btn-success" id="btn_owner">
				                		<i class="fa fa-check"></i> Issued
			                		</button>
				                	{!! Form::close() !!}
				                	@actionEnd

			                	@elseif($franchise_fee->status == 3)
			                		@actionStart('franchise_fee', 'paid')
				                	{!! Form::open(['route' => ['editor.tax_pph.finance_payment', $franchise_fee->id], 'method' => 'PUT', 'id' => 'form_paid']) !!}
				                	<button type="button" class="btn btn-lg btn-success" id="btn_paid">
				                		<i class="fa fa-check"></i> Paid
			                		</button>
				                	{!! Form::close() !!}
				                	@actionEnd

			                	@endif
	                		</div>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop
@actionEnd

@section('modal')
<div class="modal fade" id="modal_summary">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title"><i class="fa fa-warning"></i>Confirmation</h5>
      		</div>
      		<div class="modal-body">
        		<div id="modal_summary_body"></div>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-success" id="btn_confirm">OK</button>
      			<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
$('#btn_submit').on('click', function()
{
	$('#modal_summary_body').html('Submit Tax PPH ?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_submit').submit();
	});
});

$('#btn_finance_reject').on('click', function()
{
	$('#finance_review').val('0');
	$('#modal_summary_body').html('Reject Tax PPH ?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_finance').submit();
	});
});

$('#btn_finance_approve').on('click', function()
{
	$('#finance_review').val('1');
	$('#modal_summary_body').html('Approve Tax PPH?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_finance').submit();
	});
});

$('#btn_owner').on('click', function()
{
	$('#modal_summary_body').html('Issue Tax PPH?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_owner').submit();
	});
});

$('#btn_paid').on('click', function()
{
	$('#modal_summary_body').html('Pay Tax PPH?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_paid').submit();
	});
});
</script>
@stop