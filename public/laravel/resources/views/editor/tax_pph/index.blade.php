@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->

<style type="text/css">
	th { font-size: 11px; }
	td { font-size: 11px; }
</style>
<section class="content-header hidden-xs">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="#"><i class="fa fa-industry"></i> Tax PPN</a></li>
  </ol>
</section>
@actionStart('franchise_fee', 'read')
<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-12">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-industry"></i> Tax PPN
		                	@actionStart('franchise_fee', 'create')
		                	<a href="{{ URL::route('editor.tax_pph.create') }}" class="btn btn-success btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
		                	@actionEnd
	                	</h2>
		                <hr>
			           <div class="x_content">
			                <table id="table_franchise_feex" class="table dataTable rwd-table">
							  	<thead>
							  	  	<tr>
								      	<th width="5%">#</th>
								      	<th>Date</th>
								      	<th>Status</th>
								      	<th>Royalty Percentage</th>
								      	<th>Royalty Value</th>
								      	<th>Invoice Date</th>
								      	<th>Invoice File</th>
								      	<th>Attachment Receipt</th>
								      	<th>Action Status</th>
								      	<th width="10%">Action</th>
							    	</tr>
							  	</thead>
							  	<tbody>
								@foreach($franchise_fees as $key => $franchise_fee)
									<tr>
										<td data-th="#">{{$number++}}</td>
										<td id="date_{{$franchise_fee->id}}" data-th="Date">
											{{ date('Y F', strtotime($franchise_fee->year.'-'.$franchise_fee->month)) }}
										</td>
										<td data-th="Status">
											@if($franchise_fee->status == 0)
											<span class="label label-danger"><i class="fa fa-unlock"></i> Open</span>
											@elseif($franchise_fee->status == 1)
											<span class="label label-warning"><i class="fa fa-lock"></i> Waiting Finance Approval</span>
											@elseif($franchise_fee->status == 2)
											<span class="label label-warning"><i class="fa fa-lock"></i> Waiting Owner Approval</span>
											@elseif($franchise_fee->status == 3)
											<span class="label label-warning"><i class="fa fa-lock"></i> Waiting Finance Payment</span>
											@elseif($franchise_fee->status == 4)
											<span class="label label-success"><i class="fa fa-check"></i> Paid</span>
											@endif
										</td>
										<td data-th="Royalty %">{{number_format($franchise_fee->royalty_percentage)}}%</td>
										<td data-th="Royalty Value">{{number_format($franchise_fee->royalty_value)}}</td>
										<td data-th="Invoice Dat">{{$franchise_fee->invoice_date}}</td>
										<td data-th="Invoice File">
											<a class="fancybox" rel="group" href="{{ Config::get('constants.path.uploads') }}/tax_pph/{{ $franchise_fee->invoice_file }}">
												<img src="{{ Config::get('constants.path.uploads') }}/tax_pph/thumbnail/{{ $franchise_fee->invoice_file }}" class="img-thumbnail img-responsive">
											</a>
											{{-- <a class="fancybox btn btn-primary btn-sm" rel="group" href="{{ Config::get('constants.path.uploads') }}/franchise_fee/{{ $franchise_fee->invoice_file }}">
												<i class="fa fa-image"></i> View File
											</a> --}}
										</td>
										<td data-th="Attachment">
											@if($franchise_fee->attachment_receipt == null)
											Tidak ada lampiran
											@else
											<a target="_blank" href="{{Config::get('constants.path.uploads')}}/tax_pph/attachment_receipt/{{$franchise_fee->attachment_receipt}}"><i class="fa fa-download"></i>&nbsp;Download</a>

											@endif
										</td>
										<td>
											@if($franchise_fee->status == 0)
						                		@actionStart('franchise_fee', 'submit')
							                	{!! Form::open(['route' => ['editor.tax_pph.submit', $franchise_fee->id], 'method' => 'PUT', 'id' => 'form_submit_'.$franchise_fee->id]) !!}
							                	<button type="button" class="btn btn-sm btn-success btn_submit" value="{{$franchise_fee->id}}">
							                		<i class="fa fa-check"></i> Submit
						                		</button>
							                	{!! Form::close() !!}
							                	@actionEnd

						                	@elseif($franchise_fee->status == 1)
						                		@actionStart('franchise_fee', 'paid')
							                	{!! Form::open(['route' => ['editor.tax_pph.finance_approve', $franchise_fee->id], 'method' => 'PUT', 'id' => 'form_finance_'.$franchise_fee->id]) !!}
							                	<input type="hidden" name="review" value="0" id="finance_review">
					                			<div class="act_tb">
					                				<div>
									                	<button type="button" class="btn btn-sm btn-danger btn_finance_reject" value="{{$franchise_fee->id}}" title="Finance Reject">
									                		<i class="fa fa-remove"></i>
								                		</button>
							                		</div>
								                	<div>
									                	<button type="button" class="btn btn-sm btn-success btn_finance_approve" value="{{$franchise_fee->id}}" title="Finance Approve">
									                		<i class="fa fa-check"></i>
								                		</button>
							                		</div>
						                		</div>
							                	{!! Form::close() !!}
							                	@actionEnd

						                	@elseif($franchise_fee->status == 2)
						                		@actionStart('franchise_fee', 'issued')
							                	{!! Form::open(['route' => ['editor.tax_pph.owner_approve', $franchise_fee->id], 'method' => 'PUT', 'id' => 'form_owner_'.$franchise_fee->id]) !!}
							                	<button type="button" class="btn btn-sm btn-success btn_owner" value="{{$franchise_fee->id}}">
							                		<i class="fa fa-check"></i> Issued
						                		</button>
							                	{!! Form::close() !!}
							                	@actionEnd

						                	@elseif($franchise_fee->status == 3)
						                	@actionStart('franchise_fee', 'paid')
							                	<a href="{{ URL::route('editor.tax_pph.paid', [$franchise_fee->id]) }}" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Paid</a>
							                		@actionEnd
						                	@endif
										</td>
										<td align="center">
											<div class="act_tb">
												<div>
													<a href="{{ URL::route('editor.tax_pph.detail', [$franchise_fee->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>
												</div>
												@actionStart('franchise_fee', 'update')
												<div>
													@if($franchise_fee->status == 0)
													<a href="{{ URL::route('editor.tax_pph.edit', [$franchise_fee->id]) }}" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></a>
													@endif
												</div>
												@actionEnd
												@actionStart('franchise_fee', 'delete')
												<div>
													@if($franchise_fee->status == 0)
													{!! Form::open(['route' => ['editor.tax_pph.delete', $franchise_fee->id], 'method' => 'delete']) !!}
													<button type="submit" onclick="return confirm('Delete?')" class="btn btn-sm btn-danger"><i class="fa fa-remove"></i></button>
													{!! Form::close() !!}
													@endif
												</div>
												@actionEnd
											</div>
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
			            </div>
						{{$franchise_fees->links()}}
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop
@actionEnd

@section('modal')
<div class="modal fade" id="modal_summary">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title"><i class="fa fa-warning"></i>Confirmation</h5>
      		</div>
      		<div class="modal-body">
        		<div id="modal_summary_body"></div>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-success" id="btn_confirm">OK</button>
      			<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<!-- Add fancyBox -->
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script>
$(document).ready(function () {
    $("#table_franchise_fee").DataTable();
    $(".fancybox").fancybox();
});

$('.btn_submit').on('click', function()
{
	var id = $(this).val();
	$('#modal_summary_body').html('Submit Tax PPH for '+$('#date_'+id).text()+'?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_submit_'+id).submit();
	});
});

$('.btn_finance_reject').on('click', function()
{
	var id = $(this).val();
	$('#finance_review').val('0');
	$('#modal_summary_body').html('Reject Tax PPH for '+$('#date_'+id).text()+'?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_finance_'+id).submit();
	});
});

$('.btn_finance_approve').on('click', function()
{
	var id = $(this).val();
	$('#finance_review').val('1');
	$('#modal_summary_body').html('Approve Tax PPH for '+$('#date_'+id).text()+'?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_finance_'+id).submit();
	});
});

$('.btn_owner').on('click', function()
{
	var id = $(this).val();
	$('#modal_summary_body').html('Issue Tax PPH for '+$('#date_'+id).text()+'?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_owner_'+id).submit();
	});
});

$('.btn_paid').on('click', function()
{
	var id = $(this).val();
	$('#modal_summary_body').html('Pay Tax PPH for '+$('#date_'+id).text()+'?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_paid_'+id).submit();
	});
});
</script>
@stop