@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->

<style type="text/css">
	th { font-size: 11px; }
	td { font-size: 11px; }
</style>
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="#"><i class="fa fa-dollar"></i> Cashbond</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							<i class="fa fa-dollar"></i> Cashbond Bank
						</h2>
						<hr>
						<div class="x_content">
							<table id="cashbondTablex" class="table dataTable rwd-table">
								<thead>
									<tr>
										<th rowspan="2">#</th>
										<th colspan="2" class="text-center">Period</th>
										<th rowspan="2">Cashbond Type</th>
										<th rowspan="2">Budget Request</th>
										<th rowspan="2">Budget Issued</th>
										<th rowspan="2">Cashout</th>
										<th rowspan="2">Remainder</th>
										<th rowspan="2">Add Amount</th>
										<th rowspan="2">Grand Total</th>
										<th rowspan="2">Branch</th>
										<th rowspan="2">Transfer Receipt</th>
											<th rowspan="2">Remainder Receipt</th>
										<th rowspan="2">Status</th>
										<th rowspan="2">Action</th>
									</tr>
									<tr>
										<th>Start Date</th>
										<th>End Date</th>

									</tr>
								</thead>
								<tbody>
									@foreach($cashbonds as $key => $cashbond)
									<tr>
										<td data-th="#">{{$number++}}</td>
										<td data-th="Start Date">{{date("d M Y", strtotime($cashbond->start_date))}}</td>
										<td data-th="End Date">{{date("d M Y", strtotime($cashbond->end_date))}}</td>
										<td data-th="Type">{{ $cashbond->cashbond_type }}</td>
										<td data-th="Budget Request">{{ number_format($cashbond->budget_request,0) }}</td>
										<td data-th="Budget Issued">{{ number_format($cashbond->budget_issued,0) }}</td>
										<td data-th="Cashout">{{ number_format($cashbond->cashout,0) }}</td>
										<td data-th="Remainder">{{ number_format($cashbond->remainder,0) }}</td>
										<td data-th="Add Amount">{{ number_format($cashbond->add_amount,0) }}</td>
										<td data-th="Grand Total">{{ number_format($cashbond->cashout + $cashbond->add_amount,0) }}</td>
										<td data-th="Branch">{{ $cashbond->branch->branch_name }}</td>
										<td data-th="Attachment">
											@if($cashbond->transfer_receipt == null)
											Tidak ada lampiran
											@else
											<a target="_blank" href="{{Config::get('constants.path.uploads')}}/cashbond/{{$cashbond->transfer_receipt}}"><i class="fa fa-download"></i>&nbsp;Download</a>
											@endif
										</td>
										<td data-th="Attachment">
											@if($cashbond->remainder_receipt == null)
											Tidak ada lampiran
											@else
											<a target="_blank" href="{{Config::get('constants.path.uploads')}}/cashbond/remainder/{{$cashbond->remainder_receipt}}"><i class="fa fa-download"></i>&nbsp;Download</a>
											@endif
										</td>
										<td data-th="Status">
											<span class=""><span class="label label-success"><i class="fa fa-check"></i>&nbsp;{{$cashbond->status_code}}</span><span>
											</td>
										<td align="center">
											@if($cashbond->status_code=='Request')
												<a href="{{ URL::route('editor.cashbond.edit', [$cashbond->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
											@else

												<a href="{{ URL::route('editor.cashbond.view', [$cashbond->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>

											@endif
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						{{ $cashbonds->links() }}
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#cashbondTable").DataTable();
	});
</script>
<script>
	$(".waitingowner").on("submit", function(){
		return confirm("Do you want to submit this cashbond?");
	});
	$(".waitingfinalize").on("submit", function(){
		return confirm("Do you want to transfer this cashbond?");
	});

</script>
@stop