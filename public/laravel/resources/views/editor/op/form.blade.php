@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="{{ URL::route('editor.maintenance.index') }}"><i class="fa fa-industry"></i> Operational Expense</a></li>
		<li class="active">
			@if(isset($op))
			<i class="fa fa-pencil"></i> Edit
			@else
			<i class="fa fa-plus"></i> Create
			@endif
		</li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-12">
					<div class="x_panel">
						<h2>

							Operational Expense

						</h2>
					</div>
					<hr>
					<div class="col-md-12">
					@include('errors.error')
					@if(isset($op))
					{!! Form::model($op, array('route' => ['editor.op.update', $op->id], 'files' => 'true', 'method' => 'PUT', 'class'=>'update', 'id' => 'form_payroll'))!!}
					@else
					{!! Form::open(array('route' => 'editor.op.store', 'files' => 'true', 'class'=>'create', 'id' => 'form_payroll'))!!}
					@endif
					{{ csrf_field() }}
						<div class="x_content">
						@if(isset($op))
						    {{ Form::label('start_date', 'Start Date') }}
							{{ Form::text('start_date', old('start_date'), array('class' => 'form-control', 'placeholder' => 'Start Date', 'required' => 'true')) }}<br/>

							{{ Form::label('end_date', 'End Date') }}
							{{ Form::text('end_date', old('end_date'), array('class' => 'form-control', 'placeholder' => 'End Date', 'required' => 'true')) }}<br/>

							{{ Form::label('franchise_amortization', 'franchise amortization') }}
							{{ Form::text('franchise_amortization', old('franchise_amortization'), array('class' => 'form-control', 'placeholder' => 'franchise amortization', 'required' => 'true')) }}<br/>

							{{ Form::label('asset_deprecation', 'asset deprecation') }}
							{{ Form::text('asset_deprecation', old('asset_deprecation'), array('class' => 'form-control', 'placeholder' => 'asset deprecation', 'required' => 'true')) }}<br/>


							{{ Form::label('rent_ruko', 'rent ruko') }}
							{{ Form::number('rent_ruko', old('rent_ruko'), array('class' => 'form-control', 'placeholder' => 'rent ruko', 'required' => 'true')) }}<br/>


							<br>
						@else

							 {{ Form::label('start_date', 'Start Date') }}
							{{ Form::text('start_date', old('start_date'), array('class' => 'form-control', 'placeholder' => 'Start Date', 'required' => 'true')) }}<br/>

							{{ Form::label('end_date', 'End Date') }}
							{{ Form::text('end_date', old('end_date'), array('class' => 'form-control', 'placeholder' => 'End Date', 'required' => 'true')) }}<br/>

							{{ Form::label('franchise_amortization', 'franchise amortization') }}
							{{ Form::text('franchise_amortization', old('franchise_amortization'), array('class' => 'form-control', 'placeholder' => 'franchise amortization', 'required' => 'true')) }}<br/>

							{{ Form::label('asset_deprecation', 'asset deprecation') }}
							{{ Form::text('asset_deprecation', old('asset_deprecation'), array('class' => 'form-control', 'placeholder' => 'asset deprecation', 'required' => 'true')) }}<br/>


							{{ Form::label('rent_ruko', 'rent ruko') }}
							{{ Form::number('rent_ruko', old('rent_ruko'), array('class' => 'form-control', 'placeholder' => 'rent ruko', 'required' => 'true')) }}<br/>



						@endif
							<button type="submit" id="btn_confirm" class="btn btn-success"><i class="fa fa-check"></i> Save</button>

						</div>

					{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop


@section('scripts')
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>


@stop
