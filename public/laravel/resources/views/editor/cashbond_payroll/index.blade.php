@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->

<style type="text/css">
	th { font-size: 11px; }
	td { font-size: 11px; }
</style>
<section class="content-header hidden-xs">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><i class="fa fa-dollar"></i> Cashbond Payroll</li>
  </ol>
</section>
@actionStart('cashbond_payroll', 'read')
<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-12">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-dollar"></i> Cashbond Payroll
		                	@actionStart('cashbond_payroll', 'create')
		                	<a href="{{ URL::route('editor.cashbond_payroll.create') }}" class="btn btn-success btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
		                	@actionEnd
	                	</h2>
		                <hr>
			           <div class="x_content">
			                <table id="table_cashbond_payrollx" class="table dataTable rwd-table">
							  	<thead>
							  	  	<tr>
								      	<th width="5%">#</th>
								      	<th>Date</th>
								      	<th>Attachment Receipt</th>
								      	<th>Status</th>
								      	<th>Total</th>
								      	<th>Action Status</th>
								      	<th width="10%">Action</th>
							    	</tr>
							  	</thead>
							  	<tbody>
								@foreach($cashbond_payrolls as $key => $cashbond_payroll)
									<tr>
										<td data-th="#">{{$number++}}</td>
										<td id="date_{{$cashbond_payroll->id}}" data-th="Date">{{date("D, d M Y", strtotime($cashbond_payroll->date))}}</td>
										<td data-th="Attachment">
											@if($cashbond_payroll->attachment_receipt == null)
											Tidak ada lampiran
											@else
											<a target="_blank" href="{{Config::get('constants.path.uploads')}}/cashbondpayroll/attachment_receipt/{{$cashbond_payroll->attachment_receipt}}"><i class="fa fa-download"></i>&nbsp;Download</a>

											@endif
										</td>
										<td data-th="Status">
											@if($cashbond_payroll->status == 0)
											<span class="label label-danger"><i class="fa fa-unlock"></i> Open</span>
											@elseif($cashbond_payroll->status == 1)
											<span class="label label-warning"><i class="fa fa-lock"></i> Waiting Finance Approval</span>
											@elseif($cashbond_payroll->status == 2)
											<span class="label label-warning"><i class="fa fa-lock"></i> Waiting Owner Approval</span>
											@elseif($cashbond_payroll->status == 3)
											<span class="label label-warning"><i class="fa fa-lock"></i> Waiting Finance Payment</span>
											@elseif($cashbond_payroll->status == 4)
											<span class="label label-success"><i class="fa fa-check"></i> Paid</span>
											@endif
										</td>
										<td data-th="Total">{{number_format($cashbond_payroll->cashbond_payroll_detail->sum('cashbond'), 2)}}</td>
										<td>
											@if($cashbond_payroll->status == 0)
						                		@actionStart('cashbond_payroll', 'submit')
							                	{!! Form::open(['route' => ['editor.cashbond_payroll.submit', $cashbond_payroll->id], 'method' => 'PUT', 'id' => 'form_submit_'.$cashbond_payroll->id]) !!}
							                	<button type="button" class="btn btn-sm btn-success btn_submit" value="{{$cashbond_payroll->id}}">
							                		<i class="fa fa-check"></i> Submit
						                		</button>
							                	{!! Form::close() !!}
							                	@actionEnd

						                	@elseif($cashbond_payroll->status == 1)
						                		@actionStart('cashbond_payroll', 'paid')
							                	{!! Form::open(['route' => ['editor.cashbond_payroll.finance_approve', $cashbond_payroll->id], 'method' => 'PUT', 'id' => 'form_finance_'.$cashbond_payroll->id]) !!}
							                	<input type="hidden" name="review" value="0" id="finance_review">
						                		<table>
					                				<tr>
					                					<td>
										                	<button type="button" class="btn btn-sm btn-danger btn_finance_reject" value="{{$cashbond_payroll->id}}" title="Finance Reject">
										                		<i class="fa fa-remove"></i>
									                		</button>
									                	</td>
									                	<td>
										                	<button type="button" class="btn btn-sm btn-success btn_finance_approve" value="{{$cashbond_payroll->id}}" title="Finance Approve">
										                		<i class="fa fa-check"></i>
									                		</button>
									                	</td>
								                	</tr>
							                	</table>
							                	{!! Form::close() !!}
							                	@actionEnd

						                	@elseif($cashbond_payroll->status == 2)
						                		@actionStart('cashbond_payroll', 'issued')
							                	{!! Form::open(['route' => ['editor.cashbond_payroll.owner_approve', $cashbond_payroll->id], 'method' => 'PUT', 'id' => 'form_owner_'.$cashbond_payroll->id]) !!}
							                	<button type="button" class="btn btn-sm btn-success btn_owner" value="{{$cashbond_payroll->id}}">
							                		<i class="fa fa-check"></i> Issued
						                		</button>
							                	{!! Form::close() !!}
							                	@actionEnd

						                	@elseif($cashbond_payroll->status == 3)
						                		@actionStart('cashbond_payroll', 'paid')
							                	{!! Form::open(['route' => ['editor.cashbond_payroll.finance_payment', $cashbond_payroll->id], 'method' => 'PUT', 'id' => 'form_paid_'.$cashbond_payroll->id]) !!}
							                	<a class="btn btn-sm btn-success" href="{{ URL::route('editor.cashbond_payroll.detail', [$cashbond_payroll->id]) }}">
							                		<i class="fa fa-check"></i> Paid
						                		</a>
							                	{!! Form::close() !!}
							                	@actionEnd

						                	@endif
										</td>
										<td data-th="">
											<div class="act_tb">
													<div>
														<a href="{{ URL::route('editor.cashbond_payroll.detail', [$cashbond_payroll->id]) }}" class="btn btn-sm btn-default"><i class="fa fa-search"></i></a>
													</div>&nbsp;
													@actionStart('cashbond_payroll', 'delete')
													<div>
														{!! Form::open(['route' => ['editor.cashbond_payroll.delete', $cashbond_payroll->id], 'method' => 'delete']) !!}
														<button type="submit" onclick="return confirm('Delete?')" class="btn btn-sm btn-danger"><i class="fa fa-remove"></i></button>
														{!! Form::close() !!}
													</div>
													@actionEnd
													@if($cashbond_payroll->status == 0)
													@actionStart('cashbond_payroll', 'update')
													<div>
														<a href="{{ URL::route('editor.cashbond_payroll.edit', [$cashbond_payroll->id]) }}" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></a>
													</div>&nbsp;
													@actionEnd
													@endif
													@if($cashbond_payroll->status == 0)

													@endif
											</div>
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
			            </div>
						{{$cashbond_payrolls->links()}}
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop
@actionEnd

@section('modal')
<div class="modal fade" id="modal_summary">
  	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
    		    <h5 class="modal-title"><i class="fa fa-warning"></i> Confirmation!</h5>
      		</div>
      		<div class="modal-body">
        		<div id="modal_summary_body"></div>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-primary" id="btn_confirm">YES</button>
        		<button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#table_cashbond_payroll").DataTable();
    });

$('.btn_submit').on('click', function()
{
	var id = $(this).val();
	$('#modal_summary_body').html('Submit Cashbond Payroll '+$('#date_'+id).text()+'?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_submit_'+id).submit();
	});
});

$('.btn_finance_reject').on('click', function()
{
	var id = $(this).val();
	$('#finance_review').val('0');
	$('#modal_summary_body').html('Reject Cashbond Payroll '+$('#date_'+id).text()+'?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_finance_'+id).submit();
	});
});

$('.btn_finance_approve').on('click', function()
{
	var id = $(this).val();
	$('#finance_review').val('1');
	$('#modal_summary_body').html('Approve Cashbond Payroll '+$('#date_'+id).text()+'?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_finance_'+id).submit();
	});
});

$('.btn_owner').on('click', function()
{
	var id = $(this).val();
	$('#modal_summary_body').html('Issue Cashbond Payroll '+$('#date_'+id).text()+'?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_owner_'+id).submit();
	});
});

$('.btn_paid').on('click', function()
{
	var id = $(this).val();
	$('#modal_summary_body').html('Pay Cashbond Payroll '+$('#date_'+id).text()+'?');
	$('#modal_summary').modal('show');
	$('#btn_confirm').on('click', function()
	{
		$(this).attr('disabled', true);
		$('#form_paid_'+id).submit();
	});
});
</script>
@stop