@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->

<style type="text/css">
	th { font-size: 11px; }
	td { font-size: 11px; }
</style>
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="{{ URL::route('editor.cashbond_payroll.index') }}"><i class="fa fa-dollar"></i> Cashbond Payroll</a></li>
		<li class="active"><i class="fa fa-search"></i> Detail</li>
	</ol>
</section>
@actionStart('cashbond_payroll', 'read')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							<i class="fa fa-dollar"></i> Cashbond Payroll
							@if($cashbond_payroll->status == 0)
							@actionStart('cashbond_payroll', 'update')
							<a href="{{ URL::route('editor.cashbond_payroll.edit', [$cashbond_payroll->id]) }}" class="btn btn-default btn-lg pull-right"><i class="fa fa-pencil"></i> Edit</a>
							@actionEnd
							@endif
						</h2>
						<hr>
						<div class="x_content">
							<h3>
								Period {{ date('F Y', strtotime($cashbond_payroll->date)) }}
								{{-- @if($cashbond_payroll->status == 0)
								<span class="label label-danger">Pending</span>
								@elseif($cashbond_payroll->status == 1)
								<span class="label label-warning">Waiting Finance</span>
								@elseif($cashbond_payroll->status == 2)
								<span class="label label-warning">Waiting Owner</span>
								@elseif($cashbond_payroll->status == 3)
								<span class="label label-success">Done</span>
								@endif --}}
							</h3>
							<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th width="5%">#</th>
										<th>Name</th>
										<th>Amount</th>
										<th>Miscellaneous</th>
										<th>Remark</th>
									</tr>
								</thead>
								<tbody>
									@foreach($cashbond_payroll->cashbond_payroll_detail as $key => $detail)
									<tr>
										<td width="5%">{{$key+1}}</td>
										@if(isset($detail->employee->emp_full_name))
											 <td>{{$detail->employee->emp_full_name}}</td>
										@else
											<td>Resign</td>
										@endif

										<td>{{number_format($detail->cashbond, 2)}}</td>
										<td>{{number_format($detail->miscellaneous, 2)}}</td>
										<td>{{ $detail->remark }}</td>
									</tr>
									@endforeach
								</tbody>
								<tfoot>
									<tr>
										<td colspan="2" align="right"><b>Total</b></td>
										<td><b>{{number_format($cashbond_payroll->cashbond_payroll_detail->sum('cashbond'), 2)}}</b></td>
										<td><b>{{number_format($cashbond_payroll->cashbond_payroll_detail->sum('miscellaneous'), 2)}}</b></td>
									</tr>
								</tfoot>
							</table>

							{{-- APPROVAL AND PAYMENT --}}
							<div class="col-md-12 col-xs-12">
								@if($cashbond_payroll->status == 0)
								@actionStart('cashbond_payroll', 'submit')
								{!! Form::open(['route' => ['editor.cashbond_payroll.submit', $cashbond_payroll->id], 'method' => 'PUT', 'id' => 'form_submit']) !!}
								<button type="button" class="btn btn-lg btn-success" id="btn_submit">
									<i class="fa fa-check"></i> Submit
								</button>
								{!! Form::close() !!}
								@actionEnd

								@elseif($cashbond_payroll->status == 1)
								@actionStart('cashbond_payroll', 'paid')
								{!! Form::open(['route' => ['editor.cashbond_payroll.finance_approve', $cashbond_payroll->id], 'method' => 'PUT', 'id' => 'form_finance']) !!}
								<input type="hidden" name="review" value="0" id="finance_review">
								<table>
									<tr>
										<td>
											<button type="button" class="btn btn-lg btn-danger" id="btn_finance_reject">
												<i class="fa fa-remove"></i> Reject
											</button>
										</td>
										<td>
											<button type="button" class="btn btn-lg btn-success" id="btn_finance_approve">
												<i class="fa fa-check"></i> Approve
											</button>
										</td>
									</tr>
								</table>
								{!! Form::close() !!}
								@actionEnd

								@elseif($cashbond_payroll->status == 2)
								@actionStart('cashbond_payroll', 'issued')
								{!! Form::open(['route' => ['editor.cashbond_payroll.owner_approve', $cashbond_payroll->id], 'method' => 'PUT', 'id' => 'form_owner']) !!}
								<button type="button" class="btn btn-lg btn-success" id="btn_owner">
									<i class="fa fa-check"></i> Issued
								</button>
								{!! Form::close() !!}
								@actionEnd

								@elseif($cashbond_payroll->status == 3)
								@actionStart('cashbond_payroll', 'paid')
								{!! Form::open(['route' => ['editor.cashbond_payroll.finance_payment', $cashbond_payroll->id], 'method' => 'PUT', 'id' => 'form_paid', 'files' => 'true']) !!}

								{{ Form::label('attachment_receipt', 'Attachment Receipt') }}
								{{ Form::file('attachment_receipt') }}<br/>
								<button type="button" class="btn btn-lg btn-success" id="btn_paid">
									<i class="fa fa-check"></i> Paid
								</button>
								{!! Form::close() !!}
								@actionEnd

								@endif

							</div>
							{{-- APPROVAL AND PAYMENT --}}

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
@stop
@actionEnd

@section('modal')
<div class="modal fade" id="modal_summary">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><i class="fa fa-warning"></i> Confirmation!</h5>
			</div>
			<div class="modal-body">
				<div id="modal_summary_body"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btn_confirm">YES</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
	$('#btn_submit').on('click', function()
	{
		$('#modal_summary_body').html('Submit Cashbond Payroll?');
		$('#modal_summary').modal('show');
		$('#btn_confirm').on('click', function()
		{
			$(this).attr('disabled', true);
			$('#form_submit').submit();
		});
	});

	$('#btn_finance_reject').on('click', function()
	{
		$('#finance_review').val('0');
		$('#modal_summary_body').html('Reject Cashbond Payroll?');
		$('#modal_summary').modal('show');
		$('#btn_confirm').on('click', function()
		{
			$(this).attr('disabled', true);
			$('#form_finance').submit();
		});
	});

	$('#btn_finance_approve').on('click', function()
	{
		$('#finance_review').val('1');
		$('#modal_summary_body').html('Approve Cashbond Payroll?');
		$('#modal_summary').modal('show');
		$('#btn_confirm').on('click', function()
		{
			$(this).attr('disabled', true);
			$('#form_finance').submit();
		});
	});

	$('#btn_owner').on('click', function()
	{
		$('#modal_summary_body').html('Issue Cashbond Payroll?');
		$('#modal_summary').modal('show');
		$('#btn_confirm').on('click', function()
		{
			$(this).attr('disabled', true);
			$('#form_owner').submit();
		});
	});

	$('#btn_paid').on('click', function()
	{
		$('#modal_summary_body').html('Pay Cashbond Payroll?');
		$('#modal_summary').modal('show');
		$('#btn_confirm').on('click', function()
		{
			$(this).attr('disabled', true);
			$('#form_paid').submit();
		});
	});
</script>
@stop