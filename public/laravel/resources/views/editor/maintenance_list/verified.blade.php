<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>
@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.maintenance_list.index') }}"><i class="fa fa-dollar"></i>Maintenance</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($cashbond))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i>
							@endif
							&nbsp;Maintenance Finalized
						</h2>
					</div>
					<hr>
					<div class="col-md-6">
						<div class="x_content">
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">

								@include('errors.error')
								{!! Form::model($cashbond, array('route' => ['editor.maintenance_list.storeclosing', $cashbond->id],  'class'=>'create'))!!}
								{{ csrf_field() }}


								<table id="table_revenuex" class="table  dataTable rwd-table">
									<tbody>
										<tr>
											<th width="30%">Start Date</th>
											<td align="left">{{date('d-M-Y', strtotime($cashbond->start_date))}}</td>
										</tr>
										<tr>
											<th width="30%">End Date</th>
											<td align="left">{{date('d-M-Y', strtotime($cashbond->end_date))}}</td>
										</tr>
										<tr>
											<th width="30%">Budget Request</th>
											<td align="left">{{number_format($cashbond->budget_request,0)}}</td>
										</tr>
										<tr>
											<th width="30%">Cashout</th>
											<td align="left">{{number_format($cashbond->cashout,0)}}</td>
										</tr>
										<tr>
											<th width="30%">Remainder</th>
											<td align="left">{{number_format($cashbond->budget_issued-$cashbond->cashout,0)}} </td>
										</tr>
										<tr>
											<th width="30%">Add Amount</th>
											<td align="left">{{number_format($cashbond->add_amount,0)}}</td>
										</tr>
										<tr>
											<th width="30%">Grand Total</th>
											<td align="left">{{number_format($cashbond->cashout + $cashbond->add_amount,0)}}</td>
										</tr>
										<tr>
											<th width="30%">Comment</th>
											<td align="left">{{$cashbond->comment}}</td>
										</tr>
									</tbody>
								</table>

								{!! Form::close() !!}
								{!! Form::open(array('route' => ['editor.maintenance_list.updateverified', $cashbond->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_cashbondverified'))!!}
								{{ csrf_field() }}
								<button  type="button" data-toggle="modal" data-target="#modal_cashbondverified" class="btn btn-success pull-right" style="margin-right: 10px"><i class="fa fa-check"></i> Verified</button>
								{!! Form::close() !!}

								{!! Form::open(array('route' => ['editor.maintenance_list.updatereject', $cashbond->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_cashbondreject'))!!}
								{{ csrf_field() }}
								<button  type="button" data-toggle="modal" data-target="#modal_cashbondreject" class="btn btn-danger pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Reject</button>
								{!! Form::close() !!}
								<a href="{{ URL::route('editor.maintenance_list.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a><br>
								<hr>

								{{-- @foreach($array_all AS $all) --}}
								<table id="drinkTable" class="table table-striped dataTable">
									<thead>
										<tr>
											<th>Date</th>
											<th>Nota Number</th>
											<th >Description</th>
											<th>Amount</th>
											<th>Attachment</th>
										</tr>
									</thead>
									<tbody>

									@foreach($cashbond_detail as $key => $cashbond_details)
											<tr>

												<td>{{$cashbond_details->date}}</td>
												<td>{{$cashbond_details->nota_number}}</td>
												<td>{{$cashbond_details->description }}</td>
												<td>{{ number_format($cashbond_details->amount,0) }}</td>
												<td data-th="Attachment">
													@if($cashbond_details->cashbond_attachment == null)
													Tidak ada lampiran
													@else
													<a target="_blank" href="{{Config::get('constants.path.uploads')}}/cashop_detail/{{$cashbond_details->cashbond_attachment}}"><i class="fa fa-download"></i>&nbsp;Download</a>

													@endif
												</td>


											</tr>
								@endforeach



									</tbody>
								</table> <br>


							</div>
						</div>
					</div>
				</div>
				<hr>
			</div>
		</div>
	</div>
</div>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_cashbondreject">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Reject this Maintenance?</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit_reject" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_cashbondverified">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Verified this Maintenance?</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit_verified" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
	$('#btn_submit_reject').on('click', function()
	{
		$('#form_cashbondreject').submit();
	});

	$('#btn_submit_verified').on('click', function()
	{
		$('#form_cashbondverified').submit();
	});
</script>
@stop