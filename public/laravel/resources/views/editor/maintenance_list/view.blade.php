@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.maintenance_list.index') }}"><i class="fa fa-dollar"></i> Maintenance</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($cashbond))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i>
							@endif
							&nbsp;Maintenance Finalized
						</h2>
					</div>
					<hr>
					<div class="col-md-6">
						<div class="x_content">
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">
								<table id="table_revenuex" class="table  dataTable rwd-table">
									<tbody>
										<tr>
											<th width="30%">Start Date</th>
											<td align="left">{{date('d-M-Y', strtotime($cashbond->start_date))}}</td>
										</tr>
										<tr>
											<th width="30%">End Date</th>
											<td align="left">{{date('d-M-Y', strtotime($cashbond->end_date))}}</td>
										</tr>
										<tr>
											<th width="30%">Budget Request</th>
											<td align="left">{{number_format($cashbond->budget_request,0)}}</td>
										</tr>
										<tr>
											<th width="30%">Cashout</th>
											<td align="left">{{number_format($cashbond->cashout,0)}}</td>
										</tr>
										<tr>
											<th width="30%">Remainder</th>
											<td align="left">{{number_format($cashbond->budget_issued-$cashbond->cashout,0)}}</td>
										</tr>
										<tr>
											<th width="30%">Add Amount</th>
											<td align="left">{{number_format($cashbond->add_amount,0)}}</td>
										</tr>
										<tr>
											<th width="30%">Grand Total</th>
											<td align="left">{{number_format($cashbond->cashout + $cashbond->add_amount,0)}}</td>
										</tr>
										<tr>
											<th width="30%">Comment</th>
											<td align="left">{{$cashbond->comment}}</td>
										</tr>
									</tbody>
								</table>
								<hr>
								<div class="div_overflow">
									<table id="invoiceTablex" class="table dataTable rwd-table">
										<thead>
											<tr>
												<th>#</th>
												<th>Date</th>
												<th>Nota Number</th>
												<th>Amount</th>
												<th>Attachment</th>


											</tr>
										</thead>
										<tbody>
											@foreach($cashbond_detail as $key => $cashbond_details)
											<tr>
												<td>{{$key+1}}</td>
												<td>{{$cashbond_details->date}}</td>
												<td>{{$cashbond_details->nota_number}}</td>
												<td>{{ number_format($cashbond_details->amount,0) }}</td>
												<td data-th="Attachment">
													@if($cashbond_details->cashbond_attachment == null)
													Tidak ada lampiran
													@else
													<a target="_blank" href="{{Config::get('constants.path.uploads')}}/maintenance_list/{{$cashbond_details->cashbond_attachment}}"><i class="fa fa-download"></i>&nbsp;Download</a>

													@endif
												</td>


											</tr>
											@endforeach
										</tbody>
									</table>
								</div>


							</div>
						</div>

						<a href="{{ URL::route('editor.maintenance_listbank.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a><br>
					</div>

				</div>
				<hr>
			</div>
		</div>
	</div>
</div>
</section>

@stop
