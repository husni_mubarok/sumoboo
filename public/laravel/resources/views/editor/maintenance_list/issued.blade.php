<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>

@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.maintenance_list.index') }}"><i class="fa fa-dollar"></i> Maintenance</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($cashbond))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i>
							@endif
							&nbsp;Maintenance Issued
						</h2>
					</div>
					<hr>
					@include('errors.error')
					{!! Form::model($cashbond, array('route' => ['editor.maintenance_list.updateissued', $cashbond->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_cashbondfinalize', 'files' => 'true'))!!}
					{{ csrf_field() }}
					<div class="col-md-6">
						<div class="x_content">
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">

								{{ Form::label('start_date', 'Start Date') }}
								<input type="text" class="form-control" name="start_date" value="{{date('d-M-Y', strtotime($cashbond->start_date))}}" disabled="disabled"><br/>

								{{ Form::label('end_date', 'End Date') }}<br/>
								<input type="text" class="form-control" name="start_date" value="{{date('d-M-Y', strtotime($cashbond->end_date))}}" disabled="disabled"><br/>

								{{ Form::label('budget_request', 'Budget Request') }}<br/>
								<input type="text" class="form-control" name="start_date" value="{{number_format($cashbond->budget_request,0)}}" disabled="disabled"><br/>

								{{ Form::label('budget_issued_show', 'Budget Issued') }}
								<input type="text" class="form-control" name="budget_issued_show" value="{{number_format($cashbond->budget_issued,0)}}" id="budget_issued_show" oninput="cal_sparator();">
								{{ Form::hidden('budget_issued', old('budget_issued'), array('id' => 'budget_issued')) }}<br/>

							</div>
						</div>
						<button type="button" data-toggle="modal" data-target="#modal_cashbondfinalize" class="btn btn-success pull-right"><i class="fa fa-check"></i> Issued</button>
						<a href="{{ URL::route('editor.maintenance_list.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
					</div>
				</div>
				<hr>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_cashbondfinalize">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Issued this Maintenance?</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
	$('#btn_submit').on('click', function()
	{
		$('#form_cashbondfinalize').submit();
	});

	function cal_sparator() {
	var budget_issued_show = document.getElementById('budget_issued_show').value;
	var result = document.getElementById('budget_issued');
	var rsbudgetissued = (budget_issued_show);
	result.value = rsbudgetissued.replace(/,/g, "");
}

window.onload= function(){

	n2= document.getElementById('budget_issued_show');

	n2.onkeyup=n2.onchange= function(e){
		e=e|| window.event;
		var who=e.target || e.srcElement,temp;
		if(who.id==='budget_issued')  temp= validDigits(who.value,0);
		else temp= validDigits(who.value);
		who.value= addCommas(temp);
	}
	n2.onblur= function(){
		var
		temp2=parseFloat(validDigits(n2.value));
		if(temp2)n2.value=addCommas(temp2.toFixed(0));
	}

}
</script>
@stop