@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->

<style type="text/css">
	th { font-size: 11px; }
	td { font-size: 11px; }
</style>
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="#"><i class="fa fa-file-text-o"></i> Invoice Utilization</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							<i class="fa fa-file-text-o"></i> Invoice Utilization
							@actionStart('invoice', 'create')
							<a href="{{ URL::route('editor.invoice.create') }}" class="btn btn-success btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
							@actionEnd
						</h2>
						<hr>

						<ul class="nav nav-tabs">
							<li class="active"><a  href="{{ URL::route('editor.invoice.index') }}">Invoice  Utilization</a></li>
							<li><a href="{{ URL::route('editor.invoice.bank') }}">Invoice Utilization Bank</a></li>
						</ul>
						<div class="x_content">
							<br>
							<form class="form-inline" >
								<div class="form-group">
									<select name="type" class="form-control" id="type">

										<option value="">All</option>
										@foreach($invoice_type_list as $type)
											@if($type->id == $invoice_type)
												<option value="{{ $type->id }}" selected>{{ $type->inv_type_name }}</option>
											@else
												<option value="{{ $type->id }}">{{ $type->inv_type_name }}</option>
											@endif

										@endforeach
									</select>
								</div>
								<button type="submit" class="btn btn-default">Filter</button>
							</form>
							<br>
							<table id="invoiceTablex" class="table dataTable rwd-table">
								<thead>
									<tr>
										<th>#</th>
										<th>Invoice Type</th>
										<th>Invoice Date</th>
										<th>Period</th>
										<th>Vendor</th>
										<th>Bank</th>
										<th>Rek No</th>
										<th>Ref No</th>
										<th>Add Cost</th>
										<th>Add Cost Notes</th>
										<th>Total Invoice</th>
										<th>Grand Total</th>
										<th>Attachment</th>
										<th>Branch</th>
										<th>Status</th>
										<th>Action Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
										@forelse($invoices as $key => $invoice)
											<tr>
											<td data-th="#">{{$number++}}</td>
											<td data-th="Invoice Type">{{$invoice->inv_type_name}}</td>
											<td data-th="Invoice Date">{{date("d M Y", strtotime($invoice->invoice_date))}}</td>
											<td data-th="Period">{{ date('Y F', strtotime($invoice->year.'-'.$invoice->month)) }}</td>
											<td data-th="Vendor">{{$invoice->vendor_name}}</td>
											<td data-th="Bank">{{$invoice->invoice_bank}}</td>
											<td data-th="Rek No">{{$invoice->invoice_rekening}}</td>
											<td data-th="Ref No">{{$invoice->reference_no}}</td>
											<td data-th="Add Cost">{{ number_format($invoice->additional_cost,0) }}</td>
											<td data-th="Add Cost Desc">{{$invoice->add_cost_desc}}</td>
											<td data-th="Total">{{ number_format($invoice->invoice_total,0) }}</td>
											<td data-th="Grand Total">{{ number_format($invoice->additional_cost+$invoice->invoice_total,0) }}</td>
											<!-- <td>
												@if($invoice->invoice_attachment == null)
												<br/><a class="fancybox" rel="group" href="{{Config::get('constants.path.uploads')}}/invoice/placeholder.png"><img src="{{Config::get('constants.path.uploads')}}/invoice/thumbnail/placeholder.png" class="img-thumbnail img-responsive" /></a><br/>
												@else
												<br/><a target="_blank" href="{{Config::get('constants.path.uploads')}}/invoice/{{$invoice->invoice_attachment}}"><img src="{{Config::get('constants.path.uploads')}}/invoice/thumbnail/{{$invoice->invoice_attachment}}" class="img-thumbnail img-responsive" height="42" width="42"/></a>
												<br/>
												@endif
											</td>  -->
											<td data-th="Attachment">
												@if($invoice->invoice_attachment == null)
												Tidak ada lampiran
												@else
												<a target="_blank" href="{{Config::get('constants.path.uploads')}}/invoice/{{$invoice->invoice_attachment}}"><i class="fa fa-download"></i>&nbsp;Download</a>

												@endif
											</td>
											<td data-th="branch">{{ $invoice->branch_name }}</td>
											<td data-th="Status">

											@if($invoice->approved==0)
											<span class="label label-danger"><i class="fa fa-unlock"></i>
												Request
												<span>
													@elseif($invoice->approved==1 & $invoice->paid=='')
													<span class="label label-warning"><i class="fa fa-history"></i>
														Approved
													</span>
													@elseif($invoice->paid==1)
													<span class="label label-warning"><i class="fa fa-history"></i>
														Paid
													</span>
													@endif

												</td>
												<td data-th="Action Status" align="center">
													@if($invoice->approved==0)
													{!! Form::open(array('route' => ['editor.invoice.updaterequset', $invoice->id], 'method' => 'PUT', 'class'=>'approved'))!!}
													{{ csrf_field() }}
													@actionStart('invoice', 'issued')
													<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i>&nbsp;Issued</button>
													@actionEnd
													{!! Form::close() !!}
													@elseif($invoice->approved==1 & $invoice->paid=='')
													@actionStart('invoice', 'paid')
													<a href="{{ URL::route('editor.invoice.paid', [$invoice->id]) }}" class="btn btn-success btn-sm"><i class="fa fa-money"></i>&nbsp;Paid</a>
													@actionEnd

													@elseif($invoice->paid==1)
													-
													@endif
												</td>

												<td align="center">
													@if($invoice->approved==0)
													@actionStart('invoice', 'update')
													<a href="{{ URL::route('editor.invoice.edit', [$invoice->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
													@actionEnd

													@actionStart('invoice', 'delete')
													{!! Form::open(array('route' => ['editor.invoice.delete', $invoice->id], 'method' => 'delete', 'class'=>'delete'))!!}
													{{ csrf_field() }}
													<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></button>
													{!! Form::close() !!}
													@actionEnd

													@else


													<a href="{{ URL::route('editor.invoice.view', [$invoice->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>

													@actionStart('invoice', 'delete')
													{!! Form::open(array('route' => ['editor.invoice.delete', $invoice->id], 'method' => 'delete', 'class'=>'delete'))!!}
													{{ csrf_field() }}
													<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></button>
													{!! Form::close() !!}
													@actionEnd

													@endif
												</td>
												</tr>
										@empty
											<tr><td colspan="18" ><center><h5>No Data</h5></center></td></tr>
										@endforelse
									</tbody>
								</table>
							</div>
								{{ $invoices->links() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
	@stop

	@section('scripts')
	<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
	<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
	<script>
		$(document).ready(function () {
			$("#invoiceTable").DataTable();
		});
	</script>
	<script>
		$(".approved").on("submit", function(){
			return confirm("Do you want to issued this invoice?");
		});

		$(".paid").on("submit", function(){
			return confirm("Do you want to paid this invoice?");
		});
	</script>
	<script>
		$(".delete").on("submit", function(){
			return confirm("Delete this invoice?");
		});
	</script>
	@stop