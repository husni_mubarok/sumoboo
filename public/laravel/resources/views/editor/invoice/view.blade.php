@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.invoice.index') }}"><i class="fa fa-file-text-o"></i> Invoice Utilization</a></li>
	</ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-5">
					<div class="x_panel">
						<h2>
							@if(isset($invoice))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							&nbsp;Invoice Utilization
						</h2>
						<hr>
						<div class="x_content">
							@include('errors.error')
							@if(isset($invoice))
							{!! Form::model($invoice, array('route' => ['editor.invoice.update', $invoice->id], 'method' => 'PUT', 'class'=>'update'))!!}
							@else
							{!! Form::open(array('route' => 'editor.invoice.store', 'class'=>'create', 'files' => 'true'))!!}
							@endif
							{{ csrf_field() }}
							<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
								{{ Form::label('Invoice Type') }}
								{{ Form::select('invoice_type_id', $invoice_type_list, old('invoice_type_id'), array('class' => 'form-control', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('invoice_date', 'Invoice Date') }}  
								<input type="text" class="form-control" name="invoice_date" value="{{date('d-M-Y', strtotime($invoice->invoice_date))}}" disabled="disabled"><br/> 

								{{ Form::label('month', 'Month') }}
								{{ Form::select('month', $month_list, old('month'), ['class' => 'form-control', 'placeholder' => 'Select Month', 'id' => 'month', 'disabled' => 'disabled']) }}<br>

								{{ Form::label('year', 'Year') }}
								{{ Form::text('year', old('year'), array('class' => 'form-control', 'placeholder' => 'Year*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('Vendor') }}
								{{ Form::select('vendor_id', $vendor_list, old('vendor_id'), array('class' => 'form-control', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('invoice_bank', 'Bank') }}
								{{ Form::text('invoice_bank', old('invoice_bank'), array('class' => 'form-control', 'placeholder' => 'Bank*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('invoice_rekening', 'Rek No') }}
								{{ Form::text('invoice_rekening', old('invoice_rekening'), array('class' => 'form-control', 'placeholder' => 'Rek No*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('additional_cost', 'Total') }}  
								<input type="text" class="form-control" name="add_cost_desc" value="{{$invoice->add_cost_desc,0}}" disabled="disabled"><br/>

								{{ Form::label('add_cost_desc', 'Additional Cost Notes') }}  
								<input type="text" class="form-control" name="invoice_total" value="{{number_format($invoice->invoice_total,0)}}" disabled="disabled"><br/>

								{{ Form::label('invoice_total', 'Total') }}  
								<input type="text" class="form-control" name="invoice_total" value="{{number_format($invoice->invoice_total,0)}}" disabled="disabled"><br/>

								{{ Form::label('comment', 'Comment') }}
								{{ Form::text('comment', old('comment'), array('class' => 'form-control', 'placeholder' => 'Comment*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('paid_date', 'Paid Date') }}
								{{ Form::text('paid_date', old('paid_date'), array('class' => 'form-control', 'placeholder' => 'Paid Date*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/> 

								{{ Form::label('reference_no', 'Reference No') }}
								{{ Form::text('reference_no', old('reference_no'), array('class' => 'form-control', 'placeholder' => 'Reference No*', 'required' => 'true', 'id' => 'reference_no', 'disabled' => 'disabled')) }}<br/>
<!-- 
								{{ Form::label('image', 'Image') }}
	                        	{{ Form::file('image') }}<br/> -->
 
								<a href="{{ URL::route('editor.invoice.bank') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
@stop

