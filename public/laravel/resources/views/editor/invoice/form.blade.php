<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>
@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.invoice.index') }}"><i class="fa fa-file-text-o"></i> Invoice Utilization</a></li>
	</ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-5">
					<div class="x_panel">
						<h2>
							@if(isset($invoice))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							&nbsp;Invoice Utilization
						</h2>
						<hr>
						<div class="x_content">
							@include('errors.error')
							@if(isset($invoice))
							{!! Form::model($invoice, array('route' => ['editor.invoice.update', $invoice->id], 'method' => 'PUT', 'class'=>'update', 'files' => 'true', 'id'=>'form_invoice'))!!}
							@else
							{!! Form::open(array('route' => 'editor.invoice.store', 'class'=>'create', 'files' => 'true', 'id'=>'form_invoice'))!!}
							@endif
							{{ csrf_field() }}
							<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
								{{ Form::label('Invoice Type') }}
								{{ Form::select('invoice_type_id', $invoice_type_list, old('invoice_type_id'), array('class' => 'form-control', 'placeholder' => 'Select Invoice Type', 'required' => 'true', 'id' => 'invoice_type_id')) }}<br/>

								{{ Form::label('invoice_date', 'Invoice Date') }}
								{{ Form::text('invoice_date', old('invoice_date'), array('class' => 'form-control', 'placeholder' => 'Invoice Date*', 'required' => 'true', 'id' => 'invoice_date')) }}<br/>

								{{ Form::label('month', 'Month') }}
								{{ Form::select('month', $month_list, old('month'), ['class' => 'form-control', 'placeholder' => 'Select Month', 'id' => 'month']) }}<br>

								{{ Form::label('year', 'Year') }} 

								@if(isset($invoice))
								{{ Form::number('year', old('year'), ['class' => 'form-control', 'min' => date('Y') - 20, 'max' => date('Y') + 20, 'placeholder' => 'Input Year', 'id' => 'year', 'disabled' => 'true']) }}
								{{ Form::hidden('year', old('year')) }}
								@else
								{{ Form::number('year', old('year', date('Y')), ['class' => 'form-control', 'min' => date('Y') - 20, 'max' => date('Y') + 20, 'placeholder' => 'Input Year', 'id' => 'year']) }}
								@endif
								<br/>

								{{ Form::label('Vendor') }}
								@if(isset($invoice)) 
								{{ Form::select('vendor_id', $vendor_list, old('vendor_id'), array('class' => 'form-control', 'required' => 'true', 'placeholder' => 'Select Vendor', 'id' => 'vendor_id')) }}
								@else
								{{ Form::select('vendor_id', $vendor_list, old('vendor_id'), array('class' => 'form-control', 'required' => 'true', 'placeholder' => 'Select Vendor', 'id' => 'vendor_id', 'disabled' => 'disabled')) }}
								@endIf
								<br/>

								{{ Form::label('invoice_bank', 'Bank') }}
								{{ Form::text('invoice_bank', old('invoice_bank'), array('class' => 'form-control', 'placeholder' => 'Bank*', 'required' => 'true', 'id' => 'invoice_bank')) }}<br/>

								{{ Form::label('invoice_rekening', 'Rek No') }}
								{{ Form::text('invoice_rekening', old('invoice_rekening'), array('class' => 'form-control', 'placeholder' => 'Rek No*', 'required' => 'true', 'id' => 'invoice_rekening')) }}<br/> 

								{{ Form::label('invoice_total', 'Total') }}  
								@if(isset($invoice)) 
								{{ Form::text('invoice_total_show',number_format($invoice->invoice_total,0), array('class' => 'form-control', 'placeholder' => 'Invoice Total*', 'required' => 'true', 'id' => 'invoice_total_show', 'oninput' => 'cal_sparator();')) }}<br/>
								@else
								{{ Form::text('invoice_total_show',old('invoice_total_show'), array('class' => 'form-control', 'placeholder' => 'Invoice Total*', 'required' => 'true', 'id' => 'invoice_total_show', 'oninput' => 'cal_sparator();')) }}<br/>
								@endif 
								{{ Form::hidden('invoice_total', old('invoice_total'), array('id' => 'invoice_total')) }}

								{{ Form::label('additional_cost', 'Additional Cost') }}  
								@if(isset($invoice)) 
								{{ Form::text('invoice_total_show',number_format($invoice->additional_cost,0), array('class' => 'form-control', 'placeholder' => 'Additional Cost*', 'required' => 'true', 'id' => 'additional_cost_show', 'oninput' => 'cal_sparator();')) }}<br/>
								@else
								{{ Form::text('additional_cost_show',old('additional_cost_show'), array('class' => 'form-control', 'placeholder' => 'Additional Cost*', 'required' => 'true', 'id' => 'additional_cost_show', 'oninput' => 'cal_sparator();')) }}<br/>
								@endif 
								{{ Form::hidden('additional_cost', old('additional_cost'), array('id' => 'additional_cost')) }}

								{{ Form::label('add_cost_desc', 'Additional Cost Notes') }}
								{{ Form::text('add_cost_desc', old('add_cost_desc'), array('class' => 'form-control', 'placeholder' => 'Additional Cost Notes*', 'required' => 'true', 'id' => 'invoice_rekening')) }}<br/> 

								{{ Form::label('comment', 'Comment') }}
								{{ Form::text('comment', old('comment'), array('class' => 'form-control', 'placeholder' => 'Comment*', 'required' => 'true')) }}<br/>

								{{ Form::label('image', 'Attachment') }}
								{{ Form::file('image') }}<br/>

								<button type="button" data-toggle="modal" data-target="#modal_invoice" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
								<a href="{{ URL::route('editor.invoice.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
@stop


@section('modal')
<div class="modal fade" id="modal_invoice">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Save this invoice?</h4>
			</div> 
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
	$("#vendor_id").on('click', function()
	{
		$.ajax({
			url : '{{ URL::route('get.vendor') }}',
			data : {'vendor_id':$("#vendor_id").val()},
			type : 'GET',
			headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
			success : function(data, textStatus, jqXHR){
				console.log(data[0].vendor_bank);

				$('#invoice_bank').empty();
				$('#invoice_rekening').empty();
				jQuery.each(data, function(i, val)
				{
					document.getElementById ("invoice_bank").value = data[0].vendor_bank;
					document.getElementById ("invoice_rekening").value = data[0].vendor_rekening;
				}); 
			},
			error : function()
			{
				$('#invoice_bank').empty(); 
				$('#invoice_rekening').empty(); 
			},
		})
	});

	$('#btn_submit').on('click', function()
	{
		$('#form_invoice').submit();
	});
</script>

<script>
	$("#invoice_type_id").on('change', function()
	{
		//console.log("sada");
		$.ajax({
			url : '{{ URL::route('get.vendorinv') }}',
			data : {'invoice_type_id':$("#invoice_type_id").val()},
			type : 'GET',
			headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
			success : function(data, textStatus, jqXHR){
				console.log(data);
				$('#vendor_id').empty();
				jQuery.each(data, function(i, val)
				{
					$('#vendor_id').append($('<option>', { 
						value: val.id,
						text : val.vendor_name 
					}));
				});
				$("#vendor_id").attr('disabled', false);
				$('#invoice_bank').empty(); 
				$('#invoice_rekening').empty(); 
			},
			error : function()
			{
				$('#vendor_id').empty();
				$('#vendor_id').attr('disabled', true);
			},
		})
	});


	function cal_sparator() {
		var invoice_total_show = document.getElementById('invoice_total_show').value;
		var result = document.getElementById('invoice_total');
		var rsinvoice_total = (invoice_total_show);
		result.value = rsinvoice_total.replace(/,/g, ""); 

		var additional_cost_show = document.getElementById('additional_cost_show').value;
		var result = document.getElementById('additional_cost');
		var rsadditional_cost = (additional_cost_show);
		result.value = rsadditional_cost.replace(/,/g, ""); 
	}

	window.onload= function(){ 
		
		n2= document.getElementById('invoice_total_show');

		n2.onkeyup=n2.onchange= function(e){
			e=e|| window.event; 
			var who=e.target || e.srcElement,temp;
			if(who.id==='invoice_total')  temp= validDigits(who.value,0); 
			else temp= validDigits(who.value);
			who.value= addCommas(temp);
		}   
		n2.onblur= function(){
			var 
			temp2=parseFloat(validDigits(n2.value));
			if(temp2)n2.value=addCommas(temp2.toFixed(0));
		}

		n3= document.getElementById('additional_cost_show');

		n3.onkeyup=n3.onchange= function(e){
			e=e|| window.event; 
			var who=e.target || e.srcElement,temp;
			if(who.id==='additional_cost')  temp= validDigits(who.value,0); 
			else temp= validDigits(who.value);
			who.value= addCommas(temp);
		}   
		n3.onblur= function(){
			var 
			temp3=parseFloat(validDigits(n3.value));
			if(temp3)n3.value=addCommas(temp3.toFixed(0));
		}

	}
</script>
@stop