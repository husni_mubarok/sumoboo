<form class="form-inline" method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <input class="form-control" value="{{ $start_date }}" placeholder="Start Date" required="true" name="start_date" type="text" id="start_date"><br/>

    </div>
    <div class="form-group">
        <input class="form-control" value="{{ $end_date }}" placeholder="End Date" required="true" name="end_date" type="text" id="end_date"><br/>

    </div>
    <button class="btn btn-default" type="submit">
        Filter
    </button>
</form>