@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<style type="text/css">
th { font-size: 11px; }
td { font-size: 11px; }
</style>
<section class="content-header hidden-xs">
    <h1>
    CMS
    <small>Content Management System</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><a href="#"><i class="fa fa-industry"></i>Profit Lost</a></li>
    </ol>
</section>
<section class="content">
    <section class="content box mobile box-solid">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-1">
                </div>
                <div class="col-md-12">
                    <div class="x_panel">
                        <h2>
                        <i class="fa fa-industry"></i> Profit Lost
                        </h2>
                            <a href="{{ URL::route('editor.op.index') }}" class="btn btn-success btn-lg pull-right"><i class="fa fa-plus"></i> Operating Expense List</a>
                        <br>
                        <div class="x_content">


                                        <br>
                                        @include('editor.profit_lost.option')
                                        <br>
                                        @if(isset($data))
                                              @include('editor.profit_lost.table')
                                        @endif


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<!-- Add fancyBox -->
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
@stop