<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>

@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.item_central.index') }}"><i class="fa fa-dot-circle-o"></i> Item Central</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12"> 
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($item_central))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i> 
							@endif
							&nbsp;ItemCentral
						</h2>
					</div>
					<hr>
					@include('errors.error')
					@if(isset($item_central))
					{!! Form::model($item_central, array('route' => ['editor.item_central.updateinvoice', $item_central->id], 'method' => 'PUT', 'class'=>'update', 'files' => 'true', 'id'=>'form_itemcentral'))!!}
					@else
					{!! Form::open(array('route' => 'editor.item_central.store', 'class'=>'create', 'files' => 'true', 'id'=>'form_itemcentral'))!!}
					@endif
					{{ csrf_field() }}
					<div class="col-md-6">
						<div class="x_content"> 
							
							{{ Form::label('date', 'Date') }}
							{{ Form::text('date', old('date'), array('class' => 'form-control', 'placeholder' => 'Date*', 'required' => 'true', 'id' => 'date')) }}<br/>

							{{ Form::label('total_item_central', 'Actual Total') }} 
							<input type="text" class="form-control" name="total_invoice" value="{{number_format($item_central->total_invoice,0)}}" id="total_invoice" disabled="disabled"><br/>

							{{ Form::label('total_transport', 'Transport') }} 
							<input type="text" class="form-control" name="total_transport_show" value="{{number_format($item_central->total_transport,0)}}" id="total_transport_show" oninput="cal_sparator();">  
							{{ Form::hidden('total_transport', old('total_transport'), array('id' => 'total_transport')) }}<br/> 
						</div>
					</div>

					<div class="col-md-6">
						<div class="x_content">
							
							{{ Form::label('total_item_central', 'Estimated Total') }}
							<input type="text" class="form-control" name="total_item_central" value="{{number_format($item_central->total_item_central,0)}}" id="total_item_central" disabled="disabled"><br/>

							{{ Form::label('comment', 'Comment') }}
							{{ Form::text('comment', old('comment'), array('class' => 'form-control', 'placeholder' => 'Comment*', 'required' => 'true')) }} <br/>

							{{ Form::label('image', 'Attachment') }}
							{{ Form::file('image') }}<br/> 
							
						</div>
					</div> 

					<div class="col-md-12">  
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#consummable">Consummable</a></li>
							<li><a data-toggle="tab" href="#supply">Supply</a></li>  
						</ul> 
						<div class="tab-content">
							<div id="consummable" class="tab-pane fade in active"> 
								<div class="box-body">
									<div class="div_overflow"> 

										<table class="table table-striped table-hover" id="item_centralTablex" style="min-width: 800px">
											<thead>
												<tr>
													<th width="20%">Item Name</th>
													<th width="5%">UOM</th> 
													<th width="5%">Stock</th>
													<th width="5%">Qty to Buy</th>
													<th width="10%">Qty Delivery</th>
													<th width="10%">Unit Price</th>
													<th width="15%">Total</th> 
												</tr>
											</thead>
											<tbody>  
												@foreach($item_central_detail as $key => $item_central_details)
												<tr>
													<td>
														{{$item_central_details->item_name}} 
													</td>
													<td> 
														{{$item_central_details->uom}} 
													</td>  
													<td> 
														{{number_format($item_central_details->quantity_stock,0)}} 
													</td> 
													<td> 
														{{number_format($item_central_details->quantity,0)}}
													</td>  
													<td> 
														{{number_format($item_central_details->quantity_actual,0)}} 
													</td> 
													<td> 
														{{number_format($item_central_details->price,0)}}  
													</td> 
												</td> 
												<td> 
													<p id="total{{$item_central_details->id}}">{{number_format($item_central_details->total,0)}} </p>
												</td> 
											</tr> 
											@endforeach
										</tbody>
									</table> 
								</div> 
								<!-- /.box-body -->
							</div>
						</div>
						<div id="supply" class="tab-pane"> 
							<div class="box-body">
								<div class="div_overflow"> 
									<table class="table table-striped table-hover" id="item_centralTablex" style="min-width: 800px">
										<thead>
											<tr>
												<th width="20%">Item Name</th>
												<th width="5%">UOM</th> 
												<th width="5%">Stock</th>
												<th width="5%">Qty to Buy</th>
												<th width="10%">Qty Delivery</th>
												<th width="10%">Unit Price</th>
												<th width="15%">Total</th> 
											</tr>
										</thead>
										<tbody>  
											@foreach($item_central_detail_supply as $key => $item_central_details)
											<tr>
												<td>
													{{$item_central_details->item_name}} 
												</td>
												<td> 
													{{$item_central_details->uom}}
												</td>  
												<td> 
													{{number_format($item_central_details->quantity_stock,0)}} 
												</td> 
												<td> 
													{{number_format($item_central_details->quantity,0)}}
												</td> 
												<td> 
													{{number_format($item_central_details->quantity_actual,0)}}

												</td> 
												<td> 
													{{number_format($item_central_details->price,0)}}

												</td> 
											</td> 
											<td> 
												<p id="total{{$item_central_details->id}}">{{number_format($item_central_details->total,0)}} </p>
											</td> 
										</tr> 
										@endforeach
									</tbody>
								</table> 
							</div> 
						</div>

					</div>
					<hr>
					<button type="button" onclick="transportVal();" data-toggle="modal" data-target="#modal_itemcentral" class="btn btn-success pull-right"><i class="fa fa-check"></i> Submit Invoice</button>
					<a href="{{ URL::route('editor.item_central.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
</section>

@stop

@section('modal')
<div class="modal fade" id="modal_itemcentral">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Submit invoice this item central?</h4>
			</div> 
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')

<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#item_centralTable").DataTable(
		{
			"language": {
				"emptyTable": "-"
			}
		} 
		);
	});
</script>  
<script type="text/javascript">
	$(document).ready(function(){
		@foreach($item_central_detail as $key => $item_central_details)
		$('#date{{$key}}').datepicker({
			sideBySide: true,
			format: 'yyyy-mm-dd',
		}); 
		@endforeach
	});
	function total(){
		@foreach($item_central_detail as $key => $item_central_details)
		var quantity{{$item_central_details->id}} = document.getElementById('quantity_actual{{$item_central_details->id}}').value;
		var price{{$item_central_details->id}} = document.getElementById('price{{$item_central_details->id}}').value;
		document.getElementById('total{{$item_central_details->id}}').innerHTML = parseInt(quantity{{$item_central_details->id}}) * parseInt(price{{$item_central_details->id}});
		@endforeach

		@foreach($item_central_detail_supply as $key => $item_central_details)
		var quantity{{$item_central_details->id}} = document.getElementById('quantity_actual{{$item_central_details->id}}').value;
		var price{{$item_central_details->id}} = document.getElementById('price{{$item_central_details->id}}').value;
		document.getElementById('total{{$item_central_details->id}}').innerHTML = parseInt(quantity{{$item_central_details->id}}) * parseInt(price{{$item_central_details->id}});
		@endforeach
	}

	function transportVal(){
		
		var quantity = document.getElementById('total_transport').value;
		// console.log(quantity);
		if(quantity==''){
			alert("No amount transport!");
		}
	}

	$('#btn_submit').on('click', function()
	{
		$('#form_itemcentral').submit();
	});

	function cal_sparator() {
		var budget_request_show = document.getElementById('total_transport_show').value;
		var result = document.getElementById('total_transport');
		var rsbudgetrequest = (budget_request_show);
		result.value = rsbudgetrequest.replace(/,/g, ""); 
	}

	window.onload= function(){ 

		n2= document.getElementById('total_transport_show');

		n2.onkeyup=n2.onchange= function(e){
			e=e|| window.event; 
			var who=e.target || e.srcElement,temp;
			if(who.id==='total_transport')  temp= validDigits(who.value,0); 
			else temp= validDigits(who.value);
			who.value= addCommas(temp);
		}   
		n2.onblur= function(){
			var 
			temp2=parseFloat(validDigits(n2.value));
			if(temp2)n2.value=addCommas(temp2.toFixed(0));
		}

	}
</script>
@stop

