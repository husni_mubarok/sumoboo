@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->

<style type="text/css">
	th { font-size: 11px; }
	td { font-size: 11px; }
</style>
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="#"><i class="fa fa-dot-circle-o"></i> Item Central Bank</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							<i class="fa fa-dot-circle-o"></i> Item Central Bank
						</h2>
						<hr>
						<div class="x_content">
							<table id="item_centralTablex" class="table dataTable rwd-table">
								<thead>
									<tr>
										<th>#</th>
										<th>Date</th>  
										<th>Transport</th> 
										<th>Estimated Total</th> 
										<th>Actual Total</th>
										<th>Grand Total</th>
										<th>Attachment</th> 
										<th>Transfer Receipt</th>
										<th>Branch</th>
										<th>Status</th> 
										<th>Action Status</td>
										<th>Action</th>
										</tr> 
									</thead>
									<tbody>
										@foreach($item_centrals as $key => $item_central)
										<tr>
											<td data-th="#">{{$number++}}</td>
											<td data-th="Date">{{date("d M Y", strtotime($item_central->date))}}</td>
											<td data-th="Estimated Total">{{ number_format($item_central->total_transport,0) }}</td>    
											<td data-th="Estimated Total">{{ number_format($item_central->total_item_central,0) }}</td>  
											<td data-th="Actual Total">{{ number_format($item_central->total_invoice,0) }}</td>  
											<td data-th="Grand Total">{{ number_format($item_central->total_invoice+$item_central->total_transport,0) }}</td> 
											<td data-th="Attachment">
												@if($item_central->itemcentral_attachment == null)
												Tidak ada lampiran
												@else
												<a target="_blank" href="{{Config::get('constants.path.uploads')}}/itemcentral/{{$item_central->itemcentral_attachment}}"><i class="fa fa-download"></i>&nbsp;Download</a>
												@endif
											</td> 
											<td data-th="Transfer Receipt"> 
												@if($item_central->transfer_receipt == null)
												Tidak ada lampiran
												@else
												<a target="_blank" href="{{Config::get('constants.path.uploads')}}/itemcentral/{{$item_central->transfer_receipt}}"><i class="fa fa-download"></i>&nbsp;Download</a>
												@endif
											</td> 
											<td data-th="Branch">{{ $item_central->branch->branch_name }}</td> 
											<td data-th="Status">
												<span class=""><span class="label label-success"><i class="fa fa-check"></i>&nbsp;{{$item_central->status_code}}<span>
												</span></span><span>
												</td> 
												<td data-th="Action Status">
													@if($item_central->status_code=='Waiting for Delivery') 

													<a href="{{ URL::route('editor.item_central.delivery', [$item_central->id]) }}" class="btn btn-success btn-sm"> Delivery</a> 

													@elseif($item_central->status_code=='Waiting for Invoice')

													<a href="{{ URL::route('editor.item_central.invoice', [$item_central->id]) }}" class="btn btn-success btn-sm"> Submit Invoice</a> 

													@elseif($item_central->status_code=='Waiting for Owner Acknowledgement')

													<a href="{{ URL::route('editor.item_central.issued', [$item_central->id]) }}" class="btn btn-success btn-sm"> Issued</a> 

													@elseif($item_central->status_code=='Pay')
													<span class=""><span class="label label-success"><i class="fa fa-check"></i>&nbsp;Complete<span>

													@endif
												</td>
												<td >
													@if($item_central->status_code=='Waiting for Delivery')
													<a href="{{ URL::route('editor.item_central.edit', [$item_central->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil" alt="Edit"></i></a>
													@else
													<a href="{{ URL::route('editor.item_central.view', [$item_central->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-search" alt="Detail"></i></a>
													@endif 
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								{{ $item_centrals->links() }}
							</div>
						</div>
					</div>
				</div>
			</section>
		</section>
		@stop

		@section('scripts')
		<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
		<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
		<script>
			$(document).ready(function () {
				$("#item_centralTable").DataTable();
			});
		</script> 
		@stop