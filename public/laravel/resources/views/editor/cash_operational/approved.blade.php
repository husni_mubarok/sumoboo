@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.cash_operational.index') }}"><i class="fa dollar"></i> Cash Operational</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($cashbond))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i>
							@endif
							&nbsp;Cash Operational
						</h2>
					</div>
					<hr>
					@include('errors.error')
					@if(isset($cashbond))
					{!! Form::model($cashbond, array('route' => ['editor.cash_operational.updaterequset', $cashbond->id], 'method' => 'PUT', 'class'=>'update', 'id' => 'form_approval'))!!}
					@else
					{!! Form::open(array('route' => 'editor.cash_operational.store', 'class'=>'create'))!!}
					@endif
					{{ csrf_field() }}
					<div class="col-md-6">
						<div class="x_content">
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">

								{{ Form::label('start_date', 'Start Date') }}
								{{ Form::text('start_date', old('start_date'), array('class' => 'form-control', 'placeholder' => 'Start Date*', 'required' => 'true', 'id' => 'start_date', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('end_date', 'End Date') }}
								{{ Form::text('end_date', old('end_date'), array('class' => 'form-control', 'placeholder' => 'End Date*', 'required' => 'true', 'id' => 'end_date', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('Consummable') }}
								<select class="form-control" required="true" name="consumable" disabled="disabled">
									<option value=0>No</option>
									<option value=1>Yes</option>
								</select><br/>
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="x_content">
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">

								{{ Form::label('total_cashbond', 'Total Cashbond') }}
								{{ Form::number('total_cashbond', old('total_cashbond'), array('class' => 'form-control', 'placeholder' => 'Total Cashbond*', 'required' => 'true', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('total_cashin', 'Total Cashin') }}
								{{ Form::number('total_cashin', old('total_cashin'), array('class' => 'form-control', 'placeholder' => 'Total Cashin*', 'required' => 'true')) }}<br/>

								{{ Form::label('total_cashout', 'Total Cashout') }}
								{{ Form::number('total_cashout', old('total_cashout'), array('class' => 'form-control', 'placeholder' => 'Total Cashout*', 'required' => 'true', 'disabled' => 'disabled')) }} <br/>

								{{ Form::label('comment', 'Comment') }}
								{{ Form::text('comment', old('comment'), array('class' => 'form-control', 'placeholder' => 'Comment*', 'required' => 'true')) }}
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="box-body">
							<div class="row">
								<table class="table table-striped table-hover" id="cashbondTable">
									<thead>
										<tr>
											<th width="20%">Item Cashbond</th>
											<th width="10%">Quantity</th>
											<th width="15%">Unit Price</th>
											<th width="10%">UOM</th>
											<th width="15%">Nota Number</th>
											<th width="10%">Date</th>
											<th width="15%">Total</th>
										</tr>
									</thead>
									<tbody>
										@foreach($cashbond_detail as $key => $cashbond_details)
										<tr>
											<td>
												{{$cashbond_details->item_name}}
											</td>
											<td>
												{{$cashbond_details->quantity}}
											</td>
											<td>
												{{$cashbond_details->price}}
											</td>
											<td>
												{{$cashbond_details->uom}}
											</td>
											<td>
												{{$cashbond_details->nota_number}}
											</td>
											<td>
												{{$cashbond_details->date}}
											</td>
											<td>
												<p id="total{{$key}}">{{$cashbond_details->total}} </p>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<hr>
						<button type="button" class="btn btn-success pull-right" id="btn_confirm" data-toggle="modal" data-target="#modal_confirm">Approve</button>
						<a href="{{ URL::route('editor.cash_operational.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_confirm">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title">Approve?</h4>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
      			<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> Yes</button>
      		</div>
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#cashbondTable").DataTable(
		{
			"language": {
				"emptyTable": "-"
			}
		}
		);
	});
</script>
<script type="text/javascript">
	$('#btn_submit').on('click', function()
	{
		$('#form_approval').submit();
	});
</script>
@stop

