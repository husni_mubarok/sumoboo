<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>

@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.cash_operational.index') }}"><i class="fa fa-dollar"></i> Cash Operational</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($cashbond_detail))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i>
							@endif
							&nbsp;Cashbond Detail Item
						</h2>
					</div>
					<hr>
					<div class="col-md-6">
						<div class="x_content">
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">

								@include('errors.error')
								{!! Form::model($cashbond_detail, array('route' => ['editor.cash_operational.storedetailitem', $cashbond_detail->id],  'class'=>'create', 'id'=>'form_cashbonditem'))!!}
								{{ csrf_field() }}

								{{ Form::label('date', 'Date') }}
								{{ Form::text('date', old('date'), array('class' => 'form-control', 'placeholder' => 'End Date*', 'required' => 'true', 'id' => 'date', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('nota_number', 'Nota Number') }}
								{{ Form::text('nota_number', old('nota_number'), array('class' => 'form-control', 'placeholder' => 'Nota Number*', 'required' => 'true', 'id' => 'nota_number', 'disabled' => 'disabled')) }}<br/>

								{{ Form::label('amount', 'Amount') }}
								{{ Form::number('amount', old('amount'), array('class' => 'form-control', 'placeholder' => 'Amount*', 'required' => 'true', 'id' => 'amount', 'disabled' => 'disabled')) }}<br/>

								<div style="width: 100%; border-bottom: 1px solid black; text-align: center">
									<span style="font-size: 20px; padding: 0 10px;">
										Item <!--Padding is optional-->
									</span>
								</div>
								{{ Form::hidden('id', old('id')) }}
								{{ Form::hidden('cashbond_id', old('cashbond_id')) }}
								{{ Form::label('item_id', 'Item') }}
								{{ Form::select('item_id', $item_list, old('item_id'), array('class' => 'form-control', 'required' => 'true', 'id' => 'item_id')) }}<br/>

								{{ Form::label('uom', 'UOM') }}
								{{ Form::text('uom', old('uom'), array('class' => 'form-control', 'placeholder' => 'UOM*', 'required' => 'uom', 'id' => 'uom')) }}<br/>

								{{ Form::label('quantity', 'Quantity') }}
								{{ Form::number('quantity', old('quantity'), array('class' => 'form-control', 'placeholder' => 'Quantity*', 'required' => 'quantity', 'id' => 'quantity', 'oninput' => 'caltotal();')) }}<br/>

								{{ Form::label('price', 'Price') }}
								{{ Form::text('price_show',old('price_show'), array('class' => 'form-control', 'placeholder' => 'Price*', 'required' => 'true', 'id' => 'price_show', 'oninput' => 'cal_sparator(); caltotal();')) }}<br/>
								{{ Form::hidden('price', old('price'), array('id' => 'price')) }}

								{{ Form::label('total', 'Total') }}
								{{ Form::text('total_show',old('total_show'), array('class' => 'form-control', 'placeholder' => 'Total*', 'required' => 'true', 'id' => 'total_show', 'oninput' => 'cal_sparator();')) }}<br/>
								{{ Form::hidden('total', old('total'), array('id' => 'total')) }}

								{{ Form::label('paid', 'Paid') }}
								{{ Form::text('paid_show',old('paid_show'), array('class' => 'form-control', 'placeholder' => 'Total*', 'required' => 'true', 'id' => 'paid_show', 'oninput' => 'cal_sparator();')) }}<br/>
								{{ Form::hidden('paid', old('paid'), array('id' => 'paid')) }}

								<button type="button" class="btn btn-sucess pull-right" id="btn_add_detail"><i class="fa fa-cart-plus"></i>&nbsp;Add</button> <br>

								<hr>
								<div class="div_overflow">
									<table id="itemTable" class="table table-striped dataTable">
										<thead>
											<tr>
												<th>Item</th>
												<th>UOM</th>
												<th>Qty</th>
												<th>Price</th>
												<th>Total</th>
												<th>Paid</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody id="cart_item">
										</tbody>
									</table>
								</div>
								<button  type="button" data-toggle="modal" data-target="#modal_cashbonditem" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
								<a href="{{ URL::route('editor.cash_operational.closing', [$cashbond_detail->cashbond_id]) }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a><br>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
				<hr>
			</div>
		</div>
	</div>
</div>
</section>

@stop

@section('modal')
<div class="modal fade" id="modal_cashbonditem">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Save this item?</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	function caltotal(){
		var quantity = document.getElementById('quantity').value;
		var price = document.getElementById('price').value;
		document.getElementById('total').value = parseFloat(quantity) * parseFloat(price);
		document.getElementById('total_show').value = numberWithCommas(parseFloat(quantity) * parseFloat(price));
		document.getElementById('paid').value = parseFloat(quantity) * parseFloat(price);
		document.getElementById('paid_show').value = numberWithCommas(parseFloat(quantity) * parseFloat(price));
	}
</script>

<script>
	var cart_item= JSON.parse("{}");
</script>
@if(isset($cashbond_detail_item))
<script>

	var cart_item = JSON.parse("{}");
	var cart_item_t = JSON.parse("{}");
	var cart_item_p = JSON.parse("{}");

// ADD BUTTON DETAIL
$("#btn_add_detail").on('click', function() {

	var inpuom = $("#uom");
	var inpquantity = $("#quantity");
	var inpprice = $("#price");
	var inptotal = $("#total");
	var inppaid = $("#paid");

	if (inpuom.val().length < 1 || inpquantity.val().length < 1 || inpprice.val().length < 1 || inptotal.val().length < 1 || inppaid.val().length < 1) {
		alert("Some data can't empty!");
	}else{

		var quantity = $("#quantity").val();
		var uom = $("#uom").val();
		var price = $("#price").val();
		var total = $("#total").val();
		var paid = $("#paid").val();

		var item_id = $("#item_id option:selected").val();
		var key = item_id+"_"+uom+"_"+price;

		if(cart_item[key] == undefined)
		{
			cart_item[key] = parseFloat(quantity);
			cart_item_t[key] = parseFloat(total);
			cart_item_p[key] = parseFloat(paid);
			var new_row = '';
			new_row += '<tr>';
			new_row += '<td>';
			new_row += '<input type="hidden" name="item_id['+key+']" value="'+item_id+'">';
			new_row += $("#item_id option:selected").text();
			new_row += '</td>';

			new_row += '<td>';
			new_row += '<input type="hidden" name="uom['+key+']" value="'+uom+'">';
			new_row += uom;
			new_row += '</td>';

			new_row += '<td>';
			new_row += '<input type="hidden" name="quantity['+key+']" value="'+cart_item[key]+'" id="quantity_'+key+'">';
			new_row += '<div id="txtquantity_'+key+'">'+quantity+'</div>';
			new_row += '</td>';

			new_row += '<td>';
			new_row += '<input type="hidden" name="price['+key+']" value="'+price+'">';
			new_row += price;
			new_row += '</td>';

			new_row += '<td>';
			new_row += '<input type="hidden" name="total['+key+']" value="'+total+'" id="total_'+key+'">';
			new_row += '<div id="txttotal_'+key+'">'+total+'</div>';
			new_row += '</td>';

			new_row += '<td>';
			new_row += '<input type="hidden" name="paid['+key+']" value="'+paid+'" id="paid_'+key+'">';
			new_row += '<div id="txtpaid_'+key+'">'+paid+'</div>';
			new_row += '</td>';

			new_row += '<td>';
			new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_'+key+'">';
			new_row += '<i class="fa fa-remove"></i>';
			new_row += '</button>';
			new_row += '</td>';
			new_row += '</tr>';

			$("#cart_item").append(new_row);

			$("#btn_remove_"+key).on('click', function()
			{
				cart_item[key] == null;
				$(this).parent().parent().remove();
			});
		}
		else
		{
		//alert("sadd");
		//console.log(cart_item[key]);

		cart_item[key] += parseFloat(quantity);
		$("#quantity_"+key).val(cart_item[key]);
		$("#txtquantity_"+key).text(cart_item[key]);

		cart_item_t[key] += parseFloat(total);
		$("#total_"+key).val(cart_item_t[key]);
		$("#txttotal_"+key).text(cart_item_t[key]);

		cart_item_p[key] += parseFloat(paid);
		$("#paid_"+key).val(cart_item_p[key]);
		$("#txtpaid_"+key).text(cart_item_p[key]);

	}

	document.getElementById ("uom").value = "";
	document.getElementById ("quantity").value = "";
	document.getElementById ("total").value = "";
	document.getElementById ("total_show").value = "";
	document.getElementById ("price").value = "";
	document.getElementById ("price_show").value = "";
	document.getElementById ("paid").value = "";
	document.getElementById ("paid_show").value = "";
}

});

jQuery.each({!! $cashbond_detail_item !!}, function( i, val ) {
	var quantity = val['quantity'];
	var uom = val['uom'];
	var price = val['price'];
	var total = val['total'];
	var paid = val['paid'];
	var item_id = val['item_id'];

	//var item_id = $("#item_id option:selected").val();

	var key = item_id+"_"+"_"+uom+"_"+price;

	$("#item_id").val(item_id);
	//console.log(key);

	//cart_item[key] = item_id;
	var new_row = '';
	new_row += '<tr>';
	new_row += '<td>';
	new_row += '<input type="hidden" name="item_id['+key+']" value="'+item_id+'">';
	new_row += $("#item_id option:selected").text();
	new_row += '</td>';

	new_row += '<td>';
	new_row += '<input type="hidden" name="uom['+key+']" value="'+uom+'">';
	new_row += uom;
	new_row += '</td>';

	new_row += '<td>';
	new_row += '<input type="hidden" name="quantity['+key+']" value="'+quantity+'">';
	new_row += quantity;
	new_row += '</td>';

	new_row += '<td>';
	new_row += '<input type="hidden" name="price['+key+']" value="'+price+'">';
	new_row += price;
	new_row += '</td>';

	new_row += '<td>';
	new_row += '<input type="hidden" name="total['+key+']" value="'+total+'">';
	new_row += total;
	new_row += '</td>';

	new_row += '<td>';
	new_row += '<input type="hidden" name="paid['+key+']" value="'+paid+'">';
	new_row += paid;
	new_row += '</td>';

	new_row += '<td>';
	new_row += '<button type="button" class="btn btn-danger btn-sm" id="btn_remove_'+key+'">';
	new_row += '<i class="fa fa-remove"></i>';
	new_row += '</button>';
	new_row += '</td>';
	new_row += '</tr>';

	$("#cart_item").append(new_row);

	$("#btn_remove_"+key).on('click', function()
	{
		cart_item[key] == null;
		$(this).parent().parent().remove();
	});
	document.getElementById ("uom").value = "";
});
</script>
@endif

//get item
<script>
	$("#item_id").on('change', function()
	{
		$.ajax({
			url : '{{ URL::route('get.item') }}',
			data : {'item_id':$("#item_id").val()},
			type : 'GET',
			headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
			success : function(data, textStatus, jqXHR){
        	// console.log(data[0].vendor_bank);

        	$('#uom').empty();
        	$('#price').empty();
        	jQuery.each(data, function(i, val)
        	{
        		document.getElementById ("uom").value = data[0].uom;
        		document.getElementById ("price").value = data[0].price;
        		document.getElementById ("price_show").value = numberWithCommas(data[0].price);

        	});
        },
        error : function()
        {
        	$('#uom').empty();
        	$('#price').empty();
        },
    })
	});
	$("#item_id").on('change', function()
	{
		document.getElementById ("quantity").value = "";
		document.getElementById ("total").value = "";
		document.getElementById ("paid").value = "";
		document.getElementById ("total_show").value = "";
		document.getElementById ("paid_show").value = "";
	});

	$('#btn_submit').on('click', function()
	{
		$('#form_cashbonditem').submit();
	});

	//Sparatior
	function cal_sparator() {
		var price_show = document.getElementById('price_show').value;
		var result = document.getElementById('price');
		var rsprice = (price_show);
		result.value = rsprice.replace(/,/g, "");

		var paid_show = document.getElementById('paid_show').value;
		var result = document.getElementById('paid');
		var rspaid = (paid_show);
		result.value = rspaid.replace(/,/g, "");
	}

	window.onload= function(){

		n2= document.getElementById('price_show');

		n2.onkeyup=n2.onchange= function(e){
			e=e|| window.event;
			var who=e.target || e.srcElement,temp;
			if(who.id==='price')  temp= validDigits(who.value,0);
			else temp= validDigits(who.value);
			who.value= addCommas(temp);
		}
		n2.onblur= function(){
			var
			temp2=parseFloat(validDigits(n2.value));
			if(temp2)n2.value=addCommas(temp2.toFixed(0));
		}



		n2= document.getElementById('paid_show');

		n2.onkeyup=n2.onchange= function(e){
			e=e|| window.event;
			var who=e.target || e.srcElement,temp;
			if(who.id==='paid')  temp= validDigits(who.value,0);
			else temp= validDigits(who.value);
			who.value= addCommas(temp);
		}
		n2.onblur= function(){
			var
			temp2=parseFloat(validDigits(n2.value));
			if(temp2)n2.value=addCommas(temp2.toFixed(0));
		}
	}
</script>
@stop

