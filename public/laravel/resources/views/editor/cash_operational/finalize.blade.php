<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>

@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.cash_operational.index') }}"><i class="fa fa-dollar"></i> Cash Operational</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($cashbond))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i>
							@endif
							&nbsp;Cash Operational Transfer
						</h2>
					</div>
					<hr>
					@include('errors.error')
					{!! Form::model($cashbond, array('route' => ['editor.cash_operational.updatefinalize', $cashbond->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_cashbondfinalize', 'files' => 'true'))!!}
					{{ csrf_field() }}
					<div class="col-md-6">
						<div class="x_content">
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">

								{{ Form::label('start_date', 'Start Date') }}
								<input type="text" class="form-control" name="start_date" value="{{date('d-M-Y', strtotime($cashbond->start_date))}}" disabled="disabled"><br/>

								{{ Form::label('end_date', 'End Date') }}<br/>
								<input type="text" class="form-control" name="start_date" value="{{date('d-M-Y', strtotime($cashbond->end_date))}}" disabled="disabled"><br/>

								{{ Form::label('budget_request', 'Budget Request') }}<br/>
								<input type="text" class="form-control" name="start_date" value="{{number_format($cashbond->budget_request,0)}}" disabled="disabled"><br/>


								{{ Form::label('budget_issued', 'Budget Issued') }}<br/>
								<input type="text" class="form-control" name="budget_issued" value="{{number_format($cashbond->budget_issued,0)}}" disabled="disabled"><br/>

								{{ Form::label('transfer_receipt', 'Transfer Receipt') }}
								{{ Form::file('transfer_receipt') }}<br/>

								{{ Form::label('comment', 'Comment') }}
								{{ Form::text('comment', old('comment'), array('class' => 'form-control', 'placeholder' => 'Comment*', 'required' => 'true')) }}<br/>


								<button type="button" data-toggle="modal" data-target="#modal_cashbondfinalize" class="btn btn-success pull-right"><i class="fa fa-check"></i> Submit</button>
								<a href="{{ URL::route('editor.cash_operational.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
							</div>
						</div>
					</div>
				</div>
				<hr>
				{!! Form::close() !!}


			</div>
		</div>
	</div>
</div>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_cashbondfinalize">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Transfer this Cash Operational?</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
	$('#btn_submit').on('click', function()
	{
		$('#form_cashbondfinalize').submit();
	});
</script>
@stop