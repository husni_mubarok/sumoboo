<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>
@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><a href="{{ URL::route('editor.employee.index') }}"><i class="fa fa-user"></i> Employee</a></li>
  </ol>
</section>
<section class="content">
<section class="content box box-solid">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-md-12">
				<div class="x_panel">
					<h2>
					
						@if(isset($employee))
						<i class="fa fa-pencil"></i>
						@else
						<i class="fa fa-plus"></i> 
						@endif
						&nbsp;Employee
					</h2>
					<hr>
					<div class="x_content">
						@include('errors.error')
						@if(isset($employee))
						{!! Form::model($employee, array('route' => ['editor.employee.update', $employee->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_employee'))!!}
						@else
						{!! Form::open(array('route' => 'editor.employee.store', 'class'=>'create', 'id'=>'form_employee'))!!}
						@endif
						{{ csrf_field() }}
						<div class="col-md-6 col-sm-6 col-xs-12 form-group"> 
						<h3>Information</h3>
						<hr>
							{{ Form::label('emp_name', 'Name') }}
							{{ Form::text('emp_name', old('emp_name'), array('class' => 'form-control', 'placeholder' => 'Name*', 'required' => 'true')) }}<br/>

							{{ Form::label('emp_full_name', 'Full Name') }}
							{{ Form::text('emp_full_name', old('emp_full_name'), array('class' => 'form-control', 'placeholder' => 'Full Name*', 'required' => 'true')) }}<br/>

							{{ Form::label('emp_bod', 'BOD') }} 
							<select class="form-control" required="true" name="emp_bod">
								<option value="0">No</option>
								<option value="1">Yes</option> 
							</select><br/>

							{{ Form::label('emp_address', 'Address') }}
							{{ Form::text('emp_address', old('emp_address'), array('class' => 'form-control', 'placeholder' => 'Address*', 'required' => 'true')) }}<br/>

							{{ Form::label('emp_ktp_number', 'KTP Number') }}
							{{ Form::text('emp_ktp_number', old('emp_ktp_number'), array('class' => 'form-control', 'placeholder' => 'KTP Number*', 'required' => 'true')) }}<br/>

							{{ Form::label('emp_bank', 'Bank') }}
							{{ Form::text('emp_bank', old('emp_bank'), array('class' => 'form-control', 'placeholder' => 'Bank*', 'required' => 'true')) }}<br/>

							{{ Form::label('emp_bank_rek', 'Rek No') }}
							{{ Form::text('emp_bank_rek', old('emp_bank_rek'), array('class' => 'form-control', 'placeholder' => 'Rek No*', 'required' => 'true')) }}<br/>

							{{ Form::label('emp_email', 'Email') }}
							{{ Form::email('emp_email', old('emp_email'), array('class' => 'form-control', 'placeholder' => 'Email*', 'required' => 'true')) }}<br/>

							{{ Form::label('emp_phone', 'Phone') }}
							{{ Form::text('emp_phone', old('emp_phone'), array('class' => 'form-control', 'placeholder' => 'Phone*', 'required' => 'true')) }}<br/>

							{{ Form::label('emp_gender', 'Gender') }} 
							<select class="form-control" required="true" name="emp_gender">
								<option value="Male">Male</option>
								<option value="Female">Female</option> 
							</select><br/>

							{{ Form::label('Religion') }}
		                    	{{ Form::select('emp_religion', $religion_list, old('emp_religion'), array('class' => 'form-control', 'required' => 'true')) }}<br/>

							{{ Form::label('emp_hiring_date', 'Hiring Date') }}
							{{ Form::text('emp_hiring_date', old('emp_hiring_date'), array('class' => 'form-control', 'placeholder' => 'Hiring Date*', 'required' => 'true')) }}<br/>

							{{ Form::label('emp_position', 'Position') }}
							{{ Form::text('emp_position', old('emp_position'), array('class' => 'form-control', 'placeholder' => 'Position*', 'required' => 'true')) }}<br/>

							{{ Form::label('status', 'Status') }}
								{{ Form::select('status', $status_list, old('status'), ['class' => 'form-control', 'id' => 'status']) }}<br> 
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 form-group">  
						<h3>Salary</h3>
						<hr> 
							{{ Form::label('slr_basic', 'Salary Basic') }}
							{{ Form::number('slr_basic', old('slr_basic'), array('class' => 'form-control', 'placeholder' => 'Salary Basic*', 'required' => 'true')) }}<br/>

							{{ Form::label('slr_voucher', 'Voucher') }}
							{{ Form::number('slr_voucher', old('slr_voucher'), array('class' => 'form-control', 'placeholder' => 'Voucher', 'required' => 'true')) }}<br/>


							{{ Form::label('slr_transport', 'Salary Transport') }}
							{{ Form::number('slr_transport', old('slr_transport'), array('class' => 'form-control', 'placeholder' => 'Salary Transport*', 'required' => 'true')) }}<br/>

							{{ Form::label('slr_tunjangan', 'Salary Tunjangan') }}
							{{ Form::number('slr_tunjangan', old('slr_tunjangan'), array('class' => 'form-control', 'placeholder' => 'Salary Tunjangan*', 'required' => 'true')) }}<br/>

							{{ Form::label('slr_tunjangan_makan', 'Salary Tunjangan Makan') }}
							{{ Form::number('slr_tunjangan_makan', old('slr_tunjangan_makan'), array('class' => 'form-control', 'placeholder' => 'Salary Tunjangan Makan*', 'required' => 'true')) }}<br/>

							{{ Form::label('slr_cashbond', 'Salary Cashbond') }}
							{{ Form::number('slr_cashbond', old('slr_cashbond'), array('class' => 'form-control', 'placeholder' => 'Salary Cashbond*', 'required' => 'true')) }}<br/>

							{{ Form::label('slr_thr', 'THR') }}
							{{ Form::number('slr_thr', old('slr_thr'), array('class' => 'form-control', 'placeholder' => 'THR*', 'required' => 'true')) }}<br/>

							{{ Form::label('slr_pot_deposit', 'Potongan Deposit') }}
							{{ Form::number('slr_pot_deposit', old('slr_pot_deposit'), array('class' => 'form-control', 'placeholder' => 'Potongan Deposit*', 'required' => 'true')) }}
						</div>
						<div class="col-md-12">  
							<br/>
							<button type="button" data-toggle="modal" data-target="#modal_employee"  class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
							<a href="{{ URL::route('editor.employee.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</section>
@stop

@section('modal')
<div class="modal fade" id="modal_employee">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Save this employee?</h4>
			</div> 
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
	$('#btn_submit').on('click', function()
	{
		$('#form_employee').submit();
	});
</script>
@stop