@extends('layouts.editor.template')
@section('content')
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.employee.index') }}"><i class="fa fa-file-text-o"></i> Employee Branch</a></li>
	</ol>
</section>
<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-5">
					<div class="x_panel">
					<h2>
						@if(isset($employee))
						<i class="fa fa-pencil"></i>
						@else
						<i class="fa fa-plus"></i> 
						@endif
						<i class="fa fa-bar-chart"></i> Employee Branch
					</h2>
					<hr>
					<div class="x_content">
						<center>
							@if(Session::has('success'))
							<div class="alert alert-info">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<strong>Error, </strong> {{ Session::get('message', '') }}
							</div>
							@endif
							<br/>
						</center>

						@include('errors.error')
						{!! Form::model($employee, array('route' => ['editor.employee.storebranch', $employee->id, 'class'=>'create', 'method' => 'PUT']))!!}
						{{ csrf_field() }}
						<div class="col-md-12 col-sm-12 col-xs-12 form-group">
							 
							{{ Form::label('Branch') }}
							{{ Form::select('branch_id', $branch_list, old('branch_id'), array('class' => 'form-control', 'required' => 'true')) }}<br/> 

							<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
							<a href="{{ URL::route('editor.employee.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a> 
						</div>
						{!! Form::close() !!}
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">          
						<table class="table table-bordered" id="detail_table">
							<thead>
								<tr>
									<th>#</th>
									<th>Branch</th> 
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($employee_branch as $key => $employee_branchs)
								<tr>
									<td>{{$key+1}}</td>
									<td>{{$employee_branchs->branch->branch_name}}</td> 
									<td align="center"> 
											<div class="col-md-2 nopadding">
												{!! Form::open(array('route' => ['editor.employee.deleteemployeebranch', $employee_branchs->id], 'method' => 'delete', 'class'=>'delete'))!!}
												{{ csrf_field() }}                              
												<button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash"></i></a></button>
												{!! Form::close() !!}
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> 
</div>
</div>
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>

<script>
	$(".delete").on("submit", function(){
		return confirm("Do you want to delete this employee branch?");
	});

</script>
@stop