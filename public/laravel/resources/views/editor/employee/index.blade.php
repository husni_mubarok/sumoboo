@extends('layouts.editor.template')
@section('content')
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="#"><i class="fa fa-users"></i> Employee</a></li>
	</ol>
</section>
<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-1"></div>
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							<i class="fa fa-users"></i> Employee List
							@actionStart('employee', 'create')
							<a href="{{ URL::route('editor.employee.create') }}" class="btn btn-success btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
							@actionEnd
						</h2> 
						<ul class="nav nav-tabs">
							@foreach($array_all AS $all)

							@if($all["id"]==1)
							<li class="active"><a data-toggle="tab" href="#{{$all["id"]}}">{{$all["branch_name"]}}</a></li> 
							@else
							<li><a data-toggle="tab" href="#{{$all["id"]}}">{{$all["branch_name"]}}</a></li> 
							@endif 
							@endforeach  
						</ul>

						<div class="tab-content">
							@foreach($array_all AS $all)

							@if($all["id"]==1)
							<div id="{{$all["id"]}}" class="tab-pane fade in active">
								@else
								<div id="{{$all["id"]}}" class="tab-pane">
									@endif

									<div class="x_content">
										<div class="div_overflow_big">
											<table id="drinkTablex" class="table dataTable rwd-table">
												<thead>
													<tr>
														<th>#</th>
														<!-- <th>Name</th> -->
														<th>Full Name</th>
														<th>Address</th>
														<th>KTP Number</th>
														<th>Bank</th>
														<th>Rek No.</th>
														<th>Email</th>
														<th>Phone</th>
														<th>Gender</th>
														<th>Religion</th>
														<th>Hiring Date</th>
														<th>Position</th>
														<th>Basic</th>
														<th>Voucher</th>
														<th>Status</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>

													@foreach($all["detail"] as $key => $detail_emp)
													<tr>
														<td data-th="#">{{$key+1}}</td> 
														<td data-th="Full Name">{{$detail_emp["emp_full_name"]}}</td>
														<td data-th="Address">{{$detail_emp["emp_address"]}}</td>
														<td data-th="KTP Number">{{$detail_emp["emp_ktp_number"]}}</td>
														<td data-th="Bank">
															{{$detail_emp["emp_bank"]}}
														</td>
														<td data-th="Rek No">
															<b>{{$detail_emp["emp_bank_rek"]}}</b>
														</td>
														<td data-th="Email">
															{{$detail_emp["emp_email"]}}
														</td>
														<td data-th="Phone">
															<b>{{$detail_emp["emp_phone"]}}</b>
														</td>
														<td data-th="Gender">{{$detail_emp["emp_gender"]}}</td>
														<td data-th="Religion">{{$detail_emp["emp_religion"]}}</td>
														<td data-th="Hiring Date">{{$detail_emp["emp_hiring_date"]}}</td>
														<td data-th="Position">{{$detail_emp["emp_position"]}}</td>
														<td data-th="Basic">{{ number_format($detail_emp["slr_basic"],0) }}</td> 
														<td data-th="Basic">{{ number_format($detail_emp["slr_voucher"],0) }}</td> 
														<td data-th="Phone">
															@if($detail_emp["emp_status"]==0 || $detail_emp["emp_status"]=='')
															<span class="label label-success"><i class="fa fa-check"></i>
																Active
																<span>
																	@else
																	<span class="label label-danger"><i class="fa fa-close"></i>
																		Not Active
																		<span>
																			@endif
																		</td>
																		<td align="center">
																			<div class="act_tb">
																				@actionStart('employee', 'update')
																				<div>
																					<a href="{{ URL::route('editor.employee.branch', [$detail_emp["id"]]) }}" class="btn btn-default btn-sm"><i class="fa fa-building"></i></a>
																				</div>
																				<div>
																					<a href="{{ URL::route('editor.employee.edit', [$detail_emp["id"]]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
																				</div>
																				@actionEnd
																				<div>
																					{!! Form::open(array('route' => ['editor.employee.delete', $detail_emp["id"]], 'method' => 'delete', 'class'=>'delete'))!!}
																					{{ csrf_field() }}	

																					@actionStart('employee', 'delete') 	
																					<button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash"></i></a></button>
																					@actionEnd

																					{!! Form::close() !!}
																				</div>
																			</div>
																		</td>
																	</tr>
																	@endforeach
																</tbody>
															</table>
														</div>

													</div>  
												</div> 
												@endforeach
											</div>
										</div>
									</div>
								</div>
							</section>
						</section>
						@stop

						@section('scripts')
						<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
						<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
						<script>
							$(document).ready(function () {
								$("#drinkTable").DataTable();
							});
						</script>

						<!-- Add fancyBox -->
						<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
						<script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
						<script type="text/javascript">
							$(document).ready(function() {
								$(".fancybox").fancybox();
							});
						</script>
						<script>
							$(".delete").on("submit", function(){
								return confirm("Delete this employee?");
							});
						</script> 
						@stop