@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->

<style type="text/css">
	th { font-size: 11px; }
	td { font-size: 11px; }
</style>
<section class="content-header hidden-xs">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    <li class="active"><i class="fa fa-money"></i> Revenue</li>
  </ol>
</section>
@actionStart('revenue', 'read')
<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-12">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-money"></i> Revenue Outstanding
		                	@actionStart('revenue', 'create')
		                	<a href="{{ URL::route('editor.revenue.create') }}" class="btn btn-success btn-lg pull-right"><i class="fa fa-plus"></i> Add</a> 
		                	@actionEnd
	                	</h2>
		                <hr>
			           <div class="x_content">
			                <table id="table_revenuex" class="table  dataTable rwd-table">
							  	<thead>
							  	  	<tr>
								      	<th width="5%">#</th>
								      	<th>Date</th>
								      	<th>Status</th>
								      	<th>Total</th>
								      	<th>Tax</th> 
								      	<th>Service Charge</th>
								      	<th>Net Revenue</th>
								      	<th>Attachment</th>
								      	<th>Branch</th>
								      	<th>Action Status</th>
								      	<th width="10%">Action</th>
							    	</tr>
							  	</thead>
							  	<tbody>
								@foreach($revenues as $key => $revenue)
									<tr>
										<td data-th="#">{{$key+1}}</td>
										<td id="date_{{$revenue->id}}" data-th="Date">{{date("D, d M Y", strtotime($revenue->date))}}</td>
										<td data-th="Status">
											@if($revenue->status == 0)
											<span class="label label-danger"><i class="fa fa-unlock"></i> Open</span>
											@elseif($revenue->status == 1)
											<span class="label label-success"><i class="fa fa-lock"></i> Close</span>
											@elseif($revenue->status == 2)
											<span class="label label-warning"><i class="fa fa-history"></i> Request Edit</span>
											@elseif($revenue->status == 3)
											<span class="label label-warning"><i class="fa fa-history"></i> Request Edit</span>
											@endif
										</td>
										<td data-th="Total">{{number_format($revenue->total, 2)}}</td>
										<td data-th="Tax">{{number_format($revenue->tax, 2)}}</td>
										<td data-th="Service Charge">{{number_format($revenue->service_charge, 2)}}</td>
										<td data-th="VAT">{{number_format($revenue->vat, 2)}}</td>
										<td data-th="attachment">
											@if($revenue->revenue_attachment)
											<i class="fa fa-check"></i>
											@else
											<i class="fa fa-remove"></i>
											@endif
										</td>
										<td id="branch_{{$revenue->id}}" data-th="Branch">{{ $revenue->branch->branch_name }}</td>
										<td>
											@if($revenue->status == 0)
						                		@actionStart('revenue', 'submit')
						                		{!! Form::open(['route' => 'editor.revenue.close', 'method' => 'PUT', 'id' => 'form_close_'.$revenue->id]) !!}
						                		<input type="hidden" name="revenue_id" value="{{$revenue->id}}">
						                		<button type="button" class="btn btn-sm btn-default btn_close" value="{{$revenue->id}}"><i class="fa fa-lock"></i> Close</button>
						                		{!! Form::close() !!}
						                		@actionEnd
					                		@elseif($revenue->status == 1)
					                			@actionStart('revenue', 'update')
					                			{!! Form::open(['route' => 'editor.revenue.request_edit', 'method' => 'PUT', 'id' => 'form_request_edit_'.$revenue->id]) !!}
						                		<input type="hidden" name="revenue_id" value="{{$revenue->id}}">
					                			<button type="button" class="btn btn-sm btn-default btn_request_edit" value="{{$revenue->id}}"><i class="fa fa-pencil"></i> Request Edit</button>
				                				{!! Form::close() !!}
				                				@actionEnd
					                		@elseif($revenue->status == 2)
					                			@actionStart('revenue', 'paid')
					                			<div class="act_tb">
													<div>
														{!! Form::open(['route' => 'editor.revenue.finance_approve', 'method' => 'PUT', 'id' => 'form_finance_reject_'.$revenue->id]) !!}
									                	<input type="hidden" name="revenue_id" value="{{$revenue->id}}">
									                	<input type="hidden" name="review" value="0">
									                	<button type="button" class="btn btn-danger btn-sm btn_finance_reject" style="margin:5px" value="{{$revenue->id}}" title="Finance Reject">
									                		<i class="fa fa-remove"></i>
								                		</button>
								                		{!! Form::close() !!}
													</div>
													<div>	
														{!! Form::open(['route' => 'editor.revenue.finance_approve', 'method' => 'PUT', 'id' => 'form_finance_approve_'.$revenue->id]) !!}
									                	<input type="hidden" name="revenue_id" value="{{$revenue->id}}">
									                	<input type="hidden" name="review" value="1">
									                	<button type="button" class="btn btn-success btn-sm btn_finance_approve" style="margin:5px" value="{{$revenue->id}}" title="Finance Approve">
									                		<i class="fa fa-check"></i>
								                		</button>
								                		{!! Form::close() !!}
													</div>	
												</div>
					                			@actionEnd
					                		@elseif($revenue->status == 3)
					                			@actionStart('revenue', 'issued')
					                			<div class="act_tb">
													<div>
														{!! Form::open(['route' => 'editor.revenue.owner_approve', 'method' => 'PUT', 'id' => 'form_owner_reject_'.$revenue->id]) !!}
									                	<input type="hidden" name="revenue_id" value="{{$revenue->id}}">
									                	<input type="hidden" name="review" value="0">
									                	<button type="button" class="btn btn-danger btn-sm btn_owner_reject" style="margin:5px" value="{{$revenue->id}}" title="Owner Reject">
									                		<i class="fa fa-remove"></i>
								                		</button>
								                		{!! Form::close() !!}
													</div>
													<div>	
														{!! Form::open(['route' => 'editor.revenue.owner_approve', 'method' => 'PUT', 'id' => 'form_owner_approve_'.$revenue->id]) !!}
									                	<input type="hidden" name="revenue_id" value="{{$revenue->id}}">
									                	<input type="hidden" name="review" value="1">
									                	<button type="button" class="btn btn-success btn-sm btn_owner_approve" style="margin:5px" value="{{$revenue->id}}" title="Owner Approve">
									                		<i class="fa fa-check"></i>
								                		</button>
								                		{!! Form::close() !!}
													</div>	
												</div>
				                				@actionEnd
					                		@endif
										</td>
										<td align="center">
											<div class="act_tb">
												<div>
													<a href="{{ URL::route('editor.revenue.detail', [$revenue->id]) }}" class="btn btn-sm btn-default"><i class="fa fa-search"></i></a>
												</div>&nbsp;
												@if($revenue->status < 4)
												@actionStart('revenue', 'update')
												<div>	
													<a href="{{ URL::route('editor.revenue.edit', [$revenue->id]) }}" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></a>
												</div>	
												@actionEnd
												@endif
												@actionStart('revenue', 'delete')
												<div>
													{!! Form::open(['route' => ['editor.revenue.delete', $revenue->id], 'method' => 'delete']) !!}
													<div>
														<button type="submit" onclick="return confirm('Delete?')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
													</div>
													{!! Form::close() !!}
												</div>
												@actionEnd
											</div>
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop
@actionEnd

@section('modal')
<div class="modal fade" id="modal_confirmation">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title"><i class="fa fa-warning"></i>Confirmation</h5>
      		</div>
      		<div class="modal-body">
        		<div id="modal_confirm_body"></div>
      		</div>
      		<div class="modal-footer">
      			<button type="button" class="btn btn-success" id="btn_confirm">OK</button>
      			<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      		</div>	
    	</div>
  	</div>
</div>
@stop

@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#table_revenue").DataTable();
    });

$('.btn_close').on('click', function()
{		
	var id = $(this).val();
	$('#modal_confirm_body').text('Close revenue for '+$('#branch_'+id).text()+', '+$('#date_'+id).text()+'?');
	$('#btn_confirm').on('click', function()
	{
		$('#form_close_'+id).submit();
		$('#btn_confirm').attr('disabled', true);
	});
	$('#modal_confirmation').modal('show');
});

$('.btn_request_edit').on('click', function()
{
	var id = $(this).val();
	$('#modal_confirm_body').text('Request edit revenue for '+$('#branch_'+id).text()+', '+$('#date_'+id).text()+'?');
	$('#btn_confirm').on('click', function()
	{
		$('#form_request_edit_'+id).submit();
		$('#btn_confirm').attr('disabled', true);
	});
	$('#modal_confirmation').modal('show');
});

$('.btn_finance_reject').on('click', function()
{
	var id = $(this).val();
	$('#modal_confirm_body').text('Reject edit revenue request for '+$('#branch_'+id).text()+', '+$('#date_'+id).text()+'?');
	$('#btn_confirm').on('click', function()
	{
		$('#form_finance_reject_'+id).submit();
		$('#btn_confirm').attr('disabled', true);
	});
	$('#modal_confirmation').modal('show');
});

$('.btn_finance_approve').on('click', function()
{
	var id = $(this).val();
	$('#modal_confirm_body').text('Approve edit revenue request for '+$('#branch_'+id).text()+', '+$('#date_'+id).text()+'?');
	$('#btn_confirm').on('click', function()
	{
		$('#form_finance_approve_'+id).submit();
		$('#btn_confirm').attr('disabled', true);
	});
	$('#modal_confirmation').modal('show');
});

$('.btn_owner_reject').on('click', function()
{
	var id = $(this).val();
	$('#modal_confirm_body').text('Reject edit revenue request for '+$('#branch_'+id).text()+', '+$('#date_'+id).text()+'?');
	$('#btn_confirm').on('click', function()
	{
		$('#form_owner_reject_'+id).submit();
		$('#btn_confirm').attr('disabled', true);
	});
	$('#modal_confirmation').modal('show');
});

$('.btn_owner_approve').on('click', function()
{
	var id = $(this).val();
	$('#modal_confirm_body').text('Approve edit revenue request for '+$('#branch_'+id).text()+', '+$('#date_'+id).text()+'?');
	$('#btn_confirm').on('click', function()
	{
		$('#form_owner_approve_'+id).submit();
		$('#btn_confirm').attr('disabled', true);
	});
	$('#modal_confirmation').modal('show');
});
</script> 
@stop