@extends('layouts.editor.template')
@section('content')



    <!-- Content Header (Page header) -->
    <section class="content-header hidden-xs">

        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-lg-12">
            <div class="row">
              <form method="post">
                {{ csrf_field() }}
              <div class="form-group">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <select class="form-control"  name="selectMonth" id="selectMonth">
                        @foreach($months as $month)
                          <option value="{{ $month->id }}">{{ $month->month_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-4">
                  <button type="button" class="btn btn-primary" id="btnFilter"><i class="fa fa-filter"></i> Filter</button>
                </div>
              </div>
            </form>
            </div>
          </div>
        <div class="col-lg-12">
          <canvas id="barChart"> </canvas>
        </div>

        <div class="col-lg-12">
          <canvas id="barChartMulti"></div>
        </div>
      </div>
    </section>
    <!-- /.content -->




@stop
@section('grafik')
<script type="text/javascript">

   $(function(){
     var url = "{{ url('api/revenue') }}";
     $('#btnFilter').on('click', function(event){
      //location.reload();

// var canvas = $('#barChartMulti')[0]; // or document.getElementById('canvas');
// canvas.width = canvas.width;
// var grapharea = document.getElementById("barChart").getContext("2d");
// var targetCanvas = document.getElementById('barChart').getContext('2d');
 // var myChart = new Chart(grapharea, { type: 'bar' });

// myChart.destroy();



      var monthInSelected = $('#selectMonth').val();
      var apiRevenue = url + '/' + monthInSelected;

       $.getJSON(apiRevenue, function(json) {
           var labels = [], data = [];
           console.log(apiRevenue);
           for(var i = 0; i < json.length; i++){
             labels.push(json[i].date);
             data.push(json[i].total);
           }
           var totalData = {
                 labels: labels,
                 datasets:[{
                    borderColor: "#FCEC0B",
                    fill: false,
                    pointBorderColor: "#FCEC0B",
                    pointBackgroundColor: "#FFF",
                    pointBorderWidth: 1,
                    pointHoverRadius: 8,
                    pointHoverBackgroundColor: "#FCEC0B",
                    pointHoverBorderColor: "#FFF",
                    pointHoverBorderWidth: 2,
                    pointRadius: 4,
                    pointHitRadius: 10,
                    label: 'Revenue',
                    data: data
                 }]
               };

               var chartBarId = document.getElementById('barChart').getContext('2d');
               if(window.bar != undefined)
                window.bar.destroy();

               window.bar = new Chart(chartBarId, {
                type: 'line',
                data: totalData,
                 options: {
                   animateScale : true,
                   tooltips:{
                     callbacks:{
                       title: function(tooltipItem, data){
                         var titleTooltips = moment(data['labels'][tooltipItem[0]['index']]).format('DD MMMM');
                         return titleTooltips;
                       },
                       label: function(tooltipItem, data){
                          var labelTooltips = data['datasets'][0]['data'][tooltipItem['index']].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                          return labelTooltips;
                        }
                       }
                   },
                   scales:{
                     xAxes:[{
                       type: 'time',
                       time:{
                         unit: 'day',
                         displayFormats:{
                          'millisecond': 'SSS [ms]',
                          'second': 'h:mm:ss a', // 11:20:01 AM
                          'minute': 'h:mm:ss a', // 11:20:01 AM
                          'hour': 'MMM D, hA', // Sept 4, 5PM
                          'day': 'DD MMM', // Sep 4 2015
                          'week': 'll', // Week 46, or maybe "[W]WW - YYYY" ?
                          'month': 'MMM YYYY', // Sept 2015
                          'quarter': '[Q]Q - YYYY', // Q3
                          'year': 'YYYY', // 2015
                         }
                       }
                     }],
                     yAxes:[{
                       ticks: {
                         beginAtZero:true,
                         min: 0,
                         max: 50000000,
                         callback: function(value, index, data){
                            value = value.toString();
                            value = value.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                            return value;
                         }

                        }
                     }]
                   }
                 }
               });

       });

     });

    });

  </script>

<script  type="text/javascript">


var listMonth = Array.apply(0, Array(12)).map(function(key, value){
  return moment().month(value).format('MMMM')
});


$(function(){

  var month = "{{ url('api/month') }}";
  var exp = "{{ url('api/expense_output') }}";
  var rev = "{{ url('api/revenue_output') }}";

   var revAmount = [];
  $.getJSON(rev , function(json){
    for (var i = 0; i < json.length; i++) {
        revAmount.push(json[i].amount);
    }

  });
  // console.log(revAmount);

  $.getJSON(exp , function(json){
    console.log(json);
      var month = [], expAmount = [];
      for (var i = 0; i < json.length; i++) {
        month.push(json[i].month_name);
        expAmount.push(json[i].amount);
      }
    // var monthExp = [], monthRev = [], typeData = [], valueData = [];
    // var exp=[], rev=[];
    // console.log(listMonth.length);
    // for (var i = 0; i < listMonth.length; i++) {
    //
    //   for(var int = 0; int < result.length; int++){
    //     // console.log(result[int].type_trans);
    //     // console.log(result[int]);
    //     if(result[int].type_trans== "Expense"){
    //       if (listMonth[i] == result[int].month_name) {
    //         exp.push(result[int].amount)
    //       }else{
    //         exp.push(0);
    //        }
    //
    //       // monthExp.push(result[int].month_name)
    //       // valueData.push(dataExp);
    //     }
    //     // else{
    //     //   // monthRev.push(result[int].month_name);
    //     //   if (listMonth[i] == result[int].month_name) {
    //     //     rev.push(result[int].amount)
    //     //   }else{
    //     //     rev.push(0);
    //     //   }
    //     //   // rev.push(result[int].amount);
    //     //   // valueData.push(dataRev);
    //     // }
    //   }
    // }

    //console.log(exp);
  var value ={
  labels: listMonth,
  datasets: [{
      label: "Expense",
      fill: false,
      lineTension: 0.1,
      borderColor: "red", // The main line color
      borderCapStyle: 'square',
      borderDash: [], // try [5, 15] for instance
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "red",
      pointBackgroundColor: "white",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "red",
      pointHoverBorderColor: "white",
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
      showTooltip: false,
      data: expAmount,
      spanGaps: true,
    },
     {
      label: "Revenue",
      fill: false,
      lineTension: 0.1,
      borderColor: "#2980b9",
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "#2980b9",
      pointBackgroundColor: "#FFF",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "#2980b9",
      pointHoverBorderColor: "#FFF",
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      showTooltip: false,
      pointHitRadius: 10,
      data:revAmount,
      spanGaps: false,
    }

  ]
  };

// Notice the scaleLabel at the same level as Ticks
    var options = {
           animateScale : true,
    scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true,
                      callback: function(value, index, data){
                         value = value.toString();
                         value = value.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                         return value;
                      }
                  },
                  scaleLabel: {
                       display: true,
                       fontSize: 20
                    }
              }]
          }
    };

    // Chart declaration:
    var barChartMulti = document.getElementById('barChartMulti');

    var myBarChart = new Chart(barChartMulti, {
      type: 'line',
      data: value,
      options: options
    });
  });
  });
</script>

  <script type="text/javascript">


   $(document).ready(function(){

    $('#emp_hiring_date').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
    $('#start_date').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
    $('#end_date').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
    $('#date').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
    $('#invoice_date').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
    $('#paid_date').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
  });
</script>
@stop