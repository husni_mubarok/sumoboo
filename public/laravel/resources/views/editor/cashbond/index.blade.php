@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->

<style type="text/css">
	th { font-size: 11px; }
	td { font-size: 11px; }
</style>
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="#"><i class="fa fa-dollar"></i> Cashbond</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">

				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							<i class="fa fa-dollar"></i> Cashbond
							@actionStart('cashbond', 'create')
							<a href="{{ URL::route('editor.cashbond.create') }}" class="btn btn-success btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
							@actionEnd
						</h2>
						<hr>
						<div class="x_content" style=" overflow: scroll;">

							<table id="cashbondTablex" class="table table-responsive  dataTable rwd-table" border="0" style="overflow-x:scroll !important;width:100%;">
								<thead>
									<tr>
										<th rowspan="2">#</th>
										<th colspan="2" class="text-center">Period</th>
										<th rowspan="2">Cashbond Type</th>
										<th rowspan="2">Budget Request</th>
										<th rowspan="2">Budget Issued</th>

										<th rowspan="2">Cashout</th>
										<th rowspan="2">Remainder</th>
										<th rowspan="2">Add Amount</th>
										<th rowspan="2">Grand Total</th>
										<th rowspan="2">Branch</th>
										<th rowspan="2">Transfer Receipt</th>
										<th rowspan="2">Remainder Receipt</th>
										<th rowspan="2">Status</th>
										<th rowspan="2">Action Status</th>
										<th rowspan="2">Action</th>
									</tr>
									<tr>
										<th>Start Date</th>
										<th>End Date</th>

									</tr>
								</thead>
								<tbody>
									@foreach($cashbonds as $key => $cashbond)
									<tr>
										<td data-th="#">{{$number++}}</td>
										<td data-th="Start Date">{{date("d M Y", strtotime($cashbond->start_date))}}</td>
										<td data-th="End Date">{{date("d M Y", strtotime($cashbond->end_date))}}</td>
										<td data-th="Type">{{ $cashbond->cashbond_type }}</td>
										<td data-th="Budget Request">{{ number_format($cashbond->budget_request,0) }}</td>
										<td data-th="Budget Issued">{{ number_format($cashbond->budget_issued,0) }}</td>
										<td data-th="Cashout">{{ number_format($cashbond->cashout,0) }}</td>
										<td data-th="Remainder">{{ number_format($cashbond->budget_issued-$cashbond->cashout,0) }}</td>
										<td data-th="Add Amount">{{ number_format($cashbond->add_amount,0) }}</td>
										<td data-th="Grand Total">{{ number_format($cashbond->cashout + $cashbond->add_amount,0) }}</td>
										<td data-th="branch">{{ $cashbond->branch->branch_name }}</td>
										<td data-th="Attachment">
											@if($cashbond->transfer_receipt == null)
											Tidak ada lampiran
											@else
											<a target="_blank" href="{{Config::get('constants.path.uploads')}}/cashbond/{{$cashbond->transfer_receipt}}"><i class="fa fa-download"></i>&nbsp;Download</a>
											@endif
										</td>
										<td data-th="Attachment">
											@if($cashbond->remainder_receipt == null)
											Tidak ada lampiran
											@else
											<a target="_blank" href="{{Config::get('constants.path.uploads')}}/cashbond/remainder/{{$cashbond->remainder_receipt}}"><i class="fa fa-download"></i>&nbsp;Download</a>
											@endif
										</td>


										<td data-th="Status">
											<span class="">
											<span class="label label-primary"><i class="fa fa-unlock"></i>
											{{$cashbond->status_code}}
											</span>
											<span>
											</td>
											<td  data-th="Action Status" align="">
												@if($cashbond->status_code=='Waiting for Owner Action')

												<!-- {!! Form::open(array('route' => ['editor.cashbond.waitingowner', $cashbond->id], 'method' => 'PUT', 'class'=>'waitingowner'))!!}
												{{ csrf_field() }}	 -->

												@actionStart('cashbond', 'issued')
												<!-- <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i>&nbspFinalize</button> -->
												<a href="{{ URL::route('editor.cashbond.issued', [$cashbond->id]) }}" class="btn btn-success btn-sm"><i class="fa fa-check"></i>&nbsp;Issued</a>
												@actionEnd
												<!-- {!! Form::close() !!} -->

												@elseif($cashbond->status_code=='Waiting for Financial Action')
												@actionStart('cashbond', 'paid')
												<a href="{{ URL::route('editor.cashbond.finalize', [$cashbond->id]) }}" class="btn btn-success btn-sm"><i class="fa fa-money"></i>&nbspTransfer</a>
												@actionEnd

												{!! Form::close() !!}

												@elseif($cashbond->status_code=='Transfered' or $cashbond->status_code=='Waiting for Revision')

												@actionStart('cashbond', 'update')
												<a href="{{ URL::route('editor.cashbond.closing', [$cashbond->id]) }}" class="btn btn-success btn-sm"><i class="fa fa-check"></i>&nbspClosing</a>
												@actionEnd



												@elseif($cashbond->status_code=='Waiting Verified')
												@actionStart('cashbond', 'paid')
												<a href="{{ URL::route('editor.cashbond.verified', [$cashbond->id]) }}" class="btn btn-success btn-sm"><i class="fa fa-check"></i>&nbspVerified</a>
												@actionEnd



												@elseif($cashbond->status_code=='remainder')
												<a href="{{ URL::route('editor.cashbond.remainder', [$cashbond->id]) }}" class="btn btn-success btn-sm"><i class="fa fa-check"></i>&nbspRemainder</a>
												@endif


											</td>
											<td  align="center">
												@if($cashbond->status_code=='Waiting for Owner Action')
												@actionStart('cashbond', 'update')
												<a href="{{ URL::route('editor.cashbond.edit', [$cashbond->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
												@actionEnd

												@actionStart('cashbond', 'delete')
												{!! Form::open(array('route' => ['editor.cashbond.delete', $cashbond["id"]], 'method' => 'delete', 'class'=>'delete'))!!}
												{{ csrf_field() }}

												<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></button>
												{!! Form::close() !!}
												@actionEnd

												@elseif($cashbond->status_code=='Waiting Verified')
												<div>
												<a href="{{ URL::route('editor.cashbond.view', [$cashbond->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a>
												</div>&nbsp;
												<div>
												@actionStart('cashbond', 'delete')
												{!! Form::open(array('route' => ['editor.cashbond.delete', $cashbond["id"]], 'method' => 'delete', 'class'=>'delete'))!!}
												{{ csrf_field() }}

												<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></button>
												{!! Form::close() !!}
												@actionEnd
												</div>
												@else


												@actionStart('cashbond', 'delete')
												{!! Form::open(array('route' => ['editor.cashbond.delete', $cashbond["id"]], 'method' => 'delete', 'class'=>'delete'))!!}
												{{ csrf_field() }}

												<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></button>
												{!! Form::close() !!}
												@actionEnd

												@endif
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							{{ $cashbonds->links() }}
						</div>
					</div>
				</div>
			</div>
		</section>
	</section>
	@stop

	@section('scripts')
	<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
	<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
	<script>
		$(document).ready(function () {
			$("#cashbondTable").DataTable();
		});
	</script>
	<script>
		$(".waitingowner").on("submit", function(){
			return confirm("Do you want to finalize this cashbond?");
		});
		$(".waitingfinalize").on("submit", function(){
			return confirm("Do you want to transfer this cashbond?");
		});
	</script>
	<script>
		$(".delete").on("submit", function(){
			return confirm("Delete this cashbond?");
		});
	</script>
	@stop