<style type="text/css">
	.modal {
		text-align: center;
		padding: 0!important;
	}

	.modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}
</style>
@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="{{ URL::route('editor.cashbond.index') }}"><i class="fa fa-dollar"></i> Cashbond</a></li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($cashbond))
							<i class="fa fa-pencil"></i>
							@else
							<i class="fa fa-plus"></i>
							@endif
							&nbsp;Cashbond Request
						</h2>
					</div>
					<hr>
					@include('errors.error')
					@if(isset($cashbond))
					{!! Form::model($cashbond, array('route' => ['editor.cashbond.update', $cashbond->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_cashbondrequest'))!!}
					@else
					{!! Form::open(array('route' => 'editor.cashbond.store', 'class'=>'create', 'id'=>'form_cashbondrequest'))!!}
					@endif
					{{ csrf_field() }}
					<div class="col-md-6">
						<div class="x_content">
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">
								{{ Form::label('cashbond Type') }}
								<select class="form-control" required="true" id="cashbond_type" name="cashbond_type">
									<option selected="selected" disabled="disabled" hidden="hidden" value="">Select Invoice Type</option>
									<option value="bahan_baku">Bahan Baku</option>
									<option value="maintenance">Maintenance</option>
								</select><br/>



								{{ Form::label('start_date', 'Start Date') }}
								{{ Form::text('start_date', old('start_date'), array('class' => 'form-control', 'placeholder' => 'Start Date*', 'required' => 'start_date', 'id' => 'date')) }}<br/>

								{{ Form::label('end_date', 'End Date') }}
								{{ Form::text('end_date', old('end_date'), array('class' => 'form-control', 'placeholder' => 'End Date*', 'required' => 'true', 'id' => 'end_date')) }}<br/>

								{{ Form::label('budget_request_show', 'Budget Request') }}
								@if(isset($cashbond))
								{{ Form::text('budget_request_show',number_format($cashbond->budget_request,0), array('class' => 'form-control', 'placeholder' => 'Budget Request*', 'required' => 'true', 'id' => 'budget_request_show', 'oninput' => 'cal_sparator();')) }}<br/>
								@else
								{{ Form::text('budget_request_show',old('budget_request_show'), array('class' => 'form-control', 'placeholder' => 'Budget Request*', 'required' => 'true', 'id' => 'budget_request_show', 'oninput' => 'cal_sparator();')) }}<br/>
								@endif
								{{ Form::hidden('budget_request', old('budget_request'), array('id' => 'budget_request')) }}

								{{ Form::label('comment', 'Comment') }}
								{{ Form::text('comment', old('comment'), array('class' => 'form-control', 'placeholder' => 'Comment*', 'required' => 'true')) }}

							</div>
						</div>
						<button type="button" data-toggle="modal" data-target="#modal_cashbondrequest" class="btn btn-success pull-right"><i class="fa fa-check"></i> Submit</button>
						<a href="{{ URL::route('editor.cashbond.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
					</div>
				</div>
				<hr>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
</section>

@stop


@section('modal')
<div class="modal fade" id="modal_cashbondrequest">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Save this cashbond request?</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Cancel</button>
				<button type="button" id="btn_submit" class="btn btn-success"><i class="fa fa-check"></i> OK</button>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
	$('#btn_submit').on('click', function()
	{
		$('#form_cashbondrequest').submit();
	});

	function cal_sparator() {
		var budget_request_show = document.getElementById('budget_request_show').value;
		var result = document.getElementById('budget_request');
		var rsbudgetrequest = (budget_request_show);
		result.value = rsbudgetrequest.replace(/,/g, "");
	}

	window.onload= function(){

		n2= document.getElementById('budget_request_show');

		n2.onkeyup=n2.onchange= function(e){
			e=e|| window.event;
			var who=e.target || e.srcElement,temp;
			if(who.id==='budget_request')  temp= validDigits(who.value,0);
			else temp= validDigits(who.value);
			who.value= addCommas(temp);
		}
		n2.onblur= function(){
			var
			temp2=parseFloat(validDigits(n2.value));
			if(temp2)n2.value=addCommas(temp2.toFixed(0));
		}

	}
</script>
@stop