@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->

<style type="text/css">
	th { font-size: 11px; }
	td { font-size: 11px; }
</style>
<section class="content-header hidden-xs">
  <h1>
    CMS
    <small>Content Management System</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
     <li><a href="{{ URL::route('editor.document.index') }}"><i class="fa fa-home"></i> Document</a></li>
    <li class="active"><a href="#"><i class="fa fa-cog"></i>Document File</a></li>
  </ol>
</section>
{{-- @actionStart('document', 'read') --}}
<section class="content">
	<section class="content box mobile box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-12">
			        <div class="x_panel">
		                <h2>
		                	<i class="fa fa-cog"></i> Document Files
		                	{{-- @actionStart('document', 'create') --}}
		                	<a href="{{ URL::route('editor.document_file.create',$id) }}" class="btn btn-success btn-lg pull-right"><i class="fa fa-plus"></i> Add</a>
		                {{-- 	@actionEnd --}}
	                	</h2>
		                <hr>
			           <div class="x_content">
			                <table id="table_franchise_feex" class="table dataTable rwd-table">
							  	<thead>
							  	  	<tr>
								      	<th width="5%">#</th>

								      	<th>Title</th>
								      	<th>Description</th>

										<th>File</th>

								      	<th width="10%">Action</th>
							    	</tr>
							  	</thead>
							  	<tbody>

								@foreach($lists as $list)
									<tr>
										<td data-th="#">{{ $loop->iteration }}</td>

										<td >{{ $list->title  }}</td>
										<td >{{ $list->description  }}</td>
										<td ><a target="_blank" href="{{Config::get('constants.path.uploads')}}/document/{{$list->file}}"><i class="fa fa-download"></i>&nbsp;Download</a></td>

										<td align="center" >{{-- @actionStart('document', 'update') --}}
											<div class="act_tb">
												<div>
													<a href="{{ URL::route('editor.document_file.edit', [$list->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
												</div>
											{{-- 	@actionEnd --}}
												<div>
													{!! Form::open(array('route' => ['editor.document_file.delete', $list->id ], 'method' => 'delete', 'class'=>'delete'))!!}
													{{ csrf_field() }}

												{{-- 	@actionStart('maintenance', 'delete') --}}
													<button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash"></i></a></button>
												{{-- 	@actionEnd --}}

													{!! Form::close() !!}
												</div>
											</div>
										</td>

									</tr>
								@endforeach

								</tbody>
							</table>
			            </div>

			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@stop
{{-- @actionEnd --}}



@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script>
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<!-- Add fancyBox -->
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script>
	$(".delete").on("submit", function(){
		return confirm("Delete this row?");
	});
</script>
@stop