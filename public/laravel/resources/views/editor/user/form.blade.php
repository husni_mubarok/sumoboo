@extends('layouts.editor.template')
@section('content')
<section class="content-header hidden-xs">
  	<h1>
    	CMS
    	<small>Content Management System</small>
  	</h1>
  	<ol class="breadcrumb">
    	<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
    	<li><a href="{{ URL::route('editor.user.index') }}"><i class="fa fa-user-circle"></i> User List</a></li>
    	<li class="active">
    		@if(isset($user))
    		<i class="fa fa-pencil"></i> Edit
    		@else
    		<i class="fa fa-plus"></i> Create
    		@endif
		</li>
  	</ol>
</section>
@actionStart('user', 'create|update')
<section class="content">
	<section class="content box box-solid">
		<div class="row">
		    <div class="col-md-12 col-sm-12 col-xs-12">
		    	<div class="col-md-1"></div>
		    	<div class="col-md-5">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($user))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i>
		                	@endif
		                	&nbsp;User
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')

			                @if(isset($user))
			                {!! Form::model($user, array('route' => ['editor.user.update', $user->id], 'method' => 'PUT', 'files' => 'true'))!!}
		                    @else
		                    {!! Form::open(array('route' => 'editor.user.store', 'files' => 'true'))!!}
		                    @endif
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
		                    	{{ Form::label('username', 'Username') }}
		                    	@if(isset($user))
		                    	{{ Form::text('username', old('username'), ['class' => 'form-control', 'disabled' => 'true']) }}
		                    	@else
		                    	{{ Form::text('username', old('username'), ['class' => 'form-control']) }}
		                    	@endif
		                    	<br>

		                    	{{ Form::label('Employee') }}
		                    	{{ Form::select('employee_id', $list_employee, old('employee_id'), array('class' => 'form-control', 'required' => 'true')) }}<br/>

		                    	{{ Form::label('password', 'Password') }}
		                    	{{ Form::password('password', ['class' => 'form-control']) }}
		                    	<br>

		                    	{{ Form::label('password_confirmation', 'Confirm Password') }}
		                    	{{ Form::password('password_confirmation', ['class' => 'form-control']) }}
		                    	<br>

	                            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
	</section>
</section>
@actionEnd
@stop
