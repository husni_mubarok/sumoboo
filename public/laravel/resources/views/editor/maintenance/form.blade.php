@extends('layouts.editor.template')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hidden-xs">
	<h1>
		CMS
		<small>Content Management System</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ URL::route('editor.index') }}"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="{{ URL::route('editor.maintenance.index') }}"><i class="fa fa-cog"></i> Maintenance</a></li>
		<li class="active">


			@if(isset($main_list))
				<i class="fa fa-pencil"></i> Edit
					@else
				<i class="fa fa-plus"></i> Create
			@endif
		</li>
	</ol>
</section>

<section class="content">
	<section class="content box box-solid">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-12">
					<div class="x_panel">
						<h2>
							@if(isset($main_list))
								<i class="fa fa-pencil"></i> Edit Maintenance
									@else
								<i class="fa fa-plus"></i> Create Maintenance
							@endif


						</h2>
					</div>
					<hr>
					<div class="col-md-12">
					@include('errors.error')
					@if(isset($main_list))
					{!! Form::model($main_list, array('route' => ['editor.maintenance.update', $main_list->id], 'files' => 'true', 'method' => 'PUT', 'class'=>'update', 'id' => 'form_payroll'))!!}
					@else
					{!! Form::open(array('route' => 'editor.maintenance.store', 'files' => 'true', 'class'=>'create', 'id' => 'form_payroll'))!!}
					@endif
					{{ csrf_field() }}
						<div class="x_content">
						@if(isset($main_list))
							{{ Form::label('date', 'Date') }}
							{{ Form::text('date', old('date'), array('class' => 'form-control', 'placeholder' => 'Date*', 'required' => 'true', 'id' => 'date','read-only' => 'true')) }}<br/>

							{{ Form::label('service_name', 'Service Name') }}
							{{ Form::text('service_name', old('service_name'), array('class' => 'form-control', 'placeholder' => 'Service Name', 'required' => 'true')) }}<br/>

							{{ Form::label('alasan', 'Reason') }}
							{{ Form::text('alasan', old('alasan'), array('class' => 'form-control', 'placeholder' => 'Reason', 'required' => 'true')) }}<br/>


							{{ Form::label('harga', 'Price') }}
							{{ Form::number('harga', old('harga'), array('class' => 'form-control', 'placeholder' => 'Price*', 'required' => 'true')) }}<br/>

							{{ Form::label('attch_foto', 'Photo Attachment') }}
							{{ Form::file('attch_foto') }}

							<br>

							{{ Form::label('attch_harga', 'Price Attachment') }}
							{{ Form::file('attch_harga') }}
							<br>
						@else

							{{ Form::label('date', 'Date') }}
							{{ Form::text('date', old('date'), array('class' => 'form-control', 'placeholder' => 'Date*', 'required' => 'true', 'id' => 'date','read-only' => 'true')) }}<br/>

							{{ Form::label('service_name', 'Service Name') }}
							{{ Form::text('service_name', old('service_name'), array('class' => 'form-control', 'placeholder' => 'Price*', 'required' => 'true')) }}<br/>

							{{ Form::label('alasan', 'Reason') }}
							{{ Form::text('alasan', old('alasan'), array('class' => 'form-control', 'placeholder' => 'Reason', 'required' => 'true')) }}<br/>


							{{ Form::label('harga', 'Price') }}
							{{ Form::number('harga', old('harga'), array('class' => 'form-control', 'placeholder' => 'Price*', 'required' => 'true')) }}<br/>

							{{ Form::label('attch_foto', 'Photo Attachment') }}
							{{ Form::file('attch_foto') }}

							<br>

							{{ Form::label('attch_harga', 'Invoice Attachment') }}
							{{ Form::file('attch_harga') }}

						@endif
						<br>
							<button type="submit" id="btn_confirm" class="btn btn-success"><i class="fa fa-check"></i> Save</button>

						</div>

					{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop


@section('scripts')
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>


@stop
