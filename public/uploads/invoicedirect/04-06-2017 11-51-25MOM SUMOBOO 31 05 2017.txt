1. Created employee->user
2. Created grouping jabatan pada previlage (hold)
3. Otorisasi pada button berdasarkan user
4. Total pada detail item di clear (cashbond detail item)
5. Jika ada input item yang sama qty nambah (cashbond detail item)
6. Action pada no nota numpuk (cashbond)
7. Tombol edit hanya aktif pada saat masih create baru (cashbond)
8. Tanggal dibuat 17 mei(3 huruf) 2017
9. Period pada invoice berupa bulan dan tahun (select 2 field) -> Invoice 
10. Notif pada invoice typo
11. Name pada invoice jadi invoice utilization
12. Daftar data yang sudah masuk bank (invoice & consummable) dibuat tab pada index
13. Approve button rename jadi issued + alert (invoice)
14. Pada invoice type tambah categori (util & consummable) dan di transaksi comboboxnya sudah difilter
15. Combobox vendor sudah auto berdasarkan invoice type yang dipilih
16. Semua button approve diganti jadi issued (jika tidak ada decision)
17. Label pada import file ditambah extention
18. Ada download template pada import item central dengan format .ods yang sama dengan yang sekarang
19. Sorting pada item central index error (item central)
20. Pada saat submit invoice detail sudah disabled (item central) -> setelah proses delivery
21. Pada submit invoice ditambah amount transport (nambah amount total)
22. Pada item central index field consummable dihide
23. Buat notif payroll
24. Dibuat domain jadi sumoboo-system.com
25. Tambah numbering pada create+modal cashbond payroll
26. Buat redirect ke index pada edit request revenue
27. Tombol close pada revenue ditambah validasi
28. Revenue servis charge yang value editable
29. Role : superuser, owner, finance, user, reader
30. 