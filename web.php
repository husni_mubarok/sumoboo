<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Front End
Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
//IMS.99 Portal
Route::get('/portal', ['as' => 'portal', 'uses' => 'HomeController@portal']);
Route::get('/ims99/purchase_setup', ['as' => 'purchase_setup', 'uses' => 'HomeController@purchase_setup']);
Route::get('/ims99/purchase', ['as' => 'purchase_setup', 'uses' => 'HomeController@purchase']);
Route::get('/ims99/production_setup', ['as' => 'production_setup', 'uses' => 'HomeController@production_setup']);
Route::get('/ims99/production', ['as' => 'production', 'uses' => 'HomeController@production']);
Route::get('/ims99/production_tab', ['as' => 'production_tab', 'uses' => 'HomeController@production_tab']);
Route::get('/ims99/end_production', ['as' => 'end_production', 'uses' => 'HomeController@end_production']);
Route::get('/ims99/storage/{id}', ['as' => 'storage', 'uses' => 'HomeController@storage']);
Route::get('/ims99/marketing_cost', ['as' => 'marketing_cost', 'uses' => 'HomeController@marketing_cost']);

Route::get('/test', ['as' => 'test', 'uses' => 'HomeController@test']);

//API
Route::group(['prefix' => 'api', 'namespace' => 'API'], function()
{
	//get purchase list
	Route::post('/purchase/index', ['middleware' => 'cors', 'as' => 'api.purchase.index', 'uses' => 'PurchaseController@index']);
	//get purchase detail
	Route::post('/purchase/detail', ['middleware' => 'cors', 'as' => 'api.purchase.detail', 'uses' => 'PurchaseController@detail']);
	//get production list
	Route::post('/production/index', ['middleware' => 'cors', 'as' => 'api.production.index', 'uses' => 'ProductionController@index']);
	//get end product list
	Route::post('/end_product/index', ['middleware' => 'cors', 'as' => 'api.end_product.index', 'uses' => 'EndProductController@index']);
	//get unstored items
	Route::post('/storage/waiting', ['middleware' => 'cors', 'as' => 'api.storage.waiting', 'uses' => 'StorageController@waiting']);
	//get stored items
	Route::post('/storage/stored', ['middleware' => 'cors', 'as' => 'api.storage.stored', 'uses' => 'StorageController@stored']);

	//get stock index
	Route::post('/stock/index', ['middleware' => 'cors', 'as' => 'api.stock.index', 'uses' => 'StockController@index']);

	//get customer
	Route::post('/customer/index', ['middleware' => 'cors', 'as' => 'api.customer.index', 'uses' => 'CustomerController@index']);
	Route::post('/customer/detail', ['middleware' => 'cors', 'as' => 'api.customer.detail', 'uses' => 'CustomerController@detail']);


	//get cart or export
	Route::post('/export/index', ['middleware' => 'cors', 'as' => 'api.export.index', 'uses' => 'ExportController@index']);
	Route::post('/export/detail', ['middleware' => 'cors', 'as' => 'api.export.detail', 'uses' => 'ExportController@detail']);
	Route::post('/export/lookup', ['middleware' => 'cors', 'as' => 'api.export.lookup', 'uses' => 'ExportController@lookup']);
	Route::post('/export/store', ['middleware' => 'cors', 'as' => 'api.export.store', 'uses' => 'ExportController@store']);
	Route::post('/export/update', ['middleware' => 'cors', 'as' => 'api.export.update', 'uses' => 'ExportController@update']);
	Route::post('/export/updatedetail', ['middleware' => 'cors', 'as' => 'api.export.updatedetail', 'uses' => 'ExportController@updatedetail']);
	Route::post('/export/storeconfirm', ['middleware' => 'cors', 'as' => 'api.export.storeconfirm', 'uses' => 'ExportController@storeconfirm']);
	Route::post('/export/storeexport', ['middleware' => 'cors', 'as' => 'api.export.storeexport', 'uses' => 'ExportController@storeexport']);
	Route::post('/export/storernd', ['middleware' => 'cors', 'as' => 'api.export.storernd', 'uses' => 'ExportController@storernd']);
	Route::post('/export/storeappcoo', ['middleware' => 'cors', 'as' => 'api.export.storeappcoo', 'uses' => 'ExportController@storeappcoo']);
	Route::post('/export/storeappceo', ['middleware' => 'cors', 'as' => 'api.export.storeappceo', 'uses' => 'ExportController@storeappceo']);
	Route::post('/export/deletedet', ['middleware' => 'cors', 'as' => 'api.export.deletedet', 'uses' => 'ExportController@deletedet']);

	Route::post('/storage_list', ['middleware' => 'cors', 'as' => 'api.storage.list', 'uses' => 'StorageController@storage_list']);
	Route::post('/storage_detail', ['middleware' => 'cors', 'as' => 'api.storage.detail', 'uses' => 'StorageController@storage_detail']);
	Route::post('/storage_notification',  ['middleware' => 'cors', 'as' => 'api.storage.notification', 'uses' => 'StorageController@notif']);
});

//Purchasing
Route::group(['namespace' => 'Home'], function()
{
		// - index
	Route::get('/purchase', ['middleware' => ['role:purchase|read'], 'as' => 'purchase.index', 'uses' => 'PurchaseController@index']);
		// - create purchase
	Route::get('/purchase/create', ['middleware' => ['role:purchase|create'], 'as' => 'purchase.create', 'uses' => 'PurchaseController@create_purchase']);
	Route::post('/purchase', ['middleware' => ['role:purchase|create'], 'as' => 'purchase.store', 'uses' => 'PurchaseController@store_purchase']);
		// - edit purchase
	Route::get('/purchase/{id}/edit', ['middleware' => ['role:purchase|update'], 'as' => 'purchase.edit', 'uses' => 'PurchaseController@edit_purchase']);
	Route::put('/purchase/{id}/edit', ['middleware' => ['role:purchase|update'], 'as' => 'purchase.update', 'uses' => 'PurchaseController@update_purchase']);
		// - create price
	Route::get('/purchase/{id}/price/create', ['middleware' => ['role:price|create'], 'as' => 'price.create', 'uses' => 'PurchaseController@create_price']);
	Route::post('/purchase/{id}/price', ['middleware' => ['role:price|create'], 'as' => 'price.store', 'uses' => 'PurchaseController@store_price']);
		// - edit price
	Route::get('/purchase/{id}/price/edit', ['middleware' => ['role:price|update'], 'as' => 'price.edit', 'uses' => 'PurchaseController@edit_price']);
		// - confirm PO RM
	Route::get('/purchase/{id}/confirm_po_rm', ['middleware' => ['role:purchase|update'], 'as' => 'purchase.confirm_po_rm', 'uses' => 'PurchaseController@confirm_po_rm']);
		// - reject PO RM
	Route::post('/purchase/{id}/reject_po_rm', ['middleware' => ['role:price|reject'], 'as' => 'purchase.reject_po_rm', 'uses' => 'PurchaseController@reject']);
		// - approve PO RM
	Route::post('/purchase/{id}/approve_po_rm', ['middleware' => ['role:price|approve'], 'as' => 'purchase.approve_po_rm', 'uses' => 'PurchaseController@approve']);
		// - create purchase detail
	Route::get('/purchase/{id}/detail', ['middleware' => ['role:purchase_detail|create'], 'as' => 'purchase_detail.create', 'uses' => 'PurchaseController@create_purchase_detail']);
	Route::post('/purchase/{id}/detail', ['middleware' => ['role:purchase_detail|create'], 'as' => 'purchase_detail.store', 'uses' => 'PurchaseController@store_purchase_detail']);
		// - edit purchase detail
	Route::get('/purchase/{id}/detail/edit', ['middleware' => ['role:purchase_detail|update'], 'as' => 'purchase_detail.edit', 'uses' => 'PurchaseController@edit_purchase_detail']);
		// - confirm Actual Delivery
	Route::get('/purchase/{id}/confirm_actual_delivery', ['middleware' => ['role:purchase|update'], 'as' => 'purchase.confirm_actual_delivery', 'uses' => 'PurchaseController@confirm_actual_delivery']);
		// - reject Actual Delivery
	Route::post('/purchase/{id}/reject_actual_delivery', ['middleware' => ['role:purchase_detail|reject'], 'as' => 'purchase.reject_actual_delivery', 'uses' => 'PurchaseController@reject']);
		//- approve Actual Delivery
	Route::post('/purchase/{id}/approve_actual_delivery', ['middleware' => ['role:purchase_detail|approve'], 'as' => 'purchase.approve_actual_delivery', 'uses' => 'PurchaseController@approve']);
		// - summary
	Route::get('/purchase/{id}/summary', ['middleware' => ['role:purchase|read'], 'as' => 'purchase.summary', 'uses' => 'PurchaseController@summary']);
		// - close
	Route::put('/purchase/{id}/close', ['middleware' => ['role:purchase|update'], 'as' => 'purchase.close', 'uses' => 'PurchaseController@close']);
		// - delete
	Route::delete('/purchase/{id}/delete', ['middleware' => ['role:purchase|delete'], 'as' => 'purchase.delete', 'uses' => 'PurchaseController@delete']);
});

//Production
Route::group(['namespace' => 'Home'], function()
{
		// - index
	Route::get('/production', ['middleware' => ['role:production_result|read'], 'as' => 'production', 'uses' => 'ProductionController@index']);
		// - unstored
	Route::get('/production/unstored', ['middleware' => ['role:production_result|read'], 'as' => 'production.unstored', 'uses' => 'ProductionController@unstored']);
		// - bank
	Route::get('/production/bank', ['middleware' => ['role:production_result|read'], 'as' => 'production.bank', 'uses' => 'ProductionController@bank']);
		// - create
	Route::get('/production/create', ['middleware' => ['role:production_result|create'], 'as' => 'production.create', 'uses' => 'ProductionController@create_production']);
	Route::post('/production/create', ['middleware' => ['role:production_result|create'], 'as' => 'production.store', 'uses' => 'ProductionController@store_production']);
		// - edit
	Route::get('/production/{id}/edit', ['middleware' => ['role:production_result|update'], 'as' => 'production.edit', 'uses' => 'ProductionController@edit_production']);
	Route::put('/production/{id}/edit', ['middleware' => ['role:production_result|update'], 'as' => 'production.update', 'uses' => 'ProductionController@update_production']);
		// - create results
	Route::get('/production/{id}/results', ['middleware' => ['role:production_result|create'], 'as' => 'production.create_results', 'uses' => 'ProductionController@create_results']);
	Route::post('/production/{id}/results', ['middleware' => ['role:production_result|create'], 'as' => 'production.store_results', 'uses' => 'ProductionController@store_results']);
		// - edit results
	Route::get('/production/{id}/results/edit', ['middleware' => ['role:production_result|update'], 'as' => 'production.edit_results', 'uses' => 'ProductionController@edit_results']);
	Route::put('/production/{id}/results/edit', ['middleware' => ['role:production_result|update'], 'as' => 'production.update_results', 'uses' => 'ProductionController@update_results']);
		// - create waste
	Route::get('/production/{id}/waste', ['middleware' => ['role:production_result|create'], 'as' => 'production.create_waste', 'uses' => 'ProductionController@create_waste']);
	Route::post('/production/{id}/waste', ['middleware' => ['role:production_result|create'], 'as' => 'production.store_waste', 'uses' => 'ProductionController@store_waste']);
		// - edit waste
	Route::get('/production/{id}/waste/edit', ['middleware' => ['role:production_result|update'], 'as' => 'production.edit_waste', 'uses' => 'ProductionController@edit_waste']);
	Route::put('/production/{id}/waste/edit', ['middleware' => ['role:production_result|update'], 'as' => 'production.update_waste', 'uses' => 'ProductionController@update_waste']);
		// - create reject
	Route::get('/production/{id}/reject', ['middleware' => ['role:production_result|create'], 'as' => 'production.create_reject', 'uses' => 'ProductionController@create_reject']);
	Route::post('/production/{id}/reject', ['middleware' => ['role:production_result|create'], 'as' => 'production.store_reject', 'uses' => 'ProductionController@store_reject']);
		// - edit reject
	Route::get('/production/{id}/reject/edit', ['middleware' => ['role:production_result|update'], 'as' => 'production.edit_reject', 'uses' => 'ProductionController@edit_reject']);
	Route::post('/production/{id}/reject/edit', ['middleware' => ['role:production_result|update'], 'as' => 'production.update_reject', 'uses' => 'ProductionController@update_reject']);
		// - create result
	Route::get('/production/{id}/result', ['middleware' => ['role:production_result|create'], 'as' => 'production.create_result', 'uses' => 'ProductionController@create_result']);
	Route::post('/production/{id}/result', ['middleware' => ['role:production_result|create'], 'as' => 'production.store_result', 'uses' => 'ProductionController@store_result']);
		// - edit result
	Route::get('/production/{id}/result/edit', ['middleware' => ['role:production_result|update'], 'as' => 'production.edit_result', 'uses' => 'ProductionController@edit_result']);
	Route::put('/production/{id}/result/edit', ['middleware' => ['role:production_result|update'], 'as' => 'production.update_result', 'uses' => 'ProductionController@update_result']);
		// - create size
	Route::get('/production/{id}/size', ['middleware' => ['role:production_result|create'], 'as' => 'production.create_size', 'uses' => 'ProductionController@create_size']);
	Route::post('/production/{id}/size', ['middleware' => ['role:production_result|create'], 'as' => 'production.store_size', 'uses' => 'ProductionController@store_size']);
		// - transaction
	Route::get('/production/{id}/transaction', ['middleware' => ['role:production_result|update'], 'as' => 'production.transaction', 'uses' => 'ProductionController@transaction']);

	Route::get('/production/{id}/transaction/det', ['middleware' => ['role:production_result|update'], 'as' => 'production.transaction_det', 'uses' => 'ProductionController@transaction_det']);
	Route::post('/production/{id}/transaction/det', ['middleware' => ['role:production_result|update'], 'as' => 'production.transaction_det_store', 'uses' => 'ProductionController@transaction_det_store']);

	Route::get('/production/{id}/transaction/det/report', ['middleware' => ['role:production_result|read'], 'as' => 'production.transaction_det_report', 'uses' => 'ProductionController@transaction_det_report']);

	Route::get('/production/{id}/transaction/add', ['middleware' => ['role:production_result|update'], 'as' => 'production.transaction_add', 'uses' => 'ProductionController@transaction_add']);
	Route::post('production/{id}/transaction/add', ['middleware' => ['role:production_result|update'], 'as' => 'production.transaction_add_store', 'uses' => 'ProductionController@transaction_add_store']);

	Route::get('/production/{id}/transaction/add/report', ['middleware' => ['role:production_result|read'], 'as' => 'production.transaction_add_report', 'uses' => 'ProductionController@transaction_add_report']);

		// - summary
	Route::get('/production/{id}/summary', ['middleware' => ['role:production_result|read'], 'as' => 'production.summary', 'uses' => 'ProductionController@summary']);
		// - close project
	Route::get('/production/{id}/close', ['middleware' => ['role:production_result|update'], 'as' => 'production.close', 'uses' => 'ProductionController@close_production_result']);
		// - download PDF
	Route::get('/production/{id}/pdf', ['middleware' => ['role:production_result|read'], 'as' => 'production.pdf', 'uses' => 'ProductionController@pdf']);

	Route::get('/productions', ['middleware' => ['role:production_result|read'], 'as' => 'productions', 'uses' => 'ProductionController@all_productions']);
});

//Production Touch
Route::group(['prefix' => 'touch', 'namespace' => 'Home'], function()
{
	// - index
	Route::get('/production_result', ['middleware' => ['role:production_result|read'], 'as' => 'production_result', 'uses' => 'ProductionTouchController@index']);
	// - create
	Route::get('/production_result/create', ['middleware' => ['role:production_result|create'], 'as' => 'production_result.create', 'uses' => 'ProductionTouchController@create_production']);
	Route::post('/production_result/create', ['middleware' => ['role:production_result|create'], 'as' => 'production_result.store', 'uses' => 'ProductionTouchController@store_production']);
	// - edit
	Route::get('/production_result/{id}/edit', ['middleware' => ['role:production_result|update'], 'as' => 'production_result.edit', 'uses' => 'ProductionTouchController@edit_production']);
	Route::put('/production_result/{id}/edit', ['middleware' => ['role:production_result|update'], 'as' => 'production_result.update', 'uses' => 'ProductionTouchController@update_production']);
	// - create waste
	Route::get('/production_result/{id}/waste', ['middleware' => ['role:production_result|create'], 'as' => 'production_result.create_waste', 'uses' => 'ProductionTouchController@create_waste']);
	Route::post('/production_result/{id}/waste', ['middleware' => ['role:production_result|create'], 'as' => 'production_result.store_waste', 'uses' => 'ProductionTouchController@store_waste']);
	// - edit waste
	Route::get('/production_result/{id}/waste/edit', ['middleware' => ['role:production_result|update'], 'as' => 'production_result.edit_waste', 'uses' => 'ProductionTouchController@edit_waste']);
	// - create reject
	Route::get('/production_result/{id}/reject', ['middleware' => ['role:production_result|create'], 'as' => 'production_result.create_reject', 'uses' => 'ProductionTouchController@create_reject']);
	Route::post('/production_result/{id}/reject', ['middleware' => ['role:production_result|create'], 'as' => 'production_result.store_reject', 'uses' => 'ProductionTouchController@store_reject']);
	// - edit reject
	Route::get('/production_result/{id}/reject/edit', ['middleware' => ['role:production_result|update'], 'as' => 'production_result.edit_reject', 'uses' => 'ProductionTouchController@edit_reject']);
	// - create result
	Route::get('/production_result/{id}/result', ['middleware' => ['role:production_result|create'], 'as' => 'production_result.create_result', 'uses' => 'ProductionTouchController@create_result']);
	Route::post('/production_result/{id}/result', ['middleware' => ['role:production_result|create'], 'as' => 'production_result.store_result', 'uses' => 'ProductionTouchController@store_result']);
	// - edit result
	Route::get('/production_result/{id}/result/edit', ['middleware' => ['role:production_result|update'], 'as' => 'production_result.edit_result', 'uses' => 'ProductionTouchController@edit_result']);
	// // - size (?)
	// Route::get('/production_result/{id}/size', ['as' => 'production_result.create_size', 'uses' => 'ProductionTouchController@create_size']);
	// Route::post('/production_result/{id}/size', ['as' => 'production_result.store_size', 'uses' => 'ProductionTouchController@store_size']);
	// - summary
	Route::get('/production_result/{id}/summary', ['middleware' => ['role:production_result|read'], 'as' => 'production_result.summary', 'uses' => 'ProductionTouchController@summary']);
	// - close project
	Route::get('/production_result/{id}/close', ['middleware' => ['role:production_result|update'], 'as' => 'production_result.close', 'uses' => 'ProductionTouchController@close_production_result']);
	// - download PDF
	Route::get('/production_result/{id}/pdf', ['middleware' => ['role:production_result|read'], 'as' => 'production_result.pdf', 'uses' => 'ProductionTouchController@pdf']);
	// - all projects
	Route::get('/production_results', ['middleware' => ['role:production_result|read'], 'as' => 'production_results', 'uses' => 'ProductionTouchController@all_productions']);
});

//Storage
// Route::group(['prefix' => 'storage', 'namespace' => 'Home'], function()
// {
// 		//index
// 	Route::get('/', ['middleware' => ['role:storage|read'], 'as' => 'storage.index', 'uses' => 'StorageController@index']);
// 		//create
// 	Route::get('/create', ['middleware' => ['role:storage|create'], 'as' => 'storage.create', 'uses' => 'StorageController@create']);
// 	Route::get('/data', ['middleware' => ['role:storage|create'], 'as' => 'storage.data', 'uses' => 'StorageController@data']);
// 	Route::get('/dataindex/{id}', ['middleware' => ['role:storage|create'], 'as' => 'storage.dataindex', 'uses' => 'StorageController@dataindex']);
// 	Route::post('/create', ['middleware' => ['role:storage|create'], 'as' => 'storage.store', 'uses' => 'StorageController@store']);
// });

//Export
Route::group(['prefix' => 'cart', 'namespace' => 'Home'], function()
{
		//index
	Route::get('/cart',  ['middleware' => ['role:cart|approve'], 'as' => 'cart.index', 'uses' => 'CartController@index']);
	Route::get('/data', ['middleware' => ['role:cart|read'], 'as' => 'cart.data', 'uses' => 'CartController@data']);


		//create
	Route::get('/cart/create', ['middleware' => ['role:cart|create'], 'as' => 'cart.create', 'uses' => 'CartController@create']);
    // Route::get('/lookupcart', ['as' => 'lookupcart.index', 'uses' => 'LookupCartController@data']);
	Route::post('/cart/create', ['as' => 'cart.store', 'uses' => 'CartController@store']);
		//edit
	Route::get('/cart/{id}/edit', ['middleware' => ['role:cart|edit'], 'as' => 'cart.edit', 'uses' => 'CartController@edit']);
	Route::put('/cart/{id}/edit', ['as' => 'cart.update', 'uses' => 'CartController@update']);
	Route::get('/cart/{id}/editdet', ['as' => 'cart.edit', 'uses' => 'CartController@edit']);
	Route::put('/cart/{id}/editdet', ['as' => 'cart.updatedetail', 'uses' => 'CartController@updatedetail']);

		//confirm
	Route::put('/cart/{id}/storeconfirm', ['as' => 'cart.storeconfirm', 'uses' => 'CartController@storeconfirm']);

	Route::delete('/cart/{id}/delete', ['middleware' => ['role:cart|delete'], 'as' => 'cart.delete', 'uses' => 'CartController@delete']);
		//delete detail
	Route::delete('/cart/{id}/deletedet', ['middleware' => ['role:cart|delete'], 'as' => 'cart.deletedet', 'uses' => 'CartController@deletedet']);

		//export
	Route::get('/cart/{id}/export', ['as' => 'cart.export', 'uses' => 'CartController@export']);
	Route::put('/cart/{id}/export', ['as' => 'cart.storeexport', 'uses' => 'CartController@storeexport']);

		//R n D
	Route::get('/cart/{id}/rnd', ['as' => 'cart.rnd', 'uses' => 'CartController@rnd']);
	Route::put('/cart/{id}/rnd', ['as' => 'cart.storernd', 'uses' => 'CartController@storernd']);

	Route::get('/cart/{id}/search', ['as' => 'cart.search', 'uses' => 'CartController@search']);
});

//End Product
Route::group(['prefix' => 'end_product', 'namespace' => 'Home'], function()
{
	// - index
	Route::get('/', ['middleware' => ['role:end_product|read'], 'as' => 'end_product', 'uses' => 'EndProductController@index']);
	// - create
	Route::get('/{id}/create', ['middleware' => ['role:end_product|create'], 'as' => 'end_product.create', 'uses' => 'EndProductController@create']);
	Route::post('/{id}/create', ['middleware' => ['role:end_product|create'], 'as' => 'end_product.store', 'uses' => 'EndProductController@store']);
	// - edit
	Route::get('/{id}/edit', ['middleware' => ['role:end_product|update'], 'as' => 'end_product.edit', 'uses' => 'EndProductController@edit']);
	Route::put('/{id}/edit', ['middleware' => ['role:end_product|update'], 'as' => 'end_product.update', 'uses' => 'EndProductController@update']);
	// - create waste
	Route::get('/{id}/waste', ['middleware' => ['role:end_product|create'], 'as' => 'end_product.create_waste', 'uses' => 'EndProductController@create_waste']);
	Route::post('/{id}/waste', ['middleware' => ['role:end_product|create'], 'as' => 'end_product.store_waste', 'uses' => 'EndProductController@store_waste']);
	// - edit waste
	Route::get('/{id}/waste/edit', ['middleware' => ['role:end_product|update'], 'as' => 'end_product.edit_waste', 'uses' => 'EndProductController@edit_waste']);
	Route::put('/{id}/waste/edit', ['middleware' => ['role:end_product|update'], 'as' => 'end_product.update_waste', 'uses' => 'EndProductController@update_waste']);
	// - create result
	Route::get('/{id}/result', ['middleware' => ['role:end_product|create'], 'as' => 'end_product.create_result', 'uses' => 'EndProductController@create_result']);
	Route::post('/{id}/result', ['middleware' => ['role:end_product|create'], 'as' => 'end_product.store_result', 'uses' => 'EndProductController@store_result']);
	// - edit result
	Route::get('/{id}/result/edit', ['middleware' => ['role:end_product|update'], 'as' => 'end_product.edit_result', 'uses' => 'EndProductController@edit_result']);
	Route::put('/{id}/result/edit', ['middleware' => ['role:end_product|update'], 'as' => 'end_product.update_result', 'uses' => 'EndProductController@update_result']);
	// - create addition
	Route::get('/{id}/addition', ['middleware' => ['role:end_product|create'], 'as' => 'end_product.create_addition', 'uses' => 'EndProductController@create_addition']);
	Route::post('/{id}/addition', ['middleware' => ['role:end_product|create'], 'as' => 'end_product.store_addition', 'uses' => 'EndProductController@store_addition']);
	// - edit addition
	Route::get('/{id}/addition/edit', ['middleware' => ['role:end_product|update'], 'as' => 'end_product.edit_addition', 'uses' => 'EndProductController@edit_addition']);
	Route::put('/{id}/addition/edit', ['middleware' => ['role:end_product|update'], 'as' => 'end_product.update_addition', 'uses' => 'EndProductController@update_addition']);
	// - summary
	Route::get('/{id}/summary', ['middleware' => ['role:end_product|read'], 'as' => 'end_product.summary', 'uses' => 'EndProductController@summary']);
	// - close
	Route::put('/{id}/close', ['middleware' => ['role:end_product|update'], 'as' => 'end_product.close', 'uses' => 'EndProductController@close']);
});

// Route::get('/test', ['as' => 'test', 'uses' => 'HomeController@test']);
// Route::post('/test', ['as' => 'test.store', 'uses' => 'HomeController@storeTest']);

//XHR
Route::get('/getprice', ['as' => 'get.price', 'uses' => 'XHRController@get_price']);
Route::get('/getproduct', ['as' => 'get.product', 'uses' => 'XHRController@get_product']);
Route::get('/product_by_purchase', ['as' => 'get.product_by_purchase', 'uses' => 'XHRController@get_product_by_purchase']);
Route::get('/getproduction', ['as' => 'get.production', 'uses' => 'XHRController@get_production']);
Route::get('/getdefaultroles', ['as' => 'get.defaultroles', 'uses' => 'XHRController@get_default_roles']);
Route::get('/getmprepproductuom', ['as' => 'get.m_prep_product_uom', 'uses' => 'XHRController@get_m_prep_product_uom']);
Route::get('/getproductionsize', ['as' => 'get.production_size', 'uses' => 'XHRController@get_production_size']);
Route::get('/getproductionresultsize', ['as' => 'get.production_result_size', 'uses' => 'XHRController@get_production_result_size']);
Route::get('/getsizeblock', ['as' => 'get.sizeblock', 'uses' => 'XHRController@getSizeBlock']);
Route::get('/getcartdetail', ['as' => 'get.cart_detail', 'uses' => 'XHRController@get_cart_detail']);
Route::get('/getcarton', ['as' => 'get.carton', 'uses' => 'XHRController@get_carton']);
Route::get('/getcartondetail', ['as' => 'get.cartondetail', 'uses' => 'XHRController@get_carton_detail']);
Route::get('/getstockitem', ['as' => 'get.stockitem', 'uses' => 'XHRController@get_stock_item']);
Route::post('/apistorage', ['uses' => 'XHRController@apistorage']);
Route::post('/movestorage', ['uses' => 'XHRController@movestorage']);
Route::post('/movestoragerev', ['uses' => 'Editor\StorageController@movestoragerev']);
Route::post('/updatestorage', ['uses' => 'Editor\StorageController@updatestorage']);
Route::post('/poststorage', ['uses' => 'Editor\StorageController@postStorage']);
Route::get('/getingredientcost', ['as' => 'get.ingredient_cost', 'uses' => 'XHRController@get_ingredient_cost']);
Route::get('/getitemuom', ['as' => 'get.item_uom', 'uses' => 'XHRController@get_item_uom']);
Route::get('/getrecipeitems', ['as' => 'get.recipe_items', 'uses' => 'XHRController@get_recipe_items']);
Route::get('/purchase_by_project', ['as' => 'get.purchase_by_project', 'uses' => 'XHRController@purchase_by_project']);

Auth::routes();

Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
	// Route::post('/apistorage', ['uses' => 'StorageController@apistorage']);

	// tetsing
	Route::get('/cart/details-data/{id}', ['as' => 'editor.cart.details-data', 'uses' => 'CartController@getDetailsData']);

	//Home
	Route::get('/', ['as' => 'editor.index', 'uses' => 'EditorController@index']);

	//Profile
		//detail
	Route::get('/profile', ['as' => 'editor.profile.show', 'uses' => 'ProfileController@show']);
		//delete profile picture
	Route::put('/profile', ['as' => 'editor.profile.delete_image', 'uses' => 'ProfileController@delete_image']);
		//edit
	Route::get('/profile/edit', ['as' => 'editor.profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('/profile/edit', ['as' => 'editor.profile.update', 'uses' => 'ProfileController@update']);
		//edit password
	Route::get('/profile/password', ['as' => 'editor.profile.edit_password', 'uses' => 'ProfileController@edit_password']);
	Route::put('/profile/password', ['as' => 'editor.profile.update_password', 'uses' => 'ProfileController@update_password']);

	//User
		//index
	Route::get('/user', ['middleware' => ['role:user|read'], 'as' => 'editor.user.index', 'uses' => 'UserController@index']);
		//create
	Route::get('/user/create', ['middleware' => ['role:user|create'], 'as' => 'editor.user.create', 'uses' => 'UserController@create']);
	Route::post('/user/create', ['middleware' => ['role:user|create'], 'as' => 'editor.user.store', 'uses' => 'UserController@store']);
		//detail
	Route::get('/user/{id}', ['middleware' => ['role:user|read'], 'as' => 'editor.user.detail', 'uses' => 'UserController@show']);
		//delete profile picture
	Route::put('/user/{id}', ['middleware' => ['role:user|update'], 'as' => 'editor.user.delete_image', 'uses' => 'UserController@delete_image']);
		//edit
	Route::get('/user/{id}/edit', ['middleware' => ['role:user|update'], 'as' => 'editor.user.edit', 'uses' => 'UserController@edit']);
	Route::put('/user/{id}/edit', ['middleware' => ['role:user|update'], 'as' => 'editor.user.update', 'uses' => 'UserController@update']);
		//edit password
	Route::get('/user/{id}/password', ['middleware' => ['role:user|update'], 'as' => 'editor.user.edit_password', 'uses' => 'UserController@edit_password']);
	Route::put('/user/{id}/password', ['middleware' => ['role:user|update'], 'as' => 'editor.user.update_password', 'uses' => 'UserController@update_password']);
		//delete
	Route::delete('/user/{id}/delete', ['middleware' => ['role:user|delete'], 'as' => 'editor.user.delete', 'uses' => 'UserController@delete']);

	// //Role
	// 	//index
	// Route::get('/role', ['as' => 'editor.role.index', 'uses' => 'RoleController@index']);
	// 	//create
	// Route::get('/role/create', ['as' => 'editor.role.create', 'uses' => 'RoleController@create']);
	// Route::post('/role/create', ['as' => 'editor.role.store', 'uses' => 'RoleController@store']);
	// 	//detail
	// Route::get('/role/{id}', ['as' => 'editor.role.detail', 'uses' => 'RoleController@show']);
	// 	//delete
	// Route::delete('/role/{id}/delete', ['as' => 'editor.role.delete', 'uses' => 'RoleController@delete'])




	//Storage
		//index
	Route::get('/storage', ['middleware' => ['role:storage|read'], 'as' => 'editor.storage.index', 'uses' => 'StorageController@index']);


	Route::get('/storage/data', ['as' => 'editor.storage.data', 'uses' => 'StorageController@data']);
	Route::get('/storage/dataindex/{id}', ['as' => 'editor.storage.dataindex', 'uses' => 'StorageController@dataindex']);
	Route::get('/storage/datasearch', ['as' => 'editor.storage.datasearch', 'uses' => 'StorageController@datasearch']);
	Route::get('/storage/dataautostore', ['uses' => 'StorageController@dataAutoStore']);
	Route::post('/storage/approve', ['uses' => 'StorageController@approve']);
	Route::get('/storage/datasearch_history', ['as' => 'editor.storage.datasearch_history', 'uses' => 'StorageController@datasearch_history']);
	//Route::get('/storage/data_trans_index', ['as' => 'editor.storage.data_trans_index', 'uses' => 'StorageController@data_trans_index']);

	Route::get('/storage/create', ['middleware' => ['role:storage|create'], 'as' => 'editor.storage.create', 'uses' => 'StorageController@create']);
	Route::get('/storage/data', ['middleware' => ['role:storage|create'], 'as' => 'editor.storage.data', 'uses' => 'StorageController@data']);
	Route::post('/storage/create', ['as' => 'editor.storage.store', 'uses' => 'StorageController@store']);
	Route::get('/storage/show/{params}', ['uses' => 'StorageController@show']);

	 //Production Export
		//index
	Route::get('/production_export', ['middleware' => ['role:export|read'], 'as' => 'editor.production_export.index', 'uses' => 'ProductionExportController@index']);
		//create
	Route::get('/production_export/create', ['middleware' => ['role:export|create'], 'as' => 'editor.production_export.create', 'uses' => 'ProductionExportController@create']);
	Route::post('/production_export/create', ['middleware' => ['role:export|create'], 'as' => 'editor.production_export.store', 'uses' => 'ProductionExportController@store']);
		//edit
	Route::get('/production_export/{id}/edit', ['middleware' => ['role:export|update'], 'as' => 'editor.production_export.edit', 'uses' => 'ProductionExportController@edit']);
	Route::put('/production_export/{id}/edit', ['middleware' => ['role:export|update'], 'as' => 'editor.production_export.update', 'uses' => 'ProductionExportController@update']);
		//delete
	Route::delete('/production_export/{id}/delete', ['middleware' => ['role:export|delete'], 'as' => 'editor.production_export.delete', 'uses' => 'ProductionExportController@delete']);

	//End Product
		//index
	Route::get('/end_product', ['middleware' => ['role:end_product|read'], 'as' => 'editor.end_product.index', 'uses' => 'EndProductController@index']);
		//create
	Route::get('/end_product/create', ['middleware' => ['role:end_product|create'], 'as' => 'editor.end_product.create', 'uses' => 'EndProductController@create']);
	Route::post('/end_product/create', ['middleware' => ['role:end_product|create'], 'as' => 'editor.end_product.store', 'uses' => 'EndProductController@store']);
		//edit
	Route::get('/end_product/{id}/edit', ['middleware' => ['role:end_product|update'], 'as' => 'editor.end_product.edit', 'uses' => 'EndProductController@edit']);
	Route::put('/end_product/{id}/edit', ['middleware' => ['role:end_product|update'], 'as' => 'editor.end_product.update', 'uses' => 'EndProductController@update']);
		//create waste
	Route::get('/end_product/{id}/waste', ['middleware' => ['role:end_product|create'], 'as' => 'editor.end_product.create_waste', 'uses' => 'EndProductController@create_waste']);
	Route::post('/end_product/{id}/waste', ['middleware' => ['role:end_product|create'], 'as' => 'editor.end_product.store_waste', 'uses' => 'EndProductController@store_waste']);
		//edit waste
	Route::get('/end_product/{id}/waste/edit', ['middleware' => ['role:end_product|update'], 'as' => 'editor.end_product.edit_waste', 'uses' => 'EndProductController@edit_waste']);
	Route::put('/end_product/{id}/waste/edit', ['middleware' => ['role:end_product|update'], 'as' => 'editor.end_product.update_waste', 'uses' => 'EndProductController@update_waste']);
		//create result
	Route::get('/end_product/{id}/result', ['middleware' => ['role:end_product|create'], 'as' => 'editor.end_product.create_result', 'uses' => 'EndProductController@create_result']);
	Route::post('/end_product/{id}/result', ['middleware' => ['role:end_product|create'], 'as' => 'editor.end_product.store_result', 'uses' => 'EndProductController@store_result']);
		//edit result
	Route::get('/end_product/{id}/result/edit', ['middleware' => ['role:end_product|update'], 'as' => 'editor.end_product.edit_result', 'uses' => 'EndProductController@edit_result']);
	Route::put('/end_product/{id}/result/edit', ['middleware' => ['role:end_product|update'], 'as' => 'editor.end_product.update_result', 'uses' => 'EndProductController@update_result']);
		//create addition
	Route::get('/end_product/{id}/addition', ['middleware' => ['role:end_product|create'], 'as' => 'editor.end_product.create_addition', 'uses' => 'EndProductController@create_addition']);
	Route::post('/end_product/{id}/addition', ['middleware' => ['role:end_product|create'], 'as' => 'editor.end_product.store_addition', 'uses' => 'EndProductController@store_addition']);
		//edit addition
	Route::get('/end_product/{id}/addition/edit', ['middleware' => ['role:end_product|update'], 'as' => 'editor.end_product.edit_addition', 'uses' => 'EndProductController@edit_addition']);
	Route::put('/end_product/{id}/addition/edit', ['middleware' => ['role:end_product|update'], 'as' => 'editor.end_product.update_addition', 'uses' => 'EndProductController@update_addition']);
		//summary
	Route::get('/end_product/{id}/summary', ['middleware' => ['role:end_product|read'], 'as' => 'editor.end_product.summary', 'uses' => 'EndProductController@summary']);
		//close
	Route::put('/end_product/{id}/close', ['middleware' => ['role:end_product|update'], 'as' => 'editor.end_product.close', 'uses' => 'EndProductController@close']);
		//pdf
	Route::get('/end_product/{id}/pdf', ['middleware' => ['role:end_product|read'], 'as' => 'editor.end_product.pdf', 'uses' => 'EndProductController@pdf']);
		//delete
	Route::delete('/end_product/{id}/delete', ['middleware' => ['role:end_product|delete'], 'as' => 'editor.end_product.delete', 'uses' => 'EndProductController@delete']);

	//Placement Storage
		//index
	Route::get('/placement_storage', ['middleware' => ['role:placement_storage|read'], 'as' => 'editor.placement_storage.index', 'uses' => 'PlacementStorageController@index']);
		//create
	Route::get('/placement_storage/create', ['middleware' => ['role:placement_storage|create'], 'as' => 'editor.placement_storage.create', 'uses' => 'PlacementStorageController@create']);
	Route::post('/placement_storage/create', ['middleware' => ['role:placement_storage|create'], 'as' => 'editor.placement_storage.store', 'uses' => 'PlacementStorageController@store']);
		//edit
	Route::get('/placement_storage/{id}/edit', ['middleware' => ['role:placement_storage|update'], 'as' => 'editor.placement_storage.edit', 'uses' => 'PlacementStorageController@edit']);
	Route::put('/placement_storage/{id}/update', ['middleware' => ['role:placement_storage|update'], 'as' => 'editor.placement_storage.update', 'uses' => 'PlacementStorageController@update']);
		//delete
	Route::delete('/placement_storage/{id}/delete', ['middleware' => ['role:placement_storage|delete'], 'as' => 'editor.placement_storage.delete', 'uses' => 'PlacementStorageController@delete']);

	// //Storage
	// 	//index
	// Route::get('/storage', ['middleware' => ['role:storage|read'], 'as' => 'editor.storage.index', 'uses' => 'StorageController@index']);
	// Route::get('/storage/datasearch', ['middleware' => ['role:storage|read'], 'as' => 'editor.storage.datasearch', 'uses' => 'StorageController@datasearch']);
	// Route::get('/storage/datasearch_history', ['middleware' => ['role:storage|read'], 'as' => 'editor.storage.datasearch_history', 'uses' => 'StorageController@datasearch_history']);
	// 	//create
	// Route::get('/storage/create', ['middleware' => ['role:storage|create'], 'as' => 'editor.storage.create', 'uses' => 'StorageController@create']);
	// Route::get('/storage/data', ['middleware' => ['role:storage|create'], 'as' => 'editor.storage.data', 'uses' => 'StorageController@data']);
	// Route::get('/storage/dataindex/{id}', ['middleware' => ['role:storage|create'], 'as' => 'editor.storage.dataindex', 'uses' => 'StorageController@dataindex']);
	// Route::post('/storage/create', ['middleware' => ['role:storage|create'], 'as' => 'editor.storage.store', 'uses' => 'StorageController@store']);

	//Doc Code
		//index
	Route::get('/doc_code', ['middleware' => ['role:doc_code|read'], 'as' => 'editor.doc_code.index', 'uses' => 'DocCodeController@index']);
		//create
	Route::get('/doc_code/create', ['middleware' => ['role:doc_code|create'], 'as' => 'editor.doc_code.create', 'uses' => 'DocCodeController@create']);
	Route::post('/doc_code/create', ['middleware' => ['role:doc_code|create'], 'as' => 'editor.doc_code.store', 'uses' => 'DocCodeController@store']);
		//edit
	Route::get('/doc_code/{id}/edit', ['middleware' => ['role:doc_code|update'], 'as' => 'editor.doc_code.edit', 'uses' => 'DocCodeController@edit']);
	Route::put('/doc_code/{id}/update', ['middleware' => ['role:doc_code|update'], 'as' => 'editor.doc_code.update', 'uses' => 'DocCodeController@update']);
		// reset
	Route::put('/doc_code/{id}/reset', ['middleware' => ['role:doc_code|delete'], 'as' => 'editor.doc_code.reset', 'uses' => 'DocCodeController@reset']);
		//delete
	Route::delete('/doc_code/{id}/delete', ['middleware' => ['role:doc_code|delete'], 'as' => 'editor.doc_code.delete', 'uses' => 'DocCodeController@delete']);

	//Product Type
		//index
	Route::get('/product_type', ['middleware' => ['role:product_type|read'], 'as' => 'editor.product_type.index', 'uses' => 'ProductTypeController@index']);
		//create
	Route::get('/product_type/create', ['middleware' => ['role:product_type|create'], 'as' => 'editor.product_type.create', 'uses' => 'ProductTypeController@create']);
	Route::post('/product_type/create', ['middleware' => ['role:product_type|create'], 'as' => 'editor.product_type.store', 'uses' => 'ProductTypeController@store']);
		//detail
	Route::get('/product_type/{id}/detail', ['middleware' => ['role:product_type|read'], 'as' => 'editor.product_type.detail', 'uses' => 'ProductTypeController@detail']);
		//edit
	Route::get('/product_type/{id}/edit', ['middleware' => ['role:product_type|update'], 'as' => 'editor.product_type.edit', 'uses' => 'ProductTypeController@edit']);
	Route::put('/product_type/{id}/edit', ['middleware' => ['role:product_type|update'], 'as' => 'editor.product_type.update', 'uses' => 'ProductTypeController@update']);
		//delete
	Route::delete('/product_type/{id}/delete', ['middleware' => ['role:product_type|update'], 'as' => 'editor.product_type.delete', 'uses' => 'ProductTypeController@delete']);
		//size
	Route::get('/product_type/{id}/size', ['middleware' => ['role:product_type|update'], 'as' => 'editor.product_type.edit_size', 'uses' => 'ProductTypeController@edit_size']);
	Route::post('/product_type/{id}/size', ['middleware' => ['role:product_type|update'], 'as' => 'editor.product_type.update_size', 'uses' => 'ProductTypeController@update_size']);

	//Product
		//index
	Route::get('/product', ['middleware' => ['role:product|read'], 'as' => 'editor.product.index', 'uses' => 'ProductController@index']);
		//create
	Route::get('/product/create', ['middleware' => ['role:product|create'], 'as' => 'editor.product.create', 'uses' => 'ProductController@create']);
	Route::post('/product/create', ['middleware' => ['role:product|create'], 'as' => 'editor.product.store', 'uses' => 'ProductController@store']);
		//edit
	Route::get('/product/{id}/edit', ['middleware' => ['role:product|update'], 'as' => 'editor.product.edit', 'uses' => 'ProductController@edit']);
	Route::put('/product/{id}/edit', ['middleware' => ['role:product|update'], 'as' => 'editor.product.update', 'uses' => 'ProductController@update']);
		//delete
	Route::delete('/product/{id}/delete', ['middleware' => ['role:product|delete'], 'as' => 'editor.product.delete', 'uses' => 'ProductController@delete']);

	//Size
		//index
	Route::get('/size', ['middleware' => ['role:size|read'], 'as' => 'editor.size.index', 'uses' => 'SizeController@index']);
		//create
	Route::get('/size/create', ['middleware' => ['role:size|create'], 'as' => 'editor.size.create', 'uses' => 'SizeController@create']);
	Route::post('/size/create', ['middleware' => ['role:size|create'], 'as' => 'editor.size.store', 'uses' => 'SizeController@store']);
		//edit
	Route::get('/size/{id}/edit', ['middleware' => ['role:size|update'], 'as' => 'editor.size.edit', 'uses' => 'SizeController@edit']);
	Route::put('/size/{id}/edit', ['middleware' => ['role:size|update'], 'as' => 'editor.size.update', 'uses' => 'SizeController@update']);
		//delete
	Route::delete('/size/{id}/delete', ['middleware' => ['role:size|delete'], 'as' => 'editor.size.delete', 'uses' => 'SizeController@delete']);

	//Supplier
		//index
	Route::get('/supplier', ['middleware' => ['role:supplier|read'], 'as' => 'editor.supplier.index', 'uses' => 'SupplierController@index']);
		//create
	Route::get('/supplier/create', ['middleware' => ['role:supplier|create'], 'as' => 'editor.supplier.create', 'uses' => 'SupplierController@create']);
	Route::post('/supplier/create', ['middleware' => ['role:supplier|create'], 'as' => 'editor.supplier.store', 'uses' => 'SupplierController@store']);
		//edit
	Route::get('/supplier/{id}/edit', ['middleware' => ['role:supplier|update'], 'as' => 'editor.supplier.edit', 'uses' => 'SupplierController@edit']);
	Route::put('/supplier/{id}/edit', ['middleware' => ['role:supplier|update'], 'as' => 'editor.supplier.update', 'uses' => 'SupplierController@update']);
		//delete
	Route::delete('/supplier/{id}/delete', ['middleware' => ['role:supplier|delete'], 'as' => 'editor.supplier.delete', 'uses' => 'SupplierController@delete']);
		//product
	Route::get('/supplier/{id}/product', ['middleware' => ['role:supplier|update'], 'as' => 'editor.supplier.edit_product', 'uses' => 'SupplierController@edit_product']);
	Route::post('/supplier/{id}/product', ['middleware' => ['role:supplier|update'], 'as' => 'editor.supplier.update_product', 'uses' => 'SupplierController@update_product']);

	//Customer
		//index
	Route::get('/customer', ['middleware' => ['role:customer|read'], 'as' => 'editor.customer.index', 'uses' => 'CustomerController@index']);
		//data
	Route::get('/customer/data', ['as' => 'editor.customer.data', 'uses' => 'CustomerController@data']);
		//create
	Route::get('/customer/create', ['middleware' => ['role:customer|create'], 'as' => 'editor.customer.create', 'uses' => 'CustomerController@create']);
	Route::post('/customer/create', ['middleware' => ['role:customer|create'], 'as' => 'editor.customer.store', 'uses' => 'CustomerController@store']);
		//edit
	Route::get('/customer/{id}/edit', ['middleware' => ['role:customer|update'], 'as' => 'editor.customer.edit', 'uses' => 'CustomerController@edit']);
	Route::put('/customer/{id}/edit', ['middleware' => ['role:customer|update'], 'as' => 'editor.customer.update', 'uses' => 'CustomerController@update']);
		//delete
	Route::delete('/customer/{id}/delete', ['middleware' => ['role:customer|delete'], 'as' => 'editor.customer.delete', 'uses' => 'CustomerController@delete']);
		//product
	Route::get('/customer/{id}/product', ['middleware' => ['role:customer|update'], 'as' => 'editor.customer.edit_product', 'uses' => 'CustomerController@edit_product']);
	Route::post('/customer/{id}/product', ['middleware' => ['role:customer|update'], 'as' => 'editor.customer.update_product', 'uses' => 'CustomerController@update_product']);

	//Purchase
		//index
	Route::get('/purchase', ['middleware' => ['role:purchase|read'], 'as' => 'editor.purchase.index', 'uses' => 'PurchaseController@index']);
		//create
	Route::get('/purchase/create', ['middleware' => ['role:purchase|create'], 'as' => 'editor.purchase.create', 'uses' => 'PurchaseController@create']);
	Route::post('/purchase/create', ['middleware' => ['role:purchase|create'], 'as' => 'editor.purchase.store', 'uses' => 'PurchaseController@store']);
		//edit
	Route::get('/purchase/{id}/edit', ['middleware' => ['role:purchase|update'], 'as' => 'editor.purchase.edit', 'uses' => 'PurchaseController@edit']);
	Route::put('/purchase/{id}/edit', ['middleware' => ['role:purchase|update'], 'as' => 'editor.purchase.update', 'uses' => 'PurchaseController@update']);
		//delete
	Route::delete('/purchase/{id}/delete', ['middleware' => ['role:purchase|delete'], 'as' => 'editor.purchase.delete', 'uses' => 'PurchaseController@delete']);
		//create price
	Route::get('/purchase/{id}/price', ['middleware' => ['role:price|create'], 'as' => 'editor.price.create', 'uses' => 'PriceController@create']);
	Route::post('/purchase/{id}/price', ['middleware' => ['role:price|create'], 'as' => 'editor.price.store', 'uses' => 'PriceController@store']);
		//edit price
	Route::get('/purchase/{id}/price/edit', ['middleware' => ['role:price|update'], 'as' => 'editor.price.edit', 'uses' => 'PriceController@edit']);
	Route::put('/purchase/{id}/price/edit', ['middleware' => ['role:price|update'], 'as' => 'editor.price.update', 'uses' => 'PriceController@update']);
		//create purchase detail
	Route::get('/purchase/{id}/detail', ['middleware' => ['role:purchase_detail|create'], 'as' => 'editor.purchase_detail.create', 'uses' => 'PurchaseDetailController@create']);
	Route::post('/purchase/{id}/detail', ['middleware' => ['role:purchase_detail|create'], 'as' => 'editor.purchase_detail.store', 'uses' => 'PurchaseDetailController@store']);
		//edit purchase detail
	Route::get('/purchase/{id}/detail/edit', ['middleware' => ['role:purchase_detail|update'], 'as' => 'editor.purchase_detail.edit', 'uses' => 'PurchaseDetailController@edit']);
	Route::put('/purchase/{id}/detail/edit', ['middleware' => ['role:purchase_detail|update'], 'as' => 'editor.purchase_detail.update', 'uses' => 'PurchaseDetailController@update']);
		//summary
	Route::get('/purchase/{id}/summary', ['middleware' => ['role:purchase_detail|read'], 'as' => 'editor.purchase.summary', 'uses' => 'PurchaseController@summary']);
		//reject
	Route::post('/purchase/{id}/reject', ['middleware' => ['role:purchase|reject'], 'as' => 'editor.purchase.reject', 'uses' => 'PurchaseController@reject']);
		//approve
	Route::get('/purchase/{id}/approve', ['middleware' => ['role:purchase|approve'], 'as' => 'editor.purchase.approve', 'uses' => 'PurchaseController@approve']);
		//confirm PO RM
	Route::get('/purchase/{id}/confirm_po_rm', ['middleware' => ['role:purchase|create'], 'as' => 'editor.purchase.confirm_po_rm', 'uses' => 'PurchaseController@confirm_po_rm']);
		//reject PO RM
	Route::post('/purchase/{id}/reject_po_rm', ['middleware' => ['role:price|reject'], 'as' => 'editor.purchase.reject_po_rm', 'uses' => 'PurchaseController@reject']);
		//approve PO RM
	Route::post('/purchase/{id}/approve_po_rm', ['middleware' => ['role:price|approve'], 'as' => 'editor.purchase.approve_po_rm', 'uses' => 'PurchaseController@approve']);
		//confirm Actual Delivery
	Route::get('/purchase/{id}/confirm_actual_delivery', ['middleware' => ['role:purchase_detail|create'], 'as' => 'editor.purchase.confirm_actual_delivery', 'uses' => 'PurchaseController@confirm_actual_delivery']);
		//reject Actual Delivery
	Route::post('/purchase/{id}/reject_actual_delivery', ['middleware' => ['role:purchase_detail|reject'], 'as' => 'editor.purchase.reject_actual_delivery', 'uses' => 'PurchaseController@reject']);
		//approve Actual Delivery
	Route::post('/purchase/{id}/approve_actual_delivery', ['middleware' => ['role:purchase_detail|approve'], 'as' => 'editor.purchase.approve_actual_delivery', 'uses' => 'PurchaseController@approve']);
		//download PDF
	Route::get('/purchase/{id}/history', ['middleware' => ['role:purchase|read'], 'as' => 'editor.purchase.history', 'uses' => 'PurchaseController@history']);
	Route::get('/purchase/{id}/pdf', ['middleware' => ['role:purchase|read'], 'as' => 'editor.purchase.pdf', 'uses' => 'PurchaseController@pdf']);

	//Role
		//index
	Route::get('/role', ['middleware' => ['role:role|read'], 'as' => 'editor.role.index', 'uses' => 'RolesController@index']);
		//create
	Route::get('/role/create', ['middleware' => ['role:role|create'], 'as' => 'editor.role.create', 'uses' => 'RolesController@create']);
	Route::post('/role/create', ['middleware' => ['role:role|create'], 'as' => 'editor.role.store', 'uses' => 'RolesController@store']);
		//edit
	Route::get('/role/{id}/edit', ['middleware' => ['role:role|update'], 'as' => 'editor.role.edit', 'uses' => 'RolesController@edit']);
	Route::put('/role/edit', ['middleware' => ['role:role|update'], 'as' => 'editor.role.update', 'uses' => 'RolesController@update']);
		//delete
	Route::delete('/role/{id}/delete', ['middleware' => ['role:role|delete'], 'as' => 'editor.role.delete', 'uses' => 'RolesController@delete']);

	//Module
		//index
	Route::get('/module', ['middleware' => ['role:module|read'], 'as' => 'editor.module.index', 'uses' => 'ModulesController@index']);
		//create
	Route::get('/module/create', ['middleware' => ['role:module|create'], 'as' => 'editor.module.create', 'uses' => 'ModulesController@create']);
	Route::post('/module/create', ['middleware' => ['role:module|create'], 'as' => 'editor.module.store', 'uses' => 'ModulesController@store']);
		//edit
	Route::get('/module/{id}/edit', ['middleware' => ['role:module|update'], 'as' => 'editor.module.edit', 'uses' => 'ModulesController@edit']);
	Route::put('/module/{id}/edit', ['middleware' => ['role:module|update'], 'as' => 'editor.module.update', 'uses' => 'ModulesController@update']);
		//delete
	Route::delete('/module/{id}/delete', ['middleware' => ['role:module|delete'], 'as' => 'editor.module.delete', 'uses' => 'ModulesController@delete']);

	//Action
		//index
	Route::get('/action', ['middleware' => ['role:action|read'], 'as' => 'editor.action.index', 'uses' => 'ActionsController@index']);
		//create
	Route::get('/action/create', ['middleware' => ['role:action|create'], 'as' => 'editor.action.create', 'uses' => 'ActionsController@create']);
	Route::post('/action/create', ['middleware' => ['role:action|create'], 'as' => 'editor.action.store', 'uses' => 'ActionsController@store']);
		//edit
	Route::get('/action/{id}/edit', ['middleware' => ['role:action|update'], 'as' => 'editor.action.edit', 'uses' => 'ActionsController@edit']);
	Route::put('/action/{id}/edit', ['middleware' => ['role:action|update'], 'as' => 'editor.action.update', 'uses' => 'ActionsController@update']);
		//delete
	Route::delete('/action/{id}/delete', ['middleware' => ['role:action|delete'], 'as' => 'editor.action.delete', 'uses' => 'ActionsController@delete']);

	//Privilege
		//index
	Route::get('/privilege', ['middleware' => ['role:privilege|read'], 'as' => 'editor.privilege.index', 'uses' => 'PrivilegeController@index']);
		//create
	Route::get('/privilege/create', ['middleware' => ['role:privilege|create'], 'as' => 'editor.privilege.create', 'uses' => 'PrivilegeController@create']);
	Route::post('/privilege/create', ['middleware' => ['role:privilege|create'], 'as' => 'editor.privilege.store', 'uses' => 'PrivilegeController@store']);
		//edit
	Route::get('/privilege/{id}/edit', ['middleware' => ['role:privilege|update'], 'as' => 'editor.privilege.edit', 'uses' => 'PrivilegeController@edit']);
	Route::put('/privilege/{id}/edit', ['middleware' => ['role:privilege|update'], 'as' => 'editor.privilege.update', 'uses' => 'PrivilegeController@update']);
		//delete
	Route::delete('/privilege/{id}/delete', ['middleware' => ['role:privilege|delete'], 'as' => 'editor.privilege.delete', 'uses' => 'PrivilegeController@delete']);
});


//Editor - Production
Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
	//Fish
		//index
	Route::get('/fish', ['middleware' => ['role:fish|read'], 'as' => 'editor.fish.index', 'uses' => 'FishController@index']);
		//create
	Route::get('/fish/create', ['middleware' => ['role:fish|create'], 'as' => 'editor.fish.create', 'uses' => 'FishController@create']);
	Route::post('/fish/create', ['middleware' => ['role:fish|create'], 'as' => 'editor.fish.store', 'uses' => 'FishController@store']);
		//edit
	Route::get('/fish/{id}/edit', ['middleware' => ['role:fish|update'], 'as' => 'editor.fish.edit', 'uses' => 'FishController@edit']);
	Route::put('/fish/{id}/edit', ['middleware' => ['role:fish|update'], 'as' => 'editor.fish.update', 'uses' => 'FishController@update']);
		//delete
	Route::delete('/fish/{id}/delete', ['middleware' => ['role:fish|delete'], 'as' => 'editor.fish.delete', 'uses' => 'FishController@delete']);
		//product
	Route::get('/fish/{id}/product', ['middleware' => ['role:fish|update'], 'as' => 'editor.fish.edit_product', 'uses' => 'FishController@edit_product']);
	Route::post('/fish/{id}/product', ['middleware' => ['role:fish|update'], 'as' => 'editor.fish.update_product', 'uses' => 'FishController@update_product']);

	//Variables
		//index
	Route::get('/variable', ['middleware' => ['role:variable|read'], 'as' => 'editor.variable.index', 'uses' => 'VariableController@index']);
		//create
	Route::get('/variable/create', ['middleware' => ['role:variable|create'], 'as' => 'editor.variable.create', 'uses' => 'VariableController@create']);
	Route::post('/variable/create', ['middleware' => ['role:variable|create'], 'as' => 'editor.variable.store', 'uses' => 'VariableController@store']);
		//edit
	Route::get('/variable/{id}/edit', ['middleware' => ['role:variable|update'], 'as' => 'editor.variable.edit', 'uses' => 'VariableController@edit']);
	Route::put('/variable/{id}/edit', ['middleware' => ['role:variable|update'], 'as' => 'editor.variable.update', 'uses' => 'VariableController@update']);
		//delete
	Route::delete('/variable/{id}/delete', ['middleware' => ['role:variable|delete'], 'as' => 'editor.variable.delete', 'uses' => 'VariableController@delete']);

	//Production
		//index
	Route::get('/production', ['middleware' => ['role:production|read'], 'as' => 'editor.production.index', 'uses' => 'ProductionController@index']);
		//create
	Route::get('/production/create', ['middleware' => ['role:production|create'], 'as' => 'editor.production.create', 'uses' => 'ProductionController@create']);
	Route::post('/production/create', ['middleware' => ['role:production|create'], 'as' => 'editor.production.store', 'uses' => 'ProductionController@store']);
		//detail
	Route::get('/production/{id}/detail', ['middleware' => ['role:production|read'], 'as' => 'editor.production.detail', 'uses' => 'ProductionController@detail']);
		//create recipe
	Route::get('/production/{id}/recipe', ['middleware' => ['role:production|create'], 'as' => 'editor.production.create_recipe', 'uses' => 'ProductionController@create_recipe']);
	Route::post('/production/{id}/recipe', ['middleware' => ['role:production|create'], 'as' => 'editor.production.store_recipe', 'uses' => 'ProductionController@store_recipe']);
		//edit recipe
	Route::get('/production/{id}/recipe/edit', ['middleware' => ['role:production|update'], 'as' => 'editor.production.edit_recipe', 'uses' => 'ProductionController@edit_recipe']);
	Route::put('/production/{id}/recipe/edit', ['middleware' => ['role:production|update'], 'as' => 'editor.production.update_recipe', 'uses' => 'ProductionController@update_recipe']);
		//edit
	Route::get('/production/{id}/edit', ['middleware' => ['role:production|update'], 'as' => 'editor.production.edit', 'uses' => 'ProductionController@edit']);
	Route::put('/production/{id}/edit', ['middleware' => ['role:production|update'], 'as' => 'editor.production.update', 'uses' => 'ProductionController@update']);
		//delete
	Route::delete('/production/{id}/delete', ['middleware' => ['role:production|delete'], 'as' => 'editor.production.delete', 'uses' => 'ProductionController@delete']);
		//variable
	Route::get('/production/{id}/variable', ['middleware' => ['role:production|update'], 'as' => 'editor.production.edit_variable', 'uses' => 'ProductionController@edit_variable']);
	Route::post('/production/{id}/variable', ['middleware' => ['role:production|update'], 'as' => 'editor.production.update_variable', 'uses' => 'ProductionController@update_variable']);
		//size
	Route::get('/production/{id}/size', ['middleware' => ['role:production|update'], 'as' => 'editor.production.edit_size', 'uses' => 'ProductionController@edit_size']);
	Route::post('/production/{id}/size', ['middleware' => ['role:production|update'], 'as' => 'editor.production.update_size', 'uses' => 'ProductionController@update_size']);

	//Reject Reason
		//index
	Route::get('/reject_reason', ['middleware' => ['role:reject_reason|read'], 'as' => 'editor.reject_reason.index', 'uses' => 'RejectReasonController@index']);
		//create
	Route::get('/reject_reason/create', ['middleware' => ['role:reject_reason|create'], 'as' => 'editor.reject_reason.create', 'uses' => 'RejectReasonController@create']);
	Route::post('/reject_reason/create', ['middleware' => ['role:reject_reason|create'], 'as' => 'editor.reject_reason.store', 'uses' => 'RejectReasonController@store']);
		//edit
	Route::get('/reject_reason/{id}/edit', ['middleware' => ['role:reject_reason|update'], 'as' => 'editor.reject_reason.edit', 'uses' => 'RejectReasonController@edit']);
	Route::put('/reject_reason/{id}/edit', ['middleware' => ['role:reject_reason|update'], 'as' => 'editor.reject_reason.update', 'uses' => 'RejectReasonController@update']);
		//delete
	Route::delete('/reject_reason/{id}/delete', ['middleware' => ['role:reject_reason|delete'], 'as' => 'editor.reject_reason.delete', 'uses' => 'RejectReasonController@delete']);

	//Reject Type
		//index
	Route::get('/reject_type', ['middleware' => ['role:reject_type|read'], 'as' => 'editor.reject_type.index', 'uses' => 'RejectTypeController@index']);
		//create
	Route::get('/reject_type/create', ['middleware' => ['role:reject_type|create'], 'as' => 'editor.reject_type.create', 'uses' => 'RejectTypeController@create']);
	Route::post('/reject_type/create', ['middleware' => ['role:reject_type|create'], 'as' => 'editor.reject_type.store', 'uses' => 'RejectTypeController@store']);
		//edit
	Route::get('/reject_type/{id}/edit', ['middleware' => ['role:reject_type|update'], 'as' => 'editor.reject_type.edit', 'uses' => 'RejectTypeController@edit']);
	Route::put('/reject_type/{id}/edit', ['middleware' => ['role:reject_type|update'], 'as' => 'editor.reject_type.update', 'uses' => 'RejectTypeController@update']);
		//delete
	Route::delete('/reject_type/{id}/delete', ['middleware' => ['role:reject_type|delete'], 'as' => 'editor.reject_type.delete', 'uses' => 'RejectTypeController@delete']);

	//System Option
		//index
	Route::get('/system_option', ['middleware' => ['role:system_option|read'], 'as' => 'editor.system_option.index', 'uses' => 'SystemOptionController@index']);
		//create
	Route::get('/system_option/create', ['middleware' => ['role:system_option|create'], 'as' => 'editor.system_option.create', 'uses' => 'SystemOptionController@create']);
	Route::post('/system_option/create', ['middleware' => ['role:system_option|create'], 'as' => 'editor.system_option.store', 'uses' => 'SystemOptionController@store']);
		//edit
	Route::get('/system_option/{id}/edit', ['middleware' => ['role:system_option|update'], 'as' => 'editor.system_option.edit', 'uses' => 'SystemOptionController@edit']);
	Route::put('/system_option/{id}/edit', ['middleware' => ['role:system_option|update'], 'as' => 'editor.system_option.update', 'uses' => 'SystemOptionController@update']);
		//delete
	Route::get('/system_option/{id}/delete', ['middleware' => ['role:system_option|delete'], 'as' => 'editor.system_option.delete', 'uses' => 'SystemOptionController@delete']);

	//Production Result
		//index
	Route::get('/production_result', ['middleware' => ['role:production_result|read'], 'as' => 'editor.production_result.index', 'uses' => 'ProductionResultController@index']);
		//create
	Route::get('/production_result/create', ['middleware' => ['role:production_result|create'], 'as' => 'editor.production_result.create', 'uses' => 'ProductionResultController@create']);
	Route::post('/production_result/create', ['middleware' => ['role:production_result|create'], 'as' => 'editor.production_result.store', 'uses' => 'ProductionResultController@store']);
		//edit
	Route::get('/production_result/{id}/edit', ['middleware' => ['role:production_result|update'], 'as' => 'editor.production_result.edit', 'uses' => 'ProductionResultController@edit']);
	Route::put('/production_result/{id}/update', ['middleware' => ['role:production_result|update'], 'as' => 'editor.production_result.update', 'uses' => 'ProductionResultController@update']);
		//create waste
	Route::get('/production_result/{id}/waste', ['middleware' => ['role:production_result|create'], 'as' => 'editor.production_result.create_waste', 'uses' => 'ProductionResultController@create_waste']);
	Route::post('/production_result/{id}/waste', ['middleware' => ['role:production_result|create'], 'as' => 'editor.production_result.store_waste', 'uses' => 'ProductionResultController@store_waste']);
		//edit waste
	Route::get('/production_result/{id}/waste/edit', ['middleware' => ['role:production_result|update'], 'as' => 'editor.production_result.edit_waste', 'uses' => 'ProductionResultController@edit_waste']);
	Route::put('/production_result/{id}/waste/edit', ['middleware' => ['role:production_result|update'], 'as' => 'editor.production_result.update_waste', 'uses' => 'ProductionResultController@update_waste']);
		//create reject
	Route::get('/production_result/{id}/reject', ['middleware' => ['role:production_result|create'], 'as' => 'editor.production_result.create_reject', 'uses' => 'ProductionResultController@create_reject']);
	Route::post('/production_result/{id}/reject', ['middleware' => ['role:production_result|create'], 'as' => 'editor.production_result.store_reject', 'uses' => 'ProductionResultController@store_reject']);
		//edit reject
	Route::get('/production_result/{id}/reject/edit', ['middleware' => ['role:production_result|update'], 'as' => 'editor.production_result.edit_reject', 'uses' => 'ProductionResultController@edit_reject']);
	Route::put('/production_result/{id}/reject/edit', ['middleware' => ['role:production_result|update'], 'as' => 'editor.production_result.update_reject', 'uses' => 'ProductionResultController@update_reject']);
		//create result
	Route::get('/production_result/{id}/result', ['middleware' => ['role:production_result|create'], 'as' => 'editor.production_result.create_result', 'uses' => 'ProductionResultController@create_result']);
	Route::post('/production_result/{id}/result', ['middleware' => ['role:production_result|create'], 'as' => 'editor.production_result.store_result', 'uses' => 'ProductionResultController@store_result']);
		//edit result
	Route::get('/production_result/{id}/result/edit', ['middleware' => ['role:production_result|update'], 'as' => 'editor.production_result.edit_result', 'uses' => 'ProductionResultController@edit_result']);
	Route::put('/production_result/{id}/result/edit', ['middleware' => ['role:production_result|update'], 'as' => 'editor.production_result.update_result', 'uses' => 'ProductionResultController@update_result']);
		//summary
	Route::get('/production_result/{id}/summary', ['middleware' => ['role:production_result|read'], 'as' => 'editor.production_result.summary', 'uses' => 'ProductionResultController@summary']);
		//close
	Route::get('/production_result/{id}/close', ['middleware' => ['role:production_result|update'], 'as' => 'editor.production_result.close', 'uses' => 'ProductionResultController@close_production_result']);
		//pdf
	Route::get('/production_result/{id}/pdf', ['middleware' => ['role:production_result|read'], 'as' => 'editor.production_result.pdf', 'uses' => 'ProductionResultController@pdf']);
		//delete
	Route::delete('/production_result/{id}/delete', ['middleware' => ['role:production_result|delete'], 'as' => 'editor.production_result.delete', 'uses' => 'ProductionResultController@delete']);
});

//Editor - Export
Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
	//Production Export
		//index
	Route::get('/production_export', ['middleware' => ['role:production_export|read'], 'as' => 'editor.production_export.index', 'uses' => 'ProductionExportController@index']);

	Route::get('/production_export/data', ['as' => 'editor.production_export.data', 'uses' => 'ProductionExportController@data']);

	//detail grid
	Route::get('/production_export/details-data/{id}', ['as' => 'editor.production_export.details-data', 'uses' => 'ProductionExportController@getDetailsData']);

		//create
	Route::get('/production_export/create', ['middleware' => ['role:production_export|create'], 'as' => 'editor.production_export.create', 'uses' => 'ProductionExportController@create']);
	Route::post('/production_export/create', ['middleware' => ['role:production_export|create'], 'as' => 'editor.production_export.store', 'uses' => 'ProductionExportController@store']);
		//edit
	Route::get('/production_export/{id}/edit', ['middleware' => ['role:production_export|update'], 'as' => 'editor.production_export.edit', 'uses' => 'ProductionExportController@edit']);
	Route::put('/production_export/{id}/edit', ['middleware' => ['role:production_export|update'], 'as' => 'editor.production_export.update', 'uses' => 'ProductionExportController@update']);

	Route::get('/production_export/{id}/editdet', ['middleware' => ['role:production_export|update'], 'as' => 'editor.production_export.edit', 'uses' => 'ProductionExportController@edit']);
	Route::put('/production_export/{id}/editdet', ['middleware' => ['role:production_export|update'], 'as' => 'editor.production_export.updatedetail', 'uses' => 'ProductionExportController@updatedetail']);

	//Route::put('/production_export/{id}/edit', ['as' => 'editor.production_export.updatedetail', 'uses' => 'ProductionExportController@update']);
		//confirm
	Route::put('/production_export/{id}/storeconfirm', ['middleware' => ['role:production_export|update'], 'as' => 'editor.production_export.storeconfirm', 'uses' => 'ProductionExportController@storeconfirm']);
		//delete header
	Route::delete('/production_export/{id}/delete', ['middleware' => ['role:production_export|delete'], 'as' => 'editor.production_export.delete', 'uses' => 'ProductionExportController@delete']);
		//delete detail
	Route::delete('/production_export/{id}/deletedet', ['middleware' => ['role:production_export|delete'], 'as' => 'editor.production_export.deletedet', 'uses' => 'ProductionExportController@deletedet']);



	//Cart
		//index
	Route::get('/cart', ['as' => 'editor.cart.index', 'uses' => 'CartController@index']);
	Route::get('/data', ['as' => 'editor.cart.data', 'uses' => 'CartController@data']);
		//create
	Route::get('/cart/create', ['as' => 'editor.cart.create', 'uses' => 'CartController@create']);
    Route::get('/lookupcart', ['as' => 'editor.lookupcart.index', 'uses' => 'LookupCartController@data']);
	Route::post('/cart/create', ['as' => 'editor.cart.store', 'uses' => 'CartController@store']);
		//edit
	Route::get('/cart/{id}/edit', ['as' => 'editor.cart.edit', 'uses' => 'CartController@edit']);
	Route::put('/cart/{id}/edit', ['as' => 'editor.cart.update', 'uses' => 'CartController@update']);
		//sashimi
	Route::get('/cart/{id}/sashimi', ['as' => 'editor.cart.sashimi', 'uses' => 'CartController@sashimi']);

	//export
	Route::get('/cart/{id}/export', ['as' => 'editor.cart.export', 'uses' => 'CartController@export']);
	Route::put('/cart/{id}/export', ['as' => 'editor.cart.storeexport', 'uses' => 'CartController@storeexport']);

	//R n D
		//index
	Route::get('/rnd', ['middleware' => ['role:rnd|read'], 'as' => 'editor.rnd.index', 'uses' => 'RndController@index']);

	Route::get('/rnd/data', ['as' => 'editor.rnd.data', 'uses' => 'ProductionExportController@data']);

	//detail grid
	Route::get('/rnd/details-data/{id}', ['as' => 'editor.rnd.details-data', 'uses' => 'RndController@getDetailsData']);

		//edit
	Route::get('/rnd/{id}/editdet', ['middleware' => ['role:rnd|read'], 'as' => 'editor.rnd.edit', 'uses' => 'RndController@edit']);
	Route::put('/rnd/{id}/edit', ['middleware' => ['role:rnd|read'], 'as' => 'editor.rnd.update', 'uses' => 'RndController@update']);
	Route::get('/cart/{id}/rnd', ['as' => 'editor.cart.rnd', 'uses' => 'CartController@rnd']);
	Route::put('/cart/{id}/rnd', ['as' => 'editor.cart.storernd', 'uses' => 'CartController@storernd']);

	Route::get('/cart/{id}/search', ['as' => 'editor.cart.search', 'uses' => 'CartController@search']);
	Route::get('/cart/{id}/pdf', ['as' => 'editor.cart.pdf', 'uses' => 'CartController@pdf']);

	//value added
	Route::get('/cart/{id}/valueadded', ['as' => 'editor.cart.valueadded', 'uses' => 'CartController@valueadded']);
	Route::put('/cart/{id}/valueadded', ['as' => 'editor.cart.storevalueadded', 'uses' => 'CartController@update']);

	Route::get('/cart/{id}/editdet', ['as' => 'editor.cart.edit', 'uses' => 'CartController@edit']);
	Route::put('/cart/{id}/editdet', ['as' => 'editor.cart.updatedetail', 'uses' => 'CartController@updatedetail']);

	//confirm
	Route::put('/cart/{id}/storeconfirm', ['as' => 'editor.cart.storeconfirm', 'uses' => 'CartController@storeconfirm']);

	Route::delete('/cart/{id}/delete', ['as' => 'editor.cart.delete', 'uses' => 'CartController@delete']);
	//delete detail
	Route::delete('/cart/{id}/deletedet', ['as' => 'editor.cart.deletedet', 'uses' => 'CartController@deletedet']);
});

//Item
Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
	//Type Item
		//index
	Route::get('/type_item', ['as' => 'editor.type_item.index', 'uses' => 'TypeItemController@index']);
		//create
	Route::get('/type_item/create', ['as' => 'editor.type_item.create', 'uses' => 'TypeItemController@create']);
	Route::post('/type_item/create', ['as' => 'editor.type_item.store', 'uses' => 'TypeItemController@store']);
		//edit
	Route::get('/type_item/{id}/edit', ['as' => 'editor.type_item.edit', 'uses' => 'TypeItemController@edit']);
	Route::put('/type_item/{id}/edit', ['as' => 'editor.type_item.update', 'uses' => 'TypeItemController@update']);
		//delete
	Route::delete('/type_item/{id}/delete', ['as' => 'editor.type_item.delete', 'uses' => 'TypeItemController@delete']);

	//Category Item
		//index
	Route::get('/category_item', ['as' => 'editor.category_item.index', 'uses' => 'CategoryItemController@index']);
		//create
	Route::get('/category_item/create', ['as' => 'editor.category_item.create', 'uses' => 'CategoryItemController@create']);
	Route::post('/category_item/create', ['as' => 'editor.category_item.store', 'uses' => 'CategoryItemController@store']);
		//edit
	Route::get('/category_item/{id}/edit', ['as' => 'editor.category_item.edit', 'uses' => 'CategoryItemController@edit']);
	Route::put('/category_item/{id}/edit', ['as' => 'editor.category_item.update', 'uses' => 'CategoryItemController@update']);
		//delete
	Route::delete('/category_item/{id}/delete', ['as' => 'editor.category_item.delete', 'uses' => 'CategoryItemController@delete']);


	//Unit
		//index
	Route::get('/unit', ['as' => 'editor.unit.index', 'uses' => 'UnitController@index']);
		//create
	Route::get('/unit/create', ['as' => 'editor.unit.create', 'uses' => 'UnitController@create']);
	Route::post('/unit/create', ['as' => 'editor.unit.store', 'uses' => 'UnitController@store']);
		//edit
	Route::get('/unit/{id}/edit', ['as' => 'editor.unit.edit', 'uses' => 'UnitController@edit']);
	Route::put('/unit/{id}/edit', ['as' => 'editor.unit.update', 'uses' => 'UnitController@update']);
		//delete
	Route::delete('/unit/{id}/delete', ['as' => 'editor.unit.delete', 'uses' => 'UnitController@delete']);

	//Master Item
		//index
	Route::get('/master_item', ['as' => 'editor.master_item.index', 'uses' => 'MasterItemController@index']);
	Route::get('/master_item/data', ['as' => 'editor.master_item.data', 'uses' => 'MasterItemController@data']);
		//detail grid
	Route::get('/master_item/details-data/{id}', ['as' => 'editor.master_item.details-data', 'uses' => 'MasterItemController@getDetailsData']);
		//create
	Route::get('/master_item/create', ['as' => 'editor.master_item.create', 'uses' => 'MasterItemController@create']);
	Route::post('/master_item/create', ['as' => 'editor.master_item.store', 'uses' => 'MasterItemController@store']);
		//edit
	Route::get('/master_item/{id}/edit', ['as' => 'editor.master_item.edit', 'uses' => 'MasterItemController@edit']);
	Route::put('/master_item/{id}/edit', ['as' => 'editor.master_item.update', 'uses' => 'MasterItemController@update']);
		//delete
	Route::delete('/master_item/{id}/delete', ['as' => 'editor.master_item.delete', 'uses' => 'MasterItemController@delete']);

	//Item
		//index
	Route::get('/item', ['as' => 'editor.item.index', 'uses' => 'ItemController@index']);
	Route::get('/item/data', ['as' => 'editor.item.data', 'uses' => 'ItemController@data']);

		//create
	Route::get('/item/create', ['as' => 'editor.item.create', 'uses' => 'ItemController@create']);
	Route::post('/item/create', ['as' => 'editor.item.store', 'uses' => 'ItemController@store']);
		//edit
	Route::get('/item/{id}/edit', ['as' => 'editor.item.edit', 'uses' => 'ItemController@edit']);
	Route::put('/item/{id}/edit', ['as' => 'editor.item.update', 'uses' => 'ItemController@update']);
		//delete
	Route::delete('/item/{id}/delete', ['as' => 'editor.item.delete', 'uses' => 'ItemController@delete']);

		//detail
	Route::get('/item/{id}/detail', ['as' => 'editor.item.detail', 'uses' => 'ItemController@detail']);
	Route::post('/item/{id}/detail', ['as' => 'editor.item.storedetail', 'uses' => 'ItemController@storedetail']);
	Route::put('/item/{id}/detail', ['as' => 'editor.item.updatedetail', 'uses' => 'ItemController@updatedetail']);
	Route::delete('/item/{id}/detail', ['as' => 'editor.item.deletedetail', 'uses' => 'ItemController@deletedetail']);

		//item uom
	Route::get('/item/{id}/itemuom', ['as' => 'editor.item.itemuom', 'uses' => 'ItemController@itemuom']);
	Route::post('/item/{id}/itemuom', ['as' => 'editor.item.storeitemuom', 'uses' => 'ItemController@storeitemuom']);
	Route::put('/item/{id}/itemuom', ['as' => 'editor.item.updateitemuom', 'uses' => 'ItemController@updateitemuom']);
	Route::delete('/item/{id}/itemuom', ['as' => 'editor.item.deleteitemuom', 'uses' => 'ItemController@deleteitemuom']);

	//Equipment
		//index
	Route::get('/equipment', ['as' => 'editor.equipment.index', 'uses' => 'EquipmentController@index']);
	Route::get('/equipment/data', ['as' => 'editor.equipment.data', 'uses' => 'EquipmentController@data']);

		//create
	Route::get('/equipment/create', ['as' => 'editor.equipment.create', 'uses' => 'EquipmentController@create']);
	Route::post('/equipment/create', ['as' => 'editor.equipment.store', 'uses' => 'EquipmentController@store']);
		//edit
	Route::get('/equipment/{id}/edit', ['as' => 'editor.equipment.edit', 'uses' => 'EquipmentController@edit']);
	Route::put('/equipment/{id}/edit', ['as' => 'editor.equipment.update', 'uses' => 'EquipmentController@update']);
		//delete
	Route::delete('/equipment/{id}/delete', ['as' => 'editor.equipment.delete', 'uses' => 'EquipmentController@delete']);

		//detail
	Route::get('/equipment/{id}/detail', ['as' => 'editor.equipment.detail', 'uses' => 'EquipmentController@detail']);
	Route::post('/equipment/{id}/detail', ['as' => 'editor.equipment.storedetail', 'uses' => 'EquipmentController@storedetail']);
	Route::put('/equipment/{id}/detail', ['as' => 'editor.equipment.updatedetail', 'uses' => 'EquipmentController@updatedetail']);
	Route::delete('/equipment/{id}/detail', ['as' => 'editor.equipment.deletedetail', 'uses' => 'EquipmentController@deletedetail']);

		//equipment uom
	Route::get('/equipment/{id}/itemuom', ['as' => 'editor.equipment.itemuom', 'uses' => 'EquipmentController@itemuom']);
	Route::post('/equipment/{id}/itemuom', ['as' => 'editor.equipment.storeitemuom', 'uses' => 'EquipmentController@storeitemuom']);
	Route::put('/equipment/{id}/itemuom', ['as' => 'editor.equipment.updateitemuom', 'uses' => 'EquipmentController@updateitemuom']);
	Route::delete('/equipment/{id}/itemuom', ['as' => 'editor.equipment.deleteitemuom', 'uses' => 'EquipmentController@deleteitemuom']);


	//Connsummable
		//index
	Route::get('/consummable', ['as' => 'editor.consummable.index', 'uses' => 'ConsummableController@index']);
	Route::get('/consummable/data', ['as' => 'editor.consummable.data', 'uses' => 'ConsummableController@data']);

		//create
	Route::get('/consummable/create', ['as' => 'editor.consummable.create', 'uses' => 'ConsummableController@create']);
	Route::post('/consummable/create', ['as' => 'editor.consummable.store', 'uses' => 'ConsummableController@store']);
		//edit
	Route::get('/consummable/{id}/edit', ['as' => 'editor.consummable.edit', 'uses' => 'ConsummableController@edit']);
	Route::put('/consummable/{id}/edit', ['as' => 'editor.consummable.update', 'uses' => 'ConsummableController@update']);
		//delete
	Route::delete('/consummable/{id}/delete', ['as' => 'editor.consummable.delete', 'uses' => 'ConsummableController@delete']);

		//detail
	Route::get('/consummable/{id}/detail', ['as' => 'editor.consummable.detail', 'uses' => 'ConsummableController@detail']);
	Route::post('/consummable/{id}/detail', ['as' => 'editor.consummable.storedetail', 'uses' => 'ConsummableController@storedetail']);
	Route::put('/consummable/{id}/detail', ['as' => 'editor.consummable.updatedetail', 'uses' => 'ConsummableController@updatedetail']);
	Route::delete('/consummable/{id}/detail', ['as' => 'editor.consummable.deletedetail', 'uses' => 'ConsummableController@deletedetail']);

		//consummable uom
	Route::get('/consummable/{id}/itemuom', ['as' => 'editor.consummable.itemuom', 'uses' => 'ConsummableController@itemuom']);
	Route::post('/consummable/{id}/itemuom', ['as' => 'editor.consummable.storeitemuom', 'uses' => 'ConsummableController@storeitemuom']);
	Route::put('/consummable/{id}/itemuom', ['as' => 'editor.consummable.updateitemuom', 'uses' => 'ConsummableController@updateitemuom']);
	Route::delete('/consummable/{id}/itemuom', ['as' => 'editor.consummable.deleteitemuom', 'uses' => 'ConsummableController@deleteitemuom']);


	//Packaging
		//index
	Route::get('/packaging', ['as' => 'editor.packaging.index', 'uses' => 'PackagingController@index']);
	Route::get('/packaging/data', ['as' => 'editor.packaging.data', 'uses' => 'PackagingController@data']);

		//create
	Route::get('/packaging/create', ['as' => 'editor.packaging.create', 'uses' => 'PackagingController@create']);
	Route::post('/packaging/create', ['as' => 'editor.packaging.store', 'uses' => 'PackagingController@store']);
		//edit
	Route::get('/packaging/{id}/edit', ['as' => 'editor.packaging.edit', 'uses' => 'PackagingController@edit']);
	Route::put('/packaging/{id}/edit', ['as' => 'editor.packaging.update', 'uses' => 'PackagingController@update']);
		//delete
	Route::delete('/packaging/{id}/delete', ['as' => 'editor.packaging.delete', 'uses' => 'PackagingController@delete']);

		//detail
	Route::get('/packaging/{id}/detail', ['as' => 'editor.packaging.detail', 'uses' => 'PackagingController@detail']);
	Route::post('/packaging/{id}/detail', ['as' => 'editor.packaging.storedetail', 'uses' => 'PackagingController@storedetail']);
	Route::put('/packaging/{id}/detail', ['as' => 'editor.packaging.updatedetail', 'uses' => 'PackagingController@updatedetail']);
	Route::delete('/packaging/{id}/detail', ['as' => 'editor.packaging.deletedetail', 'uses' => 'PackagingController@deletedetail']);

		//packaging uom
	Route::get('/packaging/{id}/itemuom', ['as' => 'editor.packaging.itemuom', 'uses' => 'PackagingController@itemuom']);
	Route::post('/packaging/{id}/itemuom', ['as' => 'editor.packaging.storeitemuom', 'uses' => 'PackagingController@storeitemuom']);
	Route::put('/packaging/{id}/itemuom', ['as' => 'editor.packaging.updateitemuom', 'uses' => 'PackagingController@updateitemuom']);
	Route::delete('/packaging/{id}/itemuom', ['as' => 'editor.packaging.deleteitemuom', 'uses' => 'PackagingController@deleteitemuom']);


		//item history
	Route::get('/item/{id}/history', ['as' => 'editor.item.history', 'uses' => 'ItemController@itemhistory']);

	//Custom Detail
		//index
	Route::get('/custom_detail', ['as' => 'editor.custom_detail.index', 'uses' => 'CustomDetailController@index']);
		//create
	Route::get('/custom_detail/create', ['as' => 'editor.custom_detail.create', 'uses' => 'CustomDetailController@create']);
	Route::post('/custom_detail/create', ['as' => 'editor.custom_detail.store', 'uses' => 'CustomDetailController@store']);
	 	//edit
	Route::get('/custom_detail/{id}/edit', ['as' => 'editor.custom_detail.edit', 'uses' => 'CustomDetailController@edit']);
	Route::put('/custom_detail/{id}/edit', ['as' => 'editor.custom_detail.update', 'uses' => 'CustomDetailController@update']);
	// 	//delete
	Route::delete('/custom_detail/{id}/delete', ['as' => 'editor.custom_detail.delete', 'uses' => 'CustomDetailController@delete']);


	//Conversion Item
		//index
	Route::get('/conversion_item', ['as' => 'editor.conversion_item.index', 'uses' => 'ConversionItemController@index']);
		//edit
	Route::get('/conversion_item/{id}/edit', ['as' => 'editor.conversion_item.edit', 'uses' => 'ConversionItemController@edit']);
	Route::put('/conversion_item/{id}/edit', ['as' => 'editor.conversion_item.update', 'uses' => 'ConversionItemController@update']);

	Route::get('/conversion_item/{id1}/{id2}/conversion', ['as' => 'editor.conversion_item.conversion', 'uses' => 'ConversionItemController@conversion']);


	//Item Migration
		//index
	Route::get('/item_migration', ['as' => 'editor.item_migration.index', 'uses' => 'ItemMigrationController@index']);
	Route::post('/item_migration', ['as' => 'editor.item_migration.store', 'uses' => 'ItemMigrationController@store']);

});


//marketing cost
Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
	//marketing cost
		//index
	Route::get('/marketing_cost', ['as' => 'editor.marketing_cost.index', 'uses' => 'MarketingCostController@index']);
		//data
	Route::get('/marketing_cost/data', ['as' => 'editor.marketing_cost.data', 'uses' => 'MarketingCostController@data']);
	// 	//create
	Route::get('/marketing_cost/create', ['as' => 'editor.marketing_cost.create', 'uses' => 'MarketingCostController@create']);
	Route::post('/marketing_cost/create', ['as' => 'editor.marketing_cost.store', 'uses' => 'MarketingCostController@store']);

	Route::get('/marketing_cost/{id}/editnew', ['as' => 'editor.marketing_cost.editnew', 'uses' => 'MarketingCostController@editnew']);
	Route::put('/marketing_cost/{id}/editnew', ['as' => 'editor.marketing_cost.editnew', 'uses' => 'MarketingCostController@store']);

	// //edit
	Route::get('/marketing_cost/{id}/edit', ['as' => 'editor.marketing_cost.edit', 'uses' => 'MarketingCostController@edit']);
	Route::put('/marketing_cost/{id}/edit', ['as' => 'editor.marketing_cost.update', 'uses' => 'MarketingCostController@update']);

	//calculate
	Route::get('/marketing_cost/{id}/calculate', ['as' => 'editor.marketing_cost.calculate', 'uses' => 'MarketingCostController@calculate']);

});

	//Marketing
	Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
		//index
	Route::get('/marketing', ['middleware' => ['role:marketing|read'], 'as' => 'editor.marketing.index', 'uses' => 'MarketingController@index']);
		//data
	Route::get('/marketing/data', ['as' => 'editor.marketing.data', 'uses' => 'MarketingController@data']);
		//create
	Route::get('/marketing/create', ['middleware' => ['role:marketing|create'], 'as' => 'editor.marketing.create', 'uses' => 'MarketingController@create']);
	Route::post('/marketing/create', ['middleware' => ['role:marketing|create'], 'as' => 'editor.marketing.store', 'uses' => 'MarketingController@store']);
		//edit
	Route::get('/marketing/{id}/edit', ['middleware' => ['role:marketing|update'], 'as' => 'editor.marketing.edit', 'uses' => 'MarketingController@edit']);
	Route::put('/marketing/{id}/edit', ['middleware' => ['role:marketing|update'], 'as' => 'editor.marketing.update', 'uses' => 'MarketingController@update']);
		//delete
	Route::delete('/marketing/{id}/delete', ['middleware' => ['role:marketing|delete'], 'as' => 'editor.marketing.delete', 'uses' => 'MarketingController@delete']);
		//product
	Route::get('/marketing/{id}/product', ['middleware' => ['role:marketing|update'], 'as' => 'editor.marketing.edit_product', 'uses' => 'MarketingController@edit_product']);
	Route::post('/marketing/{id}/product', ['middleware' => ['role:marketing|update'], 'as' => 'editor.marketing.update_product', 'uses' => 'MarketingController@update_product']);
});